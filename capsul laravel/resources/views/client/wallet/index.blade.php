@extends('layouts.client.clientPanel')

@section('content')
    <div class="row">
        <div class="col-12 mx-auto" style="max-width:500px">
            <div class="card card-primary mt-3">
                <div class="card-header">
                    <h3 class="card-title">شارژ کیف پول</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" id="myForm" action="{{route('wallet.increase')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="amount" class="col-sm-6 control-label">مبلغ مورد نظر</label>

                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input name="amount" type="text"
                                                   class="form-control @error('amount') is-invalid @enderror" id="amount"
                                                   placeholder="مبلغ را وارد نمایید">
                                            @error('amount')
                                            <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-sm-2 mt-1">
                                            <h5 class="text-warning">تومان</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-lg btn-success">پرداخت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        var textbox = '#amount';
        $(textbox).keyup(function () {
            var num = $(textbox).val();
            console.log(num)
            var numCommas = addCommas(num);
            console.log(numCommas)
            $(textbox).val(numCommas);
        });
        $('#myForm').submit(function () {
            var num = $(textbox).val();
            var numwithoutCommas = remCommas(num);
            $(textbox).val(numwithoutCommas);
            return true;
        });

        function remCommas(nStr) {
            nStr += '';
            var comma = /,/g;
            nStr = nStr.replace(comma, '');
            return nStr;
        }

        function addCommas(nStr) {
            nStr += '';
            var comma = /,/g;
            nStr = nStr.replace(comma, '');
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }
    </script>
@endsection