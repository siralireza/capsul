@extends('layouts.client.clientPanel')

@section('content')
    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title">پیشنهادات</h3>
        </div>
        <div class="card-body row">
            @if($complaints->isEmpty())
                <div class="w-100 text-center">
                    <h5 class="text-warning">پیشنهادی ثبت نشده است</h5>

                </div>
            @endif
            @foreach($complaints as $comp)
                <div class="card card-light col-lg-4">
                    <div class="card-header">
                        <span class="ill-name text-white">{{$comp->title}}</span>
                        @php
                            $dateTime = new \Hekmatinasser\Verta\Verta($comp->created_at);
                            $date = $dateTime->format('Y/n/j');
                        @endphp
                        <span class="pull-left text-white">{{$date}}</span><br>
                        @if($comp->status == 'accepted')
                            <span class="float-left badge badge-success">{{translateCompStatus($comp)}}</span>
                        @elseif($comp->status == 'waiting')
                            <span class="float-left badge badge-warning">{{translateCompStatus($comp)}}</span>
                        @elseif($comp->status == 'reserved')
                            <span class="float-left badge badge-info">{{translateCompStatus($comp)}}</span>
                        @elseif($comp->status == 'finished')
                            <span class="float-left badge badge-dark">{{translateCompStatus($comp)}}</span>
                        @else
                            <span class="float-left badge badge-danger">{{translateCompStatus($comp)}}</span>
                        @endif


                    </div>
                    <div class="card-body">
                        <h5>توضیحات: </h5>
                        <p>{{$comp->desc}}</p>
                        <div class="form-group">
                            <label for="inputForDiag">پاسخ:</label>
                            @if($comp->answer)
                                <textarea class="form-control mt-3" id="inputForDiag" rows="1"
                                          disabled>{{$comp->answer}}</textarea>
                            @else
                                <p class="text-warning" id="inputForAdvise">پاسخی دریافت نشده است</p>
                            @endif
                        </div>
                    </div>
{{--                    <div class="card-footer">--}}
{{--                        @if($comp->status == 'waiting')--}}
{{--                            <div class="w-100">--}}
{{--                                <button class="btn btn-danger pull-left" data-toggle="modal"--}}
{{--                                        data-target="#deleteModal{{$loop->index}}" >حذف</button>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                    </div>--}}
                </div>
                    @if($comp->user_id == userId() and $comp->status == 'waiting')
                        <div class="modal fade" id="deleteModal{{$loop->index}}" tabindex="-1" role="dialog" aria-labelledby="rateModal"
                             aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">حذف پیشنهاد</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="{{route('complaints.destroy',['id' => $comp->id])}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <div class="modal-body">
                                            <h4 class="text-danger">آیا مطمئن هستید که این پیشنهاد حذف شود؟</h4>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success mx-2">بله</button>
                                            <button type="button" class="btn btn-primary justify-content-end" data-dismiss="modal">خیر
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    @endif

                @endforeach

            <div class="card-footer text-center w-100">
                <a href="{{route('complaints.create')}}" class="btn btn-lg btn-primary">ثبت پیشنهاد جدید</a>
            </div>
        </div>
    </div>

@endsection