@extends('layouts.client.clientPanel')

@section('content')
    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title">
                {{translateRequestTitle($type)}}
            </h3>
        </div>
        <div class="card-body row">
            @if($reqs->isEmpty())
                <div class="text-center w-100">
                    <h5 class="text-warning">در این بخش درخواستی وجود ندارد</h5>
                </div>
            @endif
            @foreach($reqs as $req)
                <div class="card col-lg-4">
                    <div class="card-body">
                        <span class="my-1 ill-name">{{translateRequestType($req)}}</span>
                        @php
                                $dateTime = new \Hekmatinasser\Verta\Verta($req->created_at);
                                $date = $dateTime->format('Y/n/j');
                        @endphp
                        <span class="my-1 float-left small mt-1">{{$date}}</span><br>
                        <span class="text-info">نوع تخصص: {{$req->Expertise->expertise_name}}</span><br>

                    @if($req->status == 'accepted')
                            <span class="my-1 badge badge-success">{{translateRequestStatus($req)}}</span>
                        @elseif($req->status == 'waiting')
                            <span class="my-1 badge badge-warning">{{translateRequestStatus($req)}}</span>
                        @elseif($req->status == 'reserved')
                            <span class="my-1 badge badge-info">{{translateRequestStatus($req)}}</span>
                        @elseif($req->status == 'finished')
                            <span class="my-1 badge badge-dark">{{translateRequestStatus($req)}}</span>
                        @else
                            <span class="my-1 badge badge-danger">{{translateRequestStatus($req)}}</span>
                        @endif

                        @if($req->status == 'finished')
                            @if($req->score)
                                <a href="#"><i
                                            class="my-1 fa fa-star mt-2 fa-3x float-left"
                                            style="color: #fffc00;" onclick="return false;"
                                            data-toggle="tooltip" data-placement="left"
                                            title="امتیاز ثبت شده: {{$req->score}}"
                                    ></i></a><br>

                            @else
                                <a href="#"><i
                                            class="my-1 fa fa-star-o mt-2 fa-3x float-left"
                                            style="color: #fffc00;" onclick="return false;"
                                            data-toggle="tooltip" data-placement="left"
                                            title="از بخش پیگیری میتوانید امتیاز خود را ثبت کنید"
                                    ></i></a><br>
                            @endif
                        @else
                            <a href="#"><i
                                        class="my-1 fa fa-star-o mt-2 fa-3x float-left"
                                        style="color: #fffc00;" onclick="return false;"
                                        data-toggle="tooltip" data-placement="left"
                                        title="برای امتیاز دهی باید درخواست پایان یافته باشد"
                                ></i></a><br>
                        @endif
                        <div class="d-flex my-3">
                            @if($req->Doctor and $req->Doctor->User->pic)
                                <div class="image">
                                    <img src="{{$req->Doctor->User->pic}}" class="img-circle elevation-2"
                                         alt="User Image"
                                         style="width: 3.5rem">
                                </div>
                            @elseif($req->Doctor)
                                <div class="image">
                                    <img src="{{asset('dist/img/avatar5.png')}}" class="img-circle elevation-2"
                                         alt="User Image"
                                         style="width: 3.5rem">
                                </div>
                            @endif

                            <div class="info mr-3">
                                @if($req->dr_id)
                                    <a class="d-block">{{$req->Doctor->User->fname . ' '. $req->Doctor->User->lname}}</a>
                                    <a class="d-block">شماره نظام پزشکی: {{$req->Doctor->dr_code}}</a>
                                @else
                                    <p class="text-warning">مشخصات پزشک پس از تایید نمایش داده می‌شود</p>
                                @endif
                                @if($req->status == 'waiting')
                                    <p class="d-block text-danger">حداکثر زمان پاسخ
                                        گویی: {{\App\CapsulSetting::latest()->first()->answer_time}} ساعت</p>
                                @elseif($req->status == 'reserved')
                                        @php
                                            $date = new \Hekmatinasser\Verta\Verta($req->reserve_date);
                                            $reserveDate = $date->format('Y/n/j');
                                        @endphp
                                    <p class="d-block text-danger">تاریخ رزرو: {{$reserveDate}}</p>
                                @endif
                            </div>
                        </div>
                        <a href="{{route('reqs.show',['req'=>$req])}}"
                           class="my-1 btn btn-dark btn-sm">پیگیری درخواست</a>
                        <br>
                    </div>
                </div>

            @endforeach

        </div>
        <div class="card-footer text-center">
            <a href="{{route('reqs.index')}}" class="btn btn-lg btn-primary">بازگشت</a>
        </div>
    </div>

@endsection