@extends('layouts.client.clientPanel')

@section('content')
    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title">لیست درخواست ها</h3>
        </div>
        <div class="card-body row">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>{{$reqs->where('status','accepted')->count()}}</h3>

                        <p>درخواست های جاری</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-plus"></i>
                    </div>
                    <a href="{{route('reqs.index').'?status[0]=accepted'}}" class="small-box-footer">مشاهده <i
                                class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3>{{$reqs->where('status','waiting')->count()}}</h3>

                        <p>درخواست های منتظر تایید</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-pause"></i>
                    </div>
                    <a href="{{route('reqs.index').'?status[0]=waiting'}}" class="small-box-footer">مشاهده <i
                                class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gray">
                    <div class="inner">
                        <h3>{{$reqs->whereIn('status',['finished','rejected'])->count()}}</h3>

                        <p>درخواست های پایان یافته</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-check"></i>
                    </div>
                    <a href="{{route('reqs.index').'?status[0]=finished&status[1]=rejected'}}" class="small-box-footer">مشاهده <i
                                class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$reqs->where('status','reserved')->count()}}</h3>

                        <p>درخواست های رزور شده</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <a href="{{route('reqs.index').'?status[0]=reserved'}}" class="small-box-footer">مشاهده <i
                                class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <div class="card-footer text-center">
            <a href="{{route('reqs.create')}}" class="btn btn-lg btn-primary">درخواست خدمت جدید</a>
        </div>
    </div>


@endsection
