@extends('layouts.client.clientPanel')

@section('content')
    <div class="card card-primary mt-3 mx-auto" style="max-width: 500px">
        <div class="card-header">
            <h3 class="card-title">اطلاعات بیشتر</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="form-group">
                @if($req->Doctor)
                    @if($req->Doctor->User->pic)
                        <div class="image text-center">
                            <img src="{{$req->Doctor->User->pic}}" class="img-circle elevation-2" alt="User Image"
                                 style="width: 4.5rem">
                        </div>
                    @else
                        <div class="image text-center">
                            <img src="{{asset('dist/img/avatar5.png')}}" class="img-circle elevation-2" alt="User Image"
                                 style="width: 4.5rem">
                        </div>
                    @endif
                    <h5>نام: {{$req->Doctor->User->fname . ' '. $req->Doctor->User->lname}}</h5>
                    <h6>نوع تخصص: {{$req->Expertise->expertise_name}}</h6>
                    <h6>کد علوم پزشکی: {{$req->Doctor->dr_code}}</h6>
                @else
                    <p class="text-warning">مشخصات پزشک پس از تایید نمایش داده می‌شود</p>
                @endif
                @if($req->status == 'waiting')
                    <a class="d-block text-danger">حداکثر زمان پاسخ
                        گویی: {{\App\CapsulSetting::latest()->first()->answer_time}} ساعت</a>
                @endif


            </div>

        </div>
        <!-- /.card-body -->
        {{--        delete form --}}
        <div class="card-footer text-center">
            <div class="mx-auto text-center row">
                @if($req->status == 'accepted')
                    <span class="my-1 bg-success rounded mx-auto p-2">وضعیت: {{translateRequestStatus($req)}}</span>
                @elseif($req->status == 'waiting')
                    <span class="my-1 bg-warning rounded mx-auto p-2">وضعیت: {{translateRequestStatus($req)}}</span>
                @elseif($req->status == 'reserved')
                    <span class="my-1 bg-info rounded mx-auto p-2">وضعیت: {{translateRequestStatus($req)}}</span>
                @elseif($req->status == 'finished')
                    <span class="my-1 bg-dark rounded mx-auto p-2">وضعیت: {{translateRequestStatus($req)}}</span>
                @elseif($req->status == 'rejected')
                    <span class="my-1 bg-danger rounded mx-auto p-2">وضعیت: {{translateRequestStatus($req)}}</span>
                @else
                    <span class="my-1 bg-danger rounded mx-auto p-2">وضعیت: {{translateRequestStatus($req)}}</span>
                @endif
                @if($req->status == 'accepted')
                    <button type="button" class="mt-3 col-12 btn btn-lg btn-info" data-toggle="modal"
                            data-target="#adviseModal">
                        مشاهده
                        توصیه پزشک
                    </button>
                @elseif($req->status == 'waiting')
                    <button type="button" class="mt-3 col-12 btn btn-lg btn-info" data-toggle="modal"
                            data-target="#adviseModal">مشاهده توضیحات
                    </button>
                @elseif($req->status == 'reserved')
                    <button type="button" class="mt-3 col-12 btn btn-lg btn-info" data-toggle="modal"
                            data-target="#adviseModal">مشاهده توضیحات
                    </button>
                @elseif($req->status == 'finished')
                    <button type="button" class="mt-3 col-12 btn btn-lg btn-info" data-toggle="modal"
                            data-target="#adviseModal">
                        مشاهده
                        توصیه پزشک
                    </button>
                @elseif($req->status == 'rejected')
                    <button class="btn btn-white text-danger disabled">علت رد شدن: {{translateRejectReason($req)}}</button>
                @else
                    <span class="my-1 bg-danger rounded mx-auto p-2">وضعیت: {{translateRequestStatus($req)}}</span>
                @endif

                @if($req->status == 'accepted')
                    <a href="{{route('chats.show',['id' => $req->id])}}" class="mt-3 col-12 btn btn-lg btn-success">پرسش
                        از
                        پزشک</a>
                    {{--                @elseif($req->status == 'waiting')--}}
                    {{--                    <a href="{{route('chats.show',['id' => $req->id])}}"--}}
                    {{--                       class="mt-3 col-12 btn btn-lg btn-success disabled">پرسش از--}}
                    {{--                        پزشک</a>--}}
                    {{--                @elseif($req->status == 'reserved')--}}
                    {{--                    <a href="{{route('chats.show',['id' => $req->id])}}"--}}
                    {{--                       class="mt-3 col-12 btn btn-lg btn-success disabled">پرسش از--}}
                    {{--                        پزشک</a>--}}
                @elseif($req->status == 'finished')
                    <a href="{{route('chats.show',['id' => $req->id])}}" class="mt-3 col-12 btn btn-lg btn-dark">مشاهده
                        پیام‌ها</a>
                    {{--                @else--}}
                    {{--                    <a href="{{route('chats.show',['id' => $req->id])}}"--}}
                    {{--                       class="mt-3 col-12 btn btn-lg btn-success disabled">پرسش از--}}
                    {{--                        پزشک</a>--}}
                @endif

                @if($req->status == 'finished')
                    <button type="button" class="mt-3 col-12 btn btn-lg btn-light" data-toggle="modal"
                            data-target="#rateModal">امتیاز
                        به
                        پزشک
                    </button>
                @endif
                @if($req->user_id == userId() and $req->status == 'waiting')

                    <button type="submit" class="mt-3 col-12 btn btn-lg btn-danger" data-toggle="modal"
                            data-target="#deleteModal">حذف
                    </button>
                @endif
                <a href="{{route('reqs.index').'?status[0]='.$req->status}}" class="mt-3 col-12 btn btn-lg btn-primary">بازگشت</a>
            </div>
        </div>

    </div>

    <div class="modal fade mt-5" id="adviseModal" tabindex="-1" role="dialog" aria-labelledby="adviseModal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">مشاهده توصیه پزشک</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputForName">دلیل اصلی مراجعه به پزشک</label>
                        <input type="text" class="form-control" value="{{$req->main_reason}}" id="inputForName"
                               disabled>
                    </div>
                    <div class="form-group">
                        <label for="inputForAdvise">توصیه پزشک</label>
                        @if($req->status == 'waiting' or $req->status == 'reserved')
                            <p class="text-warning" id="inputForAdvise">توصیه پزشک پس از تایید درخواست وارد می‌شود</p>
                        @else
                            <textarea class="form-control mt-3" id="inputForAdvise" rows="1"
                                      disabled>{{$req->dr_first_answer}}</textarea>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="inputForDiag">تشخیص نهایی پزشک</label>
                        @if($req->status == 'finished')
                            <textarea class="form-control mt-3" id="inputForDiag" rows="1"
                                      disabled>{{$req->final_diagnosis}}</textarea>
                        @else
                            <p class="text-warning" id="inputForAdvise">تشخیص نهایی پس از پایان درخواست وارد می‌شود</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="inputForDiag">مراجه مجدد</label>
                        @if($req->status == 'finished')
                            @if($req->re_visit)
                                <textarea class="form-control mt-3" id="inputForDiag" rows="1"
                                          disabled>{{$req->re_visit}}</textarea>
                            @else
                                <p class="text-warning" id="inputForAdvise">دکتر اطلاعتی مربوط به مراجعه مجدد برای شما
                                    ثبت نکرده است</p>
                            @endif
                        @else
                            <p class="text-warning" id="inputForAdvise">مراجعه مجدد پس از پایان درخواست مشخص می‌شود</p>
                        @endif
                    </div>
                    @php
                        $flag = true;
                    @endphp
                    @for($i = 1;$i<4;$i ++)
                        @if($req->{'answer'.$i})
                            @if($flag)
                                <hr class="bg-white">
                                <h6>توضیحات وارد شده</h6>
                                @php($flag = false)
                            @endif
                            <textarea class="form-control mt-3" rows="1"
                                      disabled>{{ $req->{'answer'.$i} }}</textarea>
                        @endif
                    @endfor
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">بستن</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="rateModal" tabindex="-1" role="dialog" aria-labelledby="rateModal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">امتیاز به پزشک</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('reqs.update',['req' => $req])}}" method="post">
                    @csrf
                    @method('patch')
                    <div class="modal-body">
                        @if($req->status != 'finished')
                            <h5 class="text-warning">برای امتیاز به پزشک باید درخواست پایان یافته باشد</h5>
                        @else
                            <h5 class="mb-3">نام
                                پزشک: {{$req->Doctor->User->fname . ' '. $req->Doctor->User->lname}}</h5>
                            <div class="form-group">
                                <div class="rate mx-auto text-center">
                                    <input type="radio" id="star5" name="score" value="5"/>
                                    <label for="star5" title="text">5 stars</label>
                                    <input type="radio" id="star4" name="score" value="4"/>
                                    <label for="star4" title="text">4 stars</label>
                                    <input type="radio" id="star3" name="score" value="3"/>
                                    <label for="star3" title="text">3 stars</label>
                                    <input type="radio" id="star2" name="score" value="2"/>
                                    <label for="star2" title="text">2 stars</label>
                                    <input type="radio" id="star1" name="score" value="1" checked/>
                                    <label for="star1" title="text">1 star</label>
                                    @error('score')
                                    <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="mt-3">
                                    <div class="form-check mt-2">
                                        <label class="form-check-label">
                                            <input type="checkbox" name="rateAnswer1"
                                                   class="size-radio form-check-input" value="1">
                                            <span class="mr-2">دکتر به موقع پاسخ نداد</span>
                                        </label>
                                        @error('rateAnswer1')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-check mt-2">
                                        <label class="form-check-label">
                                            <input type="checkbox" name="rateAnswer2"
                                                   class="size-radio form-check-input" value="1">
                                            <span class="mr-2">دارو تاثیر گذار نبود</span>
                                        </label>
                                        @error('rateAnswer2')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-check mt-2">
                                        <label class="form-check-label">
                                            <input type="checkbox" name="rateAnswer3"
                                                   class="size-radio form-check-input" value="1">
                                            <span class="mr-2">برخورد پزشک مناسب نبود</span>
                                        </label>
                                        @error('rateAnswer3')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                        @endif
                    </div>
                    <div class="modal-footer @if($req->status == 'finished') justify-content-between @endif">
                        @if($req->status == 'finished')
                            <button type="submit" class="btn btn-success mx-2">ثبت</button>
                        @endif
                        <button type="button" class="btn btn-primary justify-content-end" data-dismiss="modal">بستن
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    @if($req->user_id == userId() and $req->status == 'waiting')
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="rateModal"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">حذف درخواست</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{route('reqs.destroy',['id' => $req->id])}}" method="post">
                        @csrf
                        @method('delete')
                        <div class="modal-body">
                            <h4 class="text-danger">آیا مطمئن هستید که این درخواست حذف شود؟</h4>
                        </div>
                        <div class="modal-footer @if($req->status == 'finished') justify-content-between @endif">
                            <button type="submit" class="btn btn-success mx-2">بله</button>
                            <button type="button" class="btn btn-primary justify-content-end" data-dismiss="modal">خیر
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endif

@endsection