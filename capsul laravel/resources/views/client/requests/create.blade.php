@extends('layouts.client.clientPanel')

@section('content')
    {{--    @if($errors->any())--}}
    {{--        @foreach($errors->all() as $error)--}}
    {{--            <div class="text-center mx-auto my-4 five-second-fade">--}}
    {{--                <span class="alert alert-class alert-danger alert-dismissible--}}
    {{--                    text-center">--}}
    {{--                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"--}}
    {{--                            style="margin-left: 8px!important;">&times;--}}
    {{--                    </button>--}}
    {{--                    {{$error}}</span>--}}
    {{--            </div>--}}
    {{--        @endforeach--}}
    {{--    @endif--}}
    <div class="card card-primary mt-3 mx-auto" style="max-width: 600px">
        <div class="card-header">
            <h3 class="card-title">درخواست خدمت جدید</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{route('reqs.store')}}" method="post" id="thisForm">
            @csrf
            <input type="hidden" class="hidden-date" name="reserve_date" value="{{old('reserve_date')}}">
            <div class="card-body">
                <div class="form-group">
                    <label class="col-sm-10 control-label" for="inputExp">نوع تخصص&nbsp;<i
                                class="fa fa-star text-danger"></i></label>
                    <div class="col-sm-10">

                        <select class="form-control @error('exp_id') is-invalid @enderror"
                                id="inputExp" name="exp_id" required>
                            <option selected disabled>انتخاب کنید</option>
                            @foreach(\App\Expertise::all() as $exp)
                                <option value="{{$exp->id}}" data-cost="{{$exp->cost}}"
                                        @if (old('exp_id') == $exp->id) selected @endif
                                >{{$exp->expertise_name}}</option>
                            @endforeach
                        </select>
                        @error('exp_id')
                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                </div>

                <input type="hidden" name="type" value="send_prescription">
                {{--                <div class="form-group">--}}
                {{--                    <label class="col-sm-10 control-label" for="inputForType">نوع خدمت&nbsp;<i--}}
                {{--                                class="fa fa-star text-danger"></i></label>--}}
                {{--                    <div class="col-sm-10">--}}
                {{--                        <select class="form-control" name="type" id="inputForType" required>--}}
                {{--                            @if(old('type') == 'lab')--}}
                {{--                                <option value="lab" selected>آزمایشگاهی</option>--}}
                {{--                                <option value="renew_prescription">تمدید نسخه</option>--}}
                {{--                                <option value="send_prescription">ارسال نسخه</option>--}}
                {{--                            @elseif(old('type') == 'renew_prescription')--}}
                {{--                                <option value="lab">آزمایشگاهی</option>--}}
                {{--                                <option value="renew_prescription" selected>تمدید نسخه</option>--}}
                {{--                                <option value="send_prescription">ارسال نسخه</option>--}}
                {{--                            @elseif(old('type') == 'send_prescription')--}}
                {{--                                <option value="lab">آزمایشگاهی</option>--}}
                {{--                                <option value="renew_prescription">تمدید نسخه</option>--}}
                {{--                                <option value="send_prescription" selected>ارسال نسخه</option>--}}
                {{--                            @else--}}
                {{--                                <option selected disabled>انتخاب کنید</option>--}}
                {{--                                <option value="lab">آزمایشگاهی</option>--}}
                {{--                                <option value="renew_prescription">تمدید نسخه</option>--}}
                {{--                                <option value="send_prescription">ارسال نسخه</option>--}}
                {{--                            @endif--}}
                {{--                        </select>--}}
                {{--                        @error('type')--}}
                {{--                        <span class="invalid text-danger" role="alert">--}}
                {{--                                        <strong>{{ $message }}</strong>--}}
                {{--                                    </span>--}}
                {{--                        @enderror--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                <div class="form-group">
                    <label for="main_reason" class="col-sm-10 control-label">دلیل اصلی مراجعه امروز شما به پزشک&nbsp;<i
                                class="fa fa-star text-danger"></i></label>

                    <div class="col-sm-10">
                                    <textarea name="main_reason" rows=2 required maxlength="60"
                                              class="form-control @error('main_reason') is-invalid @enderror"
                                              id="main_reason"
                                              placeholder="دلیل خود را بنویسید">{{ old('main_reason') }}</textarea>
                        @error('main_reason')
                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="answer1" class="col-sm-10 control-label"> از چه زمانی شروع ویا تشدید شده است؟<i
                                class="fa fa-star text-danger"></i></label>

                    <div class="col-sm-10">
                                    <textarea name="answer1" rows=2 required
                                              class="form-control @error('answer1') is-invalid @enderror" id="answer1"
                                              placeholder="زمان مورد نظر را دقیق وارد نمایید">{{old('answer1')}}</textarea>
                        @error('answer1')
                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="answer2" class="col-sm-10 control-label"> علائم همراه را نام ببرید:<i
                                class="fa fa-star text-danger"></i></label>

                    <div class="col-sm-10">
                                    <textarea name="answer2" rows=2 required
                                              class="form-control @error('answer2') is-invalid @enderror" id="answer2"
                                              placeholder="مثلا تب یا سرفه">{{old('answer2')}}</textarea>
                        @error('answer2')
                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="answer3" class="col-sm-10 control-label"> آیا دارویی نیز مصرف کرده‌اید؟<i
                                class="fa fa-star text-danger"></i></label>

                    <div class="col-sm-10">
                                    <textarea name="answer3" rows=2 required
                                              class="form-control @error('answer3') is-invalid @enderror" id="answer3"
                                              placeholder="چه دارویی، ‌چه مقدار و مدت زمان مصرف را بنویسید">{{old('answer3')}}</textarea>
                        @error('answer3')
                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-10">در صورت رزرو تاریخ را انتخاب کنید</label>

                    <div class="input-group col-sm-10">
                        <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="fa fa-calendar"></i>
                      </span>
                        </div>
                        <input class="date-picker form-control" name="persian_date" value="{{old('persian_date')}}"
                               readonly/>

                    </div>
                    @error('reserve_date')
                    <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>

                <div class="mx-3">
                    <h6 class="text-bold">مبلغ قابل پرداخت:</h6>
                    <h4 class="text-danger"><span class="text-danger" id="cost">0</span> تومان</h4>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <!--                                <button type="submit" class="btn btn-primary">اتخاب پزشک</button>-->
                <button type="submit" class="btn btn-success btn-lg">تایید و ادامه</button>
                <a class="btn btn-primary btn-lg" href="{{route('reqs.index')}}">بازگشت</a>
            </div>
        </form>
    </div>
    <div class="modal fade" id="costModal" tabindex="-1" role="dialog" aria-labelledby="costModal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">کمبود موجودی کیف پول</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="myForm" action="{{route('wallet.increase')}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <p class="text-danger text-bold">موجودی کیف پول شما کمتر از هزینه این تخصص میباشد
                                        حداقل مبلغ شارژ در کادر زیر نوشته شده است، اما میتوانید مبالغ بیشتر هم شارژ
                                        نمایید.</p>
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input name="amount" type="text"
                                                   class="form-control @error('amount') is-invalid @enderror"
                                                   id="amount">
                                            @error('amount')
                                            <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-sm-2 mt-1">
                                            <h5 class="text-warning">تومان</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->


                    </div>
                    <div class="modal-footer">
                            <button type="submit" class="btn btn-lg btn-success">پرداخت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        var wallet = {{userWallet()}};
        $('#inputExp').change(function () {
            var cost = $(this).find(':selected').data('cost')
            if (wallet < cost) {
                diff = cost - wallet
                // dargah kamtar az hezaro qabol nmikone
                if (diff < 100)
                    diff = 100
                $('#amount').val(diff);
                $('#costModal').modal('show');
            }
            // $('#myModal').modal('hide');
            // $('#myModal').modal('toggle');

            $('#cost').text(toPersianNum(cost));
        });
        $('.date-picker').persianDatepicker({
            initialValue: false,
            format: 'YYYY/MM/DD',
        });
        $('#thisForm').submit(function (e) {
            e.preventDefault();
            var date = $('.date-picker').val();
            if (date) {
                date = toEnglishNum(date);
                date = date.split('/');
                date = date.map(Number);
                date = new persianDate(date).toCalendar('gregorian').toLocale('en').format('YYYY/MM/DD');
                $('.hidden-date').val(date)
            }
            $(this).unbind().submit();
        });
    </script>
    <script>
        var textbox = '#amount';
        $(textbox).keyup(function () {
            var num = $(textbox).val();
            console.log(num)
            var numCommas = addCommas(num);
            console.log(numCommas)
            $(textbox).val(numCommas);
        });
        $('#myForm').submit(function () {
            var num = $(textbox).val();
            var numwithoutCommas = remCommas(num);
            $(textbox).val(numwithoutCommas);
            return true;
        });

        function remCommas(nStr) {
            nStr += '';
            var comma = /,/g;
            nStr = nStr.replace(comma, '');
            return nStr;
        }

        function addCommas(nStr) {
            nStr += '';
            var comma = /,/g;
            nStr = nStr.replace(comma, '');
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }
    </script>
@endsection