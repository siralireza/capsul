@extends('layouts.client.clientPanel')
@section('refresh')
    <meta http-equiv="refresh" content="10"/>
@overwrite
@section('content')
    <div class="card card-primary mt-3 mx-auto" style="max-width: 900px">
        <div class="card-header">
            <h3 class="card-title">لیست پیام ها</h3>
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-body p-0">
                    @if($reqChats->isNotEmpty())

                        <table class="table">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>نام دکتر</th>
                                <th>علت مراجعه</th>
                                <th style="width: 125px"></th>
                            </tr>
                            @foreach($reqChats as $req)
                                <tr>
                                    <td>{{$loop->index + 1}}</td>
                                    <td>{{$req->Doctor->User->fname .' '.$req->Doctor->User->lname}}</td>
                                    <td>{{$req->main_reason}}</td>
                                    <td class="text-center"><a href="{{route('chats.show',['id' => $req->id])}}"
                                                               @if($req->have_unread) class="text-danger" @endif><span
                                                    class="badge badge-danger mx-2">{{$req->have_unread}}</span><i
                                                    class="fa fa-commenting-o fa-2x"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <div class="w-100 text-center my-3">

                            <h5 class="text-warning">گفت و گویی یافت نشد</h5>
                        </div>
                    @endif
                </div>
                <!-- /.card-body -->
            </div>

        </div>
    </div>
@endsection