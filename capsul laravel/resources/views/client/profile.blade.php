@extends('layouts.client.clientPanel')

@section('content')
    <div class="row">
        <div class="col-12 mx-auto" style="max-width: 900px">
            <div class="card card-primary mt-3">
                <div class="card-header">
                    <h3 class="card-title">ویرایش پروفایل</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="{{route('updateProfile')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="image text-center">
                                        <img src="{{userPic() ?: asset('dist/img/avatar04.png')}}" class="img-circle elevation-2" alt="User Image" style="width: 4.5rem">
                                    </div>
                                    <div class="text-center">
                                        <label class="btn-sm btn btn-outline-dark mt-3" for="exampleInputFile">تغییر عکس</label>
                                        <input type="file" name="pic" style="display: none" class="custom-file-input" id="exampleInputFile">
                                        @error('pic')
                                        <span class="invalid text-danger text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputfname" class="col-sm-6 control-label">نام</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="fname"
                                               class="form-control @error('fname') is-invalid @enderror"
                                               id="inputfname"
                                               placeholder="نام خود را وارد کنید" value="{{ Auth::user()->fname }}"
                                               autofocus>
                                        @error('fname')
                                        <span class="invalid text-danger text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="inputlname" class="col-sm-6 control-label">نام خانوادگی</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="lname"
                                               class="form-control @error('lname') is-invalid @enderror"
                                               id="inputlname"
                                               placeholder="نام خانوادگی را وارد کنید" value="{{Auth::user()->lname}}"
                                        >
                                        @error('lname')
                                        <span class="invalid text-danger text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="inputPassword0" class="col-sm-6 control-label">کلمه عبور فعلی</label>

                                    <div class="col-sm-10">
                                        <input type="password" name="pass"
                                               class="form-control @error('pass') is-invalid @enderror"
                                               id="inputPassword0"
                                               placeholder="پسورد کنونی وارد کنید"
                                               value="{{ old('pass') }}"
                                                {{--                                                   autocomplete="current-password"--}}
                                        >
                                        @error('pass')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-6 control-label">کلمه عبور جدید</label>

                                    <div class="col-sm-10">
                                        <input type="password" name="password"
                                               class="form-control @error('password') is-invalid @enderror"
                                               id="inputPassword3"
                                               placeholder="پسورد را وارد کنید"
                                               value="{{ old('username') }}"
                                                {{--                                                   autocomplete="current-password"--}}
                                        >
                                        @error('password')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPasswordconf" class="col-sm-6 control-label">تکرار کلمه
                                        عبور جدید</label>

                                    <div class="col-sm-10">
                                        <input type="password" name="password_confirmation"
                                               class="form-control @error('password_confirmation') is-invalid @enderror"
                                               id="inputPasswordconf"
                                               placeholder="تکرار پسورد را وارد کنید"
                                               value="{{ old('password_confirmation') }}"
                                        >
                                        @error('password_confirmation')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-6 control-label">ایمیل</label>

                                    <div class="col-sm-10">
                                        <input type="email" name="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               id="inputEmail"
                                               placeholder="ایمیل را وارد کنید" value="{{ Auth::user()->email }}">
                                        @error('email')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPhone" class="col-sm-6 control-label">شماره موبایل</label>

                                    <div class="col-sm-10">
                                        <input type="tel" name="phone"
                                               class="form-control @error('phone') is-invalid @enderror"
                                               id="inputPhone"
                                               placeholder="شماره موبایل را وارد کنید" value="{{ Auth::user()->Client->phone }}">
                                        @error('phone')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputAge" class="col-sm-6 control-label">سن</label>

                                    <div class="col-sm-10">
                                        <input type="tel" name="age"
                                               class="form-control @error('age') is-invalid @enderror"
                                               id="inputAge"
                                               placeholder="سن خود را وارد کنید" value="{{ Auth::user()->Client->age }}">
                                        @error('age')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-6 control-label" for="inputSex">جسنیت</label>
                                    <div class="col-sm-10">

                                        <select class="form-control @error('sex') is-invalid @enderror"
                                                id="inputSex" name="sex">
                                            @if (Auth::user()->sex == 'female')
                                                <option value="male">مذکر</option>
                                                <option value="female" selected>مونث</option>
                                            @else
                                                <option value="male" selected>مذکر</option>
                                                <option value="female">مونث</option>
                                            @endif
                                        </select>
                                        @error('sex')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="answer1" class="col-sm-6 control-label">آیا سابقه بیماری خاصی دارید؟
                                        نام
                                        ببرید.</label>

                                    <div class="col-sm-10">
                                    <textarea name="answer1" rows=2
                                              class="form-control @error('answer1') is-invalid @enderror" id="answer1"
                                              placeholder="نام بیماری ها را بنویسید">{{ Auth::user()->Client->InitHistory->answer1 }}</textarea>
                                        @error('answer1')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="answer2" class="col-sm-10 control-label">آیا در حال حاظر یا گذشته
                                        داروی
                                        خاصی
                                        استفاده میکردید؟
                                        <p class="text-sm"><i class="fa fa-star text-warning"></i>
                                            در صورت مثبت بودن پاسخ، نام دارو، مدت مصرف و آخرین نوبت مصرف را ذکر
                                            کنید.
                                            توجه فرمایید در صورتیکه از داروهای گیاهی نیز استفاده میکنید نام آن دارو
                                            اهمیت
                                            دارد.
                                        </p>
                                    </label>

                                    <div class="col-sm-10">
                                    <textarea name="answer2" rows=2
                                              class="form-control @error('answer2') is-invalid @enderror" id="answer2"
                                              placeholder="نام دارو، مدت مصرف و آخرین نوبت مصرف را ذکر کنید">{{Auth::user()->Client->InitHistory->answer2}}</textarea>
                                        @error('answer2')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="answer3" class="col-sm-10 control-label">آیا تا کنون بستری بوده‌اید؟
                                        یا
                                        عمل
                                        جراحی داشته‌اید؟</label>

                                    <div class="col-sm-10">
                                    <textarea name="answer3" rows=2
                                              class="form-control @error('answer3') is-invalid @enderror" id="answer3"
                                              placeholder="توضیحات مربوطه را بنویسید">{{Auth::user()->Client->InitHistory->answer3}}</textarea>
                                        @error('answer3')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="answer4" class="col-sm-10 control-label">سوابق بیماری در خانواده خود
                                        را
                                        بنویسید.
                                        <p class="text-sm"><i class="fa fa-star text-warning"></i>
                                            پدر، مادر، خواهر، برادر، سابقه بیماری های زنان و پستان در خاله، عمه و
                                            مادربزرگ‌ها اهمیت دارد. همچنین سابقه بیماری پروستات در عمو، دایی و
                                            پدربزرگ‌ها
                                            مهم است و سابقه بیماری هموفیلی در دایی
                                        </p>
                                    </label>

                                    <div class="col-sm-10">
                                    <textarea name="answer4" rows=2
                                              class="form-control @error('answer4') is-invalid @enderror" id="answer4"
                                              placeholder="توضیحات مربوطه را بنویسید">{{Auth::user()->Client->InitHistory->answer4}}</textarea>
                                        @error('answer4')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="answer5" class="col-sm-10 control-label">سابقه حساسیت غذایی یا
                                        دارویی
                                        دارید؟
                                        (حساسیت به پروتئین، سویا و تخم مرغ)</label>

                                    <div class="col-sm-10">
                                    <textarea name="answer5" rows=2
                                              class="form-control @error('answer5') is-invalid @enderror" id="answer5"
                                              placeholder="توضیحات حساسیت را بنویسید">{{Auth::user()->Client->InitHistory->answer5}}</textarea>
                                        @error('answer5')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-lg btn-primary">ویرایش</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection