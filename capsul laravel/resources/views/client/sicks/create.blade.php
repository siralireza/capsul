@extends('layouts.client.clientPanel')

@section('content')
    <div class="row mt-3">
        <div class="col-12 mx-auto" style="max-width: 500px">
            <div class="card card-primary mt-3">
                <div class="card-header">
                    <h3 class="card-title">ثبت سابقه پزشکی</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{route('sicks.store')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title" class="col-sm-10 control-label">نام بیماری</label>

                            <div class="col-sm-10">
                                    <textarea name="sick" rows=2
                                              class="form-control @error('sick') is-invalid @enderror" id="title"
                                              placeholder="نام بیماری را وارد کنید">{{old('sick')}}</textarea>
                                @error('sick')
                                <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="desc" class="col-sm-10 control-label">توضیحات</label>

                            <div class="col-sm-10">
                                    <textarea name="desc" rows=2
                                              class="form-control @error('desc') is-invalid @enderror" id="desc"
                                              placeholder="اقدامات درمانی، تشخیص انجام شده و دارو های درحال مصرف را وارد نمایید">{{old('desc')}}</textarea>
                                @error('desc')
                                <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">تایید</button>
                        <a href="{{route('sicks.index')}}" class="btn btn-primary pull-left">بازگشت</a></div>
                </form>
            </div>
        </div>
    </div>

@endsection