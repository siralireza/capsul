@extends('layouts.client.clientPanel')

@section('content')
    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title">سوابق پزشکی</h3>
        </div>
        <div class="card-body row">
            @if($sicks->isEmpty())
                <div class="w-100 text-center">
                    <h5 class="text-warning">سابقه‌ای ثبت نشده است</h5>
                </div>
            @endif
            @foreach($sicks as $sick)
                <div class="card card-light col-lg-4">
                    <div class="card-header">
                        <span class="ill-name text-white">{{$sick->sick}}</span>
                        @php
                            $dateTime = new \Hekmatinasser\Verta\Verta($sick->created_at);
                            $date = $dateTime->format('Y/n/j');
                        @endphp
                        <span class="pull-left text-white">{{$date}}</span><br>


                    </div>
                    <div class="card-body">
                        <h5>توضیحات: </h5>
                        <p>{{$sick->desc}}</p>
                    </div>
                    <div class="card-footer">
                        @if($sick->added_by == 'client')
                            <div class="w-100">
                                <button class="btn btn-danger pull-left" data-toggle="modal"
                                        data-target="#deleteModal{{$loop->index}}">حذف
                                </button>
                            </div>
                        @endif
                        @if($sick->added_by == 'dr')
                            <span class="badge badge-success pull-left m-2">توسط پزشک</span>
                        @endif
                    </div>
                </div>
                @if($sick->user_id == userId())
                    <div class="modal fade" id="deleteModal{{$loop->index}}" tabindex="-1" role="dialog"
                         aria-labelledby="rateModal"
                         aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">حذف سابقه پزشکی</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{route('sicks.destroy',['id' => $sick->id])}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <div class="modal-body">
                                        <h4 class="text-danger">آیا مطمئن هستید که این سابقه حذف شود؟</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success mx-2">بله</button>
                                        <button type="button" class="btn btn-primary justify-content-end"
                                                data-dismiss="modal">خیر
                                        </button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                @endif

            @endforeach

        </div>
        <div class="card-footer">
            <div class="card-footer text-center w-100">
                <a href="{{route('sicks.create')}}" class="btn btn-lg btn-primary">ثبت بیماری جدید</a>
            </div>
        </div>
    </div>

@endsection