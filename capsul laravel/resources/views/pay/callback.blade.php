@extends('layouts.main')
@section('title')
    <title>نتیجه پرداخت</title>
@endsection

@section('body')

    <div class="container">
        <div class="row text-center" style="height: 80vh">
            <div class="col-sm-12 my-auto mx-auto bg-off-color rounded p-4" style="max-width: 500px">
                @if(isset($transAction) and $transAction->status)
                    <h3 class="text-success">{{$message}}</h3>
                    <h6 class="text-warning my-3">شماره پیگیری: {{$transAction->transId}}</h6>
                @endif
                @if(isset($transAction) and $transAction->comeFrom == 'web')
                    <a href="@auth {{route('panel')}} @else {{route('home')}} @endauth"
                       class="btn btn-primary">بازگشت</a>
                @elseif(isset($transAction) and $transAction->comeFrom == 'app')
                    <a href="{{env('SUCCESS_ANDROID_LINK')}}" class="btn btn-primary">بازگشت به اپلیکیشن</a>
                @else
                    <h3 class="text-danger">{{$message}}</h3>
                    <h6 class="text-warning my-3">درصورت کم شدن وجه از حساب به صورت خودکار پس از ۷۲ ساعت به حساب شما
                        برگشت زده خواهد شد</h6>
                    <a href="@auth {{route('panel')}} @else {{route('home')}} @endauth"
                       class="btn btn-primary">بازگشت</a>
                    <a href="{{env('FAILED_ANDROID_LINK')}}"
                       class="btn btn-primary">بازگشت به اپلیکیشن</a>
                @endif
            </div>
        </div>
    </div>
@endsection