@extends('layouts.index')
@section('content')
    <style>
        body{
            background-color: #042F3A !important;
        }
    </style>
    <div class="container" style="margin-top: 120px">
        @foreach($blogs as $blog)
            <div class="mx-auto mb-3">
                <!-- Box Comment -->
                <div class="card card-widget">
                    <div class="card-header">
                        <div class="user-block">
                            <span class="card-title ">{{$blog->title}}</span>
                            <span class="description float-left">{{$blog->created_at}}</span>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        @if($blog->img)
                            <div class="row">
                                <div class="col-lg-6">
                                    <img class="img-fluid pad"
                                         src="{{Storage::disk(config('voyager.storage.disk'))->url($blog->img)}}"
                                         alt="blog-image">
                                </div>
                                <div class="col-lg-6">
                                    @endif
                                    <p class="text-justify">{{$blog->description}}</p>
                                    @if($blog->img)
                                </div>
                            </div>

                        @endif

                    </div>
                </div>
                <!-- /.card -->
            </div>
        @endforeach
    </div>
@endsection