@extends('layouts.main')
@section('title')
    <title>یافت نشد</title>
@endsection
@section('body')
    <div class="container">
        <div class="row text-center" style="height: 100vh">
            <div class="col-sm-12 my-auto mx-auto" style="max-width: 500px">
                <h2> 404 | صفحه مورد نظر یافت نشد</h2>
                <a href="{{URL::previous()}}" class="btn btn-primary mt-2">بازگشت</a>
                <a href="/" class="btn btn-success mt-2">ضفحه اصلی</a>
            </div>
        </div>
    </div>
@endsection