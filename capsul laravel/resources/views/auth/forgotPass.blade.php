@extends('layouts.main')
@section('title')
    <title>وبسایت کپسول</title>
@endsection
@section('body')
    @if(Session::has('message'))
        <div class="text-center mx-auto my-4 five-second-fade">
        <span class="alert {{ Session::get('alert-class', 'alert-info')}} alert-dismissible
        text-cente">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="margin-left: 8px!important;">&times;
        </button>
        {{ Session::get('message') }}</span>
        </div>
    @endif
    <div class="container">
        <div class="row" style="height: 100vh">
            <div class="col-sm-12 my-auto mx-auto" style="max-width: 500px">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">فراموشی رمز عبور</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="{{ route('forgotPassSMS') }}">
                        @csrf

                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-6 control-label">شماره موبایل</label>

                                <div class="col-sm-10">
                                    <input type="text" name="phone"
                                           class="form-control @error('phone') is-invalid @enderror" id="inputEmail3"
                                           placeholder="شماره موبایل خود را وارد کنید" value="{{ old('phone') }}" required
                                           autocomplete="phone" autofocus>
                                    @error('phone')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">ارسال پیامک</button>
{{--                            <a href="{{route('register')}}" class="btn btn-outline-primary float-left">ثبت نام</a>--}}

                            <a href="{{route('login')}}" class="btn btn-outline-primary mx-1 float-left">صفحه ورود</a>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>

        </div>


    </div>
@endsection
