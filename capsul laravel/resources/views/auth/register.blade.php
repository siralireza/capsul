@extends('layouts.main')
@section('title')
    <title>ثبت‌نام در کپسول</title>
@endsection
@section('body')
    @if(Session::has('message'))
        <div class="text-center mx-auto my-4 five-second-fade">
        <span class="alert {{ Session::get('alert-class', 'alert-info')}} alert-dismissible
        text-cente">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="margin-left: 8px!important;">&times;
        </button>
        {{ Session::get('message') }}</span>
        </div>
    @endif
    <div class="container">
        <div class="row" style="height: 100vh">
            <div class="col-sm-12 my-auto mx-auto" style="max-width: 500px">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">ثبت نام</h3>
                    </div>
                    <!-- /.card-header -->
                        <div class="card-body  text-center ">
                            <a href="{{route('registerClient')}}" class="btn btn-lg my-1 btn-primary">ثبت نام به عنوان کاربر</a>
                            <a href="{{route('registerDoctor')}}" class="btn btn-lg my-1 btn-info">ثبت نام به عنوان دکتر</a>
                        </div>
                        <!-- /.card-body -->
                </div>
            </div>

        </div>


    </div>

@endsection
