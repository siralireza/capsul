@extends('layouts.main')
@section('title')
    <title>ثبت نام متخصص</title>
@endsection
@section('body')
    <div class="container">
        <div class="row" style="height: 100vh">
            <div class="col-sm-10 my-auto mx-auto" style="max-width: 600px">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">ثبت نام متخصص جدید</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="register_type" value="doctor">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputfname" class="col-sm-6 control-label">نام&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>

                                <div class="col-sm-10">
                                    <input type="text" name="fname"
                                           class="form-control @error('fname') is-invalid @enderror"
                                           id="inputfname"
                                           placeholder="نام خود را وارد کنید" value="{{ old('fname') }}"
                                           required autofocus>
                                    @error('fname')
                                    <span class="invalid text-danger text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="inputlname" class="col-sm-6 control-label">نام خانوادگی&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>

                                <div class="col-sm-10">
                                    <input type="text" name="lname"
                                           class="form-control @error('lname') is-invalid @enderror"
                                           id="inputlname"
                                           placeholder="نام خانوادگی را وارد کنید" value="{{ old('lname') }}"
                                           required>
                                    @error('lname')
                                    <span class="invalid text-danger text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="inputUsername" class="col-sm-6 control-label">نام کاربری&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>

                                <div class="col-sm-10">
                                    <input type="text" name="username"
                                           class="form-control @error('username') is-invalid @enderror"
                                           id="inputUsername"
                                           placeholder="نام کاربری را وارد کنید" value="{{ old('username') }}"
                                           required>
                                    @error('username')
                                    <span class="invalid text-danger text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-6 control-label">کلمه عبور&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>

                                <div class="col-sm-10">
                                    <input type="password" name="password"
                                           class="form-control @error('password') is-invalid @enderror"
                                           id="inputPassword3"
                                           placeholder="پسورد را وارد کنید" required
                                           value="{{ old('password') }}"
                                            {{--                                                   autocomplete="current-password"--}}
                                    >
                                    @error('password')
                                    <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPasswordconf" class="col-sm-6 control-label">تکرار کلمه
                                    عبور&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>

                                <div class="col-sm-10">
                                    <input type="password" name="password_confirmation"
                                           class="form-control @error('password_confirmation') is-invalid @enderror"
                                           id="inputPasswordconf"
                                           placeholder="تکرار پسورد را وارد کنید" required
                                           value="{{ old('password_confirmation') }}"
                                    >
                                    @error('password_confirmation')
                                    <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-6 control-label">ایمیل</label>

                                <div class="col-sm-10">
                                    <input type="email" name="email"
                                           class="form-control @error('email') is-invalid @enderror"
                                           id="inputEmail"
                                           placeholder="ایمیل را وارد کنید" value="{{ old('email') }}">
                                    @error('email')
                                    <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPhone" class="col-sm-6 control-label">شماره موبایل&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>

                                <div class="col-sm-10">
                                    <input type="tel" name="phone"
                                           class="form-control @error('phone') is-invalid @enderror"
                                           id="inputPhone"
                                           placeholder="شماره موبایل را وارد کنید" value="{{ old('phone') }}" required>
                                    @error('phone')
                                    <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label" for="inputSex">جسنیت&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>
                                <div class="col-sm-10">

                                    <select class="form-control @error('sex') is-invalid @enderror"
                                            id="inputSex" name="sex" required>
                                        @if (old('sex') == 'female')
                                            <option value="male">مذکر</option>
                                            <option value="female" selected>مونث</option>
                                        @else
                                            <option value="male" selected>مذکر</option>
                                            <option value="female">مونث</option>
                                        @endif
                                    </select>
                                    @error('sex')
                                    <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label" for="inputExp">تخصص&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>
                                <div class="col-sm-10">

                                    <select class="form-control @error('exp_id') is-invalid @enderror"
                                            id="inputExp" name="exp_id" required>
                                        @foreach(\App\Expertise::all() as $exp)
                                            <option value="{{$exp->id}}"
                                                    @if (old('exp_id') == $exp->id) selected @endif>{{$exp->expertise_name}}</option>
                                        @endforeach
                                    </select>
                                    @error('exp_id')
                                    <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="inputWorkExp" class="col-sm-6 control-label">تجربه کاری (سال)&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>
                                <div class="col-sm-10">
                                    <input type="number" name="work_experience"
                                           class="form-control @error('work_experience') is-invalid @enderror"
                                           id="inputWorkExp"
                                           placeholder="تجربه کاری خود را به سال وارد کنید"
                                           value="{{ old('work_experience') }}">
                                    @error('work_experience')
                                    <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputDrCode" class="col-sm-6 control-label">شماره نظام پزشکی&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>
                                <div class="col-sm-10">
                                    <input type="number" name="dr_code"
                                           class="form-control @error('dr_code') is-invalid @enderror"
                                           id="inputDrCode"
                                           placeholder="شماره نظام پزشکی خود را وارد کنید" value="{{ old('dr_code') }}">
                                    @error('dr_code')
                                    <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <h6 class="text-bold col-sm-6 control-label">فایل مدارک</h6>
                                <p class="text-sm"><i class="fa fa-star text-warning"></i>
                                    امکان آپلود فقط برای فایل فشرده وجود دارد.(rar ,zip ,7z)
                                </p>
                                <div class="text-right">
                                    <label class="btn-sm btn btn-outline-secondary mt-1" for="exampleInputFile">انتخاب
                                        فایل</label>
                                    <div class="col-sm-10">
                                        <input type="file" name="file"
                                               class="form-control custom-file-input @error('file') is-invalid @enderror"
                                               id="exampleInputFile">
                                        @error('file')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class=" col-sm-10">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="add_history" value="1"
                                               id="exampleCheck2" required>
                                        <label class="form-check-label" for="exampleCheck2">تمام <a href="{{route('termsDr')}}" target="_blank" class="text-bold text-danger">قوانین و تعهدات</a> را میپذیرم.</label>
                                    </div>
                                </div>
                            </div>
                            </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">ثبت نام</button>
                            <a href="{{route('login')}}" class="btn btn-outline-primary mx-2">ورود</a>
                            <a href="/" class="btn btn-outline-info mx-2 pull-left">صفحه اصلی</a>
                        </div>

                    </form>
                </div>

                <!-- /.card-footer -->
            </div>
        </div>

    </div>

@endsection
@section('script')
    <script>
        $('#inputUsername').on('keypress', function (event) {
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });
    </script>
@endsection