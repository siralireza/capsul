@extends('layouts.main')
@section('title')
    <title>ثبت نام کاربر</title>
@endsection
@section('body')
    <div class="container">
        <div class="row" style="height: 100vh">
            <div class="col-sm-10 my-auto mx-auto">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">ثبت نام کاربر جدید</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        @csrf
                        <input type="hidden" name="register_type" value="client">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="inputfname" class="col-sm-6 control-label">نام&nbsp;<i
                                                    class="fa fa-star text-danger"></i></label>

                                        <div class="col-sm-10">
                                            <input type="text" name="fname"
                                                   class="form-control @error('fname') is-invalid @enderror"
                                                   id="inputfname"
                                                   placeholder="نام خود را وارد کنید" value="{{ old('fname') }}"
                                                   required autofocus>
                                            @error('fname')
                                            <span class="invalid text-danger text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="inputlname" class="col-sm-6 control-label">نام خانوادگی&nbsp;<i
                                                    class="fa fa-star text-danger"></i></label>

                                        <div class="col-sm-10">
                                            <input type="text" name="lname"
                                                   class="form-control @error('lname') is-invalid @enderror"
                                                   id="inputlname"
                                                   placeholder="نام خانوادگی را وارد کنید" value="{{ old('lname') }}"
                                                   required>
                                            @error('lname')
                                            <span class="invalid text-danger text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="inputUsername" class="col-sm-6 control-label">نام کاربری&nbsp;<i
                                                    class="fa fa-star text-danger"></i></label>

                                        <div class="col-sm-10">
                                            <input type="text" name="username"
                                                   class="form-control @error('username') is-invalid @enderror"
                                                   id="inputUsername"
                                                   placeholder="نام کاربری را وارد کنید" value="{{ old('username') }}"
                                                   required>
                                            @error('username')
                                            <span class="invalid text-danger text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                    </div>
                                    {{--                                    <div class="form-group">--}}
                                    {{--                                        <label for="inputPassword3" class="col-sm-6 control-label">کلمه عبور&nbsp;<i--}}
                                    {{--                                                    class="fa fa-star text-danger"></i></label>--}}

                                    {{--                                        <div class="col-sm-10">--}}
                                    {{--                                            <input type="password" name="password"--}}
                                    {{--                                                   class="form-control @error('password') is-invalid @enderror"--}}
                                    {{--                                                   id="inputPassword3"--}}
                                    {{--                                                   placeholder="پسورد را وارد کنید" required--}}
                                    {{--                                                   value="{{ old('username') }}"--}}
                                    {{--                                                    --}}{{--                                                   autocomplete="current-password"--}}
                                    {{--                                            >--}}
                                    {{--                                            @error('password')--}}
                                    {{--                                            <span class="invalid text-danger" role="alert">--}}
                                    {{--                                        <strong>{{ $message }}</strong>--}}
                                    {{--                                    </span>--}}
                                    {{--                                            @enderror--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                    {{--                                    <div class="form-group">--}}
                                    {{--                                        <label for="inputPasswordconf" class="col-sm-6 control-label">تکرار کلمه--}}
                                    {{--                                            عبور&nbsp;<i--}}
                                    {{--                                                    class="fa fa-star text-danger"></i></label>--}}

                                    {{--                                        <div class="col-sm-10">--}}
                                    {{--                                            <input type="password" name="password_confirmation"--}}
                                    {{--                                                   class="form-control @error('password_confirmation') is-invalid @enderror"--}}
                                    {{--                                                   id="inputPasswordconf"--}}
                                    {{--                                                   placeholder="تکرار پسورد را وارد کنید" required--}}
                                    {{--                                                   value="{{ old('username') }}"--}}
                                    {{--                                            >--}}
                                    {{--                                            @error('password_confirmation')--}}
                                    {{--                                            <span class="invalid text-danger" role="alert">--}}
                                    {{--                                        <strong>{{ $message }}</strong>--}}
                                    {{--                                    </span>--}}
                                    {{--                                            @enderror--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-6 control-label">ایمیل</label>

                                        <div class="col-sm-10">
                                            <input type="email" name="email"
                                                   class="form-control @error('email') is-invalid @enderror"
                                                   id="inputEmail"
                                                   placeholder="ایمیل را وارد کنید" value="{{ old('email') }}">
                                            @error('email')
                                            <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPhone" class="col-sm-6 control-label">شماره موبایل&nbsp;<i
                                                    class="fa fa-star text-danger"></i></label>

                                        <div class="col-sm-10">
                                            <input type="tel" name="phone"
                                                   class="form-control @error('phone') is-invalid @enderror"
                                                   id="inputPhone"
                                                   placeholder="شماره موبایل را وارد کنید" value="{{ old('phone') }}"
                                                   required>
                                            @error('phone')
                                            <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputAge" class="col-sm-6 control-label">سن&nbsp;<i
                                                    class="fa fa-star text-danger"></i></label>

                                        <div class="col-sm-10">
                                            <input type="tel" name="age"
                                                   class="form-control @error('age') is-invalid @enderror"
                                                   id="inputAge"
                                                   placeholder="سن خود را وارد کنید" value="{{ old('age') }}">
                                            @error('age')
                                            <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label" for="inputSex">جسنیت&nbsp;<i
                                                    class="fa fa-star text-danger"></i></label>
                                        <div class="col-sm-10">

                                            <select class="form-control @error('sex') is-invalid @enderror"
                                                    id="inputSex" name="sex" required>
                                                @if (old('sex') == 'female')
                                                    <option value="male">مذکر</option>
                                                    <option value="female" selected>مونث</option>
                                                @else
                                                    <option value="male" selected>مذکر</option>
                                                    <option value="female">مونث</option>
                                                @endif
                                            </select>
                                            @error('sex')
                                            <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class=" col-sm-10">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" name="add_history"
                                                       value="1"
                                                       id="exampleCheck2" required>
                                                <label class="form-check-label" for="exampleCheck2">تمام <a
                                                            href="{{route('termsClient')}}" target="_blank"
                                                            class="text-bold text-danger">قوانین و تعهدات</a> را
                                                    میپذیرم.</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-10 mt-lg-5 text-center text-bold">
                                        <h5 class="text-danger">توجه داشته باشید کلمه عبور برای شما پیامک خواهد شد</h5>
                                    </div>

                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="answer1" class="col-sm-6 control-label">آیا سابقه بیماری خاصی دارید؟
                                            نام
                                            ببرید.</label>

                                        <div class="col-sm-10">
                                    <textarea name="answer1" rows=2
                                              class="form-control @error('answer1') is-invalid @enderror" id="answer1"
                                              placeholder="نام بیماری ها را بنویسید">{{ old('answer1') }}</textarea>
                                            @error('answer1')
                                            <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="answer2" class="col-sm-10 control-label">آیا در حال حاظر یا گذشته
                                            داروی
                                            خاصی
                                            استفاده میکردید؟
                                            <p class="text-sm"><i class="fa fa-star text-warning"></i>
                                                در صورت مثبت بودن پاسخ، نام دارو، مدت مصرف و آخرین نوبت مصرف را ذکر
                                                کنید.
                                                توجه فرمایید در صورتیکه از داروهای گیاهی نیز استفاده میکنید نام آن دارو
                                                اهمیت
                                                دارد.
                                            </p>
                                        </label>

                                        <div class="col-sm-10">
                                    <textarea name="answer2" rows=2
                                              class="form-control @error('answer2') is-invalid @enderror" id="answer2"
                                              placeholder="نام دارو، مدت مصرف و آخرین نوبت مصرف را ذکر کنید">{{old('answer2')}}</textarea>
                                            @error('answer2')
                                            <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="answer3" class="col-sm-10 control-label">آیا تا کنون بستری بوده‌اید؟
                                            یا
                                            عمل
                                            جراحی داشته‌اید؟</label>

                                        <div class="col-sm-10">
                                    <textarea name="answer3" rows=2
                                              class="form-control @error('answer3') is-invalid @enderror" id="answer3"
                                              placeholder="توضیحات مربوطه را بنویسید">{{old('answer3')}}</textarea>
                                            @error('answer3')
                                            <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="answer4" class="col-sm-10 control-label">سوابق بیماری در خانواده خود
                                            را
                                            بنویسید.
                                            <p class="text-sm"><i class="fa fa-star text-warning"></i>
                                                پدر، مادر، خواهر، برادر، سابقه بیماری های زنان و پستان در خاله، عمه و
                                                مادربزرگ‌ها اهمیت دارد. همچنین سابقه بیماری پروستات در عمو، دایی و
                                                پدربزرگ‌ها
                                                مهم است و سابقه بیماری هموفیلی
                                            </p>
                                        </label>

                                        <div class="col-sm-10">
                                    <textarea name="answer4" rows=2
                                              class="form-control @error('answer4') is-invalid @enderror" id="answer4"
                                              placeholder="توضیحات مربوطه را بنویسید">{{old('answer4')}}</textarea>
                                            @error('answer4')
                                            <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="answer5" class="col-sm-10 control-label">سابقه حساسیت غذایی یا
                                            دارویی
                                            دارید؟
                                            (حساسیت به پروتئین، سویا و تخم مرغ)</label>

                                        <div class="col-sm-10">
                                    <textarea name="answer5" rows=2
                                              class="form-control @error('answer5') is-invalid @enderror" id="answer5"
                                              placeholder="توضیحات حساسیت را بنویسید">{{old('answer5')}}</textarea>
                                            @error('answer5')
                                            <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">ثبت نام</button>
                            <a href="{{route('login')}}" class="btn btn-outline-primary mx-2">ورود</a>
                            <a href="/" class="btn btn-outline-info mx-2 pull-left">صفحه اصلی</a>
                        </div>
                    </form>
                </div>

                <!-- /.card-footer -->
            </div>
        </div>

    </div>

@endsection
@section('script')
    <script>
        $('#inputUsername').on('keypress', function (event) {
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });
    </script>
@endsection