@extends('layouts.main')
@section('title')
    <title>ورود به کپسول</title>
@endsection
@section('body')
    @if(Session::has('message'))
        <div class="text-center mx-auto my-4 five-second-fade">
        <span class="alert {{ Session::get('alert-class', 'alert-info')}} alert-dismissible
        text-cente">
{{--        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="margin-left: 8px!important;">&times;--}}
{{--        </button>--}}
        {{ Session::get('message') }}</span>
        </div>
    @endif
    <div class="container">
        <div class="row" style="height: 100vh">
            <div class="col-sm-12 my-auto mx-auto" style="max-width: 500px">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">ورود به پنل کابری</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-6 control-label">نام کاربری</label>

                                <div class="col-sm-10">
                                    <input type="text" name="username"
                                           class="form-control @error('username') is-invalid @enderror" id="inputEmail3"
                                           placeholder="نام کاربری را وارد کنید" value="{{ old('username') }}" required
                                           autocomplete="username" autofocus>
                                    @error('username')
                                    <span class="invalid-feedback text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-6 control-label">کلمه عبور</label>

                                <div class="col-sm-10">
                                    <input type="password" name="password"
                                           class="form-control @error('password') is-invalid @enderror"
                                           id="inputPassword3"
                                           placeholder="پسورد را وارد کنید" required autocomplete="current-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="remember"
                                               id="exampleCheck2" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="form-check-label" for="exampleCheck2">مرا به خاطر بسپار</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">ورود</button>
{{--                            <a href="{{route('register')}}" class="btn btn-outline-primary float-left">ثبت نام</a>--}}
                            <a class="btn btn-outline-primary my-sm-0 pull-left" data-toggle="dropdown"
                               href="#">
                                ثبت‌نام
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu bg-off-color text-right">
                                <li class="nav-item text-right"><a class="nav-link bg-off-color text-right"
                                                        href="{{route('registerClient')}}">ثبت‌نام
                                        کاربر</a></li>
                                <li class="nav-item text-right"><a class="nav-link bg-off-color text-right"
                                                        href="{{route('registerDoctor')}}">ثبت نام
                                        متخصص</a></li>

                                <!-- dropdown menu links -->
                            </ul>

                            <a href="{{route('home')}}" class="btn btn-outline-info mx-1 float-left">صفحه اصلی</a>
                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="
                                {{ route('password.request') }}
                                        ">
                                    {{ __('رمز خود را فراموش کرده‌اید؟') }}
                                </a>
                            @endif
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>

        </div>


    </div>
@endsection
