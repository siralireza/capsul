@extends('layouts.index')
@section('content')
    <header class="bg-gradient" id="home">
        <div class="container mt-5">
            <h1>کپسول</h1>
            <p class="tagline">خدمات پزشکی آنلاین</p>
        </div>
        <div class="img-holder mt-3"><img src="{{asset('home/images/iphonex.png')}}" alt="phone" class="img-fluid"></div>
    </header>

    <!--    <div class="client-logos my-5">-->
    <!--        <div class="container text-center">-->
    <!--            <img src="images/client-logos.png" alt="client logos" class="img-fluid">-->
    <!--        </div>-->
    <!--    </div>-->

    <div class="section light-bg" id="features">


        <div class="container">

            <div class="section-title">
                {{--            <small>شاخص های سرویس های ما</small>--}}
                <h3>ویژگی هایی که به آنها افتخار میکنیم</h3>
            </div>


            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                <span class="ti-face-smile gradient-fill ti-3x ml-3"></span>
                                <div class="media-body">
                                    <h4 class="card-title">دسترسی سریع به متخصص</h4>
                                    <p class="card-text">با چند کلیک در خدمت سلامت شما هستیم</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                <span class="ti-settings gradient-fill ti-3x ml-3"></span>
                                <div class="media-body">
                                    <h4 class="card-title">دسترسی همیشگی</h4>
                                    <p class="card-text">پاسخ گویی ۲۴ ساعته در تمام روزهای سال</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                <span class="ti-lock gradient-fill ti-3x ml-3"></span>
                                <div class="media-body">
                                    <h4 class="card-title">کادر پزشکی مجرب</h4>
                                    <p class="card-text">تیم کاملی از پزشکان متعهد و با سابقه</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
    <!-- // end .section -->
    <div class="section">

        <div class="container">
            <div class="row">
                <div class="col-lg-1">
                    <div class="box-icon"><span class="ti-notepad gradient-fill ti-3x"></span></div>
                </div>
                <div class="col-lg-11">
                    <h2>از اخبار جدید آگاه شوید</h2>
{{--                    <p class="mb-4">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک--}}
{{--                        است لورم ایپسوم متن ساختگی با </p>--}}
                    <a href="{{route('blog')}}" class="btn btn-primary mt-lg-2">ورود به بخش دانشنامه پزشکی</a>
                </div>
            </div>
        </div>

    </div>
    <!-- // end .section -->


    {{--<div class="section light-bg">--}}
    {{--    <div class="container">--}}
    {{--        <div class="section-title">--}}
    {{--            <small>امکانات</small>--}}
    {{--            <h3>با اپلیکیشن ما به امکانات بیشتری دسترسی داشته باشید</h3>--}}
    {{--        </div>--}}

    {{--        <ul class="nav nav-tabs nav-justified" role="tablist">--}}
    {{--            <li class="nav-item">--}}
    {{--                <a class="nav-link active" data-toggle="tab" href="#communication">ارتباطات</a>--}}
    {{--            </li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a class="nav-link" data-toggle="tab" href="#schedule">زمانبندی</a>--}}
    {{--            </li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a class="nav-link" data-toggle="tab" href="#messages">پیام ها</a>--}}
    {{--            </li>--}}
    {{--            <li class="nav-item">--}}
    {{--                <a class="nav-link" data-toggle="tab" href="#livechat">گفتگوی زنده</a>--}}
    {{--            </li>--}}
    {{--        </ul>--}}
    {{--        <div class="tab-content">--}}
    {{--            <div class="tab-pane fade show active" id="communication">--}}
    {{--                <div class="d-flex flex-column flex-lg-row">--}}
    {{--                    <img src="{{asset('home/images/graphic.png')}}" alt="graphic"--}}
    {{--                         class="img-fluid rounded align-self-start ml-lg-5 mb-5 mb-lg-0">--}}
    {{--                    <div>--}}

    {{--                        <h2>به آسانی ارتباط برقرار کنید</h2>--}}
    {{--                        <p class="lead">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان--}}
    {{--                            گرافیک است. </p>--}}
    {{--                        <p>صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و--}}
    {{--                            سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود--}}
    {{--                            ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان--}}
    {{--                            جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی--}}
    {{--                            الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ای،--}}
    {{--                        </p>--}}
    {{--                        <p> صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و--}}
    {{--                            سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود--}}
    {{--                            ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان--}}
    {{--                            جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی--}}
    {{--                            الخصوص.--}}
    {{--                        </p>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="tab-pane fade" id="schedule">--}}
    {{--                <div class="d-flex flex-column flex-lg-row">--}}
    {{--                    <div>--}}
    {{--                        <h2>برنامه ریزی در زمان دلخواه شما</h2>--}}
    {{--                        <p class="lead">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان--}}
    {{--                            گرافیک است. </p>--}}
    {{--                        <p>صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و--}}
    {{--                            سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود--}}
    {{--                            ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان--}}
    {{--                            جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی--}}
    {{--                            الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ای،--}}
    {{--                        </p>--}}
    {{--                        <p> صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و--}}
    {{--                            سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود--}}
    {{--                            ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان--}}
    {{--                            جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی--}}
    {{--                            الخصوص.--}}
    {{--                        </p>--}}
    {{--                    </div>--}}
    {{--                    <img src="{{asset('home/images/graphic.png')}}" alt="graphic"--}}
    {{--                         class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="tab-pane fade" id="messages">--}}
    {{--                <div class="d-flex flex-column flex-lg-row">--}}
    {{--                    <img src="{{asset('home/images/graphic.png')}}" alt="graphic"--}}
    {{--                         class="img-fluid rounded align-self-start ml-lg-5 mb-5 mb-lg-0">--}}
    {{--                    <div>--}}
    {{--                        <h2>سرویس پیام رسان زنده</h2>--}}
    {{--                        <p class="lead">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان--}}
    {{--                            گرافیک است. </p>--}}
    {{--                        <p>صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و--}}
    {{--                            سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود--}}
    {{--                            ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان--}}
    {{--                            جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی--}}
    {{--                            الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ای،--}}
    {{--                        </p>--}}
    {{--                        <p> صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و--}}
    {{--                            سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود--}}
    {{--                            ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان--}}
    {{--                            جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی--}}
    {{--                            الخصوص.--}}
    {{--                        </p>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="tab-pane fade" id="livechat">--}}
    {{--                <div class="d-flex flex-column flex-lg-row">--}}
    {{--                    <div>--}}
    {{--                        <h2>گفتگوی زنده هر زمان که به آن احتیاج داشته باشید</h2>--}}
    {{--                        <p class="lead">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان--}}
    {{--                            گرافیک است. </p>--}}
    {{--                        <p>صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و--}}
    {{--                            سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود--}}
    {{--                            ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان--}}
    {{--                            جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی--}}
    {{--                            الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ای،--}}
    {{--                        </p>--}}
    {{--                        <p> صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و--}}
    {{--                            سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود--}}
    {{--                            ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان--}}
    {{--                            جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی--}}
    {{--                            الخصوص.--}}
    {{--                        </p>--}}
    {{--                    </div>--}}
    {{--                    <img src="{{asset('home/images/graphic.png')}}" alt="graphic"--}}
    {{--                         class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}


    {{--    </div>--}}
    {{--</div>--}}
    <!-- // end .section -->


    <div class="section light-bg">

        <div class="container">
            <div class="row">
                <div class="col-md-8 d-flex align-items-center">
                    <ul class="list-unstyled ui-steps">
                        <li class="media">
                            <div class="circle-icon mr-4">1</div>
                            <div class="media-body">
                                <h5>ایجاد حساب کاربری</h5>
                                <p>به جای خود درمانی با چند کلیک ساده با متخصصان ما در ارتباط باشید</p>
                            </div>
                        </li>
                        <li class="media my-4">
                            <div class="circle-icon mr-4">2</div>
                            <div class="media-body">
                                <h5>ثبت درخواست</h5>
                                <p>میتوانید مشکل خود را با استفاده از عکس، صدا و یا متن با ما در میان بگذارید</p>
                            </div>
                        </li>
                        <li class="media">
                            <div class="circle-icon mr-4">3</div>
                            <div class="media-body">
                                <h5>ارتباط با پزشک</h5>
                                <p>به جای معطلی در اتاق انتظار پزشکان این جا با آن ها در ارتباط باشید</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <img src="{{asset('home/images/iphonex.png')}}" alt="iphone" class="img-fluid">
                </div>

            </div>

        </div>

    </div>
    <!-- // end .section -->
    <div class="section" id="download">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{asset('home/images/dualphone.png')}}" alt="dual phone" class="img-fluid">
                </div>
                <div class="col-md-6 d-flex align-items-center">
                    <div class="text-center">
                        {{--                    <div class="box-icon"><span class="ti-rocket gradient-fill ti-3x"></span></div>--}}
                        <h3>همین حالا اپلیکیشن را دانلود کنید</h3>
                        {{--                    <p>*نیازمنداندروید ۵ و بالاتر.--}}
                        {{--                    </p>--}}
                        {{--                    <p class="mb-4">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان--}}
                        {{--                        گرافیک است. </p>--}}
                        {{--                <a href="#" class="btn btn-light"><img src="{{asset('home/images/appleicon.png')}}" alt="icon"> اپ استور</a>--}}
                        <a href="#" class="btn btn-light"><img src="{{asset('home/images/playicon.png')}}" alt="icon"> گوگل
                            پلی</a>
                        <form action="{{route('iosPost')}}" method="post">
                            @csrf
                            <button type="submit" class="btn btn-light"><img src="{{asset('home/images/appleicon.png')}}" alt="icon">دانلود نسخه ios</button>
                        </form>
                        <a href="{{ Storage::disk(config('voyager.storage.disk'))->url(json_decode(\App\CapsulSetting::first()->app_client)[0]->download_link   ?? '')}}"
                           target="_blank" class="btn btn-light"><i class="fa fa-download">دانلود مستقیم</i></a>
                    </div>

                </div>
            </div>

        </div>

    </div>
    <!-- // end .section -->

    <!--    <div class="section">-->
    <!--        <div class="container">-->
    <!--            <div class="section-title">-->
    <!--                <small>نظرات مشتریان</small>-->
    <!--                <h3>کاربران درباره ما چه میگویند</h3>-->
    <!--            </div>-->

    <!--            <div class="testimonials owl-carousel">-->
    <!--                <div class="testimonials-single">-->
    <!--                    <img src="images/client.png" alt="client" class="client-img">-->
    <!--                    <blockquote class="blockquote">سرویس دهی فوق العاده قوی و تاثیر گذار در 24 ساعت شبانه روز شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان و متن دلخواه شما.</blockquote>-->
    <!--                    <h5 class="mt-4 mb-2">کریستال گوردون</h5>-->
    <!--                    <p class="text-primary">کشوری در دنیا</p>-->
    <!--                </div>-->
    <!--                <div class="testimonials-single">-->
    <!--                    <img src="images/client.png" alt="client" class="client-img">-->
    <!--                    <blockquote class="blockquote">سرویس دهی فوق العاده قوی و تاثیر گذار در 24 ساعت شبانه روز شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان و متن دلخواه شما.</blockquote>-->
    <!--                    <h5 class="mt-4 mb-2">کریستال گوردون</h5>-->
    <!--                    <p class="text-primary">کشوری در دنیا</p>-->
    <!--                </div>-->
    <!--                <div class="testimonials-single">-->
    <!--                    <img src="images/client.png" alt="client" class="client-img">-->
    <!--                    <blockquote class="blockquote">سرویس دهی فوق العاده قوی و تاثیر گذار در 24 ساعت شبانه روز شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان و متن دلخواه شما.</blockquote>-->
    <!--                    <h5 class="mt-4 mb-2">کریستال گوردون</h5>-->
    <!--                    <p class="text-primary">کشوری در دنیا</p>-->
    <!--                </div>-->
    <!--            </div>-->

    <!--        </div>-->

    <!--    </div>-->
    <!-- // end .section -->


    {{--<div class="section light-bg" id="gallery">--}}
    {{--    <div class="container">--}}
    {{--        <div class="section-title">--}}
    {{--            <small>گالری</small>--}}
    {{--            <h3>اسکرین شات های برنامه</h3>--}}
    {{--        </div>--}}

    {{--        <div class="img-gallery owl-carousel owl-theme">--}}
    {{--            <img src="{{asset('home/images/app/1.jpg')}}" alt="image">--}}
    {{--            <img src="{{asset('home/images/app/2.jpg')}}" alt="image">--}}
    {{--            <img src="{{asset('home/images/app/3.jpg')}}" alt="image">--}}
    {{--            <!--                <img src="images/screen1.jpg" alt="image">-->--}}
    {{--        </div>--}}

    {{--    </div>--}}

    {{--</div>--}}
    <!-- // end .section -->


    <!--    <div class="section" id="pricing">-->
    <!--        <div class="container">-->
    <!--            <div class="section-title">-->
    <!--                <small>سرویس ها و هزینه ها</small>-->
    <!--                <h3>ارتقا به نسخه ی حرفه ای</h3>-->
    <!--            </div>-->

    <!--            <div class="card-deck">-->
    <!--                <div class="card pricing">-->
    <!--                    <div class="card-head">-->
    <!--                        <small class="text-primary">شخصی</small>-->
    <!--                        <span class="price">14000<sub>/تومان ماهانه</sub></span>-->
    <!--                    </div>-->
    <!--                    <ul class="list-group list-group-flush">-->
    <!--                        <div class="list-group-item">10 پروژه</div>-->
    <!--                        <div class="list-group-item">5 گیگابایت فضا</div>-->
    <!--                        <div class="list-group-item">پشتیبانی از طریق فروم</div>-->
    <!--                        <div class="list-group-item"><del>همکاری گروهی</del></div>-->
    <!--                        <div class="list-group-item"><del>گزارش و آمار</del></div>-->
    <!--                    </ul>-->
    <!--                    <div class="card-body">-->
    <!--                        <a href="#" class="btn btn-primary btn-lg btn-block">انتخاب پلن</a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="card pricing popular">-->
    <!--                    <div class="card-head">-->
    <!--                        <small class="text-primary">برای تیم ها</small>-->
    <!--                        <span class="price">29000<sub>/تومان ماهانه</sub></span>-->
    <!--                    </div>-->
    <!--                    <ul class="list-group list-group-flush">-->
    <!--                        <div class="list-group-item">نامحدود پروژه</div>-->
    <!--                        <div class="list-group-item">100 گیگابایت فضا</div>-->
    <!--                        <div class="list-group-item">پشتیبانی بر اساس اولویت</div>-->
    <!--                        <div class="list-group-item">همکاری گروهی</div>-->
    <!--                        <div class="list-group-item">گزارش و آمار</div>-->
    <!--                    </ul>-->
    <!--                    <div class="card-body">-->
    <!--                        <a href="#" class="btn btn-primary btn-lg btn-block">انتخاب پلن</a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="card pricing">-->
    <!--                    <div class="card-head">-->
    <!--                        <small class="text-primary">شرکتی</small>-->
    <!--                        <span class="price">249000<sub>/تومان ماهانه</sub></span>-->
    <!--                    </div>-->
    <!--                    <ul class="list-group list-group-flush">-->
    <!--                        <div class="list-group-item">نامحدود پروژه</div>-->
    <!--                        <div class="list-group-item">فضای نامحدود</div>-->
    <!--                        <div class="list-group-item">همکاری گروهی</div>-->
    <!--                        <div class="list-group-item">گزارش ها و آمار</div>-->
    <!--                        <div class="list-group-item">وب هوک ها</div>-->
    <!--                    </ul>-->
    <!--                    <div class="card-body">-->
    <!--                        <a href="#" class="btn btn-primary btn-lg btn-block">انتخاب پلن</a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--            &lt;!&ndash; // end .pricing &ndash;&gt;-->


    <!--        </div>-->

    <!--    </div>-->
    <!-- // end .section -->


    <div class="section pt-0 sec-faq">
        <div class="container">
            <div class="section-title">
                {{--            <small>پاسخ به برخی</small>--}}
                <h3>سوالات متداول</h3>
            </div>

            <div class="row pt-4">
                <div class="col-md-6">
                    <h4 class="mb-3">آیا میتوانم اپلیکیشن را پیش از ثبت نام تست کنم؟</h4>
                    <p class="light-font mb-5">بله بدون نیاز به هیچ گونه ثبت نامی شما میتوانید اپلیکیشن کپسول را نصب و تست
                        نمایید</p>
                    <h4 class="mb-3">چه شیوه های پرداختی را میپذیرید؟</h4>
                    <p class="light-font mb-5">شما میتوانید با استفاده از کیف پول الکترونیکی خود و همچنین درگاه پرداخت
                        اینترنتی برای پرداخت هزینه ویزیت استفاده نمایید</p>

                </div>
                <div class="col-md-6">
                    <h4 class="mb-3">آیا میتوانم برای آینده درخواست ثبت کنم؟</h4>
                    <p class="light-font mb-5">در کپسول امکان رزرو ویزیت پزشک در آینده نیز وجود دارد</p>
                    <h4 class="mb-3">حداکثر زمان پاسخ گویی چه مدت میباشد؟</h4>
                    <p class="light-font mb-5">پزشکان ما حداکثر در ۶۰ دقیقه به درخواست شما پاسخ خواهند داد</p>
                </div>
            </div>
        </div>

    </div>
    <!-- // end .section -->


    {{--<div class="section bg-gradient" id="download">--}}
    {{--    <div class="container">--}}
    {{--        <div class="call-to-action">--}}

    {{--            <div class="box-icon"><span class="ti-mobile gradient-fill ti-3x"></span></div>--}}
    {{--            <h2>از هر کجا که مایلید دانلود کنید</h2>--}}
    {{--            <p class="tagline">اپلیکیشن ما را میتوانید بر روی انواع دیوایس ها اعم از موبایل ها و کامپیوترهای دسکتاپ مورد--}}
    {{--                استفاده قرار دهید. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان--}}
    {{--                گرافیک است. </p>--}}
    {{--            <div class="my-4">--}}

    {{--                --}}{{--                <a href="#" class="btn btn-light"><img src="{{asset('home/images/appleicon.png')}}" alt="icon"> اپ استور</a>--}}
    {{--                <a href="#" class="btn btn-light"><img src="{{asset('home/images/playicon.png')}}" alt="icon"> گوگل پلی</a>--}}
    {{--                <a href="{{ Storage::disk(config('voyager.storage.disk'))->url(json_decode(\App\CapsulSetting::first()->app_client)[0]->download_link) }}" target="_blank" class="btn btn-light"><i class="fa fa-download">دانلود مستقیم</i></a>--}}
    {{--            </div>--}}
    {{--            <p class="text-primary text-white">--}}
    {{--                <small><i>*نیازمنداندروید ۵ و بالاتر. </i></small>--}}
    {{--            </p>--}}
    {{--        </div>--}}
    {{--    </div>--}}

    {{--</div>--}}
    <!-- // end .section -->

    <div class="light-bg py-5" id="contact">
        <div class="container">
            {{--        <div class="row">--}}

            {{--            <div class="col-lg-3 text-center text-lg-right mt-5">--}}
            {{--                <p class="mb-2"><span class="ti-location-pin mr-2"></span> آدرس شما، خیابان شما، پلاک شما، شماره 12</p>--}}
            {{--                <div class=" d-block d-sm-inline-block">--}}
            {{--                    <p class="mb-2">--}}
            {{--                        <span class="ti-email mr-2"></span> <a class="mr-4" href="mailto:support@mobileapp.com"--}}
            {{--                                                               style="margin-right: 20px !important;">support@website.com</a>--}}
            {{--                    </p>--}}
            {{--                </div>--}}
            {{--                <div class="d-block d-sm-inline-block">--}}
            {{--                    <p class="mb-0">--}}
            {{--                        <span class="ti-headphone-alt mr-2"></span> <a href="tel:51836362800"--}}
            {{--                                                                       style="margin-right: 20px !important;">21-8888-2233</a>--}}
            {{--                    </p>--}}
            {{--                </div>--}}

            {{--            </div>--}}
            {{--            <div class="col-lg-6">--}}
            {{--                <h3 class="text-center">درباره ما</h3>--}}
            {{--                <p class="text-bold"> مجموعه کپسول تیمی متشکل از پزشکان متخصص و عمومی و متخصصان رشته های مختلف علوم--}}
            {{--                    پزشکی مانند پرستاری، روان شناسی، تغذیه و ... است "که پس از سالها ارائه خدمات" حضوری و شبانه روزی در--}}
            {{--                    نقاط مختلف محروم، نیمه محروم و همچنین برخوردار کشور، "و تجربه کارآفرینی در حوزه سلامت"، با در نظر--}}
            {{--                    گرفتن نیاز جامعه و در جهت گسترش عدالت اجتماعی در زمینه "دسترسی همگانی به خدمات سطح بالای پزشکی و--}}
            {{--                    پیراپزشکی، در هر لحظه از شبانه روز و از هر کجای میهن عزیزمان ایران"، اقدام به ارائه خدمات برخط و یا--}}
            {{--                    هماهنگی شفاف برای ارائه خدمات حضوری کرده است.--}}
            {{--                    نکته قابل توجه اینکه این خدمات <span class="text-danger">"به هیچ وجه در مورد بیماران اورژانس صدق نمی کند"</span> و برای "برخی--}}
            {{--                    بیماریهای غیر اورژانس نیز" ممکن است ارجاع جهت معاینه حضوری تعیین گردد.--}}
            {{--                    امید است با ارائه نظرات و پیشنهادات خود، گروه علمی کپسول را در ارائه هر چه بهتر و کاملتر خدمات--}}
            {{--                    راهنمایی فرمائید.</p>--}}
            {{--            </div>--}}
            {{--            <div class="col-lg-3">--}}
            {{--                <div class="social-icons mt-5">--}}
            {{--                    <a href="#"><span class="ti-twitter-alt"></span></a>--}}
            {{--                    <a href="#"><span class="ti-instagram"></span></a>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--        </div>--}}
            <div class="">
                <h3 class="text-center">درباره ما</h3>
                <p class="text-bold"> مجموعه کپسول تیمی متشکل از پزشکان متخصص و عمومی و متخصصان رشته های مختلف علوم
                    پزشکی مانند پرستاری، روان شناسی، تغذیه و ... است "که پس از سالها ارائه خدمات" حضوری و شبانه روزی در
                    نقاط مختلف محروم، نیمه محروم و همچنین برخوردار کشور، "و تجربه کارآفرینی در حوزه سلامت"، با در نظر
                    گرفتن نیاز جامعه و در جهت گسترش عدالت اجتماعی در زمینه "دسترسی همگانی به خدمات سطح بالای پزشکی و
                    پیراپزشکی، در هر لحظه از شبانه روز و از هر کجای میهن عزیزمان ایران"، اقدام به ارائه خدمات برخط و یا
                    هماهنگی شفاف برای ارائه خدمات حضوری کرده است.
                    نکته قابل توجه اینکه این خدمات <span class="text-danger">"به هیچ وجه در مورد بیماران اورژانس صدق نمی کند"</span>
                    و برای "برخی
                    بیماریهای غیر اورژانس نیز" ممکن است ارجاع جهت معاینه حضوری تعیین گردد.
                    امید است با ارائه نظرات و پیشنهادات خود، گروه علمی کپسول را در ارائه هر چه بهتر و کاملتر خدمات
                    راهنمایی فرمائید.</p>
            </div>
            <h3 class="text-center">تماس با ما</h3>
            <div class="text-center">
                <p class="mb-2"><span class="ti-location-pin mr-2"></span>مشهد ، بلوار پنجتن ، پنجتن 68 درمانگاه مصلی</p>
                <div class=" d-block d-sm-inline-block">
                    <p class="mb-2">
                        <span class="ti-email mr-2"></span> <a class="mr-4" href="mailto:support@mobileapp.com"
                                                               style="margin-right: 20px !important;">mrazavi8200@gmail.com</a>
                    </p>
                </div>
                <div class="d-block d-sm-inline-block">
                    <p class="mb-0">
                        <span class="ti-headphone-alt mr-2"></span> <a href="tel:05132110379"
                                                                       style="margin-right: 20px !important;">05132110379</a>
                    </p>
                </div>

            </div>


        </div>

    </div>
@endsection