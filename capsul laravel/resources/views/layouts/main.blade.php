<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
@yield('title')
    @section('refresh')
        @show
    <meta http-equiv="refresh" content="300"/>
<!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('plugins/ionicicons/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('plugins/iCheck/flat/blue.css')}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{asset('plugins/morris/morris.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{asset('plugins/datepicker/datepicker3.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/persian-datepicker.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="{{asset('plugins/google-fonts/font.css')}}" rel="stylesheet">
    <!-- bootstrap rtl -->
    <link rel="stylesheet" href="{{asset('dist/css/bootstrap-rtl.min.css')}}">
    <!-- template rtl version -->
    <link rel="stylesheet" href="{{asset('dist/css/custom-style.css')}}">
    <!-- custom css-->
    <link rel="stylesheet" href="{{asset('dist/css/main.css')}}">

</head>
<body class="hold-transition sidebar-mini">

{{--@if(Session::has('message'))--}}
{{--    <div class="text-center mx-auto my-4 five-second-fade">--}}
{{--        <span class="alert {{ Session::get('alert-class', 'alert-info')}} alert-dismissible--}}
{{--        text-center">--}}
{{--        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="margin-left: 8px!important;">&times;--}}
{{--        </button>--}}
{{--        {{ Session::get('message') }}</span>--}}
{{--    </div>--}}
{{--@endif--}}

{{--@if($errors->any())--}}
{{--    @foreach($errors->all() as $error)--}}
{{--        <p class="alert alert-class alert-danger alert-dismissible">--}}
{{--            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">--}}
{{--                &times;--}}
{{--            </button>--}}
{{--            {{$error}}</p>--}}
{{--    @endforeach--}}
{{--@endif--}}
@yield('body')
<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('plugins/jquery/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('plugins/raphael/raphael.js')}}"></script>
<script src="{{asset('plugins/morris/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('plugins/momentjs/moment.min.js')}}"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<script src="{{asset('dist/js/main.js')}}"></script>
<!-- Persian Data Picker -->
<script src="{{asset('dist/js/persian-date.min.js')}}"></script>
<script src="{{asset('dist/js/persian-datepicker.min.js')}}"></script>
{{-- my scripts--}}
<script>
    $('.alert').delay(5000).fadeOut('slow');
    $('input[required]').on('input', function () {
        this.setCustomValidity("");
    });
    $('textarea[required]').on('input', function () {
        this.setCustomValidity("");
    });
    $('input[required]').on('invalid', function () {
        this.setCustomValidity("لطفا این فیلد را پر نمایید");
    });
    $('textarea[required]').on('invalid', function () {
        this.setCustomValidity("لطفا این فیلد را پر نمایید");
    });
    setInterval(function () {
        if ($('.text-white-danger').hasClass('text-white'))
            $('.text-white-danger').removeClass('text-white').addClass('text-danger');
        else if ($('.text-white-danger').hasClass('text-danger'))
            $('.text-white-danger').removeClass('text-danger').addClass('text-white');
        else
            $('.text-white-danger').addClass('text-white')
    }, 500);
</script>
@yield('script')
</body>
</html>
