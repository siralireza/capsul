@extends('layouts.main')
@section('body')
    <div class="container">
        <div class="row mt-lg-5">
            <div class="col-sm-12 my-auto mx-auto" style="max-width: 600px">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">قوانین و تعهد نامه کپسول</h3>
                    </div>
                    <div class="card-body text-justify">
                        @if($for == 'client')
                            <p>{{\App\CapsulSetting::first()->terms_client}}</p>
                        @elseif($for == 'dr')
                            <p>{{\App\CapsulSetting::first()->terms_dr}}</p>
                        @endif
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection