@extends('layouts.doctor.doctorPanel')
@section('content')
    <div class="card card-primary mt-3 direct-chat direct-chat-primary" style="min-height: 90vh">
        <div class="card-header">
            <div class="row">
                <div class="col-lg-4">
                    <h3 class="card-title">پرسش و پاسخ</h3>
                </div>
                <div class="col-lg-8 d-flex justify-content-end">
                    <a href="{{route('reqs.show',['id'=>$req->id])}}" class="btn btn-lg btn-primary">عملیات بیشتر</a>
                </div>
            </div>

        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <!-- Conversations are loaded here -->
            <div class="direct-chat-messages" id="chat-window">
                <!-- Message. Default to the left -->
                @if($chats->lastPage() != $chats->currentPage())
                    <div class="text-center mx-auto">
                        <a href="{{$chats->nextPageUrl()}}" class="btn btn-secondary">مشاهده چت های قدیمی‌تر</a>
                    </div>
                @endif

                @foreach($chats->reverse() as $ch)
                    @php
                        $dateTime = new \Hekmatinasser\Verta\Verta($ch->created_at);
                        $date = $dateTime->format('H:i:s - Y/n/j');
                    @endphp
                    @if($ch->sender_id == userId())
                        <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name float-right">شما</span>
                            </div>
                            <!-- /.direct-chat-info -->
                            <img class="direct-chat-img" src="{{userPic() ?: asset('dist/img/avatar04.png')}}"
                                 alt="message user image">
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text d-inline-block">
                                <h6 class="small">{{$date}}</h6>
                                @if($ch->msg)
                                    <p>{{$ch->msg}}</p>
                                @endif
                                {{--  video and text needed later maybe --}}
                                @if($ch->msg_type == 'text')
                                @elseif($ch->msg_type == 'video')
                                @elseif($ch->msg_type == 'file')
                                    <a href="{{$ch->path}}" class="text-white" download="file">
                                        <i class="fa fa-file fa-3x text-white">
                                        </i><br>
                                        <span class="mr-1">{{(int)(Storage::size($ch->getOriginal('path'))/1000)}}کیلوبایت </span>
                                    </a>
                                @elseif($ch->msg_type == 'audio')
                                    <audio controls>
                                        <source src="{{$ch->path}}">
                                        صدا توسط مرورگر شما پشتیبانی نمی شود.
                                    </audio>
                                @elseif($ch->msg_type == 'img')
                                    <img class="direct-chat-image w-100" src="{{$ch->path}}" alt="message user image">
                                    <br>
                                    <div class="mx-auto my-2 text-center">
                                        <a target="_blank" rel="noopener noreferrer" href="{{$ch->path}}"
                                           class="btn btn-dark">مشاهده تصویر اصلی</a>
                                    </div>
                                @endif
                                <br>
                                @if($ch->read_status)
                                    <i class="fa fa-check"><i class="fa fa-check"></i></i>

                                @else
                                    <i class="fa fa-check"></i>
                                @endif
                            </div>
                            <!-- /.direct-chat-text -->
                        </div>
                    @else
                        <div class="direct-chat-msg text-left">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name float-left">{{$ch->Sender->fname .' '. $ch->Sender->lname}}</span>
                            </div>
                            <!-- /.direct-chat-info -->
                            <img class="direct-chat-img"
                                 src="{{$ch->Sender->{$ch->sender_type}->User->pic ?: asset('dist/img/avatar5.png')}}"
                                 alt="message user image">
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text d-inline-block">

                                <h6 class="small">{{$date}}</h6>
                                @if($ch->msg)
                                    <p class="text-dark">{{$ch->msg}}</p>
                                @endif

                                {{--  video and text needed later maybe --}}
                                @if($ch->msg_type == 'text')
                                @elseif($ch->msg_type == 'video')
                                @elseif($ch->msg_type == 'file')
                                    <a href="{{$ch->path}}" class="text-dark" download="file">
                                        <i class="fa fa-file fa-3x text-dark">
                                        </i><br>
                                        <span class="mr-1">{{(int)(Storage::size($ch->getOriginal('path'))/1000)}}کیلوبایت </span>
                                    </a>
                                @elseif($ch->msg_type == 'audio')
                                    <audio controls>
                                        <source src="{{$ch->path}}">
                                        صدا توسط مرورگر شما پشتیبانی نمی شود.
                                    </audio>
                                @elseif($ch->msg_type == 'img')
                                    <img class="direct-chat-image w-100" src="{{$ch->path}}" alt="message user image">
                                    <br>
                                    <div class="mx-auto my-2 text-center">
                                        <a target="_blank" rel="noopener noreferrer" href="{{$ch->path}}"
                                           class="btn btn-dark">مشاهده تصویر اصلی</a>
                                    </div>
                                @endif
                                <br>
                                @if($ch->read_status)
                                    <i class="fa fa-check"><i class="fa fa-check"></i></i>

                                @else
                                    <i class="fa fa-check"></i>
                                @endif

                            </div>
                            <!-- /.direct-chat-text -->
                        </div>
                    @endif
                @endforeach

            <!-- /.direct-chat-msg -->
                @if(!$chats->onFirstPage())
                    <div class="text-center mx-auto mb-2">
                        <a href="{{$chats->previousPageUrl()}}" class="btn btn-light text-dark">مشاهده چت های
                            جدید‌تر</a>
                    </div>
                @endif

            </div>
            <!--/.direct-chat-messages-->

        </div>
        <!-- /.card-body -->
        @if(isset($chats) and $chats->isNotEmpty())
            @if($chats->first()->Request->status == 'accepted')
                <div class="card-footer">

                    @error('msg')
                    <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                    @enderror
                    @error('file')
                    <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                    @enderror
                    <form action="{{route('chats.store')}}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="req_id" value="{{$chats->first()->req_id}}">
                        @csrf
                        <div class="input-group">
                            <input type="text" name="msg" id="msg" placeholder="پیام خود را وارد کنید..."
                                   value="{{old('msg')}}"
                                   class="form-control">
                            <span class="input-group-append">
                                    <div class="text-center " style="width: 2.9rem;height: 2.6rem">
                                            <label class="btn-sm btn btn-outline-dark" for="exampleInputFile">
                                                <i class="fa fa-paperclip fa-2x"></i>
                                            </label>
                                            <input type="file" name="file"
                                                   class="input-file-custom" id="exampleInputFile">
                                        </div>
                                <button type="submit" class="btn btn-success mx-1">ارسال</button>
                                <a href="{{route('chats.index')}}" class="btn btn-primary">بازگشت</a>
                    </span>
                        </div>
                    </form>
                </div>
            @else
                <div class="w-100 text-center">
                    <a href="{{route('reqs.show',['req' => $req])}}" class="btn btn-primary w-25">بازگشت</a>
                </div>
        @endif
    @endif
    <!-- /.card-footer-->
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#chat-window').animate({
                scrollTop: $('#chat-window').get(0).scrollHeight
            }, 0);
        });

        // Function for auto refreshing the page, and add or append a query string key/value pair to the URL
        function manageQueryStringParam(key, val, uri) {
            // if uri is not passed in, set a default
            uri = uri || window.location.href;
            // if key does not exist in queryString, create it and append to URL
            if (uri.indexOf(key) < 0) {
                uri += (uri.indexOf('?') >= 0 ? '&' : '?') + key + "=" + val;
            }
            return uri;
        }

        var isTyping = false;
        $('#exampleInputFile').on('click', function () {
            isTyping = true;
        });
        $('#msg').keyup(function () {
            if ($(this).val().length)
                isTyping = true;
            else {
                isTyping = false;
                setTimeout(function () {
                    if (!isTyping) {
                        window.location = manageQueryStringParam('source', 'autorefresh');
                    }
                }, 10000);
            }
        });

        setTimeout(function () {
            if (!isTyping) {
                window.location = manageQueryStringParam('source', 'autorefresh');
            }
        }, 10000);
    </script>
@endsection