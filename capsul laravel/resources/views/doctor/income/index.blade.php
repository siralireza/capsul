@extends('layouts.doctor.doctorPanel')

@section('content')
    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title">تاریخچه درآمدها</h3>
        </div>
        <div class="card-body">

{{--            <form action="{{route('wallet.income')}}" method="get" id="thisForm">--}}
{{--                <div class="row mx-auto">--}}
{{--                    <div class="col-md-4">--}}
{{--                        <div class="form-group">--}}
{{--                            <label class="input-group row" style="font-size: 1.2rem">--}}
{{--                                <span class="input-group-text col-lg-6 col-md-6 col-sm-6">از تاریخ :</span>--}}
{{--                                <input class="dt-picker form-control col-lg-5 col-md-6 col-sm-6"--}}
{{--                                       name="start_date" id="start_date"--}}
{{--                                       value="{{isset($start_date) ? vertaDateFormatter(den2dfa($start_date)):''}}"--}}
{{--                                       readonly/>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4">--}}
{{--                        <div class="form-group">--}}
{{--                            <label class="input-group row" style="font-size: 1.2rem">--}}
{{--                                <span class="input-group-text col-lg-6 col-md-6 col-sm-6">تا تاریخ :</span>--}}
{{--                                <input class="dt-picker form-control col-lg-5 col-md-6 col-sm-6"--}}
{{--                                       name="end_date" id="end_date"--}}
{{--                                       value="{{isset($end_date) ? vertaDateFormatter(den2dfa($end_date)):''}}"--}}
{{--                                       readonly/>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-4">--}}
{{--                        <button type="submit" class="btn btn-primary">مشاهده</button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </form>--}}
            @if($incomes->isEmpty())
                <div class="w-100 text-center mt-lg-5">
                    <h5 class="text-warning">درآمدی وجود ندارد</h5>

                </div>
            @endif
            <div class="row">
                @foreach($incomes as $income)
                    <div class="card card-light col-lg-4">
                        <div class="card-header">
                            <span class="ill-name text-white">{{$income->title}}</span>
                            @php
                                $dateTime = new \Hekmatinasser\Verta\Verta($income->finished_at);
                                $date = $dateTime->format('Y/n/j');
                            @endphp
                            <span class="pull-left text-white">{{$date}}</span><br>

                        </div>
                        <div class="card-body">
                            {{--                        <h5>نوع ویزیت: {{translateRequestType($income)}}</h5>--}}
                            <h6>نام بیمار: {{$income->Client->User->fname.' '.$income->Client->User->lname}}</h6>
                            <div class="row">
                                <div class="col-6">
                                    <p>دلیل مراجعه بیمار: {{$income->main_reason}}</p>
                                </div>
                                <div class="col-6 text-bold">
                                    <p class="text-danger">مبلغ دریافتی: {{$income->exp_cost}} تومان </p>

                                </div>

                            </div>

                        </div>
                        <div class="card-footer">
                            @if($income->status == 'waiting')
                                <div class="w-100">
                                    <button class="btn btn-danger pull-left" data-toggle="modal"
                                            data-target="#deleteModal{{$loop->index}}">حذف
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>

                @endforeach

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('.dt-picker').persianDatepicker({
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
        });
        $('#thisForm').submit(function (e) {
            e.preventDefault();
            var date = $('#start_date').val();
            if(date){
                date = toEnglishNum(date);
                date = date.split('/');
                date = date.map(Number);
                date = new persianDate(date).toCalendar('gregorian').toLocale('en').format('YYYY/MM/DD');
                $('#start_date').val(date)
            }
            var date = $('#end_date').val();
            if(date){
                date = toEnglishNum(date);
                date = date.split('/');
                date = date.map(Number);
                date = new persianDate(date).toCalendar('gregorian').toLocale('en').format('YYYY/MM/DD');
                $('#end_date').val(date)
            }
            $(this).unbind().submit();
        });
    </script>
@endsection