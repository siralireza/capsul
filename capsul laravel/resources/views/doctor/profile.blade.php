@extends('layouts.doctor.doctorPanel')

@section('content')
    <div class="row">
        <div class="col-12 mx-auto" style="max-width: 900px">
            <div class="card card-primary mt-3">
                <div class="card-header">
                    <h3 class="card-title">ویرایش پروفایل</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="{{route('updateProfile')}}" method="post" enctype="multipart/form-data"
                      id="ProfileForm">
                    @csrf
                    @method('patch')
                    <div class="card-body">
                        <div class="w-100">
                            <div class="form-group">
                                <div class="image text-center">
                                    <img src="{{userPic() ?: asset('dist/img/avatar5.png')}}"
                                         class="img-circle elevation-2" alt="User Image" style="width: 4.5rem">
                                </div>
                                <div class="text-center">
                                    <label class="btn-sm btn btn-outline-dark mt-3" for="exampleInputPic">تغییر
                                        عکس</label>
                                    <input type="file" name="pic" style="display: none" class="custom-file-input"
                                           id="exampleInputPic">
                                    @error('pic')
                                    <span class="invalid text-danger text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputfname" class="col-sm-6 control-label">نام</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="fname"
                                               class="form-control @error('fname') is-invalid @enderror"
                                               id="inputfname"
                                               placeholder="نام خود را وارد کنید" value="{{ Auth::user()->fname }}"
                                               autofocus>
                                        @error('fname')
                                        <span class="invalid text-danger text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="inputlname" class="col-sm-6 control-label">نام خانوادگی</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="lname"
                                               class="form-control @error('lname') is-invalid @enderror"
                                               id="inputlname"
                                               placeholder="نام خانوادگی را وارد کنید" value="{{Auth::user()->lname}}"
                                        >
                                        @error('lname')
                                        <span class="invalid text-danger text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="inputPassword0" class="col-sm-6 control-label">کلمه عبور فعلی</label>

                                    <div class="col-sm-10">
                                        <input type="password" name="pass"
                                               class="form-control @error('pass') is-invalid @enderror"
                                               id="inputPassword0"
                                               placeholder="پسورد کنونی وارد کنید"
                                               value="{{ old('pass') }}"
                                                {{--                                                   autocomplete="current-password"--}}
                                        >
                                        @error('pass')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-6 control-label">کلمه عبور جدید</label>

                                    <div class="col-sm-10">
                                        <input type="password" name="password"
                                               class="form-control @error('password') is-invalid @enderror"
                                               id="inputPassword3"
                                               placeholder="پسورد را وارد کنید"
                                               value="{{ old('username') }}"
                                                {{--                                                   autocomplete="current-password"--}}
                                        >
                                        @error('password')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPasswordconf" class="col-sm-6 control-label">تکرار کلمه
                                        عبور جدید</label>

                                    <div class="col-sm-10">
                                        <input type="password" name="password_confirmation"
                                               class="form-control @error('password_confirmation') is-invalid @enderror"
                                               id="inputPasswordconf"
                                               placeholder="تکرار پسورد را وارد کنید"
                                               value="{{ old('password_confirmation') }}"
                                        >
                                        @error('password_confirmation')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-6 control-label">ایمیل</label>

                                    <div class="col-sm-10">
                                        <input type="email" name="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               id="inputEmail"
                                               placeholder="ایمیل را وارد کنید"
                                               value="{{ Auth::user()->email }}">
                                        @error('email')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPhone" class="col-sm-6 control-label">شماره موبایل</label>

                                    <div class="col-sm-10">
                                        <input type="tel" name="phone"
                                               class="form-control @error('phone') is-invalid @enderror"
                                               id="inputPhone"
                                               placeholder="شماره موبایل را وارد کنید"
                                               value="{{ Auth::user()->Doctor->phone }}">
                                        @error('phone')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label" for="inputSex">جسنیت</label>
                                    <div class="col-sm-10">

                                        <select class="form-control @error('sex') is-invalid @enderror"
                                                id="inputSex" name="sex">
                                            @if (Auth::user()->sex == 'female')
                                                <option value="male">مذکر</option>
                                                <option value="female" selected>مونث</option>
                                            @else
                                                <option value="male" selected>مذکر</option>
                                                <option value="female">مونث</option>
                                            @endif
                                        </select>
                                        @error('sex')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="inputWorkExp" class="col-sm-6 control-label">تجربه کاری (سال)</label>
                                    <div class="col-sm-10">
                                        <input type="number" name="work_experience"
                                               class="form-control @error('work_experience') is-invalid @enderror"
                                               id="inputWorkExp"
                                               placeholder="تجربه کاری خود را به سال وارد کنید"
                                               value="{{ Auth::user()->Doctor->work_experience }}">
                                        @error('work_experience')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputDrCode" class="col-sm-6 control-label">شماره نظام پزشکی</label>
                                    <div class="col-sm-10">
                                        <input type="number" name="dr_code"
                                               class="form-control @error('dr_code') is-invalid @enderror"
                                               id="inputDrCode"
                                               placeholder="شماره نظام پزشکی خود را وارد کنید"
                                               value="{{ Auth::user()->Doctor->dr_code }}">
                                        @error('dr_code')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputSheba" class="col-sm-6 control-label">شماره شبا</label>
                                    <div class="col-sm-10">
                                        <input type="number" name="sheba"
                                               class="form-control @error('sheba') is-invalid @enderror"
                                               id="inputSheba"
                                               placeholder="شماره شبا خود را وارد کنید"
                                               value="{{ Auth::user()->Doctor->sheba }}">
                                        @error('sheba')
                                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h6 class="text-bold col-sm-6 control-label">فایل مدارک</h6>
                                    <p class="text-sm"><i class="fa fa-star text-warning mt-3"></i>
                                        امکان آپلود فقط برای فایل فشرده وجود دارد.(rar ,zip ,7z)
                                    </p>
                                    @if(Auth::user()->Doctor->documents_path)
                                        <div class="w-100 text-center">
                                            <a href="{{Auth::user()->Doctor->documents_path}}"
                                               class="btn btn-sm btn-outline-info">دانلود فایل قبلی</a>
                                        </div>
                                    @endif
                                    <div class="w-100">
                                        <div class="text-center">
                                            <label class="btn-sm btn btn-outline-secondary mt-1" for="exampleInputFile">انتخاب
                                                فایل</label>
                                            <div class="col-sm-10">
                                                <input type="file" name="file"
                                                       class="form-control custom-file-input @error('file') is-invalid @enderror"
                                                       id="exampleInputFile">
                                                @error('file')
                                                <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-lg btn-primary">ویرایش</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $('#ProfileForm').on('submit', function (e) {
            e.preventDefault();
            if ($('#inputPhone').val() == '{{Auth::user()->Doctor->phone}}') {
                $('#inputPhone').attr("disabled", true);
            }if ($('#inputDrCode').val() == '{{Auth::user()->Doctor->dr_code}}') {
                $('#inputDrCode').attr("disabled", true);
            }
            e.currentTarget.submit();
        })

    </script>
@endsection