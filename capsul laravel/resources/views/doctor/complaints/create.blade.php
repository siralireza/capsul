@extends('layouts.doctor.doctorPanel')

@section('content')
    <div class="row mt-3">
        <div class="col-12 mx-auto" style="max-width: 500px">
            <div class="card card-primary mt-3">
                <div class="card-header">
                    <h3 class="card-title">ثبت پیشنهاد</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{route('complaints.store')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title" class="col-sm-10 control-label">عنوان پیشنهاد</label>

                            <div class="col-sm-10">
                                    <textarea name="title" rows=2
                                              class="form-control @error('title') is-invalid @enderror" id="title"
                                              placeholder="عنوان شکایت را وارد کنید">{{old('title')}}</textarea>
                                @error('title')
                                <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label class="col-sm-10 control-label" for="inputExp">دکتر مربوطه</label>--}}
{{--                            <div class="col-sm-10">--}}

{{--                                <select class="form-control @error('dr_id') is-invalid @enderror"--}}
{{--                                        id="inputExp" name="dr_id" required>--}}
{{--                                    <option selected disabled>انتخاب کنید</option>--}}
{{--                                    @foreach($reqDoctors as $req)--}}
{{--                                        <option value="{{$req->dr_id}}"--}}
{{--                                                @if (old('dr_id') == $req->id) selected @endif>{{$req->Doctor->User->fname .' '. $req->Doctor->User->lname}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                                @error('dr_id')--}}
{{--                                <span class="invalid text-danger" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}

{{--                        </div>--}}
                        <div class="form-group">
                            <label for="desc" class="col-sm-10 control-label">توضیحات را وارد کنید</label>

                            <div class="col-sm-10">
                                    <textarea name="desc" rows=2
                                              class="form-control @error('desc') is-invalid @enderror" id="desc"
                                              placeholder="توضیحات مربوطه را بنویسید">{{old('desc')}}</textarea>
                                @error('desc')
                                <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">تایید</button>
                        <a href="{{route('complaints.index')}}" class="btn btn-primary pull-left">بازگشت</a></div>
                </form>
            </div>
        </div>
    </div>

@endsection