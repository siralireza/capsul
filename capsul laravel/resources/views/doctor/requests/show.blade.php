@extends('layouts.doctor.doctorPanel')


@section('content')
    @if($errors->any())
        @foreach($errors->all() as $error)
            <p class="alert alert-class alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    &times;
                </button>
                {{$error}}</p>
        @endforeach
    @endif
    <div class="card card-primary mt-3 mx-auto" style="max-width: 500px">
        <div class="card-header">
            <h3 class="card-title">مشاهده درخواست</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="form-group">
                @if($req->Client)
                    @if($req->Client->User->pic)
                        <div class="image text-center">
                            <img src="{{$req->Client->User->pic}}" class="img-circle elevation-2" alt="User Image"
                                 style="width: 4.5rem">
                        </div>
                    @else
                        <div class="image text-center">
                            <img src="{{asset('dist/img/avatar04.png')}}" class="img-circle elevation-2"
                                 alt="User Image"
                                 style="width: 4.5rem">
                        </div>
                    @endif
                    <h5>نام: {{$req->Client->User->fname . ' '. $req->Client->User->lname}}</h5>
                    <h6>سن: {{$req->Client->age ?? 'تعیین نشده'}}</h6>
                    <h6>جنسیت: {{$req->Client->User->sex == 'male' ? 'مرد' : 'زن'}}</h6>
                @endif


            </div>

        </div>
        <!-- /.card-body -->
        {{--        delete form --}}
        <div class="card-footer text-center">
            <div class="mx-auto text-center row">
                @if($req->status == 'accepted')
                    <span class="my-1 bg-success rounded mx-auto p-2">وضعیت: {{translateRequestStatus($req)}}</span>
                @elseif($req->status == 'waiting')
                    <span class="my-1 bg-warning rounded mx-auto p-2">وضعیت: {{translateRequestStatus($req)}}</span>
                @elseif($req->status == 'reserved')
                    <span class="my-1 bg-info rounded mx-auto p-2">وضعیت: {{translateRequestStatus($req)}}</span>
                @elseif($req->status == 'finished')
                    <span class="my-1 bg-dark rounded mx-auto p-2">وضعیت: {{translateRequestStatus($req)}}</span>
                @else
                    <span class="my-1 bg-danger rounded mx-auto p-2">وضعیت: {{translateRequestStatus($req)}}</span>
                @endif
                <button type="button" class="mt-3 col-12 btn btn-lg btn-info" data-toggle="modal"
                        data-target="#adviseModal">
                    مشاهده
                    توضیحات
                </button>
                <button type="button" class="mt-3 col-12 btn btn-lg btn-dark" data-toggle="modal"
                        data-target="#historyModal">مشاهده سوابق پزشکی
                </button>
                @if($req->status == 'accepted')
                    <a href="{{route('chats.show',['id' => $req->id])}}" class="mt-3 col-12 btn btn-lg btn-success">انتقال
                        به صفحه پرسش و پاسخ</a>

                    <button class="mt-3 col-12 btn btn-lg btn-warning" data-toggle="modal" data-target="#rejectModal">رد
                        درخواست
                    </button>
                    <button class="mt-3 col-12 btn btn-lg btn-danger" data-toggle="modal" data-target="#closeModal">بستن
                        درخواست
                    </button>

                @elseif($req->status == 'waiting')
                    <button class="mt-3 col-12 btn btn-lg btn-success" data-toggle="modal" data-target="#acceptModal">
                        قبول
                        درخواست
                    </button>
                    <button class="mt-3 col-12 btn btn-lg btn-warning" data-toggle="modal" data-target="#rejectModal">رد
                        درخواست
                    </button>
                @elseif($req->status == 'reserved')
                    <a href="{{route('chats.show',['id' => $req->id])}}"
                       class="mt-3 col-12 btn btn-lg btn-info disabled">پرسش از
                        پزشک</a>
                @elseif($req->status == 'finished')
                    <a href="{{route('chats.show',['id' => $req->id])}}" class="mt-3 col-12 btn btn-lg btn-info">مشاهده
                        پیام‌ها</a>
                @else
                    <a href="{{route('chats.show',['id' => $req->id])}}"
                       class="mt-3 col-12 btn btn-lg btn-info disabled">پرسش از
                        پزشک</a>
                @endif
                @if($req->user_id == userId() and $req->status == 'waiting')

                    <button type="submit" class="mt-3 col-12 btn btn-lg btn-danger" data-toggle="modal"
                            data-target="#deleteModal">حذف
                    </button>
                @endif
                <a href="{{route('reqs.index').'?status[0]='.$req->status}}" class="mt-3 col-12 btn btn-lg btn-primary">بازگشت</a>
            </div>
        </div>

    </div>

    <div class="modal fade mt-5" id="adviseModal" tabindex="-1" role="dialog" aria-labelledby="adviseModal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">مشاهده توصیه پزشک</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputForName">دلیل اصلی مراجعه به پزشک</label>
                        <input type="text" class="form-control" value="{{$req->main_reason}}" id="inputForName"
                               disabled>
                    </div>
                    @php
                        $flag = true;
                    @endphp
                    @for($i = 1;$i<4;$i ++)
                        @if($req->{'answer'.$i})
                            @if($flag)
                                <hr class="bg-white">
                                <h6>توضیحات وارد شده</h6>
                                @php($flag = false)
                            @endif
                            <textarea class="form-control mt-3" rows="1"
                                      disabled>{{ $req->{'answer'.$i} }}</textarea>
                        @endif
                    @endfor
                    <div class="form-group">
                        <label for="inputForAdvise">توصیه</label>
                        @if($req->status == 'waiting' or $req->status == 'reserved')
                            <p class="text-warning" id="inputForAdvise">توصیه پزشک پس از تایید درخواست وارد می‌شود</p>
                        @else
                            <textarea class="form-control mt-3" id="inputForAdvise" rows="1"
                                      disabled>{{$req->dr_first_answer}}</textarea>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="inputForDiag">تشخیص نهایی</label>
                        @if($req->status == 'finished')
                            <textarea class="form-control mt-3" id="inputForDiag" rows="1"
                                      disabled>{{$req->final_diagnosis}}</textarea>
                        @else
                            <p class="text-warning" id="inputForAdvise">تشخیص نهایی پس از پایان درخواست وارد می‌شود</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="inputForDiag">مراجعه مجدد</label>
                        @if($req->status == 'finished')
                            @if($req->re_visit)
                                <textarea class="form-control mt-3" id="inputForDiag" rows="1"
                                          disabled>{{$req->re_visit}}</textarea>
                            @else
                                <p class="text-warning" id="inputForAdvise">دکتر اطلاعتی مربوط به مراجعه مجدد برای شما
                                    ثبت نکرده است</p>
                            @endif
                        @else
                            <p class="text-warning" id="inputForAdvise">مراجعه مجدد پس از پایان درخواست مشخص می‌شود</p>
                        @endif
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">بستن</button>
                </div>
            </div>
        </div>
    </div>
    @if($req->status == 'waiting')

        <div class="modal fade" id="acceptModal" tabindex="-1" role="dialog" aria-labelledby="rateModal"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">قبول درخواست</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{route('acceptReq',['req' => $req])}}" method="post">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group  mt-3">
                                <label for="inputForNoskhe mr-1">توصیه اولیه</label>
                                <textarea class="form-control @error('first_answer') is-invalid @enderror"
                                          name="first_answer"
                                          id="inputForNoskhe" rows="3"
                                          placeholder="لطفا پاسخ خود را وارد نمایید" required></textarea>
                            </div>
                            @error('first_answer')
                            <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-success mx-2">قبول و ارسال پاسخ</button>
                            <button type="button" class="btn btn-primary justify-content-end" data-dismiss="modal">بستن
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endif

    <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rateModal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">رد درخواست</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('rejectReq',['id' => $req->id])}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-10 control-label" for="inputReason">دلیل رد کردن درخواست را انتخاب
                                کنید:&nbsp;<i
                                        class="fa fa-star text-danger"></i></label>
                            <div class="col-sm-10">

                                <select class="form-control @error('reject_reason') is-invalid @enderror"
                                        id="inputReason" name="reject_reason" required>
                                    <option value="presence">حضوری</option>
                                    <option value="emergency">اورژانس</option>
                                    <option value="other_exp">تخصص دیگر</option>
                                </select>
                                @error('reject_reason')
                                <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="submit" class="btn btn-success mx-2">تایید</button>
                        <button type="button" class="btn btn-primary justify-content-end" data-dismiss="modal">بازگشت
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <div class="modal fade mt-5" id="historyModal" tabindex="-1" role="dialog" aria-labelledby="rateModal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">سوابق پزشکی</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @php($secondHistoryFlag = true)
                        @foreach($userHistory as $history)
                            @if($loop->last)
                                <div class="col-md-6">
                                    <h5 class="text-warning">سوابق اولیه</h5>
                                    @if(!($history->answer1 and
                                            $history->answer2 and
                                            $history->answer3 and
                                            $history->answer4 and
                                            $history->answer5))
                                        <p class="text-danger">چیزی درج نشده است</p>
                                    @endif
                                    @if($history->answer1)
                                        <div class="border border-warning p-2 my-2">
                                            <p>سابقه بیماری خاص:</p>
                                            <p>{{$history->answer1}}</p>
                                        </div>
                                    @endif
                                    @if($history->answer2)
                                        <div class="border border-warning p-2 my-2">
                                            <p>سابقه دارویی:</p>
                                            <p>{{$history->answer2}}</p>
                                        </div>
                                    @endif
                                    @if($history->answer3)
                                        <div class="border border-warning p-2 my-2">
                                            <p>سابقه بستری یا جراحی:</p>
                                            <p>{{$history->answer3}}</p>
                                        </div>
                                    @endif
                                    @if($history->answer4)
                                        <div class="border border-warning p-2 my-2">
                                            <p>سابقه بیماری در خانواده:</p>
                                            <p>{{$history->answer4}}</p>
                                        </div>
                                    @endif
                                    @if($history->answer5)
                                        <div class="border border-warning p-2 my-2">
                                            <p>سابقه حساسیت غذایی یا دارویی:</p>
                                            <p>{{$history->answer5}}</p>
                                        </div>
                                    @endif
                                </div>


                            @else
                                @if($secondHistoryFlag)
                                    <div class="col-md-6">
                                        <h5 class="text-info">سوابق ثانویه</h5>

                                        @php($secondHistoryFlag = false)
                                        @endif
                                        <div class="border @if($history->added_by == 'dr') border-success @endif p-2 my-2">
                                            <span class="pull-left text-white">{{toJalali($history->created_at)}}</span>
                                            @if($history->added_by == 'dr')
                                                <p class="text-success">توسط دکتر</p>
                                            @endif
                                            <p>شکایت اولیه: {{$history->sick}}</p>
                                            <p>توضیحات:</p>
                                            <p>{{$history->desc}}</p>
                                        </div>

                                    </div>
                                @endif
                                @endforeach


                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-primary justify-content-end" data-dismiss="modal">بازگشت
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="closeModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">بستن درخواست</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('closeReq',['req'=>$req])}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group  mt-3">
                            <label for="inputForTashkhis mr-1">تشخیض نهایی</label>
                            <textarea class="form-control @error('final_diagnosis') is-invalid @enderror"
                                      name="final_diagnosis"
                                      id="inputForTashkhis" rows="1"
                                      placeholder="یک جمله توضیح داده شود" required></textarea>
                        </div>
                        @error('final_diagnosis')
                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <div class="form-group">
                            <div class=" col-sm-10">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="add_history" value="1"
                                           id="exampleCheck2" {{ old('add_history') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="exampleCheck2">به سابقه پزشکی بیمار اضافه
                                        شود</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group  mt-3">
                            <label for="inputForRevisit mr-1">مراجعه مجدد</label>
                            <textarea class="form-control @error('re_visit') is-invalid @enderror"
                                      name="re_visit"
                                      id="inputForRevisit" rows="3"
                                      placeholder="در صورت نیاز مراجعه مجدد توضیحات و تاریخ ذکر شود"></textarea>
                        </div>
                        @error('re_visit')
                        <span class="invalid text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="submit" class="btn btn-success">تایید</button>
                        <button type="button" class="btn btn-primary">بازگشت</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

@endsection