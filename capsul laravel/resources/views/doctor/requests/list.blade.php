@extends('layouts.doctor.doctorPanel')

@section('content')
    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title">
                {{translateRequestTitle($type)}}
            </h3>
        </div>
        <div class="card-body row">
            @if($reqs->isEmpty())
                <div class="text-center w-100">
                    <h5 class="text-warning">در این بخش درخواستی وجود ندارد</h5>
                </div>
            @endif
            @foreach($reqs as $req)
                <div class="card col-lg-4">
                    <div class="card-body">
                        <span class="my-1 ill-name">{{translateRequestType($req)}}</span>
                        @php
                                $dateTime = new \Hekmatinasser\Verta\Verta($req->created_at);
                                $date = $dateTime->format('Y/n/j');
                        @endphp
                        <span class="my-1 float-left small mt-1">{{$date}}</span><br>
                        @if($req->status == 'accepted')
                            <span class="my-1 badge badge-success">{{translateRequestStatus($req)}}</span>
                        @elseif($req->status == 'waiting')
                            <span class="my-1 badge badge-warning">{{translateRequestStatus($req)}}</span>
                        @elseif($req->status == 'reserved')
                            <span class="my-1 badge badge-info">{{translateRequestStatus($req)}}</span>
                        @elseif($req->status == 'finished')
                            <span class="my-1 badge badge-dark">{{translateRequestStatus($req)}}</span>
                        @else
                            <span class="my-1 badge badge-danger">{{translateRequestStatus($req)}}</span>
                        @endif

                        <div class="d-flex my-3">
                            @if($req->Client and $req->Client->User->pic)
                                <div class="image">
                                    <img src="{{$req->Client->User->pic}}" class="img-circle elevation-2"
                                         alt="User Image"
                                         style="width: 3.5rem">
                                </div>
                            @elseif($req->Client)
                                <div class="image">
                                    <img src="{{asset('dist/img/avatar04.png')}}" class="img-circle elevation-2"
                                         alt="User Image"
                                         style="width: 3.5rem">
                                </div>
                            @endif

                            <div class="info mr-3">
                                @if($req->user_id)
                                    <a class="d-block">{{$req->Client->User->fname . ' '. $req->Client->User->lname}}</a>
                                    <a class="d-block">سن: {{$req->Client->age ?? 'تعیین نشده'}}</a>
                                @endif

                            </div>
                        </div>
                        <a href="{{route('reqs.show',['req'=>$req])}}"
                           class="my-1 btn btn-dark btn-sm">اطلاعات بیشتر</a>
                        <br>
                    </div>
                </div>

            @endforeach

        </div>
        <div class="card-footer text-center">
            <a href="{{route('reqs.index')}}" class="btn btn-lg btn-primary">بازگشت</a>
        </div>
    </div>

@endsection