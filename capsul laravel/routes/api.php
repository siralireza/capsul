<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// git pull
use App\Banner;

Route::get('/git/pull', function () {
    return shell_exec("cd " . env('GIT_ROOT') . " && git reset --hard HEAD && git pull && cd capsul\ laravel/public && rm storage && cd .. && php artisan storage:link && chmod 777 -R storage/");
});
Route::get('/db/migrate', function () {
    Artisan::call('migrate:fresh', ['--seed' => true]);
    return 'Migrations Done!';
});

// app information
Route::get('/app/info', function () {
    return [
        'api_version' => env('API_VERSION'),
        'last_client_version' => 1,
    ];
});
Route::post('sendNotif'
    , function () {
    sendNotification('salam qashang sal',['eoRKLqYuFtE:APA91bHwFh4dc2sy4gDqpkuNysnyhttlk21fUCLUSbuqlVfDDEcC8dezIElhR_aOLnljNQmfQQAKvDzMFLj9XCYq3sEYRSwrpgAuW-Ew1RnM9GoMJ_SuBAP4OO_nqzVQNiI0HNvhLrF6']);
        return 'ok';
    });

// app banner
Route::get('/app/banner', function () {
    return Banner::all();
});

// Api version route group
Route::prefix(env('API_VERSION'))->group(function () {
    // Authentication routes with throttle
    Route::middleware('throttle:3000,1')->group(function () {
        Route::post('register', 'Auth\RegisterController@register');
        Route::post('login', 'Auth\LoginController@login');
    });
    Route::middleware('throttle:3000,1')->group(function () {
        Route::post('forgotPass', 'Auth\ForgotPasswordController@recoverPassSms')->name('forgotPassSMS');
    });

    // get items for sicks and exps
    Route::get('sicks', 'SickController@index');
    Route::get('expertises', 'ExpertiseController@index');


    // Routes need auth and client specific urls
    Route::prefix('client')->middleware('auth:api', 'role')->group(function () {
        // update user information
        Route::match(['put', 'patch', 'post'], 'user/update', 'Auth\RegisterController@update');
        // return current user information
        Route::get('/user', 'Auth\LoginController@current_user');
        // user wallet actions
        Route::get('/user/wallet/increase', 'WalletController@increase');
        Route::get('/user/wallet', 'WalletController@wallet');
        // Api resources
        Route::apiResource('user/sicks', 'UserSickController');
        Route::apiResource('user/reqs', 'UserRequestController');
        Route::get('user/complaints/doctors', 'ComplaintController@doctors');
        Route::apiResource('user/complaints', 'ComplaintController');
        Route::apiResource('user/chats', 'ChatController');

    });
    // Routes need auth and doctor specific urls

    Route::prefix('doctor')->middleware('auth:api', 'role')->group(function () {
        // update user information
        Route::match(['put', 'patch', 'post'], 'user/update', 'Auth\RegisterController@update');
        // return current user information
        Route::get('/user', 'Auth\LoginController@current_user');
        // user wallet actions
        Route::get('/user/wallet/increase', 'WalletController@increase');
        Route::get('/user/wallet', 'WalletController@wallet');
        Route::get('/user/income', 'WalletController@income');

        // Api resources
        Route::get('client/sicks/{user}', 'UserSickController@clientSicksDr');
        Route::get('user/reqs/new', 'UserRequestController@newRequestsDr');
        Route::post('user/reqs/new/{req}', 'UserRequestController@acceptRequestDr');
        Route::post('user/reqs/close/{req}', 'UserRequestController@closeRequestDr');
        Route::post('user/reqs/reject/{req}', 'UserRequestController@rejectRequestDr');
        Route::apiResource('user/complaints', 'ComplaintController');
        Route::apiResource('user/reqs', 'UserRequestController');
        Route::apiResource('user/chats', 'ChatController');

    });
});
