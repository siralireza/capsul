<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Blog;
use Jenssegers\Agent\Agent;

Route::get('/', function () {
    $agent = new Agent();
    if ($agent->isPhone() and $agent->isSafari() and Auth::check())
        return redirect()->route('panel');
    return view('home');
})->name('home');

Route::get('blog', function () {
    $blogs = Blog::all();
    return view('blog.index', ['blogs' => $blogs]);
})->name('blog');

Route::get('ios', function () {
    return redirect()->route('login');
})->name('iosGet');
Route::post('ios', function () {
    $agent = new Agent();
    if ($agent->isSafari())
        return view('ios')->with(['safari'=>true]);
    return view('ios');
})->name('iosPost');

//Route::get('/', 'Auth\LoginController@showLoginForm')->name('home');

Route::get('/terms/client', function (Request $request) {
    return view('terms', ['for' => 'client']);
})->name('termsClient');
Route::get('/terms/dr', function (Request $request) {
    return view('terms', ['for' => 'dr']);
})->name('termsDr');

Auth::routes();

// Authentication routes with throttle
// Later we have to add recaptcha to this routes
// every 1 min 3000 requests
Route::middleware('throttle:2,1')->group(function () {
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('login', 'Auth\LoginController@login');
});
Route::middleware('throttle:2,2')->group(function () {
    Route::post('forgotPass', 'Auth\ForgotPasswordController@recoverPassSms')->name('forgotPassSMS');
});

// Resources without auth
Route::get('register/client', 'Auth\RegisterController@showRegistrationFormClient')->name('registerClient');
Route::get('register/doctor', 'Auth\RegisterController@showRegistrationFormDoctor')->name('registerDoctor');
Route::get('/payir', 'PaymentController@pay')->name('pay');
Route::get('/payir/callback', 'PaymentController@verify');


// Routes need auth
Route::middleware(['auth', 'role'])->group(function () {
    Route::get('panel', 'PanelController@index')->name('panel');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    Route::resource('reqs', 'UserRequestController');
    Route::get('profile', 'Auth\LoginController@current_user')->name('profile');
    Route::match(['put', 'patch'], 'user/update', 'Auth\RegisterController@update')->name('updateProfile');
    Route::resource('sicks', 'UserSickController');
    Route::resource('complaints', 'ComplaintController');
    Route::resource('chats', 'ChatController');
    Route::get('wallet/increase', 'WalletController@increaseIndex')->name('wallet.index');
    Route::get('wallet/income', 'WalletController@income')->name('wallet.income');
    Route::post('wallet/increase', 'WalletController@increase')->name('wallet.increase');

    //dr routes should set middleware later
    Route::post('reqs/new/{req}', 'UserRequestController@acceptRequestDr')->name('acceptReq');
    Route::post('reqs/close/{req}', 'UserRequestController@closeRequestDr')->name('closeReq');
    Route::post('reqs/reject/{req}', 'UserRequestController@rejectRequestDr')->name('rejectReq');

    Route::group(['prefix' => env('ADMIN_PREFIX/') . 'admin'], function () {
        Voyager::routes();
        Route::get('reqs/cost/back', 'UserRequestController@costBack')->name('costBack');
        Route::get('doctor/accept', 'DoctorController@accept')->name('drAccept');
    });

});

