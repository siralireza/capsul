<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserSickTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_sicks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('sick');
//            $table->unsignedBigInteger('sick_id')->nullable();
//            $table->foreign('user_id')->references('user_id')->on('clients')
//                ->onDelete('cascade')->onUpdate('cascade');
//            $table->foreign('sick_id')->references('id')->on('sicks');
            $table->mediumText('desc')->nullable();
            $table->enum('added_by', ['dr', 'client'])->default('client');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_sicks');
    }
}
