<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientInitialHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_initial_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('user_id')->on('clients')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->mediumText('answer1')->nullable();
            $table->mediumText('answer2')->nullable();
            $table->mediumText('answer3')->nullable();
            $table->mediumText('answer4')->nullable();
            $table->mediumText('answer5')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_initial_histories');
    }
}
