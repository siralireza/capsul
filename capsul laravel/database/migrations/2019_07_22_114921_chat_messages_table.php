<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChatMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sender_id');
            $table->unsignedBigInteger('req_id');
            $table->foreign('req_id')->references('id')->on('user_requests')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->enum('sender_type', ['client', 'doctor']);
            $table->longText('msg')->nullable();
            $table->enum('msg_type', ['text', 'file', 'audio', 'video', 'img']);
            $table->boolean('read_status')->default('0');
            $table->string('path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_messages');
    }
}
