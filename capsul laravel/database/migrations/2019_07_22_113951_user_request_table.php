<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('exp_id');
            $table->unsignedBigInteger('dr_id')->nullable();
            $table->foreign('user_id')->references('user_id')->on('clients')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->enum('type', ['lab', 'send_prescription', 'renew_prescription']);
            $table->foreign('exp_id')->references('id')->on('expertises');
            $table->mediumInteger('exp_cost');
            $table->boolean('cost_back_client')->nullable();
            $table->boolean('cost_back_doctor')->nullable();
            $table->foreign('dr_id')->references('user_id')->on('doctors')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->longText('main_reason')->nullable();
            $table->mediumText('re_visit')->nullable();
            $table->mediumText('answer1')->nullable();
            $table->mediumText('answer2')->nullable();
            $table->mediumText('answer3')->nullable();
            $table->boolean('read_by_dr')->nullable();
            $table->mediumText('dr_first_answer')->nullable();
            $table->text('final_diagnosis')->nullable();
            $table->enum('status', ['waiting', 'accepted', 'rejected', 'finished', 'reserved'])->default('waiting');
            $table->boolean('viewer_checked')->nullable();
            $table->enum('reject_reason', ['presence', 'emergency', 'other_exp'])->default(null)->nullable();
            $table->tinyInteger('score')->nullable();
            $table->date('reserve_date')->nullable();
            $table->timestamp('finished_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_requests');
    }
}
