<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('name')->nullable();
            $table->string('pic')->nullable();
            $table->enum('sex', ['male', 'female'])->default('male');
            $table->enum('status', ['active', 'deactive', 'non_verified'])->default('non_verified');
            $table->enum('capsul_role', ['client', 'admin', 'doctor'])->default('client');
            $table->string('username', 50)->nullable();
            $table->string('email', 100)->unique()->nullable();
            $table->string('password');
            $table->string('recovery_password')->nullable();
            $table->rememberToken();
            $table->string('api_token', 80)->unique()->nullable()->default(null);
            $table->string('fcm_token', 257)->nullable();
            $table->dateTime('phone_verified_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
