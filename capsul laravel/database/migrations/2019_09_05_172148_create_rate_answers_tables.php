<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRateAnswersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('req_id');
            $table->foreign('req_id')->references('id')->on('user_requests')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('rateAnswer1')->nullable();
            $table->boolean('rateAnswer2')->nullable();
            $table->boolean('rateAnswer3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_answers');
    }
}
