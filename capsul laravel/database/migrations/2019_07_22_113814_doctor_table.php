<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DoctorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->primary();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->string('phone', 15)->unique()->nullable();
            $table->unsignedMediumInteger('wallet')->default(0);
            $table->string('sheba', 100)->nullable();
            $table->tinyInteger('work_experience')->nullable();
            $table->unsignedBigInteger('exp_id')->nullable();
            $table->foreign('exp_id')->references('id')->on('expertises');
            $table->string('dr_code',100)->nullable();
            $table->string('documents_path')->nullable();
            $table->string('name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
