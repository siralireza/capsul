<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_actions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->string('factorId',100)->nullable();
            $table->unsignedBigInteger('transId')->nullable();
            $table->unsignedBigInteger('traceNumber')->nullable();
            $table->unsignedBigInteger('amount');
            $table->string('mobile', 15)->nullable();
            $table->string('cardNumber', 20)->nullable();
            $table->string('description', 100)->nullable();
            $table->string('messageReceived', 50)->nullable();
            $table->unsignedTinyInteger('status')->nullable();
            $table->enum('comeFrom', ['web', 'app'])->default('web');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_actions');
    }
}
