<?php

use App\Expertise;
use App\CapsulSetting;
use App\Sick;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // i wrote seeders directly here
        Sick::insert([
            [
                'sick_name' => 'سردرد',
                'desc' => 'توضیحات مربوط به مقدار زمان سردرد و محل سر را در توضیحات بنویسید.',
            ],
            [
                'sick_name' => 'سرماخوردگی',
                'desc' => 'علامی نظیر آب ریزش بینی و سردرد و گلو دارد را اگر دارید در توضیحات بنویسید.',
            ],
            [
                'sick_name' => 'قرمزی پوست',
                'desc' => 'محل و شکل قرمزی را در توضیحات بنویسید.',
            ],
        ]);
        Expertise::insert([
            [
                'expertise_name' => 'چشم',
                'cost' => 6000,
                'desc' => 'اطلاعات مربوط به چشم و سردرد و کم بینایی را در توضیحات بنویسید.',
            ],
            [
                'expertise_name' => 'چشم',
                'cost' => 6000,
                'desc' => 'اطلاعات مربوط به چشم و سردرد و کم بینایی را در توضیحات بنویسید.',
            ],
            [
                'expertise_name' => 'گوش',
                'cost' => 5000,
                'desc' => 'اطلاعات مربوط به درد گوش را در توضیحات بنویسید.',
            ],
            [
                'expertise_name' => 'پوست',
                'cost' => 8000,
                'desc' => 'اطلاعات مربوط به مشکل را در توضیحات بنویسید.',
            ],

        ]);
        CapsulSetting::create([
            'answer_time' => 1
        ]);
        // $this->call(UsersTableSeeder::class);
    }
}
