<?php

namespace App;


class Client extends MotherModel
{
    protected $primaryKey = 'user_id';

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Requests()
    {
        return $this->hasMany(UserRequest::class);
    }

    public function InitHistory()
    {
        return $this->hasOne(InitHistory::class, 'user_id', 'user_id');
    }

    public function getNameAttribute($value)
    {
        if (isAdmin())
            return $value . "(" . $this->user_id . ")";
    }
}
