<?php

namespace App\Exceptions;

use Auth;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Seld\JsonLint\DuplicateKeyException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if (Auth::user() and Auth::user()->username == 'programmer')
            return parent::render($request, $exception);

        if ($exception instanceof ModelNotFoundException && $request->expectsJson()) {
            if ($request->expectsJson())
                return response()->json([
                    'error' => 'آیتم مورد نظر یافت نشد.'
                ], 404);
            else
                return view('errors.404');
        }
        if ($exception instanceof DuplicateKeyException) {
            if (!$request->expectsJson()) {

                flashMsg('آیتم مورد نظر در پایگاه داده وجود دارد', 0);
                return redirect()->back();
            }
        }
        if ($exception instanceof ThrottleRequestsException) {
            if (!$request->expectsJson()) {
                flashMsg('تعداد درخواست های شما بیشتر از حد مجاز بوده است لطفا ۲ دقیقه دیگر دوباره تلاش کنید.', 0);
                return redirect()->back();
            }
        }
        if ($exception->getCode() == '23000') {
            if (!$request->expectsJson()) {

                flashMsg($exception->getCode() . ' - این آیتم در حال استفاده میباشد و یا از قبل موجود است بنابراین امکان انجام عملیات مورد نظر وجود ندارد', 0);
                return redirect()->back();
            }
        }
//        else {
////            dd($exception instanceof \Illuminate\Validation\ValidationException);
//            if (!$request->expectsJson() and !($exception instanceof \Illuminate\Validation\ValidationException)) {
//
//                flashMsg($exception->getCode() . ' - متاسفانه درخواست شما با مشکل مواجه شد', 0);
//                return redirect()->back();
//            }
//        }
        return parent::render($request, $exception);
    }
}
