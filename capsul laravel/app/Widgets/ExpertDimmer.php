<?php

namespace App\Widgets;

use App\Client;
use App\Complaint;
use App\Expertise;
use App\Support;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class ExpertDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Expertise::count();
        $string = 'تخصص';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-file-text',
            'title' => "{$count} {$string}",
            'text' => "برای مشاهده لیست " . $string . " ها کلیک کنید",
            'button' => [
                'text' => 'مشاهده همه ' . $string . ' ها',
                'link' => route('voyager.expertises.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/03.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return app('VoyagerAuth')->user()->can('browse', Voyager::model('User'));
    }
}
