<?php

namespace App;

use Hekmatinasser\Verta\Verta;
use Storage;

class ChatMessage extends MotherModel
{
    protected $hidden = [
        'sender_id', 'req_id',
//        'read_status',
        'updated_at', 'deleted_at'];

    public function Sender()
    {
        return $this->belongsTo(User::class, 'sender_id', 'id')->select('id', 'fname', 'lname', 'sex', 'pic');
    }

    public function Request()
    {
        return $this->belongsTo(UserRequest::class, 'req_id', 'id');

    }

    public function getPathAttribute($value)
    {
        $request = request();
        if ($request->expectsJson() or isNotAdmin())
            return $value ? url(Storage::url($value)) : null;
        else
            return $value;
    }

    public function getCreatedAtAttribute($value)
    {
        if (isAdmin()) {
            $dateTime = new Verta($value);
            $dateTimeString = $dateTime->format('H:i:s - Y/n/j');
            return $dateTimeString;
        }
        return $value;
    }

    public function getReadStatusAttribute($value)
    {
        return $value ? true : false;
    }



}
