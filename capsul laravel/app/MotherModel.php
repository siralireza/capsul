<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MotherModel extends Model
{
    protected $guarded = ['created_at', 'updated_at'];
    protected $hidden = ['id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
}
