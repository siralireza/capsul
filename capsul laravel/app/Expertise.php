<?php

namespace App;

use Storage;

class Expertise extends MotherModel
{
    protected $hidden = ['user_id','sort_col', 'created_at', 'updated_at', 'deleted_at'];

    public function Requests()
    {
        return $this->hasMany(UserRequest::class);
    }

    public function Doctors()
    {
        return $this->hasMany(Doctor::class);
    }

    public function getIconAttribute($value)
    {
        $request = request();
        if ($request->expectsJson())
            return $value ? url(Storage::url($value)) : null;
        else
            return $value;

    }
}
