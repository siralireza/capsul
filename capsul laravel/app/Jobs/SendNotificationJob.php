<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $msg;
    private $fcm_tokens;

    /**
     * Create a new job instance.
     *
     * @param $msg
     * @param $fcm_tokens
     */
    public function __construct($msg, $fcm_tokens)
    {
        $this->msg = $msg;
        $this->fcm_tokens = $fcm_tokens;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return sendNotification($this->msg, $this->fcm_tokens);
    }
}
