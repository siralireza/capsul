<?php

namespace App;


use Hekmatinasser\Verta\Verta;

class TransAction extends MotherModel
{
    protected $table = 'trans_actions';

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function getCreatedAtAttribute($value)
    {
        if (isAdmin()) {
            $dateTime = new Verta($value);
            $dateTimeString = $dateTime->format('H:i:s - Y/n/j');
            return $dateTimeString;
        }
        return $value;
    }
}
