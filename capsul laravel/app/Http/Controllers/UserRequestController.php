<?php

namespace App\Http\Controllers;

use App\ChatMessage;
use App\Doctor;
use App\Expertise;
use App\RateAnswer;
use App\User;
use App\UserRequest;
use App\UserSick;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return UserRequest[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        $columns = ['id', 'user_id', 'exp_id', 'dr_id', 'type',
            'main_reason', 'read_by_dr', 'reserve_date', 'status', 'score', 'created_at'];
        if (isClient()) {
            $request->validate([
                'status' => 'array|in:waiting,finished,reserved,accepted,rejected',
            ]);
            $clientRequests = UserRequest::
            with('Doctor:user_id,dr_code', 'Expertise:id,expertise_name,icon')
                ->where([
                    ['user_id', userId()],
                ])->whereIn('status', $request->status ?? ['waiting', 'finished', 'reserved', 'accepted', 'rejected'])
                ->select($columns)->orderBy('created_at', 'desc')->get();
            if ($request->expectsJson()) {
                return $clientRequests;
            } else {
                if ($request->exists('status')) {
                    return view('client.requests.list', ['reqs' => $clientRequests, 'type' => $request->status[0]]);

                } else {
                    return view('client.requests.index', ['reqs' => $clientRequests]);
                }
            }

        } else if (isDoctor()) {
            $request->validate([
                'status' => 'array|in:finished,accepted,waiting',
            ]);
            if ($request->exists('status')) {
                if ($request->status[0] == 'waiting') {
                    foreach ($this->newRequestsDr() as $req) {
                        $req->read_by_dr = 1;
                        $req->save();
                    }
                    if ($request->expectsJson())
                        return $this->newRequestsDr();
                    return view('doctor.requests.list', ['reqs' => $this->newRequestsDr(), 'type' => $request->status[0]]);
                }
            }
            $drRequests = UserRequest::
            with('Client:user_id,age')
                ->where([
                    ['dr_id', userId()],
                ])->whereIn('status', $request->status ?? ['finished', 'accepted'])
                ->select($columns)->orderBy('created_at', 'desc')->get();
            if ($request->expectsJson()) {
                return $drRequests;
            } else {
                if ($request->exists('status')) {
                    return view('doctor.requests.list', ['reqs' => $drRequests, 'type' => $request->status[0]]);

                } else {
                    return view('doctor.requests.index', ['reqs' => $drRequests, 'newReqs' => $this->newRequestsDr()]);
                }
            }
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.requests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isDoctor())
            return errorResp('شما نمیتوانید درخواست ثبت کنید.');

        $request->validate([
            'exp_id' => 'required|exists:expertises,id',
            'type' => 'required|in:lab,send_prescription,renew_prescription',
            'main_reason' => 'required|string|max:1500',
            'reserve_date' => 'nullable|date|after:today',
            'answer1' => ['nullable', 'string', 'max:800'],
            'answer2' => ['nullable', 'string', 'max:800'],
            'answer3' => ['nullable', 'string', 'max:800'],
        ]);
        $exp = Expertise::find($request->exp_id);
        if (!decreaseWallet($exp->cost)) {
            if ($request->expectsJson())
                return errorResp('موجودی شما برای درخواست کافی نمیباشد ابتدا کیف پول خود را شارژ نمایید.');

            flashMsg('ابتدا کیف پول خود را شارژ نمایید', 0);
            return redirect()->back();
        }
        $resereved = false;
        if ($request->exists('reserve_date') and $request->reserve_date) {
            $resereved = true;
            $request['status'] = 'reserved';
            $request['reserve_date'] = $request->reserve_date;
        } else
            $request['status'] = 'waiting';

        $request['user_id'] = userId();
        $request['exp_cost'] = $exp->cost;
        $userReq = UserRequest::create($request->only([
            'exp_id', 'exp_cost', 'status',
            'type', 'main_reason', 'user_id',
            'reserve_date', 'answer1', 'answer2', 'answer3'
        ]));
        //send notification to dr(s)
        if ($userReq->dr_id)
            $ids = [$userReq->Doctor->User->fcm_token];
        else
            $ids = Doctor::with('User')
                ->where('exp_id', $userReq->exp_id)
                ->get()
                ->pluck('User.fcm_token');
        $msg = 'درخواست جدیدی برای تخصص شما ثبت شده است.';
        sendNotification($msg, $ids);

        if ($request->expectsJson())
            return apiResp('درخواست ساخته شد.');
        flashMsg('درخواست با موفقیت ثبت شد.');
        if ($resereved)
            return redirect(route('reqs.index') . '?status[0]=reserved');
        return redirect(route('reqs.index') . '?status[0]=waiting');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param \App\UserRequest $req
     * @return UserRequest|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function show(Request $request, UserRequest $req)
    {
        $columns = ['id', 'user_id', 'exp_id', 'dr_id', 'type',
            'main_reason', 'dr_first_answer', 'final_diagnosis', 'reserve_date', 'reject_reason', 'status',
            're_visit', 'answer1', 'answer2', 'answer3', 'created_at'
        ];
        if (isClient() and $req->user_id == userId()) {
            $item = $req->with('Doctor:user_id,dr_code', 'Expertise:id,expertise_name,icon')
                ->select($columns)->findOrFail($req->id);
            if ($request->expectsJson())
                return $item;
            else {
                return view('client.requests.show', ['req' => $item]);
            }
        } else if (isDoctor()) {
            $item = $req->with('Client:user_id,age')
                ->select($columns)->findOrFail($req->id);
            if ($request->expectsJson())
                return $item;
            else {

                return view('doctor.requests.show', ['req' => $item, 'userHistory' => UserSickController::clientSicksDr($request, $item->Client->User)]);
            }
        } else
            if ($request->expectsJson())
                return errorResp('این درخواست متعق به شما نیست.');
            else {
                throw new NotFoundHttpException('این درخواست متعق به شما نیست.');
            }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param \App\UserRequest $req
     * @return void
     */
    public function edit(Request $request, UserRequest $req)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\UserRequest $req
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserRequest $req)
    {
        $request->validate([
            'type' => 'in:lab,send_prescription,renew_prescription',
            'main_reason' => 'string|max:500',
            'score' => 'in:1,2,3,4,5',
            'rateAnswer1' => 'boolean',
            'rateAnswer2' => 'boolean',
            'rateAnswer3' => 'boolean',
            'reserve_date' => 'date|after:today',
            'answer1' => ['string', 'max:800'],
            'answer2' => ['string', 'max:800'],
            'answer3' => ['string', 'max:800'],
        ]);

        if ($request->exists('score')) {
            if ($req->status == 'finished') {
                $req->score = $request->score;
                $req->save();
                RateAnswer::where('req_id', $req->id)->delete();
                RateAnswer::create([
                    'req_id' => $req->id,
                    'rateAnswer1' => $request->rateAnswer1 ?? 0,
                    'rateAnswer2' => $request->rateAnswer2 ?? 0,
                    'rateAnswer3' => $request->rateAnswer3 ?? 0,
                ]);
            } else
                return errorResp('درخواست هنوز بسته نشده است!');
        }
        if (($request->exists('type') or $request->exists('main_reason')) and $req->status != 'waiting')
            return errorResp('شما نمیتوانید این درخواست را ویرایش کنید!');
        $req->update($request->only(['type', 'main_reason', 'answer1', 'answer2', 'answer3', 'reserve_date']));
        if ($request->expectsJson())
            return apiResp('درخواست ویرایش شد.');
        else {
            flashMsg('امتیاز ثبت شد');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param \App\UserRequest $req
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request, UserRequest $req)
    {
        if ($req->status != 'waiting' or $req->user_id != userId() or isDoctor()) {
            return errorResp('شما نمیتوانید این درخواست را حذف کنید.');
        }
        $status = $req->status;
        increaseWallet($req->exp_cost);
        $req->delete();
        if ($request->expectsJson())
            return apiResp('درخواست حذف شد.');
        flashMsg('درخواست مورد نظر حذف شد.', 0);
        return redirect(route('reqs.index') . '?status[0]=' . $status);
    }

    // doctor api specific functions :

    public function newRequestsDr()
    {
        return UserRequest::
        with('Client:user_id,age')
            ->where([
                ['exp_id', drExp()],
                ['status', 'waiting'],
            ])->select(
                'id', 'user_id', 'type', 'read_by_dr',
                'main_reason', 'reserve_date', 'status', 'created_at')->orderBy('created_at', 'desc')->get();

    }

    public function acceptRequestDr(Request $request, UserRequest $req)
    {
        $request->validate([
            'first_answer' => 'required|string|max:1000'
        ]);
        if (UserRequest::where([
            'user_id' => $req->user_id,
            'dr_id' => userId(),
            'status' => 'accepted',
        ])->exists()) {
            $msg = 'شما در حال حاظر یک درخواست جاری از این کابر دارید.';
            if ($request->expectsJson())
                return errorResp($msg);
            flashMsg($msg, 0);
            return redirect()->back();
        }

        if ($req->status != 'waiting' or $req->exp_id != drExp()) {

            $msg = 'شما نمیتوانید این درخواست را قبول کنید.';
            if ($request->expectsJson())
                return errorResp($msg);
            flashMsg($msg, 0);
            return redirect()->back();
        }

        $req->dr_id = userId();
        $req->dr_first_answer = $request->first_answer;
        $req->status = 'accepted';
        $req->save();


        ChatMessage::create([
            'sender_id' => userId(),
            'req_id' => $req->id,
            'sender_type' => 'doctor',
            'msg' => $request->first_answer,
            'msg_type' => 'text',
        ]);

        // notification to client or send message
        $msg = 'درخواست کاربر توسط شما قبول شد.';
        if ($request->expectsJson())
            return apiResp($msg);
        flashMsg($msg, 1);
        return redirect()->back();
    }

    public function closeRequestDr(Request $request, UserRequest $req)
    {
        $request->validate([
            'final_diagnosis' => 'required|string|max:1000',
            'add_history' => 'boolean',
            're_visit' => 'nullable|string|max:500',
        ]);
        if ($req->status != 'accepted' or $req->dr_id != userId()) {
            $msg = 'شما نمیتوانید این درخواست را ببندید.';
            if ($request->expectsJson())
                return errorResp($msg);
            flashMsg($msg, 0);
            return redirect()->back();

        }

        $req->dr_id = userId();
        $req->final_diagnosis = $request->final_diagnosis;
        $req->re_visit = $request->re_visit ?? null;
        $req->status = 'finished';
        $req->viewer_checked = 0;
        $req->finished_at = now();
        $req->save();

//        increaseWallet($req->Expertise->cost);

        if ($request->add_history)
            UserSick::create([
                'user_id' => $req->user_id,
                'sick' => $req->main_reason,
                'desc' => $request->final_diagnosis . ' - توسط: ' . $req->Doctor->User->fname . ' ' . $req->Doctor->User->lname,
                'added_by' => 'dr',
            ]);
        $msg = 'درخواست کاربر توسط شما بسته شد.';
        if ($request->expectsJson())
            return apiResp($msg);
        flashMsg($msg);
        return redirect()->back();
    }

    public function rejectRequestDr(Request $request, UserRequest $req)
    {
        $request->validate([
            'reject_reason' => 'in:presence,emergency,other_exp'

        ]);
        if ($req->status == 'finished')
            return errorResp('شما نمیتوانید این درخواست را رد کنید.');

        $req->reject_reason = $request->reject_reason;
        $req->status = 'rejected';
        $req->save();

        $msg = 'درخواست کاربر توسط شما رد شد.';

        if ($request->expectsJson())
            return apiResp($msg);
        flashMsg('درخواست کاربر توسط شما رد شد.', 'alert-warning');
        return redirect()->route('reqs.index');
    }

    public function costBack(Request $request)
    {
        if (isNotAdmin()) {
            throw new NotFoundHttpException();
        }
        $request->validate([
            'req_id' => 'required|exists:user_requests,id',
            'who' => 'required|in:client,doctor',
        ]);

        $req = UserRequest::where('id', $request->req_id)->firstOrFail();
        $client = User::where('id', $req->Client->user_id)->firstOrFail();
        $doctor = User::where('id', $req->Doctor->user_id)->firstOrFail();
        if ($request->who == 'client') {
            increaseWallet($req->exp_cost, $client);
            $req->cost_back_client = 1;
        } else {
//            if (!decreaseWallet($req->exp_cost, $doctor)) {
//                flashMsg('کیف پول متخصص شارژ کافی ندارد', 0);
//                return redirect()->back();
//            }
            increaseWallet($req->exp_cost, $doctor);
            $req->cost_back_doctor = 1;
            $req->viewer_checked = 1;
        }
        $req->save();
        flashMsg('عملیات با موفقیت انجام شد', 0);
        return redirect()->back();
    }
}
