<?php

namespace App\Http\Controllers;

use App\TransAction;
use App\UserRequest;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    public function wallet(Request $request)
    {
        if ($request->expectsJson())
            return ['wallet' => Auth::user()->{userRole()}->wallet];


    }

    public function increaseIndex()
    {
        return view('client.wallet.index');
    }

    public function increase(Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric|min:100|max:10000000'
        ]);

        $transAction = new TransAction();
        $transAction->user_id = userId();
        $transAction->factorId = uniqid(userId());
        $transAction->amount = $request->amount;
        $transAction->description = 'افزایش اعتبار';
        $transAction->status = 0;
        $transAction->comeFrom = $request->expectsJson() ? 'app' : 'web';
        $transAction->save();

        $pay_link = route('pay',
            [
                'amount' => $request->amount,
                'factorNumber' => $transAction->factorId,
            ]);

        if ($request->expectsJson())
            return apiResp($pay_link);
        return redirect($pay_link);
    }

    public function income(Request $request)
    {
        if ($request->expectsJson()) {
            $diff = env('DAYS_BEFORE_INCOME');
            $days = datesBeforeArray(Carbon::today()->subDays($diff)->toDateString(), $diff, false);
            $result = [];
            foreach ($days as $day) {
                $items = UserRequest::where([
                    'dr_id' => userId(),
                    'status' => 'finished',
                ])
                    ->whereDate('finished_at', $day)
                    ->get();
                $sum = $items->sum('exp_cost');
                $count = $items->count();
                if ($count)
                    array_push($result, array('date' => $day, 'sum' => $sum, 'count' => $count));
            }
            return $result;
        }
        if ($request->input('start_date') and $request->input('end_date')) {
            $start_date = $request->input('start_date');
            $end_date = $request->input('end_date');
            $incomes = UserRequest::with('Client:user_id')
                ->where([
                    'dr_id' => userId(),
                    'status' => 'finished',
                ])
                ->whereDate('finished_at', '>=', $start_date)
                ->whereDate('finished_at', '<=', $end_date)
                ->select('exp_cost', 'user_id', 'type', 'finished_at', 'main_reason')
                ->orderBy('id', 'desc')->simplePaginate(300);
            return view('doctor.income.index', ['start_date' => $start_date, 'end_date' => $end_date])->with('incomes', $incomes);

        } else
            $incomes = UserRequest::with('Client:user_id')
                ->where([
                    'dr_id' => userId(),
                    'status' => 'finished',
                ])->select('exp_cost', 'user_id', 'type', 'finished_at', 'main_reason')
                ->orderBy('id', 'desc')->simplePaginate(300);
        return view('doctor.income.index')->with('incomes', $incomes);
    }
}
