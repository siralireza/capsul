<?php

namespace App\Http\Controllers\Auth;

use App\Client;
use App\Doctor;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use SoapClient;
use SoapFault;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        return view('auth.forgotPass');
    }

    public function recoverPassSms(Request $request)
    {
        $request->validate([
            'phone' => 'required|digits:11|numeric',
        ]);
        if (Client::where('phone', $request->phone)->exists()) {
            $client = Client::where('phone', $request->phone)->firstOrFail();
            $user = $client->User;
        } elseif (Doctor::where('phone', $request->phone)->exists()) {
            $doctor = Doctor::where('phone', $request->phone)->firstOrFail();
            $user = $doctor->User;
        } else {
            $msg = 'شماره مورد نظر یافت نشد لطفا شماره وارد شده را بررسی کنید';
            if ($request->expectsJson())
                return errorResp($msg);
            flashMsg($msg, 0);
            return redirect()->back();
        }
        $phone = $request->phone;
//        $phone = userPhone($user);
//        if (!$phone) {
//            $msg = 'شماره برای این اکانت ثبت نشده است لطفا با پشتیبانی تماس بگیرید.';
//            if ($request->expectsJson())
//                return errorResp($msg);
//            flashMsg($msg, 0);
//            return redirect()->back();
//        }
        $newPassword = generateRandomString(8);
//        $msg = recoverText($user->username, $newPassword);
        $user->recovery_password = Hash::make($newPassword);
        $user->save();

        if (!verifySms($phone, $user->username, $newPassword)) {
            $msg = 'مشکلی در ارسال پیام به شماره مورد نظر وجود دارد';
            if ($request->expectsJson())
                return errorResp($msg);
            flashMsg($msg, 0);
            return redirect()->back();
        }
        $msg = 'رمز عبور بازیابی برای شما ارسال شد، لطفا چند دقیقه منتظر بمانید';
        if ($request->expectsJson())
            return apiResp($msg);
        flashMsg($msg);
        return redirect()->route('login');
    }
}
