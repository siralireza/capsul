<?php

namespace App\Http\Controllers\Auth;

use App\Client;
use App\Doctor;
use App\Http\Controllers\Controller;
use App\InitHistory;
use App\User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout', 'current_user');
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string|exists:users,username',
            'password' => 'required|string',
            'fcm_token' => 'string|max:257',
        ]);
    }

    protected function attemptLogin(Request $request)
    {
        // check for recovery password
        $user = User::where('username', $request->username)->firstOrFail();
        if (Hash::check($request->password, $user->recovery_password)) {
            Auth::login($user);
            return true;
        }
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    protected function sendLoginResponse(Request $request)
    {
        if (!$request->expectsJson())
            $request->session()->regenerate();
        $this->clearLoginAttempts($request);
        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }

    protected function authenticated(Request $request, $user)
    {
        // if first login and created by admin this lines prevent from error
        if ($user->isClient()) {
            if (!$user->Client)
                Client::create([
                    'user_id' => $user->id
                ]);
            if (!($user->Client->InitHistory ?? false))
                InitHistory::create([
                    'user_id' => $user->id,
                ]);
            if ($user->status == 'non_verified') {
                $user->status = 'active';
                $user->save();
            }

        } elseif ($user->isDoctor()) {
            if (!$user->Doctor) {
                Doctor::create([
                    'user_id' => $user->id,
                ]);
            }
            if (!($user->Doctor->exp_id ?? false)) {
                Auth::logout();
                $msg = 'تخصص شما مشخص نشده است لطفا با پشتیبانی تماس بگیرید.';
                if ($request->expectsJson())
                    return errorResp($msg);
                flashMsg($msg, 0);
                return redirect()->route('login');
            }
            if ($user->status == 'non_verified') {
                Auth::logout();
                $msg = 'حساب شما تایید نشده است لطفا با پشتیبانی تماس بگیرید.';
                if ($request->expectsJson())
                    return errorResp($msg);
                flashMsg($msg, 0);
                return redirect()->route('login');
            }
        }
        // fills the fcm_token for notifications
        if ($request->exists('fcm_token')) {
            $user->fcm_token = $request->fcm_token;
        }
        // can comment this line later just this not save
        $user->api_token = Str::random(80);
        $user->save();

        if ($request->expectsJson()) {
            return Response()->json([
                'message' => 'شما با موفقیت وارد شدید.',
                'token' => $user->api_token,
            ]);
        } else {
            return redirect('panel');
        }
    }

    public function username()
    {
        return 'username';
    }

    public function current_user(Request $request)
    {
        if (userRole() == 'client') {
            if ($request->expectsJson())
                return User::with('Client', 'Client.InitHistory')->where('id', userId())->get();
            return view('client.profile');
        } elseif (userRole() == 'doctor') {
            if ($request->expectsJson())
                return User::with('Doctor')->where('id', userId())->get();
            return view('doctor.profile');

        } else
            if ($request->expectsJson())
                return Auth::user();
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }

}
