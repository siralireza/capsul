<?php

namespace App\Http\Controllers\Auth;

use App\Client;
use App\Doctor;
use App\InitHistory;
use App\User;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Storage;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('update');
    }


    public function showRegistrationFormClient()
    {
        return view('auth.registerClient');
    }

    public function showRegistrationFormDoctor()
    {
        return view('auth.registerDoctor');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'min:4', 'max:255', 'unique:users'],
            'password' => ['required_if:register_type,doctor', 'string', 'min:8', 'confirmed'],
            'phone' => ['digits:11', 'unique:doctors', 'unique:clients'],
            'email' => ['nullable', 'email', 'unique:users'],
            'age' => ['numeric', 'min:1', 'max:150'],
            'sex' => ['required', 'in:male,female'],
            'register_type' => ['required', 'in:doctor,client'],
            // client
            'answer1' => ['nullable', 'string', 'max:800'],
            'answer2' => ['nullable', 'string', 'max:800'],
            'answer3' => ['nullable', 'string', 'max:800'],
            'answer4' => ['nullable', 'string', 'max:800'],
            'answer5' => ['nullable', 'string', 'max:800'],
            // doctor
            'exp_id' => ['required_if:register_type,doctor', 'exists:expertises,id'],
            'dr_code' => ['required_if:register_type,doctor', 'numeric', 'digits_between:3,10', 'unique:doctors'],
            'work_experience' => ['required_if:register_type,doctor', 'numeric', 'min:1', 'max:100'],
            'sheba' => ['numeric', 'digits_between:10,40'],
            'file' => ['nullable', 'max:10000', 'mimes:jpeg,jpg,png,7z,rar,tar.gz,zip,pdf,txt'],
        ]);

    }

    public function validatePhone(Request $request)
    {
        $password = generateRandomString(8);
        $request['password'] = $password;
//        $msg = registerText($request->username, $password);
        if (!verifySms($request->phone, $request->username, $password)) {
            return false;
        }
        return true;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        if ($request->register_type == 'client') {
            $result = $this->validatePhone($request);

            if (!$result) {
                $msg = smsProblemText();
                if ($request->expectsJson())
                    return errorResp($msg);
                flashMsg($msg, 0);
                return redirect()->back();
            }

        }

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param Request $request
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if ($data['register_type'] == 'client') {

            $user = User::create([
                'fname' => $data['fname'],
                'lname' => $data['lname'],
                'username' => $data['username'],
                'sex' => $data['sex'],
                'capsul_role' => 'client',
                'role_id' => userRoleId(),
                'email' => $data['email'] ?? null,
                'status' => 'active',
                'password' => Hash::make($data['password']),
                'api_token' => Str::random(80),
            ]);

            Client::create([
                'user_id' => $user->id,
                'name' => $user->fname . ' ' . $user->lname,
                'phone' => $data['phone'] ?? null,
                'age' => $data['age'] ?? null,

            ]);
            InitHistory::create([
                'user_id' => $user->id,
                'answer1' => $data['answer1'] ?? null,
                'answer2' => $data['answer2'] ?? null,
                'answer3' => $data['answer3'] ?? null,
                'answer4' => $data['answer4'] ?? null,
                'answer5' => $data['answer5'] ?? null,
            ]);
        } else {
            $file = $data['file'] ?? null;
            if ($file)
                $file_path = $file->storeAs('/' . 'documents', 'doc' . '-' . time() . '-' . $file->hashName(), ['disk' => 'public']);

            $user = User::create([
                'fname' => $data['fname'],
                'lname' => $data['lname'],
                'username' => $data['username'],
                'sex' => $data['sex'],
                'capsul_role' => 'doctor',
                'role_id' => userRoleId(),
                'email' => $data['email'] ?? null,
                'password' => Hash::make($data['password']),
                'api_token' => Str::random(80),
            ]);
            Doctor::create([
                'user_id' => $user->id,
                'name' => $user->fname . ' ' . $user->lname,
                'phone' => $data['phone'] ?? null,
                'exp_id' => $data['exp_id'],
                'dr_code' => $data['dr_code'],
                'work_experience' => $data['work_experience'],
                'sheba' => $data['sheba'] ?? null,
                'documents_path' => $file_path ?? null,
            ]);

        }
        return $user;

    }

    protected function registered(Request $request, $user)
    {
        if ($user->{roleColName()} == 'client') {
            $msg = 'ثبت نام با موفقیت انجام شد، کلمه عبور و نام کابری برای شما ارسال شد' . "\n" .
                "در صورت عدم دریافت پس از ۵ دقیقه میتوانید از بخش فراموشی رمز عبور اقدام نمایید.";
        } else {
            $msg = 'ثبت نام با موفقیت انجام شد.';
        }
        if ($request->expectsJson()) {
            return apiResp($msg);
        }
        Auth::logout();
        flashMsg($msg);
        return redirect('login');
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'fcm_token' => 'string|max:257',
        ]);
        if ($request->exists('fcm_token')) {
            $user->fcm_token = $request->fcm_token;
            $user->save();
        }
        if (isClient()) {
            $request->validate([
                'fname' => ['nullable', 'string', 'max:255'],
                'lname' => ['nullable', 'string', 'max:255'],
                'pass' => ['nullable', 'required_with:password', 'string', 'min:8'],
                'password' => ['nullable', 'string', 'max:100', 'confirmed'],
                'phone' => ['nullable', 'digits:11', 'unique:doctors'],
                'email' => ['nullable', 'email'],
                'age' => ['nullable', 'numeric', 'min:1', 'max:150'],
                'sex' => ['nullable', 'in:male,female'],
                'pic' => ['nullable', 'image', 'max:3000'],
                // initial histories
                'answer1' => ['nullable', 'string', 'max:800'],
                'answer2' => ['nullable', 'string', 'max:800'],
                'answer3' => ['nullable', 'string', 'max:800'],
                'answer4' => ['nullable', 'string', 'max:800'],
                'answer5' => ['nullable', 'string', 'max:800'],
            ]);

            $pic = $request->file('pic');
            if ($pic) {
                if (!$request->expectsJson()) {

                    $img = Image::make($pic->getRealPath())->resize(300, 300);
                    $img->save();
                }
                $pic_path = $pic->storeAs('pic', 'pic-' . time() . '-' . $request->file('pic')->hashName(), ['disk' => 'public']);
                Storage::disk('public')->delete($user->getOriginal('pic'));

            }

            if ($request->exists('password') and $request->password and !(Hash::check($request->pass, Auth::user()->password) or Hash::check($request->pass, Auth::user()->recovery_password))) {
                $error = 'کلمه عبور فعلی اشتباه است';
                if ($request->expectsJson())
                    return errorResp($error);
                flashMsg($error, 0);
                return redirect()->back();
            }

            $user->update(array_filter($request->only('fname', 'lname', 'sex', 'email')));

            if ($request->password) {
                $user->recovery_password = null;
                $user->save();
            }

            $user->update([
                'password' => $request->password ? Hash::make($request->password) : $user->password,
                'pic' => $pic_path ?? null,
            ]);
            $user->Client->update(array_filter($request->only('age', 'phone')));
            $user->Client->InitHistory->update(array_filter($request->only([
                'answer1',
                'answer2',
                'answer3',
                'answer4',
                'answer5',
            ])));
            $msg = 'ویرایش اطلاعات انجام شد.';
            if ($request->expectsJson())
                return apiResp($msg);
            flashMsg($msg);
            return redirect()->back();
        } elseif (isDoctor()) {
            $request->validate([
                'fname' => ['nullable', 'string', 'max:255'],
                'lname' => ['nullable', 'string', 'max:255'],
                'pass' => ['nullable', 'required_with:password', 'string', 'min:8'],
                'password' => ['nullable', 'string', 'max:100', 'confirmed'],
                'phone' => ['nullable', 'digits:11', 'unique:doctors'],
                'sex' => ['nullable', 'in:male,female'],
                'email' => ['nullable', 'email'],
                'work_experience' => ['nullable', 'max:100'],
                'sheba' => ['nullable', 'digits_between:10,40'],
                'dr_code' => ['numeric', 'digits_between:3,10', 'unique:doctors'],
                'pic' => ['nullable', 'image', 'max:3000'],
                'file' => ['nullable', 'max:10000', 'mimes:jpeg,jpg,png,7z,rar,tar.gz,zip,pdf,txt'],
            ]);
            if ($request->exists('password') and $request->password and !(Hash::check($request->pass, Auth::user()->password) or Hash::check($request->pass, Auth::user()->recovery_password))) {
                $error = 'کلمه عبور فعلی اشتباه است';
                if ($request->expectsJson())
                    return errorResp($error);
                flashMsg($error, 0);
                return redirect()->back();
            }

            $pic = $request->file('pic');
            if ($pic) {
                if (!$request->expectsJson()) {

                    $img = Image::make($pic->getRealPath())->resize(300, 300);
                    $img->save();
                }
                $pic_path = $pic->storeAs('pic', 'pic-' . time() . '-' . $request->file('pic')->hashName(), ['disk' => 'public']);
                Storage::disk('public')->delete($user->getOriginal('pic'));

            }
            $file = $request->file('file');
            if ($file) {
                $file_path = $file->storeAs('/' . 'documents', 'doc' . '-' . time() . '-' . $file->hashName(), ['disk' => 'public']);
                $request['documents_path'] = $file_path;
                Storage::disk('public')->delete($user->Doctor->getOriginal('documents_path'));
            }
            $user->update(array_filter($request->only('fname', 'lname', 'sex', 'email')));
            if ($request->password) {
                $user->recovery_password = null;
                $user->save();
            }
            $user->update([
                'password' => $request->password ? Hash::make($request->password) : $user->password,
                'pic' => $pic_path ?? null,
            ]);
            $user->Doctor->update(array_filter($request->only('phone', 'documents_path', 'work_experience', 'dr_code', 'sheba')));
        }
        $msg = 'ویرایش اطلاعات انجام شد.';
        if ($request->expectsJson())
            return apiResp($msg);
        flashMsg($msg);
        return redirect()->back();
    }
}
