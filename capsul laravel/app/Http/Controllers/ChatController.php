<?php

namespace App\Http\Controllers;

use App\ChatMessage;
use App\Jobs\SendNotificationJob;
use App\UserRequest;
use File;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return UserRequest[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function index(Request $request, $needCollection = null)
    {

        if (isClient()) {
            // try to order by last messages
            $reqChats = UserRequest::with('Doctor:user_id,dr_code', 'Expertise:id,expertise_name')
                ->where([
                    'user_id' => userId(),
                    'status' => 'accepted',
                ])
                ->join('chat_messages', 'chat_messages.req_id', '=', 'user_requests.id')
                ->orderBy('chat_messages.updated_at', 'desc')
                ->select('user_requests.id', 'user_requests.main_reason', 'user_requests.exp_id', 'user_requests.dr_id')
                ->get()->unique();
            if ($request->expectsJson() or $needCollection)
                return $reqChats;
            return view('client.chats.index', ['reqChats' => $reqChats]);

        }

        if (isDoctor()) {
            $reqChats = UserRequest::with('Client:user_id')
                ->where([
                    'dr_id' => userId(),
                    'status' => 'accepted',
                ])
                ->join('chat_messages', 'chat_messages.req_id', '=', 'user_requests.id')
                ->orderBy('chat_messages.updated_at', 'desc')
                ->select('user_requests.id', 'main_reason', 'user_requests.exp_id', 'user_requests.user_id')
                ->get()->unique();
            if ($request->expectsJson() or $needCollection)
                return $reqChats;
            if (isClient())
                return view('client.chats.index', ['reqChats' => $reqChats]);
            if (isDoctor())
                return view('doctor.chats.index', ['reqChats' => $reqChats]);

        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return ChatMessage|ChatMessage[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public
    function store(Request $request)
    {

        $request->validate([
            'req_id' => 'required|exists:user_requests,id',
            'msg' => 'nullable|string|max:1000',
            'need_response' => 'boolean',
            'file' => 'nullable|mimes:bin,jpg,jpeg,png,mpga,wav,audio/mp3,audio/mpeg3,audio/mpeg,mpeg,ogg,mp3,7z,rar,zip,pdf,txt|max:10000'
        ]);
        $req = UserRequest::find($request->req_id);
        // check request is open and accepted by doctor
        if ($req->status != 'accepted')
            if ($request->expectsJson())
                return errorResp('شما برای این درخواست نمیتوانید پیامی ارسال کنید.');
            else
                throw new NotFoundHttpException();
        // check the sender exists in the request or not
        if (isClient()) {
            if ($req->user_id != userId())
                if ($request->expectsJson())
                    return errorResp('شما اجازه‌ی ارسال پیام به این درخواست را ندارید.');
                else
                    throw new NotFoundHttpException();
        } elseif (isDoctor()) {
            if ($req->dr_id != userId())
                if ($request->expectsJson())
                    return errorResp('شما اجازه‌ی ارسال پیام به این درخواست را ندارید.');
                else
                    throw new NotFoundHttpException();
        }
        // handle input files
        $file = $request->file('file');

        if (!$file and !$request->msg) {
            if ($request->expectsJson())
                return errorResp('فیلد پیام و فایل نمتوانند باهم خالی باشند.');
            else
                return redirect()->back();
        } elseif ($file) {
            $ext = $file->extension();
            $file_type = getFileType($ext);
            if ($file->getMimeType() == 'application/octet-stream') {
                $base = File::get($file);
                $filename = str_replace('.bin', '.mp3', $file->hashName());
                header('Content-Type: audio/mpeg; charset=utf-8');
                header("Content-length: " . filesize($file));
                header("Cache-Control: no-cache");
                header("Content-Transfer-Encoding: binary");
                $file = fopen('../storage/app/public/voice/' . $filename, 'wb');
                fwrite($file, $base);
                fclose($file);
                $file_path = 'voice/' . $filename;
                $file_type = 'audio';
                shell_exec('cd ../storage/app/public/voice/ && mv '.$filename.' x.mp3 && ffmpeg -y -i x.mp3 '.$filename.' && rm x.mp3');
            } else
                $file_path = $file->storeAs('/' . $file_type, $file_type . '-' . time() . '-' . $file->hashName(), ['disk' => 'public']);
        } else {
            $file_type = null;
        }
        $message = ChatMessage::create([
            'sender_id' => userId(),
            'req_id' => $request->req_id,
            'sender_type' => userRole(),
            'msg' => $request->msg,
            'msg_type' => $file_type ?: 'text',
            'path' => $file_path ?? null,
        ]);
        SendNotificationJob::dispatch($request->msg, [isClient() ? $req->user_id : $req->dr_id]);
        $msg = 'پیام با موفقیت ثبت شد.';
        if ($request->expectsJson())
            if ($request->need_response)
                return $this->showMessage($message->id);
            else
                return apiResp($msg);

//        flashMsg($msg, 1);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\Pagination\Paginator
     */
    public
    function show(Request $request, $id)
    {
        $req = UserRequest::findOrFail($id);
        if ($req->user_id == userId() or $req->dr_id == userId() or isAdmin()) {
            $chats = ChatMessage::with('Sender')->where('req_id', $id)->orderBy('created_at', 'desc')->paginate(20);
            foreach ($chats as $ch) {
                if ($ch->sender_id != userId() and in_array(userRole(), ['client', 'doctor'])) {
                    $ch->read_status = 1;
                    $ch->save();
                }
            }
            if ($request->expectsJson())
                return $chats;
            if ($chats->isEmpty())
                throw new NotFoundHttpException();
            if (isClient())
                return view('client.chats.show', ['chats' => $chats, 'req' => $req]);
            elseif (isDoctor())
                return view('doctor.chats.show', ['chats' => $chats, 'req' => $req]);
            else
                return view('admin.chats.show', ['chats' => $chats, 'req' => $req]);
        } else {
            $msg = 'شما اجازه‌ی مشاهده‌ی چت های این درخواست را ندارید';
            if ($request->expectsJson())

                return errorResp($msg);
            flashMsg($msg, 0);
            return redirect(route('chats.index'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return ChatMessage|ChatMessage[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public
    function showMessage($id)
    {
        return ChatMessage::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        //
    }
}
