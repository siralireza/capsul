<?php

namespace App\Http\Controllers;

use App\Complaint;
use App\Http\Resources\PublicJsonResource;
use App\Support;
use App\UserRequest;
use Illuminate\Http\Request;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Complaint[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        if (isClient())
            $complaints = Complaint::with('Doctor:user_id,dr_code')
                ->where('user_id', userId())->get();
        if (isDoctor())
            $complaints = Support::with('Doctor:user_id,dr_code')
                ->where('user_id', userId())->get();
        if ($request->expectsJson())
            return $complaints;
        if (isClient())
            return view('client.complaints.index', ['complaints' => $complaints]);
        if (isDoctor())
            return view('doctor.complaints.index', ['complaints' => $complaints]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (isClient()) {
            $doctors = $this->doctors($request);
            if ($doctors->isEmpty()) {
                flashMsg('شما هیچ درخواستی با متخصصان ما نداشته‌اید بنابراین امکان ثبت پیشنهاد برای شما وجود ندارد.', 0);
                return redirect()->back();
            }
            return view('client.complaints.create', ['reqDoctors' => $doctors]);
        }
        if (isDoctor())
            return view('doctor.complaints.create', ['reqDoctors' => $this->doctors($request)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isDoctor()) {
            $request->validate([
                'title' => 'required|string|max:100',
                'desc' => 'required|string|max:500',
            ]);
            $request['user_id'] = userId();
            Support::create($request->only('title', 'desc', 'user_id'));
        }
        if (isClient()) {
            $request->validate([
                'dr_id' => 'required|exists:doctors,user_id',
                'title' => 'required|string|max:100',
                'desc' => 'required|string|max:500',
            ]);
            if (array_key_exists($request->dr_id, $this->doctors($request, true)))
                return errorResp('درخواستی از شما که توسط دکتر مورد نظر قبول شده باشد یافت نشد.');
            $request['user_id'] = userId();
            Complaint::create($request->only('dr_id', 'title', 'desc', 'user_id'));
        }
        $msg = 'با موفقیت ثبت شد.';
        if ($request->expectsJson())
            return apiResp($msg);
        flashMsg($msg);
        return redirect(route('complaints.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Complaint $complaint
     * @return Complaint|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function show(Complaint $complaint)
    {
//        return $complaint->with('Doctor:user_id,dr_code')
//            ->where('user_id', userId())->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Complaint $complaint
     * @return \Illuminate\Http\Response
     */
    public function edit(Complaint $complaint)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Complaint $complaint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Complaint $complaint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param \App\Complaint $complaint
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request, Complaint $complaint)
    {
//        $complaint->delete();
//        $msg = 'شکایت حذف شد.';
//        if ($request->expectsJson())
//            return apiResp($msg);
//        flashMsg('شکایت حذف شد.', 0);
//        return redirect()->back();
    }

    public function doctors(Request $request, $array_response = false)
    {
        $doctors = UserRequest::where('user_id', userId())
            ->whereIn('status', ['accepted', 'finished']);
        if ($array_response)
            return $doctors->pluck('dr_id')->unique();
        else if ($request->expectsJson()) {
            $newDr = $doctors->select('dr_id')->with('Doctor:user_id')->get()->unique('user_id');
            $newDr->each(function ($dr) {
                $dr->setAppends([]);
            });
            return $newDr;
        }
        return $doctors->with('Doctor')->get()->unique('user_id');
    }
}
