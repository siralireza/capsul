<?php

namespace App\Http\Controllers;

use App\InitHistory;
use App\User;
use App\UserSick;
use Illuminate\Http\Request;

class UserSickController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $sicks = UserSick::where('user_id', userId())->get();
        if ($request->expectsJson())
            return $sicks;
        return view('client.sicks.index', compact('sicks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.sicks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'sick' => 'required|string',
            'desc' => 'required|string|max:500',
        ]);

        $request['user_id'] = userId();
        UserSick::create($request->only('sick', 'desc', 'user_id'));
        $msg = 'سابقه پزشکی کاربر ساخته شد.';

        if ($request->expectsJson())
            return apiResp($msg);
        flashMsg($msg);
        return redirect()->route('sicks.index');
    }

    public function storeOld(Request $request)
    {
        $request->validate([
            'sick_id' => 'required|exists:sicks,id',
            'desc' => 'required|string|max:500',
        ]);

        $request['user_id'] = userId();
        UserSick::create($request->only('sick_id', 'desc', 'user_id'));

        $msg = 'سابقه پزشکی کاربر ساخته شد.';
        if ($request->expectsJson())
            return apiResp($msg);
        flashMsg($msg);
        return redirect()->route('sicks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\UserSick $sick
     * @return \Illuminate\Http\Response
     */
    public function show(UserSick $sick)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param \App\UserSick $sick
     * @return void
     */
    public function edit(Request $request, UserSick $sick)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\UserSick $sick
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserSick $sick)
    {
        $request->validate([
            'sick' => 'required|string',
            'desc' => 'string|max:500',
        ]);
        if ($sick->user_id != userId() or $sick->added_by == 'dr')
            return errorResp('شما نمتوانید این سابقه را ویرایش نمایید.');
        $sick->update($request->only('sick', 'desc'));
        if ($request->expectsJson())
            return apiResp('سابقه پزشکی کاربر ویرایش شد.');
    }

    public function updateOld(Request $request, UserSick $sick)
    {
        $request->validate([
            'sick_id' => 'exists:sicks,id',
            'desc' => 'string|max:500',
        ]);
        if ($sick->user_id != userId())
            return errorResp('شما نمتوانید این سابقه را ویرایش نمایید.');
        $sick->update($request->only('sick_id', 'desc'));
        if ($request->expectsJson())
            return apiResp('سابقه پزشکی کاربر ویرایش شد.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param \App\UserSick $sick
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request, UserSick $sick)
    {
        if ($sick->user_id != userId())
            return errorResp('شما نمتوانید این سابقه را ویرایش نمایید.');
        $sick->delete();
        $msg = 'سابقه پزشکی کاربر حذف شد.';
        if ($request->expectsJson())
            return apiResp($msg);
        flashMsg($msg, 0);
        return redirect()->route('sicks.index');
    }

    // doctor specific functions

    public static function clientSicksDr(Request $request, User $user)
    {
        $sicks = UserSick::where('user_id', $user->id)->get();
        $init_history = InitHistory::where('user_id', $user->id)->get();
        $collection = collect($sicks);
        $result = $collection->merge($init_history);
        return $result->all();
    }

}
