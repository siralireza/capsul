<?php

namespace App\Http\Controllers;

use App\Expertise;
use App\Http\Resources\PublicJsonResource;
use App\Sick;
use Illuminate\Http\Request;

class ExpertiseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Expertise[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        if ($request->expectsJson())
            return Expertise::orderBy('sort_col')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param \App\Expertise $expertise
     * @return Expertise
     */
    public function show(Request $request, Expertise $expertise)
    {
        if ($request->expectsJson())
            return $expertise;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Expertise $expertise
     * @return \Illuminate\Http\Response
     */
    public function edit(Expertise $expertise)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Expertise $expertise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Expertise $expertise)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Expertise $expertise
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expertise $expertise)
    {
        //
    }
}
