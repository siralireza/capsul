<?php

namespace App\Http\Controllers;

use App\TransAction;
use Illuminate\Http\Request;
use SaeedVaziry\Payir\Exceptions\SendException;
use SaeedVaziry\Payir\Exceptions\VerifyException;
use SaeedVaziry\Payir\PayirPG;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class PaymentController extends Controller
{
    public function pay(Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric|max:1000000|min:100',
            'factorNumber' => 'required|exists:trans_actions,factorId',
            'mobile' => 'numeric|digits:11',
        ]);
        $factor = TransAction::where('factorId', $request->factorNumber)->first();
        if ($factor->status == 1) {
            $msg = 'این فاکتور قبلا پرداخت شده است';
            return view('pay.callback')->with('message', $msg);
        }
        $payir = new PayirPG();
        $payir->amount = $request->amount*10; // Required, Amount
        $payir->factorNumber = $request->factorNumber; // Optional
        $payir->description = $request->description; // Optional
        $payir->mobile = $request->mobile; // Optional, If you want to show user's saved card numbers in gateway

        try {
            $payir->send();
            return redirect($payir->paymentUrl);
        } catch
        (SendException $e) {
            flashMsg($e, 0);
            return redirect()->back();
        }
    }

    public function verify(Request $request)
    {
        $payir = new PayirPG();
        $payir->token = $request->token; // Pay.ir returns this token to your redirect url

        try {
            $verify = $payir->verify(); // returns verify result from pay.ir like (transId, cardNumber, ...)
            $transAction = TransAction::where('factorId', $verify['factorNumber'])->firstOrFail();
            if ($transAction->status == 1)
                throw new VerifyException('این فاکتور قبلا تایید شده است');
            $transAction->transId = $verify['transId'] ?? '';
            $transAction->traceNumber = $verify['traceNumber'] ?? '';
            $transAction->cardNumber = $verify['cardNumber'] ?? '';
            $transAction->messageReceived = $verify['message'] ?? '';
            $transAction->status = $verify['status'] ?? '';
            $transAction->save();

            $role = $transAction->User->{roleColName()};
            $transAction->User->{$role}->wallet += $transAction->amount;
            $transAction->User->{$role}->save();

            $msg = 'پرداخت با موفقیت انجام شد';
            return view('pay.callback')->with('transAction', $transAction)->with('message', $msg);

        } catch (VerifyException $e) {

            $msg = $e->getMessage();
            return view('pay.callback')->with('message', $msg);
        }
    }
}
