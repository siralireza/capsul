<?php

namespace App\Http\Controllers;

use App\CapsulSetting;
use Carbon\Carbon;

class PanelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (isClient()) {
            // redirect to requests page
            return redirect(route('reqs.index'));
        } elseif (isDoctor()) {
            // redirect to requests page
            return redirect(route('reqs.index'));
        } else
            return redirect()->route('voyager.dashboard');
    }
}
