<?php

namespace App\Http\Controllers;

use App\Http\Resources\PublicJsonResource;
use App\Sick;
use Illuminate\Http\Request;

class SickController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Sick[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        if ($request->expectsJson()) {
            return Sick::all();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Sick $sick
     * @return \Illuminate\Http\Response
     */
    public function show(Sick $sick)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Sick $sick
     * @return \Illuminate\Http\Response
     */
    public function edit(Sick $sick)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Sick $sick
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sick $sick)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Sick $sick
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sick $sick)
    {
        //
    }
}
