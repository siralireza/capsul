<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckRoles
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->route()->getPrefix() == 'api/' . env('API_VERSION') . '/client'
            and isDoctor())
            return errorResp('شما به بخش کاربر دسترسی ندارید.');
        elseif ($request->route()->getPrefix() == 'api/' . env('API_VERSION') . '/doctor'
            and isClient())
            return errorResp('شما به بخش مختصص دسترسی ندارید.');

        if (Auth::user()->status == 'deactive') {
            $msg = 'متاسفانه حساب کاربری شما غیر فعال میباشد.';
            if ($request->expectsJson())
                return errorResp($msg);
            flashMsg($msg, 0);
            Auth::logout();
            return redirect()->route('login');
        }
        return $next($request);
    }
}
