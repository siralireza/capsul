<?php

use App\Complaint;
use App\Http\Controllers\ChatController;
use App\User;
use App\UserRequest;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Support\Facades\Auth;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\KavenegarApi;


function apiResp($message)
{
    return Response()->json(['message' => $message], 200);
}

function errorResp($error)
{
    return Response()->json(['error' => $error], 200);
}

function userId()
{
    return Auth::id();
}

function userPic()
{
    return Auth::user()->pic;
}

function roleColName()
{
    return 'capsul_role';
}

function userRole(User $user = null)
{
    if ($user)
        return $user->{roleColName()};
    return Auth::user()->{roleColName()};
}

function isNotAdmin()
{
    return userRole() != 'admin';
}

function isAdmin()
{
    return userRole() == 'admin';
}

function isClient()
{
    return userRole() == 'client';
}

function isDoctor()
{
    return userRole() == 'doctor';
}

function userPhone(User $user = null)
{
    if (userRole($user) != 'admin')
        if ($user)
            return $user->{userRole($user)}->phone;
        else
            return Auth::user()->{userRole()}->phone;
    else
        return null;
}

function drExp()
{
    return Auth::user()->Doctor->exp_id;
}

function userWallet()
{
    if (isClient())
        return Auth::user()->Client->wallet;
    elseif (isDoctor())
        return Auth::user()->Doctor->wallet;
    return null;
}

function decreaseWallet($amount, User $user = null)
{
    if (!$user)
        $user = Auth::user();
    if ($user->{userRole($user)}->wallet < $amount)
        return false;
    else {
        $user->{userRole($user)}->wallet -= $amount;
        $user->{userRole($user)}->save();
    }
    return true;
}

function increaseWallet($amount, User $user = null)
{
    if (!$user)
        $user = Auth::user();
    $user->{userRole($user)}->wallet += $amount;
    $user->{userRole($user)}->save();
}

function getFileType($ext)
{
    // later maybe we have to add video files
    $image_extensions = ['jpg', 'png', 'jpeg'];
    $audio_extensions = ['mp3', 'mpga', 'ogg', 'wav'];
    $file_extensions = ['7z', 'rar', 'tar.gz', 'zip', 'pdf', 'txt'];

    if (in_array($ext, $image_extensions)) {
        $file_type = 'img';
    } elseif (in_array($ext, $audio_extensions)) {
        $file_type = 'audio';
    } elseif (in_array($ext, $file_extensions)) {
        $file_type = 'file';
    } else {
        $file_type = null;
    }
    return $file_type;
}

function sendNotification($msg, $fcm_tokens)
{
    $url = 'https://fcm.googleapis.com/fcm/send';

    $fcmMsg = array(
        'body' => $msg,
        'title' => 'پیام جدید',
        'sound' => "default",
        'color' => "#064657"
    );
    $payload = array(
        'registration_ids' => $fcm_tokens,
        'priority' => 'high',
        'notification' => $fcmMsg
    );

    $headers = array(
        'Authorization: key=' . env('FIREBASE_SERVER_KEY_API'),
        'Content-Type: application/json'
    );
    try {
        // todo later check the response
        CurlPost($url, $payload, $headers);
        return true;
    } catch (Exception $exception) {
        return false;
    }
}

function curlPost($url, $payload, $headers = null)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
    $output = curl_exec($ch);
    curl_close($ch);

    return $output;
}

function curlGet($url)
{
    // create curl resource
    $ch = curl_init();

    // set url
    curl_setopt($ch, CURLOPT_URL, $url);

    // set time limit for connection time out
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 100); //timeout in seconds
    set_time_limit(10);

    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $output contains the output string
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function flashMsg($message, $class = 1)
{
    if ($class == 1)
        $class = 'alert-success';
    elseif ($class == 0)
        $class = 'alert-danger';

    Session::flash('message', $message);
    Session::flash('alert-class', $class);
}


function translateRequestStatus(UserRequest $req = null, $type = null)
{

    if ($type)
        $toComparison = $type;
    elseif ($req)
        $toComparison = $req->status;
    else
        return null;

    if ($toComparison == 'accepted')
        return 'جاری';
    else if ($toComparison == 'waiting')
        return 'منتظر تایید';
    else if ($toComparison == 'finished')
        return 'پایان یافته';
    else if ($toComparison == 'reserved')
        return 'رزرو شده';
    else if ($toComparison == 'rejected')
        return 'رد شده';
}

function translateCompStatus($comp = null, $type = null)
{
    if ($type)
        $toComparison = $type;
    elseif ($comp)
        $toComparison = $comp->status;
    else
        return null;

    if ($toComparison == 'viewed')
        return 'بررسی شد';
    else if ($toComparison == 'waiting')
        return 'منتظر پاسخ';
    else if ($toComparison == 'closed')
        return 'بسته شد';
}

function translateRequestTitle($type)
{
    $preTitle = 'درخواست‌های';
    return $preTitle . ' ' . translateRequestStatus(null, $type);

}

function translateRequestType(UserRequest $req)
{
    if ($req)
        $toComparison = $req->type;
    else
        return null;
    if ($toComparison == 'lab')
        return 'آزمایشگاهی';
    else if ($toComparison == 'send_prescription')
        return 'ارسال نسخه';
    else if ($toComparison == 'renew_prescription')
        return 'تمدید نسخه';

}

function translateRejectReason(UserRequest $req)
{
    if ($req)
        $toComparison = $req->reject_reason;
    else
        return null;
    if ($toComparison == 'emergency')
        return 'اورژانسی';
    else if ($toComparison == 'presence')
        return 'حضوری';
    else if ($toComparison == 'other_exp')
        return 'تخصص دیگر';

}

function newWaitingChecker($reqs)
{
    $count = 0;
    foreach ($reqs as $req) {
        if (!$req->read_by_dr) {
            $count++;
        }
    }
    return $count;
}

function toJalali($date)
{
    $dateTime = new Verta($date);
    return $dateTime->format('Y/n/j');
}

function unReadChats($request)
{
    $chats = ChatController::index($request, true);
    $count = 0;
    foreach ($chats as $chat) {
        if ($chat->have_unread)
            $count += $chat->have_unread;
    }
    return $count;
}

function translateSex($sex)
{
    return $sex == 'male' ? 'مرد' : 'زن';
}

function dfa2den($p_date)
{
    $v = Verta::parse($p_date);
    return $v->formatGregorian('Y-m-d');
}

function den2dfa($e_date)
{
    $e_date = Carbon::createFromDate($e_date);
    $v = Verta($e_date);
//    $v = Verta::parse($e_date);
//    $v = Verta::create($v->year, $v->month, $v->day);
    return $v->formatJalaliDate();
}

function vertaDateFormatter($date)
{
    if (!$date instanceof Verta)
        $date = Verta::parse($date);

    $replace = str_replace('-', '/', Verta::persianNumbers($date->formatDate()));
    return $replace;
}

function datesBeforeArray($start_date, $diff = 30, bool $format = true)
{

    $start_date = Verta::parse($start_date);
    $dates = [$format ? VertaDateFormatter($start_date) : $start_date->formatDate()];
    for ($i = 0; $i < $diff; $i++) {
        array_push(
            $dates,
            $format ? VertaDateFormatter($start_date->addDay(1)) : $start_date->addDay(1)->formatDate()
        );
    }

    return $dates;
}

function datesRangeArray($start_date, $end_date, bool $format = true)
{

    $period = \Carbon\CarbonPeriod::create($start_date, $end_date);
    $period = $period->toArray();
    foreach ($period as $k => $date) {
        $period[$k] = $date->format('Y-m-d');
    }
    $dates = $period;
    return $dates;
}

function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

function generateRandomString($length = 10)
{
    // with capital letters
//    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    // just small letters
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function sendSmsOld(array $phones, $msg)
{
    try {
        $client = new SoapClient('http://sms-webservice.ir/v1/v1.asmx?WSDL');
        $parameters['Username'] = env('SMS_USERNAME');
        $parameters['PassWord'] = env('SMS_PASSWORD');
        $parameters['SenderNumber'] = env('SMS_SENDER_NUMBER');
        $parameters['RecipientNumbers'] = $phones;
        $parameters['MessageBodie'] = $msg;
        $parameters['Type'] = 1;
        $parameters['AllowedDelay'] = 0;

        # get credit in sms panel
//            $res = $client->GetCredit($parameters);
//            dd($res->GetCreditResult);

        $res = $client->SendMessage($parameters);
        if ($res->SendMessageResult->long < 0) {
            return false;
        }
        return true;
    } catch (SoapFault $ex) {
        // todo later handle this error
        $msg = $ex->faultstring;
        return false;
    }
}

function sendSms(array $phones, $msg)
{
    try {
        $sender = env('SMS_SENDER_NUMBER_KAVEHNEGAR');
        $receptor = $phones;
        $message = $msg;
        $api = new KavenegarApi(env('SMS_API_KEY'));
        $api->Send($sender, $receptor, $message);
        return true;
    } catch (ApiException $e) {
        // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد
//        echo $e->errorMessage();
        return false;
    } catch (\Kavenegar\Exceptions\HttpException $e) {
        // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
//        echo $e->errorMessage();
        return false;
    } catch (Exception $ex) {
        return false;
    }
}

function verifySms($phone, $token1, $token2)
{
    try {
        $receptor = $phone;
        $api = new KavenegarApi(env('SMS_API_KEY'));
        $api->VerifyLookup($receptor, $token1, $token2, null, 'verify');
        return true;
    } catch (ApiException $e) {
        return false;
    } catch (\Kavenegar\Exceptions\HttpException $e) {
        return false;
    } catch (Exception $ex) {
        return false;
    }
}

function registerText($username, $password)
{
    return "به سایت کپسول خوش آمدید" . "\n" .
        $username . " :نام کاربری" . "\n" .
        $password . " :کلمه عبور";
}

function recoverText($username, $password)
{
    return "مشخصات بازیابی حساب کاربری کپسول" . "\n" .
        $username . " :نام کاربری" . "\n" .
        $password . " :کلمه عبور";
}

function smsProblemText()
{
    return 'مشکلی در ارسال پیام به شماره مورد نظر وجود دارد، لطفا شماره خود را بررسی نمایید';
}

function userRoleId()
{
    return 2;
}

function translateRole($role)
{
    if ($role == 'doctor')
        return 'پزشک';
    if ($role == 'client')
        return 'کلاینت';
    else
        return 'مدیر';

}