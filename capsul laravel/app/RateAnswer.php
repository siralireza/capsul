<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RateAnswer extends MotherModel
{
    public function Request()
    {
        return $this->belongsTo(UserRequest::class,'req_id','id');
    }
}
