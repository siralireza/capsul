<?php

namespace App;

use Storage;

class Banner extends MotherModel
{
    public function getImageAttribute($value)
    {
        $request = request();
        if ($request->expectsJson() or isNotAdmin())
            return $value ? url(Storage::url($value)) : null;
        else
            return $value;
    }
}
