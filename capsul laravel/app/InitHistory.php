<?php

namespace App;


class InitHistory extends MotherModel
{
    protected $primaryKey = 'user_id';
    protected $table = 'client_initial_histories';

    public function Client()
    {
        return $this->belongsTo(Client::class, 'user_id', 'user_id')->with('User:id,fname,lname,sex,pic');
    }
}
