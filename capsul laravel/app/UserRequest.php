<?php

namespace App;

use Auth;
use Carbon\Carbon;
use Exception;
use Hekmatinasser\Verta\Verta;

class UserRequest extends MotherModel
{
    protected $hidden = [
        'user_id', 'exp_id', 'dr_id', 'reserve_date', 'updated_at', 'deleted_at'];
    protected $appends = ['answer_time', 'have_unread'];


    public function Doctor()
    {
        return $this->belongsTo(Doctor::class, 'dr_id', 'user_id')->with('User:id,fname,lname,sex,pic');
    }

    public function Client()
    {
        return $this->belongsTo(Client::class, 'user_id', 'user_id')->with('User:id,fname,lname,sex,pic');
    }

    public function Expertise()
    {
        return $this->belongsTo(Expertise::class, 'exp_id', 'id');
    }

    public function Chats()
    {
        return $this->hasMany(ChatMessage::class, 'req_id', 'id');
    }

    public function RateAnswers()
    {
        return $this->hasMany(RateAnswer::class);
    }

    public function getHaveUnreadAttribute()
    {
        $chats = $this->Chats()->where([
            ['read_status', 0],
            ['sender_id', '<>', userId()],
        ]);
        return $chats->exists() ? $chats->count() : false;
    }

    public function getAnswerTimeAttribute()
    {
        try {
            if ($this->reserve_date) {
//                $time = Carbon::createFromTimeString($this->created_at)->toTimeString();
//                return Carbon::createFromTimeString($this->reserve_date . ' ' . $time)->addHours(CapsulSetting::latest()->first()->answer_time ?? 0)->toDateTimeString();
                $time = '23:59:59';
                return $this->reserve_date . ' ' . $time;
            } else
                return Carbon::createFromTimeString($this->created_at)->addHours(CapsulSetting::latest()->first()->answer_time ?? 0)->toDateTimeString();
        } catch (Exception $e) {
            return null;
        }
    }
    public function getCreatedAtAttribute($value)
    {
        if (isAdmin()) {
            $dateTime = new Verta($value);
            $dateTimeString = $dateTime->format('H:i:s - Y/n/j');
            return $dateTimeString;
        }
        return $value;
    }
//    public function getFinishedAtAttribute($value)
//    {
//        $date = Carbon::createFromTimeString($value)->toDateString();
//        return $date;
//    }

}
