<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'capsul:admin {username}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add a admin to users table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = $this->argument('username');
        if (User::where('username', $username)->exists()) {
            $this->info("Username exists! try another username.");
            return;
        }
        $fname = $this->ask('Enter the admin name');
        $email = $this->ask('Enter the email');
        $password = $this->secret('Enter admin password');
        $confirmPassword = $this->secret('Confirm Password');
        if ($password != $confirmPassword) {
            $this->info("Passwords don't match");

            return;
        }

        User::create([
            'fname' => $fname,
            'username' => $username,
            'email' => $email,
            'capsul_role' => 'admin',
            'role_id' => 1,
            'password' => Hash::make($password),
        ]);
        Artisan::command('voyager:admin ' . $email, function ($project) {
            $this->info("Building {$project}!");
        });
        $this->info("admin created");
        return;

    }
}
