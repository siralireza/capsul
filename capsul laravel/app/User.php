<?php

namespace App;

use App\Events\UserCreate;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use PhpParser\Comment\Doc;
use Storage;
use TCG\Voyager\Traits\Translatable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'fname', 'email','username', 'password',
//    ];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'name', 'avatar', 'settings',
        'password', 'remember_token', 'created_at',
        'updated_at', 'deleted_at', 'api_token', 'fcm_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'phone_verified_at' => 'datetime',
    ];

    public function Client()
    {
        return $this->hasOne(Client::class);
    }

    public function Doctor()
    {
        return $this->hasOne(Doctor::class)->with('Expertise:id,expertise_name');
    }

    public function Chats()
    {
        return $this->hasMany(ChatMessage::class);
    }

    public function TransActions()
    {
        return $this->hasMany(TransAction::class);
    }


    public function getPicAttribute($value)
    {
        $request = request();
        if ($request->expectsJson() or isNotAdmin())
            return $value ? url(Storage::url($value)) : null;
        else
            return $value;

    }

    public function setNameAttribute($value)
    {
        $this->attributes['fname'] = $value;
    }

    public function setPicAttribute($value)
    {
        $this->attributes['avatar'] = $value;
        $this->attributes['pic'] = $value;
    }


    public function getSexAttribute($value)
    {
        return $value == 'male' ? 'مرد' : 'زن';
    }

    public function getAvatarAttribute($value)
    {
        return $this->pic;
    }

    public function isClient()
    {
        return $this->capsul_role == 'client';
    }

    public function isDoctor()
    {
        return $this->capsul_role == 'doctor';
    }

}
