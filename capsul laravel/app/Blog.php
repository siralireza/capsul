<?php

namespace App;

use Hekmatinasser\Verta\Verta;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    public function getCreatedAtAttribute($value)
    {
        $dateTime = new Verta($value);
        $dateTimeString = $dateTime->format('H:i - Y/n/j');
        return $dateTimeString;
    }

}
