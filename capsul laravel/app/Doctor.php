<?php

namespace App;


use Storage;

class Doctor extends MotherModel
{
    protected $primaryKey = 'user_id';

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Expertise()
    {
        return $this->belongsTo(Expertise::class, 'exp_id', 'id');
    }

    public function Requests()
    {
        return $this->hasMany(UserRequest::class);
    }

    public function Complaints()
    {
        return $this->hasMany(Complaint::class);
    }


    public function getDocumentsPathAttribute($value)
    {
        $request = request();
        if ($request->expectsJson() or isNotAdmin())
            return $value ? url(Storage::url($value)) : null;
        else
            return $value;

    }

    public function getNameAttribute($value)
    {
        if (isAdmin())
            return $value . "(" . $this->user_id . ")";
    }
}
