<?php

namespace App;


class Complaint extends MotherModel
{
    protected $hidden = ['user_id', 'created_at', 'updated_at', 'deleted_at'];

    public function Doctor()
    {
        return $this->belongsTo(Doctor::class, 'dr_id', 'user_id')->with('User:id,fname,lname,sex,pic');
    }
}
