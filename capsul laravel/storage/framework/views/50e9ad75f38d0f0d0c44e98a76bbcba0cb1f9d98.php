<?php $__env->startSection('title'); ?>
    <title>ثبت نام متخصص</title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('body'); ?>
    <div class="container">
        <div class="row" style="height: 100vh">
            <div class="col-sm-10 my-auto mx-auto" style="max-width: 600px">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">ثبت نام متخصص جدید</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="<?php echo e(route('register')); ?>"
                          enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="register_type" value="doctor">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputfname" class="col-sm-6 control-label">نام&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>

                                <div class="col-sm-10">
                                    <input type="text" name="fname"
                                           class="form-control <?php if ($errors->has('fname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('fname'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                           id="inputfname"
                                           placeholder="نام خود را وارد کنید" value="<?php echo e(old('fname')); ?>"
                                           required autofocus>
                                    <?php if ($errors->has('fname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('fname'); ?>
                                    <span class="invalid text-danger text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="inputlname" class="col-sm-6 control-label">نام خانوادگی&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>

                                <div class="col-sm-10">
                                    <input type="text" name="lname"
                                           class="form-control <?php if ($errors->has('lname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('lname'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                           id="inputlname"
                                           placeholder="نام خانوادگی را وارد کنید" value="<?php echo e(old('lname')); ?>"
                                           required>
                                    <?php if ($errors->has('lname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('lname'); ?>
                                    <span class="invalid text-danger text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="inputUsername" class="col-sm-6 control-label">نام کاربری&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>

                                <div class="col-sm-10">
                                    <input type="text" name="username"
                                           class="form-control <?php if ($errors->has('username')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('username'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                           id="inputUsername"
                                           placeholder="نام کاربری را وارد کنید" value="<?php echo e(old('username')); ?>"
                                           required>
                                    <?php if ($errors->has('username')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('username'); ?>
                                    <span class="invalid text-danger text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-6 control-label">کلمه عبور&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>

                                <div class="col-sm-10">
                                    <input type="password" name="password"
                                           class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                           id="inputPassword3"
                                           placeholder="پسورد را وارد کنید" required
                                           value="<?php echo e(old('password')); ?>"
                                            
                                    >
                                    <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>
                                    <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPasswordconf" class="col-sm-6 control-label">تکرار کلمه
                                    عبور&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>

                                <div class="col-sm-10">
                                    <input type="password" name="password_confirmation"
                                           class="form-control <?php if ($errors->has('password_confirmation')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password_confirmation'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                           id="inputPasswordconf"
                                           placeholder="تکرار پسورد را وارد کنید" required
                                           value="<?php echo e(old('password_confirmation')); ?>"
                                    >
                                    <?php if ($errors->has('password_confirmation')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password_confirmation'); ?>
                                    <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-6 control-label">ایمیل</label>

                                <div class="col-sm-10">
                                    <input type="email" name="email"
                                           class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                           id="inputEmail"
                                           placeholder="ایمیل را وارد کنید" value="<?php echo e(old('email')); ?>">
                                    <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                                    <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPhone" class="col-sm-6 control-label">شماره موبایل&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>

                                <div class="col-sm-10">
                                    <input type="tel" name="phone"
                                           class="form-control <?php if ($errors->has('phone')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('phone'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                           id="inputPhone"
                                           placeholder="شماره موبایل را وارد کنید" value="<?php echo e(old('phone')); ?>" required>
                                    <?php if ($errors->has('phone')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('phone'); ?>
                                    <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label" for="inputSex">جسنیت&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>
                                <div class="col-sm-10">

                                    <select class="form-control <?php if ($errors->has('sex')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('sex'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                            id="inputSex" name="sex" required>
                                        <?php if(old('sex') == 'female'): ?>
                                            <option value="male">مذکر</option>
                                            <option value="female" selected>مونث</option>
                                        <?php else: ?>
                                            <option value="male" selected>مذکر</option>
                                            <option value="female">مونث</option>
                                        <?php endif; ?>
                                    </select>
                                    <?php if ($errors->has('sex')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('sex'); ?>
                                    <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label" for="inputExp">تخصص&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>
                                <div class="col-sm-10">

                                    <select class="form-control <?php if ($errors->has('exp_id')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('exp_id'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                            id="inputExp" name="exp_id" required>
                                        <?php $__currentLoopData = \App\Expertise::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $exp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($exp->id); ?>"
                                                    <?php if(old('exp_id') == $exp->id): ?> selected <?php endif; ?>><?php echo e($exp->expertise_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <?php if ($errors->has('exp_id')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('exp_id'); ?>
                                    <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="inputWorkExp" class="col-sm-6 control-label">تجربه کاری (سال)&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>
                                <div class="col-sm-10">
                                    <input type="number" name="work_experience"
                                           class="form-control <?php if ($errors->has('work_experience')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('work_experience'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                           id="inputWorkExp"
                                           placeholder="تجربه کاری خود را به سال وارد کنید"
                                           value="<?php echo e(old('work_experience')); ?>">
                                    <?php if ($errors->has('work_experience')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('work_experience'); ?>
                                    <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputDrCode" class="col-sm-6 control-label">شماره نظام پزشکی&nbsp;<i
                                            class="fa fa-star text-danger"></i></label>
                                <div class="col-sm-10">
                                    <input type="number" name="dr_code"
                                           class="form-control <?php if ($errors->has('dr_code')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('dr_code'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                           id="inputDrCode"
                                           placeholder="شماره نظام پزشکی خود را وارد کنید" value="<?php echo e(old('dr_code')); ?>">
                                    <?php if ($errors->has('dr_code')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('dr_code'); ?>
                                    <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <h6 class="text-bold col-sm-6 control-label">فایل مدارک</h6>
                                <p class="text-sm"><i class="fa fa-star text-warning"></i>
                                    امکان آپلود فقط برای فایل فشرده وجود دارد.(rar ,zip ,7z)
                                </p>
                                <div class="text-right">
                                    <label class="btn-sm btn btn-outline-secondary mt-1" for="exampleInputFile">انتخاب
                                        فایل</label>
                                    <div class="col-sm-10">
                                        <input type="file" name="file"
                                               class="form-control custom-file-input <?php if ($errors->has('file')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('file'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="exampleInputFile">
                                        <?php if ($errors->has('file')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('file'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class=" col-sm-10">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="add_history" value="1"
                                               id="exampleCheck2" required>
                                        <label class="form-check-label" for="exampleCheck2">تمام <a href="<?php echo e(route('termsDr')); ?>" target="_blank" class="text-bold text-danger">قوانین و تعهدات</a> را میپذیرم.</label>
                                    </div>
                                </div>
                            </div>
                            </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">ثبت نام</button>
                            <a href="<?php echo e(route('login')); ?>" class="btn btn-outline-primary mx-2">ورود</a>
                            <a href="/" class="btn btn-outline-info mx-2 pull-left">صفحه اصلی</a>
                        </div>

                    </form>
                </div>

                <!-- /.card-footer -->
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $('#inputUsername').on('keypress', function (event) {
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/auth/registerDoctor.blade.php ENDPATH**/ ?>