<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12 mx-auto" style="max-width: 900px">
            <div class="card card-primary mt-3">
                <div class="card-header">
                    <h3 class="card-title">ویرایش پروفایل</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="<?php echo e(route('updateProfile')); ?>" method="post" enctype="multipart/form-data"
                      id="ProfileForm">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('patch'); ?>
                    <div class="card-body">
                        <div class="w-100">
                            <div class="form-group">
                                <div class="image text-center">
                                    <img src="<?php echo e(userPic() ?: asset('dist/img/avatar5.png')); ?>"
                                         class="img-circle elevation-2" alt="User Image" style="width: 4.5rem">
                                </div>
                                <div class="text-center">
                                    <label class="btn-sm btn btn-outline-dark mt-3" for="exampleInputPic">تغییر
                                        عکس</label>
                                    <input type="file" name="pic" style="display: none" class="custom-file-input"
                                           id="exampleInputPic">
                                    <?php if ($errors->has('pic')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('pic'); ?>
                                    <span class="invalid text-danger text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputfname" class="col-sm-6 control-label">نام</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="fname"
                                               class="form-control <?php if ($errors->has('fname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('fname'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputfname"
                                               placeholder="نام خود را وارد کنید" value="<?php echo e(Auth::user()->fname); ?>"
                                               autofocus>
                                        <?php if ($errors->has('fname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('fname'); ?>
                                        <span class="invalid text-danger text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="inputlname" class="col-sm-6 control-label">نام خانوادگی</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="lname"
                                               class="form-control <?php if ($errors->has('lname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('lname'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputlname"
                                               placeholder="نام خانوادگی را وارد کنید" value="<?php echo e(Auth::user()->lname); ?>"
                                        >
                                        <?php if ($errors->has('lname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('lname'); ?>
                                        <span class="invalid text-danger text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="inputPassword0" class="col-sm-6 control-label">کلمه عبور فعلی</label>

                                    <div class="col-sm-10">
                                        <input type="password" name="pass"
                                               class="form-control <?php if ($errors->has('pass')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('pass'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputPassword0"
                                               placeholder="پسورد کنونی وارد کنید"
                                               value="<?php echo e(old('pass')); ?>"
                                                
                                        >
                                        <?php if ($errors->has('pass')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('pass'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-6 control-label">کلمه عبور جدید</label>

                                    <div class="col-sm-10">
                                        <input type="password" name="password"
                                               class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputPassword3"
                                               placeholder="پسورد را وارد کنید"
                                               value="<?php echo e(old('username')); ?>"
                                                
                                        >
                                        <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPasswordconf" class="col-sm-6 control-label">تکرار کلمه
                                        عبور جدید</label>

                                    <div class="col-sm-10">
                                        <input type="password" name="password_confirmation"
                                               class="form-control <?php if ($errors->has('password_confirmation')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password_confirmation'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputPasswordconf"
                                               placeholder="تکرار پسورد را وارد کنید"
                                               value="<?php echo e(old('password_confirmation')); ?>"
                                        >
                                        <?php if ($errors->has('password_confirmation')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password_confirmation'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-6 control-label">ایمیل</label>

                                    <div class="col-sm-10">
                                        <input type="email" name="email"
                                               class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputEmail"
                                               placeholder="ایمیل را وارد کنید"
                                               value="<?php echo e(Auth::user()->email); ?>">
                                        <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPhone" class="col-sm-6 control-label">شماره موبایل</label>

                                    <div class="col-sm-10">
                                        <input type="tel" name="phone"
                                               class="form-control <?php if ($errors->has('phone')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('phone'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputPhone"
                                               placeholder="شماره موبایل را وارد کنید"
                                               value="<?php echo e(Auth::user()->Doctor->phone); ?>">
                                        <?php if ($errors->has('phone')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('phone'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label" for="inputSex">جسنیت</label>
                                    <div class="col-sm-10">

                                        <select class="form-control <?php if ($errors->has('sex')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('sex'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                id="inputSex" name="sex">
                                            <?php if(Auth::user()->sex == 'female'): ?>
                                                <option value="male">مذکر</option>
                                                <option value="female" selected>مونث</option>
                                            <?php else: ?>
                                                <option value="male" selected>مذکر</option>
                                                <option value="female">مونث</option>
                                            <?php endif; ?>
                                        </select>
                                        <?php if ($errors->has('sex')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('sex'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="inputWorkExp" class="col-sm-6 control-label">تجربه کاری (سال)</label>
                                    <div class="col-sm-10">
                                        <input type="number" name="work_experience"
                                               class="form-control <?php if ($errors->has('work_experience')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('work_experience'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputWorkExp"
                                               placeholder="تجربه کاری خود را به سال وارد کنید"
                                               value="<?php echo e(Auth::user()->Doctor->work_experience); ?>">
                                        <?php if ($errors->has('work_experience')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('work_experience'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputDrCode" class="col-sm-6 control-label">شماره نظام پزشکی</label>
                                    <div class="col-sm-10">
                                        <input type="number" name="dr_code"
                                               class="form-control <?php if ($errors->has('dr_code')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('dr_code'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputDrCode"
                                               placeholder="شماره نظام پزشکی خود را وارد کنید"
                                               value="<?php echo e(Auth::user()->Doctor->dr_code); ?>">
                                        <?php if ($errors->has('dr_code')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('dr_code'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputSheba" class="col-sm-6 control-label">شماره شبا</label>
                                    <div class="col-sm-10">
                                        <input type="number" name="sheba"
                                               class="form-control <?php if ($errors->has('sheba')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('sheba'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputSheba"
                                               placeholder="شماره شبا خود را وارد کنید"
                                               value="<?php echo e(Auth::user()->Doctor->sheba); ?>">
                                        <?php if ($errors->has('sheba')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('sheba'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h6 class="text-bold col-sm-6 control-label">فایل مدارک</h6>
                                    <p class="text-sm"><i class="fa fa-star text-warning mt-3"></i>
                                        امکان آپلود فقط برای فایل فشرده وجود دارد.(rar ,zip ,7z)
                                    </p>
                                    <?php if(Auth::user()->Doctor->documents_path): ?>
                                        <div class="w-100 text-center">
                                            <a href="<?php echo e(Auth::user()->Doctor->documents_path); ?>"
                                               class="btn btn-sm btn-outline-info">دانلود فایل قبلی</a>
                                        </div>
                                    <?php endif; ?>
                                    <div class="w-100">
                                        <div class="text-center">
                                            <label class="btn-sm btn btn-outline-secondary mt-1" for="exampleInputFile">انتخاب
                                                فایل</label>
                                            <div class="col-sm-10">
                                                <input type="file" name="file"
                                                       class="form-control custom-file-input <?php if ($errors->has('file')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('file'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                       id="exampleInputFile">
                                                <?php if ($errors->has('file')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('file'); ?>
                                                <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-lg btn-primary">ویرایش</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $('#ProfileForm').on('submit', function (e) {
            e.preventDefault();
            if ($('#inputPhone').val() == '<?php echo e(Auth::user()->Doctor->phone); ?>') {
                $('#inputPhone').attr("disabled", true);
            }if ($('#inputDrCode').val() == '<?php echo e(Auth::user()->Doctor->dr_code); ?>') {
                $('#inputDrCode').attr("disabled", true);
            }
            e.currentTarget.submit();
        })

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.doctor.doctorPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/doctor/profile.blade.php ENDPATH**/ ?>