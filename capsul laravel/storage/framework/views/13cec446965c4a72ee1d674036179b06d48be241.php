<?php $__env->startSection('content'); ?>
    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title">لیست درخواست ها</h3>
        </div>
        <div class="card-body row">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3><?php echo e($reqs->where('status','accepted')->count()); ?></h3>

                        <p>درخواست های قبول شده</p>
                        <span class="badge badge-danger invisible" >  پیام جدید</span>

                    </div>
                    <div class="icon">
                        <i class="fa fa-plus"></i>
                    </div>
                    <a href="<?php echo e(route('reqs.index').'?status[0]=accepted'); ?>" class="small-box-footer">مشاهده <i
                                class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3><?php echo e($newReqs->where('status','waiting')->count()); ?></h3>

                        <p>درخواست های منتظر پاسخ</p>
                        <?php ($newWaiting = newWaitingChecker($newReqs)); ?>
                        <span class="badge badge-danger <?php if(!$newWaiting): ?> invisible <?php endif; ?>" ><?php echo e($newWaiting); ?> جدید</span>

                    </div>
                    <div class="icon">
                        <i class="fa fa-pause"></i>
                    </div>
                    <a href="<?php echo e(route('reqs.index').'?status[0]=waiting'); ?>" class="<?php if($newWaiting): ?> bg-warn-dang-flasher <?php endif; ?> small-box-footer">مشاهده
                        <i
                                class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gray">
                    <div class="inner">
                        <h3><?php echo e($reqs->where('status','finished')->count()); ?></h3>

                        <p>درخواست های پایان یافته</p>
                        <span class="badge badge-danger invisible" >O</span>

                    </div>
                    <div class="icon">
                        <i class="fa fa-check"></i>
                    </div>
                    <a href="<?php echo e(route('reqs.index').'?status[0]=finished'); ?>" class="small-box-footer">مشاهده <i
                                class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div>
        
        
        
        
        

        
        
        
        
        
        
        
        
        <!-- ./col -->
        </div>
        
        
        
    </div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        setInterval(function () {
            if ($('.bg-warn-dang-flasher').hasClass('bg-danger'))
                $('.bg-warn-dang-flasher').removeClass('bg-danger').addClass('bg-gray-light');
            else if ($('.bg-warn-dang-flasher').hasClass('bg-gray-light'))
                $('.bg-warn-dang-flasher').removeClass('bg-gray-light').addClass('bg-danger');
            else
                $('.bg-warn-dang-flasher').addClass('bg-danger');
        }, 500);

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.doctor.doctorPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/doctor/requests/index.blade.php ENDPATH**/ ?>