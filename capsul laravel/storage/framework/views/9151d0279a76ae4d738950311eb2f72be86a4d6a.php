<?php $__env->startSection('body'); ?>
    <div class="container">
        <div class="row mt-lg-5">
            <div class="col-sm-12 my-auto mx-auto" style="max-width: 600px">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">قوانین و تعهد نامه کپسول</h3>
                    </div>
                    <div class="card-body text-justify">
                        <?php if($for == 'client'): ?>
                            <p><?php echo e(\App\CapsulSetting::first()->terms_client); ?></p>
                        <?php elseif($for == 'dr'): ?>
                            <p><?php echo e(\App\CapsulSetting::first()->terms_dr); ?></p>
                        <?php endif; ?>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/terms.blade.php ENDPATH**/ ?>