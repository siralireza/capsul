<?php $__env->startSection('content'); ?>
    <div class="row mt-3">
        <div class="col-12 mx-auto" style="max-width: 500px">
            <div class="card card-primary mt-3">
                <div class="card-header">
                    <h3 class="card-title">ثبت سابقه پزشکی</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="<?php echo e(route('sicks.store')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title" class="col-sm-10 control-label">نام بیماری</label>

                            <div class="col-sm-10">
                                    <textarea name="sick" rows=2
                                              class="form-control <?php if ($errors->has('sick')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('sick'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="title"
                                              placeholder="نام بیماری را وارد کنید"><?php echo e(old('sick')); ?></textarea>
                                <?php if ($errors->has('sick')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('sick'); ?>
                                <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="desc" class="col-sm-10 control-label">توضیحات</label>

                            <div class="col-sm-10">
                                    <textarea name="desc" rows=2
                                              class="form-control <?php if ($errors->has('desc')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('desc'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="desc"
                                              placeholder="اقدامات درمانی، تشخیص انجام شده و دارو های درحال مصرف را وارد نمایید"><?php echo e(old('desc')); ?></textarea>
                                <?php if ($errors->has('desc')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('desc'); ?>
                                <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">تایید</button>
                        <a href="<?php echo e(route('sicks.index')); ?>" class="btn btn-primary pull-left">بازگشت</a></div>
                </form>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.client.clientPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/client/sicks/create.blade.php ENDPATH**/ ?>