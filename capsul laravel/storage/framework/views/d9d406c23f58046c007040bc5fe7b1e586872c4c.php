<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12 mx-auto" style="max-width:500px">
            <div class="card card-primary mt-3">
                <div class="card-header">
                    <h3 class="card-title">شارژ کیف پول</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" id="myForm" action="<?php echo e(route('wallet.increase')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="amount" class="col-sm-6 control-label">مبلغ مورد نظر</label>

                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input name="amount" type="text"
                                                   class="form-control <?php if ($errors->has('amount')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('amount'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="amount"
                                                   placeholder="مبلغ را وارد نمایید">
                                            <?php if ($errors->has('amount')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('amount'); ?>
                                            <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>
                                        <div class="col-sm-2 mt-1">
                                            <h5 class="text-warning">تومان</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-lg btn-success">پرداخت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        var textbox = '#amount';
        $(textbox).keyup(function () {
            var num = $(textbox).val();
            console.log(num)
            var numCommas = addCommas(num);
            console.log(numCommas)
            $(textbox).val(numCommas);
        });
        $('#myForm').submit(function () {
            var num = $(textbox).val();
            var numwithoutCommas = remCommas(num);
            $(textbox).val(numwithoutCommas);
            return true;
        });

        function remCommas(nStr) {
            nStr += '';
            var comma = /,/g;
            nStr = nStr.replace(comma, '');
            return nStr;
        }

        function addCommas(nStr) {
            nStr += '';
            var comma = /,/g;
            nStr = nStr.replace(comma, '');
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.client.clientPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/client/wallet/index.blade.php ENDPATH**/ ?>