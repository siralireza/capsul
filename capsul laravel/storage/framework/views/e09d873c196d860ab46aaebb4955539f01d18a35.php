<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php echo $__env->yieldContent('title'); ?>
    <?php $__env->startSection('refresh'); ?>
        <?php echo $__env->yieldSection(); ?>
    <meta http-equiv="refresh" content="300"/>
<!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo e(asset('plugins/font-awesome/css/font-awesome.min.css')); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo e(asset('plugins/ionicicons/ionicons.min.css')); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(asset('dist/css/adminlte.min.css')); ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo e(asset('plugins/iCheck/flat/blue.css')); ?>">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo e(asset('plugins/morris/morris.css')); ?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo e(asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css')); ?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo e(asset('plugins/datepicker/datepicker3.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('dist/css/persian-datepicker.min.css')); ?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo e(asset('plugins/daterangepicker/daterangepicker-bs3.css')); ?>">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="<?php echo e(asset('plugins/google-fonts/font.css')); ?>" rel="stylesheet">
    <!-- bootstrap rtl -->
    <link rel="stylesheet" href="<?php echo e(asset('dist/css/bootstrap-rtl.min.css')); ?>">
    <!-- template rtl version -->
    <link rel="stylesheet" href="<?php echo e(asset('dist/css/custom-style.css')); ?>">
    <!-- custom css-->
    <link rel="stylesheet" href="<?php echo e(asset('dist/css/main.css')); ?>">

</head>
<body class="hold-transition sidebar-mini">




















<?php echo $__env->yieldContent('body'); ?>
<!-- jQuery -->
<script src="<?php echo e(asset('plugins/jquery/jquery.min.js')); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo e(asset('plugins/jquery/jquery-ui.min.js')); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo e(asset('plugins/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo e(asset('plugins/raphael/raphael.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/morris/morris.min.js')); ?>"></script>
<!-- Sparkline -->
<script src="<?php echo e(asset('plugins/sparkline/jquery.sparkline.min.js')); ?>"></script>
<!-- jvectormap -->
<script src="<?php echo e(asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')); ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo e(asset('plugins/knob/jquery.knob.js')); ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo e(asset('plugins/momentjs/moment.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/daterangepicker/daterangepicker.js')); ?>"></script>
<!-- datepicker -->
<script src="<?php echo e(asset('plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo e(asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')); ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo e(asset('plugins/slimScroll/jquery.slimscroll.min.js')); ?>"></script>
<!-- FastClick -->
<script src="<?php echo e(asset('plugins/fastclick/fastclick.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(asset('dist/js/adminlte.js')); ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo e(asset('dist/js/pages/dashboard.js')); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo e(asset('dist/js/demo.js')); ?>"></script>
<script src="<?php echo e(asset('dist/js/main.js')); ?>"></script>
<!-- Persian Data Picker -->
<script src="<?php echo e(asset('dist/js/persian-date.min.js')); ?>"></script>
<script src="<?php echo e(asset('dist/js/persian-datepicker.min.js')); ?>"></script>

<script>
    $('.alert').delay(5000).fadeOut('slow');
    $('input[required]').on('input', function () {
        this.setCustomValidity("");
    });
    $('textarea[required]').on('input', function () {
        this.setCustomValidity("");
    });
    $('input[required]').on('invalid', function () {
        this.setCustomValidity("لطفا این فیلد را پر نمایید");
    });
    $('textarea[required]').on('invalid', function () {
        this.setCustomValidity("لطفا این فیلد را پر نمایید");
    });
    setInterval(function () {
        if ($('.text-white-danger').hasClass('text-white'))
            $('.text-white-danger').removeClass('text-white').addClass('text-danger');
        else if ($('.text-white-danger').hasClass('text-danger'))
            $('.text-white-danger').removeClass('text-danger').addClass('text-white');
        else
            $('.text-white-danger').addClass('text-white')
    }, 500);
</script>
<?php echo $__env->yieldContent('script'); ?>
</body>
</html>
<?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/layouts/main.blade.php ENDPATH**/ ?>