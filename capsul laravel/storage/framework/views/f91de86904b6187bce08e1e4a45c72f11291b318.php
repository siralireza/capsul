<?php $__env->startSection('content'); ?>
    
    
    
    
    
    
    
    
    
    
    
    
    <div class="card card-primary mt-3 mx-auto" style="max-width: 600px">
        <div class="card-header">
            <h3 class="card-title">درخواست خدمت جدید</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="<?php echo e(route('reqs.store')); ?>" method="post" id="thisForm">
            <?php echo csrf_field(); ?>
            <input type="hidden" class="hidden-date" name="reserve_date" value="<?php echo e(old('reserve_date')); ?>">
            <div class="card-body">
                <div class="form-group">
                    <label class="col-sm-10 control-label" for="inputExp">نوع تخصص&nbsp;<i
                                class="fa fa-star text-danger"></i></label>
                    <div class="col-sm-10">

                        <select class="form-control <?php if ($errors->has('exp_id')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('exp_id'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                id="inputExp" name="exp_id" required>
                            <option selected disabled>انتخاب کنید</option>
                            <?php $__currentLoopData = \App\Expertise::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $exp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($exp->id); ?>" data-cost="<?php echo e($exp->cost); ?>"
                                        <?php if(old('exp_id') == $exp->id): ?> selected <?php endif; ?>
                                ><?php echo e($exp->expertise_name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <?php if ($errors->has('exp_id')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('exp_id'); ?>
                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                    </div>

                </div>

                <input type="hidden" name="type" value="send_prescription">
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                <div class="form-group">
                    <label for="main_reason" class="col-sm-10 control-label">دلیل اصلی مراجعه امروز شما به پزشک&nbsp;<i
                                class="fa fa-star text-danger"></i></label>

                    <div class="col-sm-10">
                                    <textarea name="main_reason" rows=2 required maxlength="50"
                                              class="form-control <?php if ($errors->has('main_reason')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('main_reason'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                              id="main_reason"
                                              placeholder="دلیل خود را بنویسید"><?php echo e(old('main_reason')); ?></textarea>
                        <?php if ($errors->has('main_reason')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('main_reason'); ?>
                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="answer1" class="col-sm-10 control-label"> از چه زمانی شروع ویا تشدید شده است؟<i
                                class="fa fa-star text-danger"></i></label>

                    <div class="col-sm-10">
                                    <textarea name="answer1" rows=2 required
                                              class="form-control <?php if ($errors->has('answer1')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer1'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="answer1"
                                              placeholder="زمان مورد نظر را دقیق وارد نمایید"><?php echo e(old('answer1')); ?></textarea>
                        <?php if ($errors->has('answer1')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer1'); ?>
                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="answer2" class="col-sm-10 control-label"> علائم همراه را نام ببرید:<i
                                class="fa fa-star text-danger"></i></label>

                    <div class="col-sm-10">
                                    <textarea name="answer2" rows=2 required
                                              class="form-control <?php if ($errors->has('answer2')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer2'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="answer2"
                                              placeholder="مثلا تب یا سرفه"><?php echo e(old('answer2')); ?></textarea>
                        <?php if ($errors->has('answer2')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer2'); ?>
                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="answer3" class="col-sm-10 control-label"> آیا دارویی نیز مصرف کرده‌اید؟<i
                                class="fa fa-star text-danger"></i></label>

                    <div class="col-sm-10">
                                    <textarea name="answer3" rows=2 required
                                              class="form-control <?php if ($errors->has('answer3')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer3'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="answer3"
                                              placeholder="چه دارویی، ‌چه مقدار و مدت زمان مصرف را بنویسید"><?php echo e(old('answer3')); ?></textarea>
                        <?php if ($errors->has('answer3')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer3'); ?>
                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-10">در صورت رزرو تاریخ را انتخاب کنید</label>

                    <div class="input-group col-sm-10">
                        <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="fa fa-calendar"></i>
                      </span>
                        </div>
                        <input class="date-picker form-control" name="persian_date" value="<?php echo e(old('persian_date')); ?>"
                               readonly/>

                    </div>
                    <?php if ($errors->has('reserve_date')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('reserve_date'); ?>
                    <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                </div>

                <div class="mx-3">
                    <h6 class="text-bold">مبلغ قابل پرداخت:</h6>
                    <h4 class="text-danger"><span class="text-danger" id="cost">0</span> تومان</h4>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <!--                                <button type="submit" class="btn btn-primary">اتخاب پزشک</button>-->
                <button type="submit" class="btn btn-success btn-lg">تایید و ادامه</button>
                <a class="btn btn-primary btn-lg" href="<?php echo e(route('reqs.index')); ?>">بازگشت</a>
            </div>
        </form>
    </div>
    <div class="modal fade" id="costModal" tabindex="-1" role="dialog" aria-labelledby="costModal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">کمبود موجودی کیف پول</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="myForm" action="<?php echo e(route('wallet.increase')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <p class="text-danger text-bold">موجودی کیف پول شما کمتر از هزینه این تخصص میباشد
                                        حداقل مبلغ شارژ در کادر زیر نوشته شده است، اما میتوانید مبالغ بیشتر هم شارژ
                                        نمایید.</p>
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input name="amount" type="text"
                                                   class="form-control <?php if ($errors->has('amount')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('amount'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                   id="amount">
                                            <?php if ($errors->has('amount')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('amount'); ?>
                                            <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>
                                        <div class="col-sm-2 mt-1">
                                            <h5 class="text-warning">تومان</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->


                    </div>
                    <div class="modal-footer">
                            <button type="submit" class="btn btn-lg btn-success">پرداخت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script>
        var wallet = <?php echo e(userWallet()); ?>;
        $('#inputExp').change(function () {
            var cost = $(this).find(':selected').data('cost')
            if (wallet < cost) {
                diff = cost - wallet
                // dargah kamtar az hezaro qabol nmikone
                if (diff < 100)
                    diff = 100
                $('#amount').val(diff);
                $('#costModal').modal('show');
            }
            // $('#myModal').modal('hide');
            // $('#myModal').modal('toggle');

            $('#cost').text(toPersianNum(cost));
        });
        $('.date-picker').persianDatepicker({
            initialValue: false,
            format: 'YYYY/MM/DD',
        });
        $('#thisForm').submit(function (e) {
            e.preventDefault();
            var date = $('.date-picker').val();
            if (date) {
                date = toEnglishNum(date);
                date = date.split('/');
                date = date.map(Number);
                date = new persianDate(date).toCalendar('gregorian').toLocale('en').format('YYYY/MM/DD');
                $('.hidden-date').val(date)
            }
            $(this).unbind().submit();
        });
    </script>
    <script>
        var textbox = '#amount';
        $(textbox).keyup(function () {
            var num = $(textbox).val();
            console.log(num)
            var numCommas = addCommas(num);
            console.log(numCommas)
            $(textbox).val(numCommas);
        });
        $('#myForm').submit(function () {
            var num = $(textbox).val();
            var numwithoutCommas = remCommas(num);
            $(textbox).val(numwithoutCommas);
            return true;
        });

        function remCommas(nStr) {
            nStr += '';
            var comma = /,/g;
            nStr = nStr.replace(comma, '');
            return nStr;
        }

        function addCommas(nStr) {
            nStr += '';
            var comma = /,/g;
            nStr = nStr.replace(comma, '');
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.client.clientPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/client/requests/create.blade.php ENDPATH**/ ?>