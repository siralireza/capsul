<?php $__env->startSection('content'); ?>
    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title">
                <?php echo e(translateRequestTitle($type)); ?>

            </h3>
        </div>
        <div class="card-body row">
            <?php if($reqs->isEmpty()): ?>
                <div class="text-center w-100">
                    <h5 class="text-warning">در این بخش درخواستی وجود ندارد</h5>
                </div>
            <?php endif; ?>
            <?php $__currentLoopData = $reqs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $req): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="card col-lg-4">
                    <div class="card-body">
                        <span class="my-1 ill-name"><?php echo e(translateRequestType($req)); ?></span>
                        <?php
                                $dateTime = new \Hekmatinasser\Verta\Verta($req->created_at);
                                $date = $dateTime->format('Y/n/j');
                        ?>
                        <span class="my-1 float-left small mt-1"><?php echo e($date); ?></span><br>
                        <?php if($req->status == 'accepted'): ?>
                            <span class="my-1 badge badge-success"><?php echo e(translateRequestStatus($req)); ?></span>
                        <?php elseif($req->status == 'waiting'): ?>
                            <span class="my-1 badge badge-warning"><?php echo e(translateRequestStatus($req)); ?></span>
                        <?php elseif($req->status == 'reserved'): ?>
                            <span class="my-1 badge badge-info"><?php echo e(translateRequestStatus($req)); ?></span>
                        <?php elseif($req->status == 'finished'): ?>
                            <span class="my-1 badge badge-dark"><?php echo e(translateRequestStatus($req)); ?></span>
                        <?php else: ?>
                            <span class="my-1 badge badge-danger"><?php echo e(translateRequestStatus($req)); ?></span>
                        <?php endif; ?>

                        <div class="d-flex my-3">
                            <?php if($req->Client and $req->Client->User->pic): ?>
                                <div class="image">
                                    <img src="<?php echo e($req->Client->User->pic); ?>" class="img-circle elevation-2"
                                         alt="User Image"
                                         style="width: 3.5rem">
                                </div>
                            <?php elseif($req->Client): ?>
                                <div class="image">
                                    <img src="<?php echo e(asset('dist/img/avatar04.png')); ?>" class="img-circle elevation-2"
                                         alt="User Image"
                                         style="width: 3.5rem">
                                </div>
                            <?php endif; ?>

                            <div class="info mr-3">
                                <?php if($req->user_id): ?>
                                    <a class="d-block"><?php echo e($req->Client->User->fname . ' '. $req->Client->User->lname); ?></a>
                                    <a class="d-block">سن: <?php echo e($req->Client->age ?? 'تعیین نشده'); ?></a>
                                <?php endif; ?>

                            </div>
                        </div>
                        <a href="<?php echo e(route('reqs.show',['req'=>$req])); ?>"
                           class="my-1 btn btn-dark btn-sm">اطلاعات بیشتر</a>
                        <br>
                    </div>
                </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
        <div class="card-footer text-center">
            <a href="<?php echo e(route('reqs.index')); ?>" class="btn btn-lg btn-primary">بازگشت</a>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.doctor.doctorPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/doctor/requests/list.blade.php ENDPATH**/ ?>