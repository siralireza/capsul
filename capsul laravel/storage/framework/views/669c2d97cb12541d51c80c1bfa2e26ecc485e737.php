<?php $__env->startSection('content'); ?>
    <style>
        body{
            background-color: #042F3A !important;
        }
    </style>
    <div class="container" style="margin-top: 120px">
        <?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="mx-auto mb-3">
                <!-- Box Comment -->
                <div class="card card-widget">
                    <div class="card-header">
                        <div class="user-block">
                            <span class="card-title "><?php echo e($blog->title); ?></span>
                            <span class="description float-left"><?php echo e($blog->created_at); ?></span>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <?php if($blog->img): ?>
                            <div class="row">
                                <div class="col-lg-6">
                                    <img class="img-fluid pad"
                                         src="<?php echo e(Storage::disk(config('voyager.storage.disk'))->url($blog->img)); ?>"
                                         alt="blog-image">
                                </div>
                                <div class="col-lg-6">
                                    <?php endif; ?>
                                    <p class="text-justify"><?php echo e($blog->description); ?></p>
                                    <?php if($blog->img): ?>
                                </div>
                            </div>

                        <?php endif; ?>

                    </div>
                </div>
                <!-- /.card -->
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/blog/index.blade.php ENDPATH**/ ?>