<?php $__env->startSection('content'); ?>
    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title">سوابق پزشکی</h3>
        </div>
        <div class="card-body row">
            <?php if($sicks->isEmpty()): ?>
                <div class="w-100 text-center">
                    <h5 class="text-warning">سابقه‌ای ثبت نشده است</h5>
                </div>
            <?php endif; ?>
            <?php $__currentLoopData = $sicks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sick): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="card card-light col-lg-4">
                    <div class="card-header">
                        <span class="ill-name text-white"><?php echo e($sick->sick); ?></span>
                        <?php
                            $dateTime = new \Hekmatinasser\Verta\Verta($sick->created_at);
                            $date = $dateTime->format('Y/n/j');
                        ?>
                        <span class="pull-left text-white"><?php echo e($date); ?></span><br>


                    </div>
                    <div class="card-body">
                        <h5>توضیحات: </h5>
                        <p><?php echo e($sick->desc); ?></p>
                    </div>
                    <div class="card-footer">
                        <?php if($sick->added_by == 'client'): ?>
                            <div class="w-100">
                                <button class="btn btn-danger pull-left" data-toggle="modal"
                                        data-target="#deleteModal<?php echo e($loop->index); ?>">حذف
                                </button>
                            </div>
                        <?php endif; ?>
                        <?php if($sick->added_by == 'dr'): ?>
                            <span class="badge badge-success pull-left m-2">توسط پزشک</span>
                        <?php endif; ?>
                    </div>
                </div>
                <?php if($sick->user_id == userId()): ?>
                    <div class="modal fade" id="deleteModal<?php echo e($loop->index); ?>" tabindex="-1" role="dialog"
                         aria-labelledby="rateModal"
                         aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">حذف سابقه پزشکی</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="<?php echo e(route('sicks.destroy',['id' => $sick->id])); ?>" method="post">
                                    <?php echo csrf_field(); ?>
                                    <?php echo method_field('delete'); ?>
                                    <div class="modal-body">
                                        <h4 class="text-danger">آیا مطمئن هستید که این سابقه حذف شود؟</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success mx-2">بله</button>
                                        <button type="button" class="btn btn-primary justify-content-end"
                                                data-dismiss="modal">خیر
                                        </button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
        <div class="card-footer">
            <div class="card-footer text-center w-100">
                <a href="<?php echo e(route('sicks.create')); ?>" class="btn btn-lg btn-primary">ثبت بیماری جدید</a>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.client.clientPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/client/sicks/index.blade.php ENDPATH**/ ?>