<?php $__env->startSection('title'); ?>
    <title>نتیجه پرداخت</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>

    <div class="container">
        <div class="row text-center" style="height: 80vh">
            <div class="col-sm-12 my-auto mx-auto bg-off-color rounded p-4" style="max-width: 500px">
                <?php if(isset($transAction) and $transAction->status): ?>
                    <h3 class="text-success"><?php echo e($message); ?></h3>
                    <h6 class="text-warning my-3">شماره پیگیری: <?php echo e($transAction->traceNumber); ?></h6>
                <?php endif; ?>
                <?php if(isset($transAction) and $transAction->comeFrom == 'web'): ?>
                    <a href="<?php if(auth()->guard()->check()): ?> <?php echo e(route('panel')); ?> <?php else: ?> <?php echo e(route('home')); ?> <?php endif; ?>"
                       class="btn btn-primary">بازگشت</a>
                <?php elseif(isset($transAction) and $transAction->comeFrom == 'app'): ?>
                    <a href="<?php echo e(env('SUCCESS_ANDROID_LINK')); ?>" class="btn btn-primary">بازگشت به اپلیکیشن</a>
                <?php else: ?>
                    <h3 class="text-danger"><?php echo e($message); ?></h3>
                    <h6 class="text-warning my-3">درصورت کم شدن وجه از حساب به صورت خودکار پس از ۷۲ ساعت به حساب شما
                        برگشت زده خواهد شد</h6>
                    <a href="<?php if(auth()->guard()->check()): ?> <?php echo e(route('panel')); ?> <?php else: ?> <?php echo e(route('home')); ?> <?php endif; ?>"
                       class="btn btn-primary">بازگشت</a>
                    <a href="<?php echo e(env('FAILED_ANDROID_LINK')); ?>"
                       class="btn btn-primary">بازگشت به اپلیکیشن</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/pay/callback.blade.php ENDPATH**/ ?>