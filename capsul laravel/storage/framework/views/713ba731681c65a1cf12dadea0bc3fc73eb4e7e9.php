<?php $__env->startSection('content'); ?>
    <style>
        body {
            background-color: #042F3A !important;
        }

        .english-word {
            font-weight: bold;
            background: #ddd;
            padding: 0 5px;
            border-radius: 4px;
            font-size: 12px;
            color: black;

        }

        .icon-share {
            width: 16px;
            vertical-align: text-bottom;
            margin: 0 4px;
        }

        .font-ios {
            font-size: 13px;
        }
    </style>
    <div class="container font-ios" style="margin-top: 120px">
        <?php if(isset($safari)): ?>
            <div class="card mx-auto p-3 " style="max-width: 500px">
                <div class="">وب اپلیکیشن <span class="text-danger">کپسول</span> را به صفحه اصلی موبایل خود اضافه کنید.
                </div>

                <div class=""
                     style="display: list-item;font-size: 14px;text-align: center;list-style: none;line-height: 2;">
                    برای نصب اپلیکیشن کپسول این صفحه را در مرورگر <span class="english-word">Safari(سافاری)</span> باز
                    کنید:
                </div>

                <ul class="">
                    <li class="my-2">
                        1- در نوار پایین دکمه <img src="<?php echo e(asset('home/images/icon-share.png')); ?>" class="icon-share"> را
                        انتخاب کنید.
                    </li>
                    <li class="my-2">
                        2- منوی بازشده را به چپ بکشید و گزینه <span class="english-word">"Add to home screen"</span> را
                        انتخاب کنید.
                        <img src="<?php echo e(asset('home/images/add-to-home-screen.png')); ?>" class="mw-100 mt-1"></li>
                    <li class="my-2">
                        3- در مرحله بعد در قسمت بالا روی <span class="english-word">"Add"</span> یا <span
                                class="english-word">"save"</span> کلیک کنید.
                    </li>
                </ul>
                <div class="text-center">
                    <div class="text-danger">از نوار پایین مراحل را شروع کنید!</div>
                    <div class="bounce"><img src="<?php echo e(asset('home/images/icon-down.png')); ?>" style="width: 20px;"></div>
                </div>
            </div>
        <?php else: ?>
            <div class="card mx-auto p-3 " style="max-width: 500px">
                <div>
                    <h4 class="text-center text-danger">
                        لطفا با مرورگر سافاری وارد شوید.
                    </h4>
                </div>
                <?php endif; ?>
            </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/ios.blade.php ENDPATH**/ ?>