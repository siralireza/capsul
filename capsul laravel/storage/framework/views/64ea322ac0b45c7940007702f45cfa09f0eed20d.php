<?php $__env->startSection('content'); ?>
    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title">پیشنهادات</h3>
        </div>
        <div class="card-body row">
            <?php if($complaints->isEmpty()): ?>
                <div class="w-100 text-center">
                    <h5 class="text-warning">پیشنهادی ثبت نشده است</h5>

                </div>
            <?php endif; ?>
            <?php $__currentLoopData = $complaints; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="card card-light col-lg-4">
                    <div class="card-header">
                        <span class="ill-name text-white"><?php echo e($comp->title); ?></span>
                        <?php
                            $dateTime = new \Hekmatinasser\Verta\Verta($comp->created_at);
                            $date = $dateTime->format('Y/n/j');
                        ?>
                        <span class="pull-left text-white"><?php echo e($date); ?></span><br>
                        <?php if($comp->status == 'accepted'): ?>
                            <span class="float-left badge badge-success"><?php echo e(translateCompStatus($comp)); ?></span>
                        <?php elseif($comp->status == 'waiting'): ?>
                            <span class="float-left badge badge-warning"><?php echo e(translateCompStatus($comp)); ?></span>
                        <?php elseif($comp->status == 'reserved'): ?>
                            <span class="float-left badge badge-info"><?php echo e(translateCompStatus($comp)); ?></span>
                        <?php elseif($comp->status == 'finished'): ?>
                            <span class="float-left badge badge-dark"><?php echo e(translateCompStatus($comp)); ?></span>
                        <?php else: ?>
                            <span class="float-left badge badge-danger"><?php echo e(translateCompStatus($comp)); ?></span>
                        <?php endif; ?>


                    </div>
                    <div class="card-body">
                        <h5>توضیحات: </h5>
                        <p><?php echo e($comp->desc); ?></p>
                        <div class="form-group">
                            <label for="inputForDiag">پاسخ:</label>
                            <?php if($comp->answer): ?>
                                <textarea class="form-control mt-3" id="inputForDiag" rows="1"
                                          disabled><?php echo e($comp->answer); ?></textarea>
                            <?php else: ?>
                                <p class="text-warning" id="inputForAdvise">پاسخی دریافت نشده است</p>
                            <?php endif; ?>
                        </div>
                    </div>








                </div>
                    <?php if($comp->user_id == userId() and $comp->status == 'waiting'): ?>
                        <div class="modal fade" id="deleteModal<?php echo e($loop->index); ?>" tabindex="-1" role="dialog" aria-labelledby="rateModal"
                             aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">حذف پیشنهاد</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="<?php echo e(route('complaints.destroy',['id' => $comp->id])); ?>" method="post">
                                        <?php echo csrf_field(); ?>
                                        <?php echo method_field('delete'); ?>
                                        <div class="modal-body">
                                            <h4 class="text-danger">آیا مطمئن هستید که این پیشنهاد حذف شود؟</h4>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success mx-2">بله</button>
                                            <button type="button" class="btn btn-primary justify-content-end" data-dismiss="modal">خیر
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <div class="card-footer text-center w-100">
                <a href="<?php echo e(route('complaints.create')); ?>" class="btn btn-lg btn-primary">ثبت پیشنهاد جدید</a>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.doctor.doctorPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/doctor/complaints/index.blade.php ENDPATH**/ ?>