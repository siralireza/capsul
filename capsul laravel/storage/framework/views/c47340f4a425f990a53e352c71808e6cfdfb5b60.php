<?php $__env->startSection('content'); ?>
    <?php if($errors->any()): ?>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <p class="alert alert-class alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    &times;
                </button>
                <?php echo e($error); ?></p>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
    <div class="card card-primary mt-3 mx-auto" style="max-width: 500px">
        <div class="card-header">
            <h3 class="card-title">مشاهده درخواست</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="form-group">
                <?php if($req->Client): ?>
                    <?php if($req->Client->User->pic): ?>
                        <div class="image text-center">
                            <img src="<?php echo e($req->Client->User->pic); ?>" class="img-circle elevation-2" alt="User Image"
                                 style="width: 4.5rem">
                        </div>
                    <?php else: ?>
                        <div class="image text-center">
                            <img src="<?php echo e(asset('dist/img/avatar04.png')); ?>" class="img-circle elevation-2"
                                 alt="User Image"
                                 style="width: 4.5rem">
                        </div>
                    <?php endif; ?>
                    <h5>نام: <?php echo e($req->Client->User->fname . ' '. $req->Client->User->lname); ?></h5>
                    <h6>سن: <?php echo e($req->Client->age ?? 'تعیین نشده'); ?></h6>
                    <h6>جنسیت: <?php echo e($req->Client->User->sex == 'male' ? 'مرد' : 'زن'); ?></h6>
                <?php endif; ?>


            </div>

        </div>
        <!-- /.card-body -->
        
        <div class="card-footer text-center">
            <div class="mx-auto text-center row">
                <?php if($req->status == 'accepted'): ?>
                    <span class="my-1 bg-success rounded mx-auto p-2">وضعیت: <?php echo e(translateRequestStatus($req)); ?></span>
                <?php elseif($req->status == 'waiting'): ?>
                    <span class="my-1 bg-warning rounded mx-auto p-2">وضعیت: <?php echo e(translateRequestStatus($req)); ?></span>
                <?php elseif($req->status == 'reserved'): ?>
                    <span class="my-1 bg-info rounded mx-auto p-2">وضعیت: <?php echo e(translateRequestStatus($req)); ?></span>
                <?php elseif($req->status == 'finished'): ?>
                    <span class="my-1 bg-dark rounded mx-auto p-2">وضعیت: <?php echo e(translateRequestStatus($req)); ?></span>
                <?php else: ?>
                    <span class="my-1 bg-danger rounded mx-auto p-2">وضعیت: <?php echo e(translateRequestStatus($req)); ?></span>
                <?php endif; ?>
                <button type="button" class="mt-3 col-12 btn btn-lg btn-info" data-toggle="modal"
                        data-target="#adviseModal">
                    مشاهده
                    توضیحات
                </button>
                <button type="button" class="mt-3 col-12 btn btn-lg btn-dark" data-toggle="modal"
                        data-target="#historyModal">مشاهده سوابق پزشکی
                </button>
                <?php if($req->status == 'accepted'): ?>
                    <a href="<?php echo e(route('chats.show',['id' => $req->id])); ?>" class="mt-3 col-12 btn btn-lg btn-success">انتقال
                        به صفحه پرسش و پاسخ</a>

                    <button class="mt-3 col-12 btn btn-lg btn-warning" data-toggle="modal" data-target="#rejectModal">رد
                        درخواست
                    </button>
                    <button class="mt-3 col-12 btn btn-lg btn-danger" data-toggle="modal" data-target="#closeModal">بستن
                        درخواست
                    </button>

                <?php elseif($req->status == 'waiting'): ?>
                    <button class="mt-3 col-12 btn btn-lg btn-success" data-toggle="modal" data-target="#acceptModal">
                        قبول
                        درخواست
                    </button>
                    <button class="mt-3 col-12 btn btn-lg btn-warning" data-toggle="modal" data-target="#rejectModal">رد
                        درخواست
                    </button>
                <?php elseif($req->status == 'reserved'): ?>
                    <a href="<?php echo e(route('chats.show',['id' => $req->id])); ?>"
                       class="mt-3 col-12 btn btn-lg btn-info disabled">پرسش از
                        پزشک</a>
                <?php elseif($req->status == 'finished'): ?>
                    <a href="<?php echo e(route('chats.show',['id' => $req->id])); ?>" class="mt-3 col-12 btn btn-lg btn-info">مشاهده
                        پیام‌ها</a>
                <?php else: ?>
                    <a href="<?php echo e(route('chats.show',['id' => $req->id])); ?>"
                       class="mt-3 col-12 btn btn-lg btn-info disabled">پرسش از
                        پزشک</a>
                <?php endif; ?>
                <?php if($req->user_id == userId() and $req->status == 'waiting'): ?>

                    <button type="submit" class="mt-3 col-12 btn btn-lg btn-danger" data-toggle="modal"
                            data-target="#deleteModal">حذف
                    </button>
                <?php endif; ?>
                <a href="<?php echo e(route('reqs.index').'?status[0]='.$req->status); ?>" class="mt-3 col-12 btn btn-lg btn-primary">بازگشت</a>
            </div>
        </div>

    </div>

    <div class="modal fade mt-5" id="adviseModal" tabindex="-1" role="dialog" aria-labelledby="adviseModal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">مشاهده توصیه پزشک</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputForName">دلیل اصلی مراجعه به پزشک</label>
                        <input type="text" class="form-control" value="<?php echo e($req->main_reason); ?>" id="inputForName"
                               disabled>
                    </div>
                    <?php
                        $flag = true;
                    ?>
                    <?php for($i = 1;$i<4;$i ++): ?>
                        <?php if($req->{'answer'.$i}): ?>
                            <?php if($flag): ?>
                                <hr class="bg-white">
                                <h6>توضیحات وارد شده</h6>
                                <?php ($flag = false); ?>
                            <?php endif; ?>
                            <textarea class="form-control mt-3" rows="1"
                                      disabled><?php echo e($req->{'answer'.$i}); ?></textarea>
                        <?php endif; ?>
                    <?php endfor; ?>
                    <div class="form-group">
                        <label for="inputForAdvise">توصیه</label>
                        <?php if($req->status == 'waiting' or $req->status == 'reserved'): ?>
                            <p class="text-warning" id="inputForAdvise">توصیه پزشک پس از تایید درخواست وارد می‌شود</p>
                        <?php else: ?>
                            <textarea class="form-control mt-3" id="inputForAdvise" rows="1"
                                      disabled><?php echo e($req->dr_first_answer); ?></textarea>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <label for="inputForDiag">تشخیص نهایی</label>
                        <?php if($req->status == 'finished'): ?>
                            <textarea class="form-control mt-3" id="inputForDiag" rows="1"
                                      disabled><?php echo e($req->final_diagnosis); ?></textarea>
                        <?php else: ?>
                            <p class="text-warning" id="inputForAdvise">تشخیص نهایی پس از پایان درخواست وارد می‌شود</p>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <label for="inputForDiag">مراجعه مجدد</label>
                        <?php if($req->status == 'finished'): ?>
                            <?php if($req->re_visit): ?>
                                <textarea class="form-control mt-3" id="inputForDiag" rows="1"
                                          disabled><?php echo e($req->re_visit); ?></textarea>
                            <?php else: ?>
                                <p class="text-warning" id="inputForAdvise">دکتر اطلاعتی مربوط به مراجعه مجدد برای شما
                                    ثبت نکرده است</p>
                            <?php endif; ?>
                        <?php else: ?>
                            <p class="text-warning" id="inputForAdvise">مراجعه مجدد پس از پایان درخواست مشخص می‌شود</p>
                        <?php endif; ?>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">بستن</button>
                </div>
            </div>
        </div>
    </div>
    <?php if($req->status == 'waiting'): ?>

        <div class="modal fade" id="acceptModal" tabindex="-1" role="dialog" aria-labelledby="rateModal"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">قبول درخواست</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="<?php echo e(route('acceptReq',['req' => $req])); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <div class="modal-body">
                            <div class="form-group  mt-3">
                                <label for="inputForNoskhe mr-1">توصیه اولیه</label>
                                <textarea class="form-control <?php if ($errors->has('first_answer')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('first_answer'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                          name="first_answer"
                                          id="inputForNoskhe" rows="3"
                                          placeholder="لطفا پاسخ خود را وارد نمایید" required></textarea>
                            </div>
                            <?php if ($errors->has('first_answer')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('first_answer'); ?>
                            <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-success mx-2">قبول و ارسال پاسخ</button>
                            <button type="button" class="btn btn-primary justify-content-end" data-dismiss="modal">بستن
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rateModal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">رد درخواست</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo e(route('rejectReq',['id' => $req->id])); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-10 control-label" for="inputReason">دلیل رد کردن درخواست را انتخاب
                                کنید:&nbsp;<i
                                        class="fa fa-star text-danger"></i></label>
                            <div class="col-sm-10">

                                <select class="form-control <?php if ($errors->has('reject_reason')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('reject_reason'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                        id="inputReason" name="reject_reason" required>
                                    <option value="presence">حضوری</option>
                                    <option value="emergency">اورژانس</option>
                                    <option value="other_exp">تخصص دیگر</option>
                                </select>
                                <?php if ($errors->has('reject_reason')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('reject_reason'); ?>
                                <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="submit" class="btn btn-success mx-2">تایید</button>
                        <button type="button" class="btn btn-primary justify-content-end" data-dismiss="modal">بازگشت
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <div class="modal fade mt-5" id="historyModal" tabindex="-1" role="dialog" aria-labelledby="rateModal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">سوابق پزشکی</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <?php ($secondHistoryFlag = true); ?>
                        <?php $__currentLoopData = $userHistory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $history): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($loop->last): ?>
                                <div class="col-md-6">
                                    <h5 class="text-warning">سوابق اولیه</h5>
                                    <?php if(!($history->answer1 and
                                            $history->answer2 and
                                            $history->answer3 and
                                            $history->answer4 and
                                            $history->answer5)): ?>
                                        <p class="text-danger">چیزی درج نشده است</p>
                                    <?php endif; ?>
                                    <?php if($history->answer1): ?>
                                        <div class="border border-warning p-2 my-2">
                                            <p>سابقه بیماری خاص:</p>
                                            <p><?php echo e($history->answer1); ?></p>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($history->answer2): ?>
                                        <div class="border border-warning p-2 my-2">
                                            <p>سابقه دارویی:</p>
                                            <p><?php echo e($history->answer2); ?></p>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($history->answer3): ?>
                                        <div class="border border-warning p-2 my-2">
                                            <p>سابقه بستری یا جراحی:</p>
                                            <p><?php echo e($history->answer3); ?></p>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($history->answer4): ?>
                                        <div class="border border-warning p-2 my-2">
                                            <p>سابقه بیماری در خانواده:</p>
                                            <p><?php echo e($history->answer4); ?></p>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($history->answer5): ?>
                                        <div class="border border-warning p-2 my-2">
                                            <p>سابقه حساسیت غذایی یا دارویی:</p>
                                            <p><?php echo e($history->answer5); ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>


                            <?php else: ?>
                                <?php if($secondHistoryFlag): ?>
                                    <div class="col-md-6">
                                        <h5 class="text-info">سوابق ثانویه</h5>

                                        <?php ($secondHistoryFlag = false); ?>
                                        <?php endif; ?>
                                        <div class="border <?php if($history->added_by == 'dr'): ?> border-success <?php endif; ?> p-2 my-2">
                                            <span class="pull-left text-white"><?php echo e(toJalali($history->created_at)); ?></span>
                                            <?php if($history->added_by == 'dr'): ?>
                                                <p class="text-success">توسط دکتر</p>
                                            <?php endif; ?>
                                            <p>شکایت اولیه: <?php echo e($history->sick); ?></p>
                                            <p>توضیحات:</p>
                                            <p><?php echo e($history->desc); ?></p>
                                        </div>

                                    </div>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-primary justify-content-end" data-dismiss="modal">بازگشت
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="closeModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">بستن درخواست</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo e(route('closeReq',['req'=>$req])); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="modal-body">
                        <div class="form-group  mt-3">
                            <label for="inputForTashkhis mr-1">تشخیض نهایی</label>
                            <textarea class="form-control <?php if ($errors->has('final_diagnosis')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('final_diagnosis'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                      name="final_diagnosis"
                                      id="inputForTashkhis" rows="1"
                                      placeholder="یک جمله توضیح داده شود" required></textarea>
                        </div>
                        <?php if ($errors->has('final_diagnosis')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('final_diagnosis'); ?>
                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                        <div class="form-group">
                            <div class=" col-sm-10">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="add_history" value="1"
                                           id="exampleCheck2" <?php echo e(old('add_history') ? 'checked' : ''); ?>>
                                    <label class="form-check-label" for="exampleCheck2">به سابقه پزشکی بیمار اضافه
                                        شود</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group  mt-3">
                            <label for="inputForRevisit mr-1">مراجعه مجدد</label>
                            <textarea class="form-control <?php if ($errors->has('re_visit')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('re_visit'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                      name="re_visit"
                                      id="inputForRevisit" rows="3"
                                      placeholder="در صورت نیاز مراجعه مجدد توضیحات و تاریخ ذکر شود"></textarea>
                        </div>
                        <?php if ($errors->has('re_visit')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('re_visit'); ?>
                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="submit" class="btn btn-success">تایید</button>
                        <button type="button" class="btn btn-primary">بازگشت</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.doctor.doctorPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/doctor/requests/show.blade.php ENDPATH**/ ?>