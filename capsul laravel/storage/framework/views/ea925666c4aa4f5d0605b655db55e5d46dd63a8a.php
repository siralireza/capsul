<?php $__env->startSection('title'); ?>
    <title>وبسایت کپسول</title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('body'); ?>
    <?php if(Session::has('message')): ?>
        <div class="text-center mx-auto my-4 five-second-fade">
        <span class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?> alert-dismissible
        text-cente">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="margin-left: 8px!important;">&times;
        </button>
        <?php echo e(Session::get('message')); ?></span>
        </div>
    <?php endif; ?>
    <div class="container">
        <div class="row" style="height: 100vh">
            <div class="col-sm-12 my-auto mx-auto" style="max-width: 500px">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">فراموشی رمز عبور</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="<?php echo e(route('forgotPassSMS')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-6 control-label">شماره موبایل</label>

                                <div class="col-sm-10">
                                    <input type="text" name="phone"
                                           class="form-control <?php if ($errors->has('phone')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('phone'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="inputEmail3"
                                           placeholder="شماره موبایل خود را وارد کنید" value="<?php echo e(old('phone')); ?>" required
                                           autocomplete="phone" autofocus>
                                    <?php if ($errors->has('phone')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('phone'); ?>
                                    <span class="invalid-feedback text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">ارسال پیامک</button>


                            <a href="<?php echo e(route('login')); ?>" class="btn btn-outline-primary mx-1 float-left">صفحه ورود</a>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>

        </div>


    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/auth/forgotPass.blade.php ENDPATH**/ ?>