<?php $__env->startSection('content'); ?>
    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title">
                <?php echo e(translateRequestTitle($type)); ?>

            </h3>
        </div>
        <div class="card-body row">
            <?php if($reqs->isEmpty()): ?>
                <div class="text-center w-100">
                    <h5 class="text-warning">در این بخش درخواستی وجود ندارد</h5>
                </div>
            <?php endif; ?>
            <?php $__currentLoopData = $reqs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $req): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="card col-lg-4">
                    <div class="card-body">
                        <span class="my-1 ill-name"><?php echo e(translateRequestType($req)); ?></span>
                        <?php
                                $dateTime = new \Hekmatinasser\Verta\Verta($req->created_at);
                                $date = $dateTime->format('Y/n/j');
                        ?>
                        <span class="my-1 float-left small mt-1"><?php echo e($date); ?></span><br>
                        <span class="text-info">نوع تخصص: <?php echo e($req->Expertise->expertise_name); ?></span><br>

                    <?php if($req->status == 'accepted'): ?>
                            <span class="my-1 badge badge-success"><?php echo e(translateRequestStatus($req)); ?></span>
                        <?php elseif($req->status == 'waiting'): ?>
                            <span class="my-1 badge badge-warning"><?php echo e(translateRequestStatus($req)); ?></span>
                        <?php elseif($req->status == 'reserved'): ?>
                            <span class="my-1 badge badge-info"><?php echo e(translateRequestStatus($req)); ?></span>
                        <?php elseif($req->status == 'finished'): ?>
                            <span class="my-1 badge badge-dark"><?php echo e(translateRequestStatus($req)); ?></span>
                        <?php else: ?>
                            <span class="my-1 badge badge-danger"><?php echo e(translateRequestStatus($req)); ?></span>
                        <?php endif; ?>

                        <?php if($req->status == 'finished'): ?>
                            <?php if($req->score): ?>
                                <a href="#"><i
                                            class="my-1 fa fa-star mt-2 fa-3x float-left"
                                            style="color: #fffc00;" onclick="return false;"
                                            data-toggle="tooltip" data-placement="left"
                                            title="امتیاز ثبت شده: <?php echo e($req->score); ?>"
                                    ></i></a><br>

                            <?php else: ?>
                                <a href="#"><i
                                            class="my-1 fa fa-star-o mt-2 fa-3x float-left"
                                            style="color: #fffc00;" onclick="return false;"
                                            data-toggle="tooltip" data-placement="left"
                                            title="از بخش پیگیری میتوانید امتیاز خود را ثبت کنید"
                                    ></i></a><br>
                            <?php endif; ?>
                        <?php else: ?>
                            <a href="#"><i
                                        class="my-1 fa fa-star-o mt-2 fa-3x float-left"
                                        style="color: #fffc00;" onclick="return false;"
                                        data-toggle="tooltip" data-placement="left"
                                        title="برای امتیاز دهی باید درخواست پایان یافته باشد"
                                ></i></a><br>
                        <?php endif; ?>
                        <div class="d-flex my-3">
                            <?php if($req->Doctor and $req->Doctor->User->pic): ?>
                                <div class="image">
                                    <img src="<?php echo e($req->Doctor->User->pic); ?>" class="img-circle elevation-2"
                                         alt="User Image"
                                         style="width: 3.5rem">
                                </div>
                            <?php elseif($req->Doctor): ?>
                                <div class="image">
                                    <img src="<?php echo e(asset('dist/img/avatar5.png')); ?>" class="img-circle elevation-2"
                                         alt="User Image"
                                         style="width: 3.5rem">
                                </div>
                            <?php endif; ?>

                            <div class="info mr-3">
                                <?php if($req->dr_id): ?>
                                    <a class="d-block"><?php echo e($req->Doctor->User->fname . ' '. $req->Doctor->User->lname); ?></a>
                                    <a class="d-block">شماره نظام پزشکی: <?php echo e($req->Doctor->dr_code); ?></a>
                                <?php else: ?>
                                    <p class="text-warning">مشخصات پزشک پس از تایید نمایش داده می‌شود</p>
                                <?php endif; ?>
                                <?php if($req->status == 'waiting'): ?>
                                    <p class="d-block text-danger">حداکثر زمان پاسخ
                                        گویی: <?php echo e(\App\CapsulSetting::latest()->first()->answer_time); ?> ساعت</p>
                                <?php elseif($req->status == 'reserved'): ?>
                                        <?php
                                            $date = new \Hekmatinasser\Verta\Verta($req->reserve_date);
                                            $reserveDate = $date->format('Y/n/j');
                                        ?>
                                    <p class="d-block text-danger">تاریخ رزرو: <?php echo e($reserveDate); ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <a href="<?php echo e(route('reqs.show',['req'=>$req])); ?>"
                           class="my-1 btn btn-dark btn-sm">پیگیری درخواست</a>
                        <br>
                    </div>
                </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
        <div class="card-footer text-center">
            <a href="<?php echo e(route('reqs.index')); ?>" class="btn btn-lg btn-primary">بازگشت</a>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.client.clientPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/client/requests/list.blade.php ENDPATH**/ ?>