<?php $__env->startSection('content'); ?>
    <div class="card card-primary mt-3 mx-auto" style="max-width: 500px">
        <div class="card-header">
            <h3 class="card-title">اطلاعات بیشتر</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="form-group">
                <?php if($req->Doctor): ?>
                    <?php if($req->Doctor->User->pic): ?>
                        <div class="image text-center">
                            <img src="<?php echo e($req->Doctor->User->pic); ?>" class="img-circle elevation-2" alt="User Image"
                                 style="width: 4.5rem">
                        </div>
                    <?php else: ?>
                        <div class="image text-center">
                            <img src="<?php echo e(asset('dist/img/avatar5.png')); ?>" class="img-circle elevation-2" alt="User Image"
                                 style="width: 4.5rem">
                        </div>
                    <?php endif; ?>
                    <h5>نام: <?php echo e($req->Doctor->User->fname . ' '. $req->Doctor->User->lname); ?></h5>
                    <h6>نوع تخصص: <?php echo e($req->Expertise->expertise_name); ?></h6>
                    <h6>کد علوم پزشکی: <?php echo e($req->Doctor->dr_code); ?></h6>
                <?php else: ?>
                    <p class="text-warning">مشخصات پزشک پس از تایید نمایش داده می‌شود</p>
                <?php endif; ?>
                <?php if($req->status == 'waiting'): ?>
                    <a class="d-block text-danger">حداکثر زمان پاسخ
                        گویی: <?php echo e(\App\CapsulSetting::latest()->first()->answer_time); ?> ساعت</a>
                <?php endif; ?>


            </div>

        </div>
        <!-- /.card-body -->
        
        <div class="card-footer text-center">
            <div class="mx-auto text-center row">
                <?php if($req->status == 'accepted'): ?>
                    <span class="my-1 bg-success rounded mx-auto p-2">وضعیت: <?php echo e(translateRequestStatus($req)); ?></span>
                <?php elseif($req->status == 'waiting'): ?>
                    <span class="my-1 bg-warning rounded mx-auto p-2">وضعیت: <?php echo e(translateRequestStatus($req)); ?></span>
                <?php elseif($req->status == 'reserved'): ?>
                    <span class="my-1 bg-info rounded mx-auto p-2">وضعیت: <?php echo e(translateRequestStatus($req)); ?></span>
                <?php elseif($req->status == 'finished'): ?>
                    <span class="my-1 bg-dark rounded mx-auto p-2">وضعیت: <?php echo e(translateRequestStatus($req)); ?></span>
                <?php elseif($req->status == 'rejected'): ?>
                    <span class="my-1 bg-danger rounded mx-auto p-2">وضعیت: <?php echo e(translateRequestStatus($req)); ?></span>
                <?php else: ?>
                    <span class="my-1 bg-danger rounded mx-auto p-2">وضعیت: <?php echo e(translateRequestStatus($req)); ?></span>
                <?php endif; ?>
                <?php if($req->status == 'accepted'): ?>
                    <button type="button" class="mt-3 col-12 btn btn-lg btn-info" data-toggle="modal"
                            data-target="#adviseModal">
                        مشاهده
                        توصیه پزشک
                    </button>
                <?php elseif($req->status == 'waiting'): ?>
                    <button type="button" class="mt-3 col-12 btn btn-lg btn-info" data-toggle="modal"
                            data-target="#adviseModal">مشاهده توضیحات
                    </button>
                <?php elseif($req->status == 'reserved'): ?>
                    <button type="button" class="mt-3 col-12 btn btn-lg btn-info" data-toggle="modal"
                            data-target="#adviseModal">مشاهده توضیحات
                    </button>
                <?php elseif($req->status == 'finished'): ?>
                    <button type="button" class="mt-3 col-12 btn btn-lg btn-info" data-toggle="modal"
                            data-target="#adviseModal">
                        مشاهده
                        توصیه پزشک
                    </button>
                <?php elseif($req->status == 'rejected'): ?>
                    <button class="btn btn-white text-danger disabled">علت رد شدن: <?php echo e(translateRejectReason($req)); ?></button>
                <?php else: ?>
                    <span class="my-1 bg-danger rounded mx-auto p-2">وضعیت: <?php echo e(translateRequestStatus($req)); ?></span>
                <?php endif; ?>

                <?php if($req->status == 'accepted'): ?>
                    <a href="<?php echo e(route('chats.show',['id' => $req->id])); ?>" class="mt-3 col-12 btn btn-lg btn-success">پرسش
                        از
                        پزشک</a>
                    
                    
                    
                    
                    
                    
                    
                    
                <?php elseif($req->status == 'finished'): ?>
                    <a href="<?php echo e(route('chats.show',['id' => $req->id])); ?>" class="mt-3 col-12 btn btn-lg btn-dark">مشاهده
                        پیام‌ها</a>
                    
                    
                    
                    
                <?php endif; ?>

                <?php if($req->status == 'finished'): ?>
                    <button type="button" class="mt-3 col-12 btn btn-lg btn-light" data-toggle="modal"
                            data-target="#rateModal">امتیاز
                        به
                        پزشک
                    </button>
                <?php endif; ?>
                <?php if($req->user_id == userId() and $req->status == 'waiting'): ?>

                    <button type="submit" class="mt-3 col-12 btn btn-lg btn-danger" data-toggle="modal"
                            data-target="#deleteModal">حذف
                    </button>
                <?php endif; ?>
                <a href="<?php echo e(route('reqs.index').'?status[0]='.$req->status); ?>" class="mt-3 col-12 btn btn-lg btn-primary">بازگشت</a>
            </div>
        </div>

    </div>

    <div class="modal fade mt-5" id="adviseModal" tabindex="-1" role="dialog" aria-labelledby="adviseModal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">مشاهده توصیه پزشک</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputForName">دلیل اصلی مراجعه به پزشک</label>
                        <input type="text" class="form-control" value="<?php echo e($req->main_reason); ?>" id="inputForName"
                               disabled>
                    </div>
                    <div class="form-group">
                        <label for="inputForAdvise">توصیه پزشک</label>
                        <?php if($req->status == 'waiting' or $req->status == 'reserved'): ?>
                            <p class="text-warning" id="inputForAdvise">توصیه پزشک پس از تایید درخواست وارد می‌شود</p>
                        <?php else: ?>
                            <textarea class="form-control mt-3" id="inputForAdvise" rows="1"
                                      disabled><?php echo e($req->dr_first_answer); ?></textarea>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <label for="inputForDiag">تشخیص نهایی پزشک</label>
                        <?php if($req->status == 'finished'): ?>
                            <textarea class="form-control mt-3" id="inputForDiag" rows="1"
                                      disabled><?php echo e($req->final_diagnosis); ?></textarea>
                        <?php else: ?>
                            <p class="text-warning" id="inputForAdvise">تشخیص نهایی پس از پایان درخواست وارد می‌شود</p>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <label for="inputForDiag">مراجه مجدد</label>
                        <?php if($req->status == 'finished'): ?>
                            <?php if($req->re_visit): ?>
                                <textarea class="form-control mt-3" id="inputForDiag" rows="1"
                                          disabled><?php echo e($req->re_visit); ?></textarea>
                            <?php else: ?>
                                <p class="text-warning" id="inputForAdvise">دکتر اطلاعتی مربوط به مراجعه مجدد برای شما
                                    ثبت نکرده است</p>
                            <?php endif; ?>
                        <?php else: ?>
                            <p class="text-warning" id="inputForAdvise">مراجعه مجدد پس از پایان درخواست مشخص می‌شود</p>
                        <?php endif; ?>
                    </div>
                    <?php
                        $flag = true;
                    ?>
                    <?php for($i = 1;$i<4;$i ++): ?>
                        <?php if($req->{'answer'.$i}): ?>
                            <?php if($flag): ?>
                                <hr class="bg-white">
                                <h6>توضیحات وارد شده</h6>
                                <?php ($flag = false); ?>
                            <?php endif; ?>
                            <textarea class="form-control mt-3" rows="1"
                                      disabled><?php echo e($req->{'answer'.$i}); ?></textarea>
                        <?php endif; ?>
                    <?php endfor; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">بستن</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="rateModal" tabindex="-1" role="dialog" aria-labelledby="rateModal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">امتیاز به پزشک</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo e(route('reqs.update',['req' => $req])); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('patch'); ?>
                    <div class="modal-body">
                        <?php if($req->status != 'finished'): ?>
                            <h5 class="text-warning">برای امتیاز به پزشک باید درخواست پایان یافته باشد</h5>
                        <?php else: ?>
                            <h5 class="mb-3">نام
                                پزشک: <?php echo e($req->Doctor->User->fname . ' '. $req->Doctor->User->lname); ?></h5>
                            <div class="form-group">
                                <div class="rate mx-auto text-center">
                                    <input type="radio" id="star5" name="score" value="5"/>
                                    <label for="star5" title="text">5 stars</label>
                                    <input type="radio" id="star4" name="score" value="4"/>
                                    <label for="star4" title="text">4 stars</label>
                                    <input type="radio" id="star3" name="score" value="3"/>
                                    <label for="star3" title="text">3 stars</label>
                                    <input type="radio" id="star2" name="score" value="2"/>
                                    <label for="star2" title="text">2 stars</label>
                                    <input type="radio" id="star1" name="score" value="1" checked/>
                                    <label for="star1" title="text">1 star</label>
                                    <?php if ($errors->has('score')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('score'); ?>
                                    <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>

                                <div class="mt-3">
                                    <div class="form-check mt-2">
                                        <label class="form-check-label">
                                            <input type="checkbox" name="rateAnswer1"
                                                   class="size-radio form-check-input" value="1">
                                            <span class="mr-2">دکتر به موقع پاسخ نداد</span>
                                        </label>
                                        <?php if ($errors->has('rateAnswer1')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('rateAnswer1'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                        </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                    <div class="form-check mt-2">
                                        <label class="form-check-label">
                                            <input type="checkbox" name="rateAnswer2"
                                                   class="size-radio form-check-input" value="1">
                                            <span class="mr-2">دارو تاثیر گذار نبود</span>
                                        </label>
                                        <?php if ($errors->has('rateAnswer2')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('rateAnswer2'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                        </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                    <div class="form-check mt-2">
                                        <label class="form-check-label">
                                            <input type="checkbox" name="rateAnswer3"
                                                   class="size-radio form-check-input" value="1">
                                            <span class="mr-2">برخورد پزشک مناسب نبود</span>
                                        </label>
                                        <?php if ($errors->has('rateAnswer3')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('rateAnswer3'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                        </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                            </div>

                        <?php endif; ?>
                    </div>
                    <div class="modal-footer <?php if($req->status == 'finished'): ?> justify-content-between <?php endif; ?>">
                        <?php if($req->status == 'finished'): ?>
                            <button type="submit" class="btn btn-success mx-2">ثبت</button>
                        <?php endif; ?>
                        <button type="button" class="btn btn-primary justify-content-end" data-dismiss="modal">بستن
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <?php if($req->user_id == userId() and $req->status == 'waiting'): ?>
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="rateModal"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">حذف درخواست</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="<?php echo e(route('reqs.destroy',['id' => $req->id])); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <?php echo method_field('delete'); ?>
                        <div class="modal-body">
                            <h4 class="text-danger">آیا مطمئن هستید که این درخواست حذف شود؟</h4>
                        </div>
                        <div class="modal-footer <?php if($req->status == 'finished'): ?> justify-content-between <?php endif; ?>">
                            <button type="submit" class="btn btn-success mx-2">بله</button>
                            <button type="button" class="btn btn-primary justify-content-end" data-dismiss="modal">خیر
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.client.clientPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/client/requests/show.blade.php ENDPATH**/ ?>