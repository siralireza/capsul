<?php $__env->startSection('content'); ?>
    <div class="row mt-3">
        <div class="col-12 mx-auto" style="max-width: 500px">
            <div class="card card-primary mt-3">
                <div class="card-header">
                    <h3 class="card-title">ثبت پیشنهاد</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="<?php echo e(route('complaints.store')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title" class="col-sm-10 control-label">عنوان پیشنهاد</label>

                            <div class="col-sm-10">
                                    <textarea name="title" rows=2
                                              class="form-control <?php if ($errors->has('title')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('title'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="title"
                                              placeholder="عنوان شکایت را وارد کنید"><?php echo e(old('title')); ?></textarea>
                                <?php if ($errors->has('title')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('title'); ?>
                                <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                        </div>




















                        <div class="form-group">
                            <label for="desc" class="col-sm-10 control-label">توضیحات را وارد کنید</label>

                            <div class="col-sm-10">
                                    <textarea name="desc" rows=2
                                              class="form-control <?php if ($errors->has('desc')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('desc'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="desc"
                                              placeholder="توضیحات مربوطه را بنویسید"><?php echo e(old('desc')); ?></textarea>
                                <?php if ($errors->has('desc')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('desc'); ?>
                                <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">تایید</button>
                        <a href="<?php echo e(route('complaints.index')); ?>" class="btn btn-primary pull-left">بازگشت</a></div>
                </form>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.doctor.doctorPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/doctor/complaints/create.blade.php ENDPATH**/ ?>