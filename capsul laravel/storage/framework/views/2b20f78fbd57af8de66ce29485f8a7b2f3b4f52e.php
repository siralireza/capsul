<?php $__env->startSection('title'); ?>
    <title>یافت نشد</title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('body'); ?>
    <div class="container">
        <div class="row text-center" style="height: 100vh">
            <div class="col-sm-12 my-auto mx-auto" style="max-width: 500px">
                <h2> 404 | صفحه مورد نظر یافت نشد</h2>
                <a href="<?php echo e(URL::previous()); ?>" class="btn btn-primary mt-2">بازگشت</a>
                <a href="/" class="btn btn-success mt-2">ضفحه اصلی</a>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/errors/404.blade.php ENDPATH**/ ?>