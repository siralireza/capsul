<!doctype html>
<html lang="fa">

<head>
    <title>Capsul</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Mobland - Mobile App Landing Page Template">
    <meta name="keywords" content="HTML5, bootstrap, mobile, app, landing, ios, android, responsive">
    <meta name="apple-mobile-web-app-title" content="Capsul">
    <meta name="application-name" content="Capsul">
    <meta name="enamad" content="465215"/>
    <link rel="apple-touch-icon" href="<?php echo e(asset('home/images/capsul-logo.png')); ?>">
    <link rel='icon' href='<?php echo e(asset('home/images/favicon.ico')); ?>' type='image/x-icon'/ >
    <!-- Font -->
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('home/css/bootstrap.min.css')); ?>">
    <!-- Themify Icons -->
    <link rel="stylesheet" href="<?php echo e(asset('home/css/themify-icons.css')); ?>">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="<?php echo e(asset('home/css/owl.carousel.min.css')); ?>">
    <!-- Main css -->
    <link href="<?php echo e(asset('home/css/style.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('home/css/bootstrap-rtl.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('home/css/rtl.css')); ?>">
    <style>
        .bg-main-color {
            background-color: #042F3A !important;
        }

        .bg-off-color {
            background-color: #064657 !important;
        }
    </style>
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="30">
<!-- Nav Menu -->

<div class="nav-menu fixed-top ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-dark navbar-expand-lg">
                    <a class="navbar-brand" href="/"><img src="<?php echo e(asset('home/images/capsul-logo.png')); ?>"
                                                          style="width: 60px;height: 60px" class="img-fluid"
                                                          alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                            aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation"><span
                                class="navbar-toggler-icon"></span></button>
                    <li class="collapse navbar-collapse" id="navbar">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link <?php echo e(Request::path() == '/' ? 'active' : ''); ?>" href="/">خانه <span class="sr-only">(شما اینجا هستید)</span></a>
                            <li class="nav-item"><a class="nav-link <?php echo e(Request::path() == 'blog' ? 'active' : ''); ?>" href="<?php echo e(route('blog')); ?>">بلاگ</a></li>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="/#features">ویژگی ها</a></li>
                            
                            <li class="nav-item"><a class="nav-link" href="/#download">دانلود</a></li>
                            <li class="nav-item"><a class="nav-link" href="/#contact">تماس با ما</a></li>
                            <li class="nav-item"><a class="nav-link" href="/#contact">درباره ما</a></li>
                            <?php if(Route::has('login') or Route::has('register')): ?>
                                <?php if(auth()->guard()->check()): ?>
                                    <li class="nav-item"><a href="<?php echo e(route('panel')); ?>"
                                                            class="btn btn-outline-light my-3 my-sm-0 ml-lg-3">پنل
                                            کاربری</a></li>
                                <?php else: ?>
                                    <div class="btn-group nav-item">
                                        <li class="nav-item mx-auto">
                                            <a class="btn btn-sm btn-light my-3 my-sm-0 ml-lg-3" data-toggle="dropdown"
                                               href="#">
                                                ورود یا ثبت‌نام
                                                <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu rtl bg-off-color rounded">
                                                <li class="nav-item"><a class="nav-link bg-off-color"
                                                                        href="<?php echo e(route('login')); ?>">ورود</a></li>
                                                <li class="nav-item"><a class="nav-link bg-off-color"
                                                                        href="<?php echo e(route('registerClient')); ?>">ثبت‌نام
                                                        کاربر</a></li>
                                                <li class="nav-item"><a class="nav-link bg-off-color"
                                                                        href="<?php echo e(route('registerDoctor')); ?>">ثبت نام
                                                        متخصص</a></li>

                                                <!-- dropdown menu links -->
                                            </ul>
                                        </li>

                                    </div>
                                    
                                    

                                <?php endif; ?>
                            <?php endif; ?>

                        </ul>
                    </li>
                </nav>
            </div>
            </nav>
        </div>
    </div>
</div>
</div>


<?php echo $__env->yieldContent('content'); ?>
<!-- // end .section -->













<!-- jQuery and Bootstrap -->
<script src="<?php echo e(asset('home/js/jquery-3.2.1.min.js')); ?>"></script>
<script src="<?php echo e(asset('home/js/bootstrap.bundle.min.js')); ?>"></script>
<!-- Plugins JS -->
<script src="<?php echo e(asset('home/js/owl.carousel.min.js')); ?>"></script>
<!-- Custom JS -->
<script src="<?php echo e(asset('home/js/script.js')); ?>"></script>

</body>

</html>
<?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/layouts/index.blade.php ENDPATH**/ ?>