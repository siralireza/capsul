<?php $__env->startSection('title'); ?>
    <title>ورود به کپسول</title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('body'); ?>
    <?php if(Session::has('message')): ?>
        <div class="text-center mx-auto my-4 five-second-fade">
        <span class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?> alert-dismissible
        text-cente">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="margin-left: 8px!important;">&times;
        </button>
        <?php echo e(Session::get('message')); ?></span>
        </div>
    <?php endif; ?>
    <div class="container">
        <div class="row" style="height: 100vh">
            <div class="col-sm-12 my-auto mx-auto" style="max-width: 500px">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">ورود به پنل کابری</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="<?php echo e(route('login')); ?>">
                        <?php echo csrf_field(); ?>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-6 control-label">نام کاربری</label>

                                <div class="col-sm-10">
                                    <input type="text" name="username"
                                           class="form-control <?php if ($errors->has('username')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('username'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="inputEmail3"
                                           placeholder="نام کاربری را وارد کنید" value="<?php echo e(old('username')); ?>" required
                                           autocomplete="username" autofocus>
                                    <?php if ($errors->has('username')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('username'); ?>
                                    <span class="invalid-feedback text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-6 control-label">کلمه عبور</label>

                                <div class="col-sm-10">
                                    <input type="password" name="password"
                                           class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                           id="inputPassword3"
                                           placeholder="پسورد را وارد کنید" required autocomplete="current-password">
                                    <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="remember"
                                               id="exampleCheck2" <?php echo e(old('remember') ? 'checked' : ''); ?>>
                                        <label class="form-check-label" for="exampleCheck2">مرا به خاطر بسپار</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">ورود</button>

                            <a class="btn btn-outline-primary my-sm-0 pull-left" data-toggle="dropdown"
                               href="#">
                                ثبت‌نام
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu bg-off-color text-right">
                                <li class="nav-item text-right"><a class="nav-link bg-off-color text-right"
                                                        href="<?php echo e(route('registerClient')); ?>">ثبت‌نام
                                        کاربر</a></li>
                                <li class="nav-item text-right"><a class="nav-link bg-off-color text-right"
                                                        href="<?php echo e(route('registerDoctor')); ?>">ثبت نام
                                        متخصص</a></li>

                                <!-- dropdown menu links -->
                            </ul>

                            <a href="<?php echo e(route('home')); ?>" class="btn btn-outline-info mx-1 float-left">صفحه اصلی</a>
                            <?php if(Route::has('password.request')): ?>
                                <a class="btn btn-link" href="
                                <?php echo e(route('password.request')); ?>

                                        ">
                                    <?php echo e(__('رمز خود را فراموش کرده‌اید؟')); ?>

                                </a>
                            <?php endif; ?>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>

        </div>


    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/auth/login.blade.php ENDPATH**/ ?>