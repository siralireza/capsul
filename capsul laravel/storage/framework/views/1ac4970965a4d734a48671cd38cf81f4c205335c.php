<?php $__env->startSection('title'); ?>
    <title>پنل کاربر</title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('body'); ?>
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav mr-auto">
                <!-- Messages Dropdown Menu -->
                <li class="nav-item">
                    <a href="<?php echo e(route('chats.index')); ?>"
                       <?php if(unReadChats(request())): ?> class="text-white-danger" <?php endif; ?>>
                        <?php if(unReadChats(request())): ?>
                            <span class="badge badge-danger mx-2"><?php echo e(unReadChats(request())); ?></span>
                        <?php endif; ?>
                        <i class="fa fa-comments-o fa-2x"></i></a>
                </li>
            </ul>
        </nav>
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-success elevation-4 ">
            <!-- Brand Logo -->
            <a href="/" class="brand-link bg-gray-light">
                <img src="<?php echo e(asset('home/images/capsul-logo.png')); ?>" alt="AdminLTE Logo"
                     class="brand-image img-circle elevation-3"
                     style="opacity: .8">
                <span class="brand-text font-weight-light">پنل کاربر</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar" style="direction: ltr">
                <div style="direction: rtl">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="<?php echo e(userPic() ?: asset('dist/img/avatar04.png')); ?>"
                                 class="img-circle elevation-2" alt="User Image"
                                 style="width: 3.5rem">
                        </div>
                        <div class="info">
                            <a class="d-block"><?php echo e(Auth::user()->fname.' '.Auth::user()->lname); ?></a>
                            <a class="d-block">کیف پول : <?php echo e(userWallet()); ?> تومان</a>
                            
                        </div>
                    </div>

                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                            data-accordion="false">
                            <!-- Add icons to the links using the .nav-icon class
                                 with font-awesome or any other icon font library -->
                            <li class="nav-item">
                                <a href="<?php echo e(route('profile')); ?>" class="nav-link">
                                    <i class="nav-icon fa fa-user"></i>
                                    <p>
                                        پروفایل
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo e(route('reqs.index')); ?>" class="nav-link">
                                    <i class="nav-icon fa fa-file"></i>
                                    <p>
                                        درخواست‌ها
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo e(route('chats.index')); ?>" class="nav-link">
                                    <i class="nav-icon fa fa-file"></i>
                                    <p>
                                        پیام ها
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo e(route('sicks.index')); ?>" class="nav-link">
                                    <i class="nav-icon fa fa-folder-open"></i>
                                    <p>
                                        سوابق پزشکی
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo e(route('complaints.index')); ?>" class="nav-link">
                                    <i class="nav-icon fa fa-file-text"></i>
                                    <p>
                                        پیشنهادات
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo e(route('wallet.index')); ?>" class="nav-link">
                                    <i class="nav-icon fa fa-file-text"></i>
                                    <p>
                                        افزایش شارژ کیف پول
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo e(route('logout')); ?>" class="nav-link">
                                    <i class="nav-icon fa fa-sign-out"></i>
                                    <p>
                                        خروج
                                    </p>
                                </a>
                            </li>

                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <?php if(Session::has('message')): ?>
                    <div class="text-center mx-auto my-2 five-second-fade w-75">
                        <div class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?> alert-dismissible"
                             role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"
                                    style="margin-left: 8px!important;">&times;
                            </button>
                            <?php echo e(Session::get('message')); ?></div>
                    </div>
        <?php endif; ?>
        <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <?php echo $__env->yieldContent('content'); ?>
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>

    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/layouts/client/clientPanel.blade.php ENDPATH**/ ?>