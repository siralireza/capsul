<?php $__env->startSection('body'); ?>
    <div class="card card-primary mt-3 direct-chat direct-chat-primary" style="min-height: 96vh">
        <div class="card-header">
            <div class="row">
                <div class="col-lg-4">
                    <h3 class="card-title">لیست پیام ها</h3>
                </div>
            </div>

        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <!-- Conversations are loaded here -->
            <div class="direct-chat-messages" id="chat-window">
                <!-- Message. Default to the left -->
                <?php if($chats->lastPage() != $chats->currentPage()): ?>
                    <div class="text-center mx-auto">
                        <a href="<?php echo e($chats->nextPageUrl()); ?>" class="btn btn-secondary">مشاهده چت های قدیمی‌تر</a>
                    </div>
                <?php endif; ?>

                <?php $__currentLoopData = $chats->reverse(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                        $date = $ch->created_at;
                    ?>
                    <?php if($ch->sender_id == userId()): ?>
                        <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name float-right">شما</span>
                            </div>
                            <!-- /.direct-chat-info -->
                            <img class="direct-chat-img" src="<?php echo e(userPic() ?: asset('dist/img/avatar04.png')); ?>"
                                 alt="message user image">
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text d-inline-block">
                                <h6 class="small"><?php echo e($date); ?></h6>
                                <?php if($ch->msg): ?>
                                    <p><?php echo e($ch->msg); ?></p>
                                <?php endif; ?>
                                
                                <?php if($ch->msg_type == 'text'): ?>
                                <?php elseif($ch->msg_type == 'video'): ?>
                                <?php elseif($ch->msg_type == 'file'): ?>
                                    <a href="<?php echo e($ch->path); ?>" class="text-white" download="file">
                                        <i class="fa fa-file fa-3x text-white">
                                        </i><br>
                                        <span class="mr-1"><?php echo e((int)(Storage::size($ch->getOriginal('path'))/1000)); ?>کیلوبایت </span>
                                    </a>
                                <?php elseif($ch->msg_type == 'audio'): ?>
                                    <audio controls>
                                        <source src="<?php echo e($ch->path); ?>">
                                        صدا توسط مرورگر شما پشتیبانی نمی شود.
                                    </audio>
                                <?php elseif($ch->msg_type == 'img'): ?>
                                    <img class="direct-chat-image w-100" src="<?php echo e($ch->path); ?>" alt="message user image">
                                    <br>
                                    <div class="mx-auto my-2 text-center">
                                        <a target="_blank" rel="noopener noreferrer" href="<?php echo e($ch->path); ?>"
                                           class="btn btn-dark">مشاهده تصویر اصلی</a>
                                    </div>
                                <?php endif; ?>
                                <br>
                                <?php if($ch->read_status): ?>
                                    <i class="fa fa-check"><i class="fa fa-check"></i></i>

                                <?php else: ?>
                                    <i class="fa fa-check"></i>
                                <?php endif; ?>
                            </div>
                            <!-- /.direct-chat-text -->
                        </div>
                    <?php else: ?>
                        <div class="direct-chat-msg text-left">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name float-left"><?php echo e($ch->Sender->fname .' '. $ch->Sender->lname.' ('.translateRole(userRole(\App\User::find($ch->Sender->id))).')'); ?></span>
                            </div>
                            <!-- /.direct-chat-info -->
                            <img class="direct-chat-img"
                                 src="<?php echo e($ch->Sender->{$ch->sender_type}->User->pic ? '/storage/'.$ch->Sender->{$ch->sender_type}->User->pic : asset('dist/img/avatar04.png')); ?>"
                                 alt="message user image">
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text d-inline-block">

                                <h6 class="small"><?php echo e($date); ?></h6>
                                <?php if($ch->msg): ?>
                                    <p class="text-dark"><?php echo e($ch->msg); ?></p>
                                <?php endif; ?>

                                
                                <?php if($ch->msg_type == 'text'): ?>
                                <?php elseif($ch->msg_type == 'video'): ?>
                                <?php elseif($ch->msg_type == 'file'): ?>
                                    <a href="<?php echo e($ch->path); ?>" class="text-dark" download="file">
                                        <i class="fa fa-file fa-3x text-dark">
                                        </i><br>
                                        <span class="mr-1"><?php echo e((int)(Storage::size($ch->getOriginal('path'))/1000)); ?>کیلوبایت </span>
                                    </a>
                                <?php elseif($ch->msg_type == 'audio'): ?>
                                    <audio controls>
                                        <source src="<?php echo e($ch->path); ?>">
                                        صدا توسط مرورگر شما پشتیبانی نمی شود.
                                    </audio>
                                <?php elseif($ch->msg_type == 'img'): ?>
                                    <img class="direct-chat-image w-100" src="<?php echo e($ch->path); ?>" alt="message user image">
                                    <br>
                                    <div class="mx-auto my-2 text-center">
                                        <a target="_blank" rel="noopener noreferrer" href="<?php echo e($ch->path); ?>"
                                           class="btn btn-dark">مشاهده تصویر اصلی</a>
                                    </div>
                                <?php endif; ?>
                                <br>
                                <?php if($ch->read_status): ?>
                                    <i class="fa fa-check"><i class="fa fa-check"></i></i>

                                <?php else: ?>
                                    <i class="fa fa-check"></i>
                                <?php endif; ?>

                            </div>
                            <!-- /.direct-chat-text -->
                        </div>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <!-- /.direct-chat-msg -->
                <?php if(!$chats->onFirstPage()): ?>
                    <div class="text-center mx-auto mb-2">
                        <a href="<?php echo e($chats->previousPageUrl()); ?>" class="btn btn-light text-dark">مشاهده چت های
                            جدید‌تر</a>
                    </div>
                <?php endif; ?>

            </div>
            <!--/.direct-chat-messages-->

        </div>
        <!-- /.card-body -->
        <?php if(isset($chats) and $chats->isNotEmpty()): ?>
            
            

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            <div class="w-100 text-center">
                <button class="btn btn-primary w-25 mb-3" onclick="window.open('', '_self', ''); window.close();">بستن
                </button>
            </div>
        
    <?php endif; ?>
    <!-- /.card-footer-->
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script>
        $('#chat-window').animate({
            scrollTop: $('#chat-window').get(0).scrollHeight
        }, 0);

        // Function for auto refreshing the page, and add or append a query string key/value pair to the URL
        function manageQueryStringParam(key, val, uri) {
            // if uri is not passed in, set a default
            uri = uri || window.location.href;
            // if key does not exist in queryString, create it and append to URL
            if (uri.indexOf(key) < 0) {
                uri += (uri.indexOf('?') >= 0 ? '&' : '?') + key + "=" + val;
            }
            return uri;
        }

        var isTyping = false;
        $('#msg').keyup(function () {
            if ($(this).val().length)
                isTyping = true;
            else {
                isTyping = false;
                setTimeout(function () {
                    if (!isTyping) {
                        window.location = manageQueryStringParam('source', 'autorefresh');
                    }
                }, 10000);
            }
        });

        setTimeout(function () {
            if (!isTyping) {
                window.location = manageQueryStringParam('source', 'autorefresh');
            }
        }, 10000);
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/admin/chats/show.blade.php ENDPATH**/ ?>