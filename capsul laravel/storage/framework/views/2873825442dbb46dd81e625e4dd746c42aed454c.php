<?php $__env->startSection('content'); ?>
    <div class="card card-primary mt-3 direct-chat direct-chat-primary" style="min-height: 90vh">
        <div class="card-header">
            <h3 class="card-title">پرسش و پاسخ</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <!-- Conversations are loaded here -->
            <div class="direct-chat-messages " id="chat-window">
                <!-- Message. Default to the left -->
                <?php if($chats->lastPage() != $chats->currentPage()): ?>
                    <div class="text-center mx-auto">
                        <a href="<?php echo e($chats->nextPageUrl()); ?>" class="btn btn-secondary">مشاهده چت های قدیمی‌تر</a>
                    </div>
                <?php endif; ?>

                <?php $__currentLoopData = $chats->reverse(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                        $dateTime = new \Hekmatinasser\Verta\Verta($ch->created_at);
                        $date = $dateTime->format('H:i:s - Y/n/j');
                    ?>
                    <?php if($ch->sender_id == userId()): ?>
                        <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name float-right">شما</span>
                            </div>
                            <!-- /.direct-chat-info -->
                            <img class="direct-chat-img" src="<?php echo e(userPic() ?: asset('dist/img/avatar04.png')); ?>"
                                 alt="message user image">
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text d-inline-block">
                                <h6 class="small"><?php echo e($date); ?></h6>
                                <?php if($ch->msg): ?>
                                    <p><?php echo e($ch->msg); ?></p>
                                <?php endif; ?>
                                
                                <?php if($ch->msg_type == 'text'): ?>
                                <?php elseif($ch->msg_type == 'video'): ?>
                                <?php elseif($ch->msg_type == 'file'): ?>
                                    <a href="<?php echo e($ch->path); ?>" class="text-white" download="file">
                                        <i class="fa fa-file fa-3x text-white">
                                        </i><br>
                                        <span class="mr-1"><?php echo e((int)(Storage::size($ch->getOriginal('path'))/1000)); ?>کیلوبایت </span>
                                    </a>
                                <?php elseif($ch->msg_type == 'audio'): ?>
                                    <audio controls>
                                        <source src="<?php echo e($ch->path); ?>">
                                        صدا توسط مرورگر شما پشتیبانی نمی شود.
                                    </audio>
                                <?php elseif($ch->msg_type == 'img'): ?>
                                    <img class="direct-chat-image w-100" src="<?php echo e($ch->path); ?>" alt="message user image">
                                    <br>
                                    <div class="mx-auto my-2 text-center">
                                        <a target="_blank" rel="noopener noreferrer" href="<?php echo e($ch->path); ?>"
                                           class="btn btn-dark">مشاهده تصویر اصلی</a>
                                    </div>
                                <?php endif; ?>
                                <br>
                                <?php if($ch->read_status): ?>
                                    <i class="fa fa-check"><i class="fa fa-check"></i></i>

                                <?php else: ?>
                                    <i class="fa fa-check"></i>
                                <?php endif; ?>
                            </div>
                            <!-- /.direct-chat-text -->
                        </div>
                    <?php else: ?>
                        <div class="direct-chat-msg text-left">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name float-left"><?php echo e($ch->Sender->fname .' '. $ch->Sender->lname); ?></span>
                            </div>
                            <!-- /.direct-chat-info -->
                            <img class="direct-chat-img"
                                 src="<?php echo e($ch->Sender->{$ch->sender_type}->User->pic ?: asset('dist/img/avatar5.png')); ?>"
                                 alt="message user image">
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text d-inline-block">

                                <h6 class="small"><?php echo e($date); ?></h6>
                                <?php if($ch->msg): ?>
                                    <p class="text-dark"><?php echo e($ch->msg); ?></p>
                                <?php endif; ?>

                                
                                <?php if($ch->msg_type == 'text'): ?>
                                <?php elseif($ch->msg_type == 'video'): ?>
                                <?php elseif($ch->msg_type == 'file'): ?>
                                    <a href="<?php echo e($ch->path); ?>" class="text-dark" download="file">
                                        <i class="fa fa-file fa-3x text-dark">
                                        </i><br>
                                        <span class="mr-1"><?php echo e((int)(Storage::size($ch->getOriginal('path'))/1000)); ?>کیلوبایت </span>
                                    </a>
                                <?php elseif($ch->msg_type == 'audio'): ?>
                                    <audio controls>
                                        <source src="<?php echo e($ch->path); ?>">
                                        صدا توسط مرورگر شما پشتیبانی نمی شود.
                                    </audio>
                                <?php elseif($ch->msg_type == 'img'): ?>
                                    <img class="direct-chat-image w-100" src="<?php echo e($ch->path); ?>" alt="message user image">
                                    <br>
                                    <div class="mx-auto my-2 text-center">
                                        <a target="_blank" rel="noopener noreferrer" href="<?php echo e($ch->path); ?>"
                                           class="btn btn-dark">مشاهده تصویر اصلی</a>
                                    </div>
                                <?php endif; ?>
                                <br>
                                <?php if($ch->read_status): ?>
                                    <i class="fa fa-check"><i class="fa fa-check"></i></i>

                                <?php else: ?>
                                    <i class="fa fa-check"></i>
                                <?php endif; ?>

                            </div>
                            <!-- /.direct-chat-text -->
                        </div>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <!-- /.direct-chat-msg -->
                <?php if(!$chats->onFirstPage()): ?>
                    <div class="text-center mx-auto mb-2">
                        <a href="<?php echo e($chats->previousPageUrl()); ?>" class="btn btn-light text-dark">مشاهده چت های
                            جدید‌تر</a>
                    </div>
                <?php endif; ?>

            </div>
            <!--/.direct-chat-messages-->

        </div>
        <!-- /.card-body -->
        <?php if(isset($chats) and $chats->isNotEmpty()): ?>
            <?php if($chats->first()->Request->status == 'accepted'): ?>
                <div class="card-footer">

                    <?php if ($errors->has('msg')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('msg'); ?>
                    <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                        </span>
                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                    <?php if ($errors->has('file')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('file'); ?>
                    <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                        </span>
                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                    <form action="<?php echo e(route('chats.store')); ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="req_id" value="<?php echo e($chats->first()->req_id); ?>">
                        <?php echo csrf_field(); ?>
                        <div class="input-group">
                            <input type="text" id="msg" name="msg" placeholder="پیام خود را وارد کنید..."
                                   value="<?php echo e(old('msg')); ?>"
                                   class="form-control">
                            <span class="input-group-append">
                                    <div class="text-center " style="width: 2.9rem;height: 2.6rem">
                                            <label class="btn-sm btn btn-outline-dark" for="exampleInputFile">
                                                <i class="fa fa-paperclip fa-2x"></i>
                                            </label>
                                            <input type="file" name="file"
                                                   class="input-file-custom" id="exampleInputFile">
                                        </div>
                                <button type="submit" class="btn btn-success mx-1">ارسال</button>
                                <a href="<?php echo e(route('chats.index')); ?>" class="btn btn-primary">بازگشت</a>
                    </span>
                        </div>
                    </form>
                </div>
            <?php else: ?>
                <div class="w-100 text-center">
                    <a href="<?php echo e(route('reqs.show',['req' => $req])); ?>" class="btn btn-primary w-25">بازگشت</a>
                </div>
        <?php endif; ?>
    <?php endif; ?>
    <!-- /.card-footer-->
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script>
        $(document).ready(function () {
            $('#chat-window').animate({
                scrollTop: $('#chat-window').get(0).scrollHeight
            }, 0);
        });


        // Function for auto refreshing the page, and add or append a query string key/value pair to the URL
        function manageQueryStringParam(key, val, uri) {
            // if uri is not passed in, set a default
            uri = uri || window.location.href;
            // if key does not exist in queryString, create it and append to URL
            if (uri.indexOf(key) < 0) {
                uri += (uri.indexOf('?') >= 0 ? '&' : '?') + key + "=" + val;
            }
            return uri;
        }


        var isTyping = false;
        $('#exampleInputFile').on('click',function () {
            isTyping = true;
        });
        $('#msg').keyup(function () {
            if ($(this).val().length)
                isTyping = true;
            else {

                isTyping = false;
                setTimeout(function () {
                    if (!isTyping) {
                        window.location = manageQueryStringParam('source', 'autorefresh');
                    }
                }, 10000);
            }
        });

        setTimeout(function () {
            if (!isTyping) {
                window.location = manageQueryStringParam('source', 'autorefresh');
            }
        }, 10000);


    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.client.clientPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/client/chats/show.blade.php ENDPATH**/ ?>