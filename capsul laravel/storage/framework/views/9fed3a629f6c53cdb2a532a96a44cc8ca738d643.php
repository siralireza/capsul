<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12 mx-auto" style="max-width: 900px">
            <div class="card card-primary mt-3">
                <div class="card-header">
                    <h3 class="card-title">ویرایش پروفایل</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="<?php echo e(route('updateProfile')); ?>" method="post" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('patch'); ?>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="image text-center">
                                        <img src="<?php echo e(userPic() ?: asset('dist/img/avatar04.png')); ?>" class="img-circle elevation-2" alt="User Image" style="width: 4.5rem">
                                    </div>
                                    <div class="text-center">
                                        <label class="btn-sm btn btn-outline-dark mt-3" for="exampleInputFile">تغییر عکس</label>
                                        <input type="file" name="pic" style="display: none" class="custom-file-input" id="exampleInputFile">
                                        <?php if ($errors->has('pic')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('pic'); ?>
                                        <span class="invalid text-danger text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputfname" class="col-sm-6 control-label">نام</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="fname"
                                               class="form-control <?php if ($errors->has('fname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('fname'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputfname"
                                               placeholder="نام خود را وارد کنید" value="<?php echo e(Auth::user()->fname); ?>"
                                               autofocus>
                                        <?php if ($errors->has('fname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('fname'); ?>
                                        <span class="invalid text-danger text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="inputlname" class="col-sm-6 control-label">نام خانوادگی</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="lname"
                                               class="form-control <?php if ($errors->has('lname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('lname'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputlname"
                                               placeholder="نام خانوادگی را وارد کنید" value="<?php echo e(Auth::user()->lname); ?>"
                                        >
                                        <?php if ($errors->has('lname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('lname'); ?>
                                        <span class="invalid text-danger text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="inputPassword0" class="col-sm-6 control-label">کلمه عبور فعلی</label>

                                    <div class="col-sm-10">
                                        <input type="password" name="pass"
                                               class="form-control <?php if ($errors->has('pass')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('pass'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputPassword0"
                                               placeholder="پسورد کنونی وارد کنید"
                                               value="<?php echo e(old('pass')); ?>"
                                                
                                        >
                                        <?php if ($errors->has('pass')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('pass'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-6 control-label">کلمه عبور جدید</label>

                                    <div class="col-sm-10">
                                        <input type="password" name="password"
                                               class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputPassword3"
                                               placeholder="پسورد را وارد کنید"
                                               value="<?php echo e(old('username')); ?>"
                                                
                                        >
                                        <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPasswordconf" class="col-sm-6 control-label">تکرار کلمه
                                        عبور جدید</label>

                                    <div class="col-sm-10">
                                        <input type="password" name="password_confirmation"
                                               class="form-control <?php if ($errors->has('password_confirmation')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password_confirmation'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputPasswordconf"
                                               placeholder="تکرار پسورد را وارد کنید"
                                               value="<?php echo e(old('password_confirmation')); ?>"
                                        >
                                        <?php if ($errors->has('password_confirmation')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password_confirmation'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-6 control-label">ایمیل</label>

                                    <div class="col-sm-10">
                                        <input type="email" name="email"
                                               class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputEmail"
                                               placeholder="ایمیل را وارد کنید" value="<?php echo e(Auth::user()->email); ?>">
                                        <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPhone" class="col-sm-6 control-label">شماره موبایل</label>

                                    <div class="col-sm-10">
                                        <input type="tel" name="phone"
                                               class="form-control <?php if ($errors->has('phone')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('phone'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputPhone"
                                               placeholder="شماره موبایل را وارد کنید" value="<?php echo e(Auth::user()->Client->phone); ?>">
                                        <?php if ($errors->has('phone')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('phone'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputAge" class="col-sm-6 control-label">سن</label>

                                    <div class="col-sm-10">
                                        <input type="tel" name="age"
                                               class="form-control <?php if ($errors->has('age')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('age'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                               id="inputAge"
                                               placeholder="سن خود را وارد کنید" value="<?php echo e(Auth::user()->Client->age); ?>">
                                        <?php if ($errors->has('age')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('age'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-6 control-label" for="inputSex">جسنیت</label>
                                    <div class="col-sm-10">

                                        <select class="form-control <?php if ($errors->has('sex')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('sex'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                id="inputSex" name="sex">
                                            <?php if(Auth::user()->sex == 'female'): ?>
                                                <option value="male">مذکر</option>
                                                <option value="female" selected>مونث</option>
                                            <?php else: ?>
                                                <option value="male" selected>مذکر</option>
                                                <option value="female">مونث</option>
                                            <?php endif; ?>
                                        </select>
                                        <?php if ($errors->has('sex')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('sex'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="answer1" class="col-sm-6 control-label">آیا سابقه بیماری خاصی دارید؟
                                        نام
                                        ببرید.</label>

                                    <div class="col-sm-10">
                                    <textarea name="answer1" rows=2
                                              class="form-control <?php if ($errors->has('answer1')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer1'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="answer1"
                                              placeholder="نام بیماری ها را بنویسید"><?php echo e(Auth::user()->Client->InitHistory->answer1); ?></textarea>
                                        <?php if ($errors->has('answer1')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer1'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="answer2" class="col-sm-10 control-label">آیا در حال حاظر یا گذشته
                                        داروی
                                        خاصی
                                        استفاده میکردید؟
                                        <p class="text-sm"><i class="fa fa-star text-warning"></i>
                                            در صورت مثبت بودن پاسخ، نام دارو، مدت مصرف و آخرین نوبت مصرف را ذکر
                                            کنید.
                                            توجه فرمایید در صورتیکه از داروهای گیاهی نیز استفاده میکنید نام آن دارو
                                            اهمیت
                                            دارد.
                                        </p>
                                    </label>

                                    <div class="col-sm-10">
                                    <textarea name="answer2" rows=2
                                              class="form-control <?php if ($errors->has('answer2')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer2'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="answer2"
                                              placeholder="نام دارو، مدت مصرف و آخرین نوبت مصرف را ذکر کنید"><?php echo e(Auth::user()->Client->InitHistory->answer2); ?></textarea>
                                        <?php if ($errors->has('answer2')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer2'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="answer3" class="col-sm-10 control-label">آیا تا کنون بستری بوده‌اید؟
                                        یا
                                        عمل
                                        جراحی داشته‌اید؟</label>

                                    <div class="col-sm-10">
                                    <textarea name="answer3" rows=2
                                              class="form-control <?php if ($errors->has('answer3')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer3'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="answer3"
                                              placeholder="توضیحات مربوطه را بنویسید"><?php echo e(Auth::user()->Client->InitHistory->answer3); ?></textarea>
                                        <?php if ($errors->has('answer3')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer3'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="answer4" class="col-sm-10 control-label">سوابق بیماری در خانواده خود
                                        را
                                        بنویسید.
                                        <p class="text-sm"><i class="fa fa-star text-warning"></i>
                                            پدر، مادر، خواهر، برادر، سابقه بیماری های زنان و پستان در خاله، عمه و
                                            مادربزرگ‌ها اهمیت دارد. همچنین سابقه بیماری پروستات در عمو، دایی و
                                            پدربزرگ‌ها
                                            مهم است و سابقه بیماری هموفیلی در دایی
                                        </p>
                                    </label>

                                    <div class="col-sm-10">
                                    <textarea name="answer4" rows=2
                                              class="form-control <?php if ($errors->has('answer4')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer4'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="answer4"
                                              placeholder="توضیحات مربوطه را بنویسید"><?php echo e(Auth::user()->Client->InitHistory->answer4); ?></textarea>
                                        <?php if ($errors->has('answer4')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer4'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="answer5" class="col-sm-10 control-label">سابقه حساسیت غذایی یا
                                        دارویی
                                        دارید؟
                                        (حساسیت به پروتئین، سویا و تخم مرغ)</label>

                                    <div class="col-sm-10">
                                    <textarea name="answer5" rows=2
                                              class="form-control <?php if ($errors->has('answer5')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer5'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="answer5"
                                              placeholder="توضیحات حساسیت را بنویسید"><?php echo e(Auth::user()->Client->InitHistory->answer5); ?></textarea>
                                        <?php if ($errors->has('answer5')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer5'); ?>
                                        <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-lg btn-primary">ویرایش</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.client.clientPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/client/profile.blade.php ENDPATH**/ ?>