<?php $__env->startSection('content'); ?>
    <div class="card card-primary mt-3">
        <div class="card-header">
            <h3 class="card-title">تاریخچه درآمدها</h3>
        </div>
        <div class="card-body">






























            <?php if($incomes->isEmpty()): ?>
                <div class="w-100 text-center mt-lg-5">
                    <h5 class="text-warning">درآمدی وجود ندارد</h5>

                </div>
            <?php endif; ?>
            <div class="row">
                <?php $__currentLoopData = $incomes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $income): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="card card-light col-lg-4">
                        <div class="card-header">
                            <span class="ill-name text-white"><?php echo e($income->title); ?></span>
                            <?php
                                $dateTime = new \Hekmatinasser\Verta\Verta($income->finished_at);
                                $date = $dateTime->format('Y/n/j');
                            ?>
                            <span class="pull-left text-white"><?php echo e($date); ?></span><br>

                        </div>
                        <div class="card-body">
                            
                            <h6>نام بیمار: <?php echo e($income->Client->User->fname.' '.$income->Client->User->lname); ?></h6>
                            <div class="row">
                                <div class="col-6">
                                    <p>دلیل مراجعه بیمار: <?php echo e($income->main_reason); ?></p>
                                </div>
                                <div class="col-6 text-bold">
                                    <p class="text-danger">مبلغ دریافتی: <?php echo e($income->exp_cost); ?> تومان </p>

                                </div>

                            </div>

                        </div>
                        <div class="card-footer">
                            <?php if($income->status == 'waiting'): ?>
                                <div class="w-100">
                                    <button class="btn btn-danger pull-left" data-toggle="modal"
                                            data-target="#deleteModal<?php echo e($loop->index); ?>">حذف
                                    </button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $('.dt-picker').persianDatepicker({
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
        });
        $('#thisForm').submit(function (e) {
            e.preventDefault();
            var date = $('#start_date').val();
            if(date){
                date = toEnglishNum(date);
                date = date.split('/');
                date = date.map(Number);
                date = new persianDate(date).toCalendar('gregorian').toLocale('en').format('YYYY/MM/DD');
                $('#start_date').val(date)
            }
            var date = $('#end_date').val();
            if(date){
                date = toEnglishNum(date);
                date = date.split('/');
                date = date.map(Number);
                date = new persianDate(date).toCalendar('gregorian').toLocale('en').format('YYYY/MM/DD');
                $('#end_date').val(date)
            }
            $(this).unbind().submit();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.doctor.doctorPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/doctor/income/index.blade.php ENDPATH**/ ?>