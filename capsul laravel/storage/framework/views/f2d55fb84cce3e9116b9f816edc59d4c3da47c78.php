<?php $__env->startSection('refresh'); ?>
    <meta http-equiv="refresh" content="10"/>
<?php $__env->stopSection(true); ?>
<?php $__env->startSection('content'); ?>
    <div class="card card-primary mt-3 mx-auto" style="max-width: 900px">
        <div class="card-header">
            <h3 class="card-title">لیست پیام ها</h3>
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-body p-0">
                    <?php if($reqChats->isNotEmpty()): ?>

                        <table class="table">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>نام بیمار</th>
                                <th>علت مراجعه</th>
                                <th style="width: 125px"></th>
                            </tr>
                            <?php $__currentLoopData = $reqChats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $req): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($loop->index + 1); ?></td>
                                    <td><?php echo e($req->Client->User->fname .' '.$req->Client->User->lname); ?></td>
                                    <td><?php echo e($req->main_reason); ?></td>
                                    <td class="text-center"><a href="<?php echo e(route('chats.show',['id' => $req->id])); ?>"
                                                               <?php if($req->have_unread): ?> class="text-danger" <?php endif; ?>><span
                                                    class="badge badge-danger mx-2"><?php echo e($req->have_unread); ?></span><i
                                                    class="fa fa-commenting-o fa-2x"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                    <?php else: ?>
                        <div class="w-100 text-center my-3">

                            <h5 class="text-warning">گفت و گویی یافت نشد</h5>
                        </div>
                    <?php endif; ?>
                </div>
                <!-- /.card-body -->
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.doctor.doctorPanel', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/doctor/chats/index.blade.php ENDPATH**/ ?>