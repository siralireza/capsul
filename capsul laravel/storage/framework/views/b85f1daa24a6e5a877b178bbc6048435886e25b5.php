<?php $__env->startSection('title'); ?>
    <title>ثبت نام کاربر</title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('body'); ?>
    <div class="container">
        <div class="row" style="height: 100vh">
            <div class="col-sm-10 my-auto mx-auto">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">ثبت نام کاربر جدید</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="<?php echo e(route('register')); ?>">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="register_type" value="client">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="inputfname" class="col-sm-6 control-label">نام&nbsp;<i
                                                    class="fa fa-star text-danger"></i></label>

                                        <div class="col-sm-10">
                                            <input type="text" name="fname"
                                                   class="form-control <?php if ($errors->has('fname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('fname'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                   id="inputfname"
                                                   placeholder="نام خود را وارد کنید" value="<?php echo e(old('fname')); ?>"
                                                   required autofocus>
                                            <?php if ($errors->has('fname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('fname'); ?>
                                            <span class="invalid text-danger text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="inputlname" class="col-sm-6 control-label">نام خانوادگی&nbsp;<i
                                                    class="fa fa-star text-danger"></i></label>

                                        <div class="col-sm-10">
                                            <input type="text" name="lname"
                                                   class="form-control <?php if ($errors->has('lname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('lname'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                   id="inputlname"
                                                   placeholder="نام خانوادگی را وارد کنید" value="<?php echo e(old('lname')); ?>"
                                                   required>
                                            <?php if ($errors->has('lname')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('lname'); ?>
                                            <span class="invalid text-danger text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="inputUsername" class="col-sm-6 control-label">نام کاربری&nbsp;<i
                                                    class="fa fa-star text-danger"></i></label>

                                        <div class="col-sm-10">
                                            <input type="text" name="username"
                                                   class="form-control <?php if ($errors->has('username')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('username'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                   id="inputUsername"
                                                   placeholder="نام کاربری را وارد کنید" value="<?php echo e(old('username')); ?>"
                                                   required>
                                            <?php if ($errors->has('username')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('username'); ?>
                                            <span class="invalid text-danger text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>

                                    </div>
                                    
                                    
                                    

                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    

                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-6 control-label">ایمیل</label>

                                        <div class="col-sm-10">
                                            <input type="email" name="email"
                                                   class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                   id="inputEmail"
                                                   placeholder="ایمیل را وارد کنید" value="<?php echo e(old('email')); ?>">
                                            <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                                            <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPhone" class="col-sm-6 control-label">شماره موبایل&nbsp;<i
                                                    class="fa fa-star text-danger"></i></label>

                                        <div class="col-sm-10">
                                            <input type="tel" name="phone"
                                                   class="form-control <?php if ($errors->has('phone')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('phone'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                   id="inputPhone"
                                                   placeholder="شماره موبایل را وارد کنید" value="<?php echo e(old('phone')); ?>"
                                                   required>
                                            <?php if ($errors->has('phone')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('phone'); ?>
                                            <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputAge" class="col-sm-6 control-label">سن&nbsp;<i
                                                    class="fa fa-star text-danger"></i></label>

                                        <div class="col-sm-10">
                                            <input type="tel" name="age"
                                                   class="form-control <?php if ($errors->has('age')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('age'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                   id="inputAge"
                                                   placeholder="سن خود را وارد کنید" value="<?php echo e(old('age')); ?>">
                                            <?php if ($errors->has('age')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('age'); ?>
                                            <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label" for="inputSex">جسنیت&nbsp;<i
                                                    class="fa fa-star text-danger"></i></label>
                                        <div class="col-sm-10">

                                            <select class="form-control <?php if ($errors->has('sex')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('sex'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                    id="inputSex" name="sex" required>
                                                <?php if(old('sex') == 'female'): ?>
                                                    <option value="male">مذکر</option>
                                                    <option value="female" selected>مونث</option>
                                                <?php else: ?>
                                                    <option value="male" selected>مذکر</option>
                                                    <option value="female">مونث</option>
                                                <?php endif; ?>
                                            </select>
                                            <?php if ($errors->has('sex')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('sex'); ?>
                                            <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class=" col-sm-10">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" name="add_history"
                                                       value="1"
                                                       id="exampleCheck2" required>
                                                <label class="form-check-label" for="exampleCheck2">تمام <a
                                                            href="<?php echo e(route('termsClient')); ?>" target="_blank"
                                                            class="text-bold text-danger">قوانین و تعهدات</a> را
                                                    میپذیرم.</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-10 mt-lg-5 text-center text-bold">
                                        <h5 class="text-danger">توجه داشته باشید کلمه عبور برای شما پیامک خواهد شد</h5>
                                    </div>

                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="answer1" class="col-sm-6 control-label">آیا سابقه بیماری خاصی دارید؟
                                            نام
                                            ببرید.</label>

                                        <div class="col-sm-10">
                                    <textarea name="answer1" rows=2
                                              class="form-control <?php if ($errors->has('answer1')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer1'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="answer1"
                                              placeholder="نام بیماری ها را بنویسید"><?php echo e(old('answer1')); ?></textarea>
                                            <?php if ($errors->has('answer1')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer1'); ?>
                                            <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="answer2" class="col-sm-10 control-label">آیا در حال حاظر یا گذشته
                                            داروی
                                            خاصی
                                            استفاده میکردید؟
                                            <p class="text-sm"><i class="fa fa-star text-warning"></i>
                                                در صورت مثبت بودن پاسخ، نام دارو، مدت مصرف و آخرین نوبت مصرف را ذکر
                                                کنید.
                                                توجه فرمایید در صورتیکه از داروهای گیاهی نیز استفاده میکنید نام آن دارو
                                                اهمیت
                                                دارد.
                                            </p>
                                        </label>

                                        <div class="col-sm-10">
                                    <textarea name="answer2" rows=2
                                              class="form-control <?php if ($errors->has('answer2')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer2'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="answer2"
                                              placeholder="نام دارو، مدت مصرف و آخرین نوبت مصرف را ذکر کنید"><?php echo e(old('answer2')); ?></textarea>
                                            <?php if ($errors->has('answer2')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer2'); ?>
                                            <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="answer3" class="col-sm-10 control-label">آیا تا کنون بستری بوده‌اید؟
                                            یا
                                            عمل
                                            جراحی داشته‌اید؟</label>

                                        <div class="col-sm-10">
                                    <textarea name="answer3" rows=2
                                              class="form-control <?php if ($errors->has('answer3')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer3'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="answer3"
                                              placeholder="توضیحات مربوطه را بنویسید"><?php echo e(old('answer3')); ?></textarea>
                                            <?php if ($errors->has('answer3')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer3'); ?>
                                            <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="answer4" class="col-sm-10 control-label">سوابق بیماری در خانواده خود
                                            را
                                            بنویسید.
                                            <p class="text-sm"><i class="fa fa-star text-warning"></i>
                                                پدر، مادر، خواهر، برادر، سابقه بیماری های زنان و پستان در خاله، عمه و
                                                مادربزرگ‌ها اهمیت دارد. همچنین سابقه بیماری پروستات در عمو، دایی و
                                                پدربزرگ‌ها
                                                مهم است و سابقه بیماری هموفیلی
                                            </p>
                                        </label>

                                        <div class="col-sm-10">
                                    <textarea name="answer4" rows=2
                                              class="form-control <?php if ($errors->has('answer4')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer4'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="answer4"
                                              placeholder="توضیحات مربوطه را بنویسید"><?php echo e(old('answer4')); ?></textarea>
                                            <?php if ($errors->has('answer4')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer4'); ?>
                                            <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="answer5" class="col-sm-10 control-label">سابقه حساسیت غذایی یا
                                            دارویی
                                            دارید؟
                                            (حساسیت به پروتئین، سویا و تخم مرغ)</label>

                                        <div class="col-sm-10">
                                    <textarea name="answer5" rows=2
                                              class="form-control <?php if ($errors->has('answer5')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer5'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="answer5"
                                              placeholder="توضیحات حساسیت را بنویسید"><?php echo e(old('answer5')); ?></textarea>
                                            <?php if ($errors->has('answer5')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('answer5'); ?>
                                            <span class="invalid text-danger" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">ثبت نام</button>
                            <a href="<?php echo e(route('login')); ?>" class="btn btn-outline-primary mx-2">ورود</a>
                            <a href="/" class="btn btn-outline-info mx-2 pull-left">صفحه اصلی</a>
                        </div>
                    </form>
                </div>

                <!-- /.card-footer -->
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $('#inputUsername').on('keypress', function (event) {
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/alireza/w/works/capsul/capsul laravel/resources/views/auth/registerClient.blade.php ENDPATH**/ ?>