import {createStore, applyMiddleware, combineReducers} from "redux";
import thunk from "redux-thunk";
import authReducer from "../reducers/authReducer";

const store = createStore(combineReducers({
    auth: authReducer,
}));

export default store;