const initialState = {
    current_action: null
};


function authReducer(state = initialState, action) {
    switch (action.type) {
        case "LOGIN":
            console.log("LOGIN");
            return {
                current_action: "LOGIN"
            };
        case "REGISTER":
            console.log("REGISTER");
            return {
                current_action: "REGISTER"
            };
        case "DATA_LOADED":
            return {
                current_action: "DATA_LOADED",
                data: action.payload
            };
        default:
            return state;
    }
}

export default authReducer;