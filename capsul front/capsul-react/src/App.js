import React from 'react';
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';
import './App.css';
import {getData, Login, Register} from "./actions/authAction";
import {connect} from "react-redux";

function mapStateToProps(state) {
    return {
        state: state.auth
    };
}

const mapDispatchToProps = {
    Login,
    getData,
    Register,
};

class App extends React.Component {
    constructor(props) {
        super(props);
        this.handle_click = this.handle_click.bind(this);
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log(this.props.state)
    }

    handle_click() {
        this.props.Login();
        this.props.Register();
        // this.props.getData();
    }

    render() {
        return (
            <Router>
                <div>
                    <Header/>
                    <h1>{this.props.state.current_action}</h1>
                    <button className={"btn btn-primary"} onClick={this.handle_click}>login</button>
                    <ul className="list-group list-group-flush">
                        {this.props.state.data ? this.props.state.data.map(el => (
                            <li className="list-group-item" key={el.id}>
                                {el.title}
                            </li>
                        )) : null}
                    </ul>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/about" component={About}/>
                        <Route path="/topics" component={Topics}/>
                        <Route component={NoMatch}/>
                    </Switch>
                </div>
            </Router>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

function NoMatch() {
    return <h1>404</h1>
}

function Home() {
    return <h2>Home</h2>;
}

function About() {
    return <h2>About</h2>;
}

function Topic({match}) {
    return <h3>Requested Param: {match.params.id}</h3>;
}

function Topics({match}) {
    return (
        <div>
            <h2>Topics</h2>

            <ul>
                <li>
                    <Link to={`${match.url}/components`}>Components</Link>
                </li>
                <li>
                    <Link to={`${match.url}/props-v-state`}>Props v. State</Link>
                </li>
            </ul>

            <Route path={`${match.path}/:id`} component={Topic}/>
            <Route
                exact
                path={match.path}
                render={() => <h3>Please select a topic.</h3>}
            />
        </div>
    );
}

function Header() {
    return (
        <ul>
            <li>
                <Link to="/">Home</Link>
            </li>
            <li>
                <Link to="/about">About</Link>
            </li>
            <li>
                <Link to="/topics">Topics</Link>
            </li>
        </ul>
    );
}

