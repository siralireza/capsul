export function pause(milliseconds) {
    let dt = new Date();
    while ((new Date()) - dt <= milliseconds) {
    }
}