-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 24, 2019 at 02:07 AM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `capsul`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `capsul_settings`
--

CREATE TABLE `capsul_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `answer_time` int(11) NOT NULL,
  `terms_dr` text COLLATE utf8mb4_unicode_ci,
  `terms_client` text COLLATE utf8mb4_unicode_ci,
  `app_dr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_client` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `capsul_settings`
--

INSERT INTO `capsul_settings` (`id`, `answer_time`, `terms_dr`, `terms_client`, `app_dr`, `app_client`, `created_at`, `updated_at`) VALUES
(1, 1, 'تابع قوانین معاونت محترم درمان دانشگاه علوم پزشکی مشهد و مقررات صنفی نظام پزشکی', 'این پلتفرم برای موارد اورژانس پزشکی نمی باشد', '[{\"download_link\":\"capsul-settings\\/December2019\\/iobFKhLe45E7XoeiTdcl.apk\",\"original_name\":\"capsool-doctor-v1.3.apk\"}]', '[{\"download_link\":\"capsul-settings\\/December2019\\/tSmFQjfdrO4azIGiZnce.apk\",\"original_name\":\"capsool-client-v1.3.apk\"}]', '2019-09-14 08:54:50', '2019-12-19 21:31:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chat_messages`
--

CREATE TABLE `chat_messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sender_id` bigint(20) UNSIGNED NOT NULL,
  `req_id` bigint(20) UNSIGNED NOT NULL,
  `sender_type` enum('client','doctor') COLLATE utf8mb4_unicode_ci NOT NULL,
  `msg` longtext COLLATE utf8mb4_unicode_ci,
  `msg_type` enum('text','file','audio','video','img') COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_status` tinyint(1) NOT NULL DEFAULT '0',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wallet` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `age` tinyint(4) DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `client_initial_histories`
--

CREATE TABLE `client_initial_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `answer1` mediumtext COLLATE utf8mb4_unicode_ci,
  `answer2` mediumtext COLLATE utf8mb4_unicode_ci,
  `answer3` mediumtext COLLATE utf8mb4_unicode_ci,
  `answer4` mediumtext COLLATE utf8mb4_unicode_ci,
  `answer5` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `dr_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci,
  `answer` longtext COLLATE utf8mb4_unicode_ci,
  `status` enum('waiting','viewed','closed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'waiting',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'آیدی کابر', 1, 1, 1, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'خالی', 0, 0, 0, 0, 0, 0, '{}', 2),
(3, 1, 'email', 'text', 'ایمیل', 0, 1, 1, 1, 1, 1, '{}', 7),
(4, 1, 'password', 'password', 'پسورد', 1, 0, 0, 1, 1, 0, '{}', 6),
(5, 1, 'remember_token', 'text', 'به یادسپاری توکن', 0, 0, 0, 0, 0, 0, '{}', 8),
(6, 1, 'created_at', 'timestamp', 'ایجاد شده در', 0, 0, 0, 0, 0, 0, '{}', 10),
(7, 1, 'updated_at', 'timestamp', 'به روز شده در', 0, 0, 0, 0, 0, 0, '{}', 12),
(8, 1, 'avatar', 'image', 'آواتار', 0, 0, 0, 0, 0, 0, '{}', 14),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'نقش', 0, 0, 0, 0, 0, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 17),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 0, 0, 0, 0, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 18),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 19),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(13, 2, 'name', 'text', 'نام', 1, 1, 1, 1, 1, 1, '{}', 2),
(14, 2, 'created_at', 'timestamp', 'ایجاد شده در', 0, 0, 0, 0, 0, 0, '{}', 3),
(15, 2, 'updated_at', 'timestamp', 'به روز شده در', 0, 0, 0, 0, 0, 0, '{}', 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(17, 3, 'name', 'text', 'نام', 1, 1, 1, 1, 1, 1, '{}', 2),
(18, 3, 'created_at', 'timestamp', 'ایجاد شده در', 0, 0, 0, 0, 0, 0, '{}', 3),
(19, 3, 'updated_at', 'timestamp', 'به روز شده در', 0, 0, 0, 0, 0, 0, '{}', 4),
(20, 3, 'display_name', 'text', 'نام نمایشی', 1, 1, 1, 1, 1, 1, '{}', 5),
(21, 1, 'role_id', 'select_dropdown', 'نقش', 0, 1, 1, 1, 1, 1, '{\"default\":\"2\",\"options\":{\"1\":\"\\u0645\\u062f\\u06cc\\u0631\",\"2\":\"\\u06a9\\u0627\\u0631\\u0628\\u0631\",\"3\":\"\\u0646\\u0627\\u0638\\u0631\"}}', 15),
(22, 1, 'fname', 'text', 'نام', 0, 1, 1, 1, 1, 1, '{}', 3),
(23, 1, 'lname', 'text', 'نام خانوادگی', 0, 1, 1, 1, 1, 1, '{}', 4),
(24, 1, 'pic', 'image', 'عکس', 0, 1, 1, 1, 1, 1, '{}', 9),
(25, 1, 'sex', 'select_dropdown', 'جنسیت', 1, 1, 1, 1, 1, 1, '{\"default\":\"male\",\"options\":{\"male\":\"\\u0645\\u0631\\u062f\",\"female\":\"\\u0632\\u0646\"}}', 11),
(26, 1, 'status', 'select_dropdown', 'وضعیت', 1, 1, 1, 1, 1, 1, '{\"default\":\"non_verified\",\"options\":{\"active\":\"\\u0641\\u0639\\u0627\\u0644\",\"deactive\":\"\\u063a\\u06cc\\u0631\\u0641\\u0639\\u0627\\u0644\",\"non_verified\":\"\\u062a\\u0627\\u06cc\\u06cc\\u062f \\u0646\\u0634\\u062f\\u0647\"}}', 13),
(27, 1, 'capsul_role', 'select_dropdown', 'نوع کاربر', 1, 1, 1, 1, 1, 1, '{\"default\":\"client\",\"options\":{\"client\":\"\\u06a9\\u0627\\u0631\\u0628\\u0631\",\"doctor\":\"\\u062f\\u06a9\\u062a\\u0631\",\"admin\":\"\\u0645\\u062f\\u06cc\\u0631\"}}', 16),
(28, 1, 'username', 'text', 'نام کاربری', 0, 1, 1, 1, 1, 1, '{}', 5),
(29, 1, 'api_token', 'text', 'Api Token', 0, 0, 0, 0, 0, 0, '{}', 20),
(30, 1, 'fcm_token', 'text', 'Fcm Token', 0, 0, 0, 0, 0, 0, '{}', 21),
(31, 1, 'phone_verified_at', 'text', 'زمان تایید شماره', 0, 1, 0, 0, 0, 0, '{}', 22),
(32, 1, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 23),
(33, 1, 'user_hasone_client_relationship', 'relationship', 'شماره کاربر', 0, 0, 0, 0, 0, 0, '{\"model\":\"App\\\\Client\",\"table\":\"clients\",\"type\":\"hasOne\",\"column\":\"user_id\",\"key\":\"user_id\",\"label\":\"user_id\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 24),
(34, 1, 'user_hasone_doctor_relationship', 'relationship', 'شماره متخصص', 0, 0, 0, 0, 0, 0, '{\"model\":\"App\\\\Doctor\",\"table\":\"doctors\",\"type\":\"hasOne\",\"column\":\"user_id\",\"key\":\"user_id\",\"label\":\"user_id\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 25),
(39, 5, 'user_id', 'text', 'ID', 1, 0, 0, 0, 0, 0, '{\"validation\":{\"rule\":\"required\"}}', 1),
(40, 5, 'wallet', 'text', 'کیف پول', 1, 1, 1, 1, 1, 1, '{\"default\":0}', 2),
(41, 5, 'age', 'text', 'سن', 0, 1, 1, 1, 1, 1, '{}', 3),
(42, 5, 'phone', 'text', 'شماره موبایل', 0, 1, 1, 1, 1, 1, '{}', 4),
(43, 5, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 5),
(44, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(45, 5, 'client_belongsto_user_relationship', 'relationship', 'آیدی کاربر', 0, 0, 0, 0, 0, 0, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(46, 6, 'user_id', 'text', 'User Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(47, 6, 'phone', 'text', 'شماره موبایل', 0, 1, 1, 1, 1, 1, '{}', 3),
(48, 6, 'wallet', 'text', 'کیف پول', 1, 1, 1, 1, 1, 1, '{\"default\":0}', 4),
(49, 6, 'sheba', 'text', 'شماره شبا', 0, 1, 1, 1, 1, 1, '{}', 5),
(50, 6, 'work_experience', 'text', 'سابقه کاری', 0, 1, 1, 1, 1, 1, '{}', 6),
(51, 6, 'exp_id', 'text', 'تخصص', 0, 1, 1, 1, 1, 1, '{}', 2),
(52, 6, 'dr_code', 'text', 'شماره نظام پزشکی', 0, 1, 1, 1, 1, 1, '{}', 7),
(53, 6, 'documents_path', 'file', 'فایل مدارک', 0, 1, 1, 0, 0, 1, '{}', 8),
(54, 6, 'created_at', 'timestamp', 'ساخته شده در', 0, 0, 0, 0, 0, 0, '{}', 9),
(55, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(56, 6, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 11),
(57, 6, 'doctor_belongsto_user_relationship', 'relationship', 'آیدی کاربر', 0, 0, 0, 0, 0, 0, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(58, 6, 'doctor_belongsto_expertise_relationship', 'relationship', 'تخصص', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Expertise\",\"table\":\"expertises\",\"type\":\"belongsTo\",\"column\":\"exp_id\",\"key\":\"id\",\"label\":\"expertise_name\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 13),
(63, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(64, 11, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(65, 11, 'answer1', 'text', 'پاسخ ۱', 0, 1, 1, 1, 1, 1, '{}', 3),
(66, 11, 'answer2', 'text', 'پاسخ ۲', 0, 1, 1, 1, 1, 1, '{}', 4),
(67, 11, 'answer3', 'text', 'پاسخ ۳', 0, 1, 1, 1, 1, 1, '{}', 5),
(68, 11, 'answer4', 'text', 'پاسخ ۴', 0, 1, 1, 1, 1, 1, '{}', 6),
(69, 11, 'answer5', 'text', 'پاسخ ۵', 0, 1, 1, 1, 1, 1, '{}', 7),
(70, 11, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 8),
(71, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(72, 11, 'client_initial_history_belongsto_client_relationship', 'relationship', 'نام کاربری', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Client\",\"table\":\"clients\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"user_id\",\"label\":\"name\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(73, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(74, 12, 'user_id', 'text', 'کاربر', 1, 0, 0, 0, 0, 0, '{}', 2),
(75, 12, 'dr_id', 'text', 'دکتر', 0, 0, 0, 0, 0, 0, '{}', 3),
(76, 12, 'title', 'text', 'عنوان', 1, 1, 1, 1, 1, 1, '{}', 4),
(77, 12, 'desc', 'text', 'توضیحات', 0, 1, 1, 1, 1, 1, '{}', 5),
(78, 12, 'answer', 'text_area', 'پاسخ', 0, 1, 1, 1, 1, 1, '{}', 6),
(79, 12, 'status', 'select_dropdown', 'وضعیت', 1, 1, 1, 1, 1, 1, '{\"default\":\"viewed\",\"options\":{\"viewed\":\"\\u0628\\u0631\\u0631\\u0633\\u06cc \\u0634\\u062f\",\"waiting\":\"\\u0645\\u0646\\u062a\\u0638\\u0631 \\u0628\\u0631\\u0631\\u0633\\u06cc\",\"closed\":\"\\u0628\\u0633\\u062a\\u0647 \\u0634\\u062f\"}}', 7),
(80, 12, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 8),
(81, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(82, 12, 'complaint_belongsto_client_relationship', 'relationship', 'کاربر', 0, 1, 1, 0, 0, 1, '{\"model\":\"App\\\\Client\",\"table\":\"clients\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"user_id\",\"label\":\"name\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(83, 12, 'complaint_hasone_doctor_relationship', 'relationship', 'متخصص', 0, 0, 0, 0, 0, 0, '{\"model\":\"App\\\\Doctor\",\"table\":\"doctors\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"user_id\",\"label\":\"name\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(84, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(85, 13, 'expertise_name', 'text', 'نام تخصص', 1, 1, 1, 1, 1, 1, '{}', 2),
(86, 13, 'desc', 'text', 'توضیحات', 0, 1, 1, 1, 1, 1, '{}', 3),
(87, 13, 'cost', 'text', 'قیمت', 1, 1, 1, 1, 1, 1, '{}', 4),
(88, 13, 'icon', 'image', 'آیکون', 0, 1, 1, 1, 1, 1, '{}', 5),
(89, 13, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 6),
(90, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(91, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(92, 14, 'user_id', 'text', 'User Id', 1, 0, 0, 0, 0, 0, '{}', 2),
(93, 14, 'sick', 'text', 'بیماری', 1, 1, 1, 1, 1, 1, '{}', 3),
(94, 14, 'desc', 'text', 'توضیحات', 0, 1, 1, 1, 1, 1, '{}', 4),
(95, 14, 'added_by', 'select_dropdown', 'اضافه شده توسط', 1, 1, 1, 1, 1, 1, '{\"default\":\"client\",\"options\":{\"client\":\"\\u06a9\\u0627\\u0631\\u0628\\u0631\",\"dr\":\"\\u062f\\u06a9\\u062a\\u0631\"}}', 5),
(96, 14, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 6),
(97, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(98, 14, 'user_sick_belongsto_client_relationship', 'relationship', 'کاربر', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Client\",\"table\":\"clients\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"user_id\",\"label\":\"name\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(99, 15, 'id', 'text', 'شماره درخواست', 1, 1, 1, 0, 0, 0, '{}', 1),
(100, 15, 'user_id', 'text', 'شماره کاربر', 1, 1, 1, 0, 0, 0, '{}', 2),
(101, 15, 'exp_id', 'text', 'شماره تخصص', 1, 0, 0, 0, 0, 0, '{}', 3),
(102, 15, 'dr_id', 'text', 'شماره دکتر', 0, 1, 1, 0, 0, 0, '{}', 4),
(103, 15, 'type', 'select_dropdown', 'نوع درخواست', 1, 1, 1, 1, 1, 1, '{\"default\":\"lab\",\"options\":{\"lab\":\"\\u0622\\u0632\\u0645\\u0627\\u06cc\\u0634\\u06af\\u0627\\u0647\\u06cc\",\"send_prescription\":\"\\u0627\\u0631\\u0633\\u0627\\u0644 \\u0646\\u0633\\u062e\\u0647\",\"renew_prescription\":\"\\u062a\\u0645\\u062f\\u06cc\\u062f \\u0646\\u0633\\u062e\\u0647\"}}', 5),
(104, 15, 'exp_cost', 'text', 'هزینه درخواست', 1, 1, 1, 0, 0, 1, '{}', 6),
(105, 15, 'main_reason', 'text', 'دلیل اصلی مراجعه به پزشک', 0, 1, 1, 1, 1, 1, '{}', 7),
(106, 15, 're_visit', 'text', 'مراجعه مجدد', 0, 1, 1, 1, 1, 1, '{}', 8),
(107, 15, 'answer1', 'text', 'پاسخ ۱', 0, 1, 1, 1, 1, 1, '{}', 9),
(108, 15, 'answer2', 'text', 'پاسخ ۲', 0, 1, 1, 1, 1, 1, '{}', 10),
(109, 15, 'answer3', 'text', 'پاسخ ۳', 0, 1, 1, 1, 1, 1, '{}', 11),
(110, 15, 'read_by_dr', 'text', 'خوانده شده توسط دکتر', 0, 0, 0, 0, 0, 0, '{}', 12),
(111, 15, 'dr_first_answer', 'text', 'توصیه پزشک', 0, 1, 1, 1, 1, 1, '{}', 13),
(112, 15, 'final_diagnosis', 'text', 'تشخیص نهایی', 0, 1, 1, 1, 1, 1, '{}', 14),
(113, 15, 'status', 'select_dropdown', 'وضعیت', 1, 1, 1, 1, 1, 1, '{\"default\":\"waiting\",\"options\":{\"accepted\":\"\\u0642\\u0628\\u0648\\u0644 \\u0634\\u062f\\u0647\",\"rejected\":\"\\u0631\\u062f \\u0634\\u062f\\u0647\",\"finished\":\"\\u062a\\u0645\\u0627\\u0645 \\u0634\\u062f\\u0647\",\"reserved\":\"\\u0631\\u0632\\u0631\\u0648 \\u0634\\u062f\\u0647\",\"waiting\":\"\\u0645\\u0646\\u062a\\u0638\\u0631 \\u062a\\u0627\\u06cc\\u06cc\\u062f \\u067e\\u0632\\u0634\\u06a9\"}}', 15),
(114, 15, 'reject_reason', 'select_dropdown', 'علت رد', 0, 1, 1, 1, 1, 1, '{\"default\":\"null\",\"options\":{\"null\":\"\\u0639\\u062f\\u0645 \\u0631\\u062f \\u062f\\u0631\\u062e\\u0648\\u0627\\u0633\\u062a\",\"presence\":\"\\u062d\\u0636\\u0648\\u0631\\u06cc\",\"emergency\":\"\\u0627\\u0648\\u0631\\u0698\\u0627\\u0646\\u0633\",\"other_exp\":\"\\u062a\\u062e\\u0635\\u0635 \\u062f\\u06cc\\u06af\\u0631\"}}', 16),
(115, 15, 'score', 'number', 'امتیاز', 0, 1, 1, 1, 1, 1, '{\"step\":1,\"min\":0,\"max\":5}', 18),
(116, 15, 'reserve_date', 'timestamp', 'تاریخ رزرو', 0, 1, 1, 1, 1, 1, '{}', 19),
(117, 15, 'finished_at', 'timestamp', 'Finished At', 1, 0, 0, 0, 0, 0, '{}', 23),
(118, 15, 'created_at', 'timestamp', 'زمان درخواست', 0, 1, 1, 0, 0, 0, '{}', 22),
(119, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 24),
(120, 15, 'user_request_belongsto_client_relationship', 'relationship', 'کاربر', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Client\",\"table\":\"clients\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"user_id\",\"label\":\"name\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 20),
(121, 15, 'user_request_belongsto_doctor_relationship', 'relationship', 'متخصص', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Doctor\",\"table\":\"doctors\",\"type\":\"belongsTo\",\"column\":\"dr_id\",\"key\":\"user_id\",\"label\":\"name\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 21),
(122, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(123, 16, 'req_id', 'text', 'Req Id', 1, 0, 0, 0, 0, 0, '{}', 2),
(124, 16, 'rateAnswer1', 'text', 'عدم پاسخ به موقع', 0, 1, 1, 1, 1, 1, '{}', 3),
(125, 16, 'rateAnswer2', 'text', 'عدم داروی تاثیر گذار', 0, 1, 1, 1, 1, 1, '{}', 4),
(126, 16, 'rateAnswer3', 'text', 'عدم برخورد مناسب', 0, 1, 1, 1, 1, 1, '{}', 5),
(127, 16, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 6),
(128, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(129, 16, 'rate_answer_belongsto_user_request_relationship', 'relationship', 'شماره درخواست', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\UserRequest\",\"table\":\"user_requests\",\"type\":\"belongsTo\",\"column\":\"req_id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(131, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(132, 17, 'sender_id', 'text', 'Sender Id', 1, 0, 0, 0, 0, 0, '{}', 3),
(133, 17, 'req_id', 'text', 'شماره دخواست', 1, 1, 1, 0, 0, 0, '{}', 2),
(134, 17, 'sender_type', 'select_dropdown', 'نوع فرستنده', 1, 1, 1, 1, 1, 1, '{\"default\":\"client\",\"options\":{\"client\":\"\\u06a9\\u0627\\u0631\\u0628\\u0631\",\"dr\":\"\\u062f\\u06a9\\u062a\\u0631\"}}', 4),
(135, 17, 'msg', 'text', 'پیام', 0, 1, 1, 1, 1, 1, '{}', 5),
(136, 17, 'msg_type', 'select_dropdown', 'نوع پیام', 1, 1, 1, 1, 1, 1, '{\"default\":\"text\",\"options\":{\"text\":\"\\u0645\\u062a\\u0646\",\"file\":\"\\u0641\\u0627\\u06cc\\u0644\",\"audio\":\"\\u0635\\u0648\\u062a\\u06cc\",\"video\":\"\\u0641\\u06cc\\u0644\\u0645\",\"img\":\"\\u0639\\u06a9\\u0633\"}}', 6),
(137, 17, 'read_status', 'text', 'Read Status', 1, 0, 0, 0, 0, 0, '{}', 7),
(138, 17, 'path', 'file', 'فایل', 0, 1, 1, 1, 1, 1, '{}', 8),
(139, 17, 'created_at', 'timestamp', 'زمان ارسال', 0, 1, 1, 0, 0, 0, '{}', 12),
(140, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(141, 17, 'chat_message_belongsto_client_relationship', 'relationship', 'فرستنده کاربر', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Client\",\"table\":\"clients\",\"type\":\"belongsTo\",\"column\":\"sender_id\",\"key\":\"user_id\",\"label\":\"name\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(142, 17, 'chat_message_belongsto_doctor_relationship', 'relationship', 'فرستنده متخصص', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Doctor\",\"table\":\"doctors\",\"type\":\"belongsTo\",\"column\":\"sender_id\",\"key\":\"user_id\",\"label\":\"name\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(143, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(144, 18, 'answer_time', 'number', 'حداکثر زمان پاسخ دهی', 1, 1, 1, 1, 0, 0, '{}', 2),
(145, 18, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(146, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(176, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(177, 21, 'user_id', 'text', 'User Id', 1, 0, 0, 0, 0, 0, '{}', 2),
(178, 21, 'factorId', 'text', 'شماره فاکتور', 0, 1, 1, 0, 1, 1, '{}', 3),
(179, 21, 'transId', 'text', 'شماره تراکنش', 0, 1, 1, 0, 1, 1, '{}', 4),
(180, 21, 'traceNumber', 'text', 'شماره پیگیری', 0, 1, 1, 0, 1, 1, '{}', 5),
(181, 21, 'amount', 'text', 'مبلغ', 1, 1, 1, 0, 1, 1, '{}', 6),
(182, 21, 'mobile', 'text', 'شماره موبایل', 0, 0, 0, 0, 0, 0, '{}', 7),
(183, 21, 'cardNumber', 'text', 'شماره کارت', 0, 1, 1, 0, 1, 1, '{}', 8),
(184, 21, 'description', 'text', 'توضیحات', 0, 1, 1, 0, 1, 0, '{}', 9),
(185, 21, 'messageReceived', 'text', 'پیام دریافتی', 0, 1, 1, 0, 0, 1, '{}', 10),
(186, 21, 'status', 'text', 'وضعیت', 0, 1, 1, 0, 1, 1, '{}', 11),
(187, 21, 'comeFrom', 'text', 'ComeFrom', 1, 0, 0, 0, 0, 0, '{}', 12),
(188, 21, 'created_at', 'timestamp', 'زمان ثبت', 0, 1, 1, 0, 0, 1, '{}', 13),
(189, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(190, 21, 'trans_action_hasone_user_relationship', 'relationship', 'نام کاربری', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"username\",\"pivot_table\":\"capsul_settings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 15),
(191, 22, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(192, 22, 'image', 'image', 'عکس', 1, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"validation\":{\"rule\":\"image|mimes:jpg,jpeg|max:1000\"}}', 2),
(193, 22, 'link', 'text', 'لینک', 0, 1, 1, 1, 1, 1, '{}', 3),
(194, 22, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 1, '{}', 4),
(195, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(196, 1, 'recovery_password', 'text', 'Recovery Password', 0, 0, 0, 0, 0, 0, '{}', 14),
(198, 15, 'viewer_checked', 'select_dropdown', 'مورد تایید ناظر', 0, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"\\u062e\\u06cc\\u0631\",\"1\":\"\\u0628\\u0644\\u0647\"}}', 17),
(199, 5, 'name', 'text', 'نام کاربر', 0, 1, 1, 1, 1, 1, '{}', 2),
(200, 6, 'name', 'text', 'نام کاربر', 0, 1, 1, 1, 1, 1, '{}', 3),
(201, 15, 'cost_back_client', 'text', 'Cost Back Client', 0, 0, 0, 0, 0, 0, '{}', 7),
(202, 15, 'cost_back_doctor', 'text', 'Cost Back Doctor', 0, 0, 0, 0, 0, 0, '{}', 8),
(204, 18, 'terms_dr', 'text', 'قوانین متخصص', 0, 1, 1, 1, 0, 0, '{}', 3),
(205, 18, 'terms_client', 'text', 'قوانین کاربر', 0, 1, 1, 1, 0, 0, '{}', 4),
(206, 18, 'app_dr', 'file', 'اپلیکیشن دکتر', 0, 1, 0, 1, 0, 0, '{}', 5),
(207, 18, 'app_client', 'file', 'اپلیکیشن کلاینت', 0, 1, 0, 1, 0, 0, '{}', 6),
(208, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(209, 23, 'user_id', 'text', 'User Id', 1, 0, 0, 0, 0, 0, '{}', 2),
(210, 23, 'dr_id', 'text', 'Dr Id', 0, 0, 0, 0, 0, 0, '{}', 3),
(211, 23, 'title', 'text', 'عنوان', 1, 1, 1, 1, 1, 1, '{}', 4),
(212, 23, 'desc', 'text', 'توضیحات', 0, 1, 1, 1, 1, 1, '{}', 5),
(213, 23, 'answer', 'text', 'پاسخ', 0, 1, 1, 1, 1, 1, '{}', 6),
(214, 23, 'status', 'select_dropdown', 'وضعیت', 1, 1, 1, 1, 1, 1, '{\"default\":\"viewed\",\"options\":{\"viewed\":\"\\u0628\\u0631\\u0631\\u0633\\u06cc \\u0634\\u062f\",\"waiting\":\"\\u0645\\u0646\\u062a\\u0638\\u0631 \\u0628\\u0631\\u0631\\u0633\\u06cc\",\"closed\":\"\\u0628\\u0633\\u062a\\u0647 \\u0634\\u062f\"}}', 7),
(215, 23, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 8),
(216, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(217, 23, 'support_hasone_doctor_relationship', 'relationship', 'نام متخصص', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Doctor\",\"table\":\"doctors\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"user_id\",\"label\":\"name\",\"pivot_table\":\"banners\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(218, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(219, 24, 'img', 'image', 'عکس', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"validation\":{\"rule\":\"image|mimes:jpg,jpeg|max:3000\"}}', 2),
(220, 24, 'title', 'text', 'عنوان', 1, 1, 1, 1, 1, 1, '{}', 3),
(221, 24, 'description', 'text_area', 'توضیحات', 1, 1, 1, 1, 1, 1, '{}', 4),
(222, 24, 'created_at', 'timestamp', 'زمان ثبت', 0, 1, 1, 0, 0, 0, '{}', 5),
(223, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(224, 15, 'user_request_hasone_expertise_relationship', 'relationship', 'نام تخصص', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Expertise\",\"table\":\"expertises\",\"type\":\"belongsTo\",\"column\":\"exp_id\",\"key\":\"id\",\"label\":\"expertise_name\",\"pivot_table\":\"banners\",\"pivot\":\"0\",\"taggable\":\"0\"}', 25),
(225, 13, 'sort_col', 'text', 'Sort Col', 0, 0, 0, 0, 0, 0, '{}', 2);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'کاربر', 'مدیران', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2019-09-14 08:54:57', '2019-12-14 08:09:17'),
(2, 'menus', 'menus', 'منو', 'منوها', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2019-09-14 08:54:57', '2019-12-14 08:08:04'),
(3, 'roles', 'roles', 'نقش', 'نقش ها', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2019-09-14 08:54:57', '2019-12-14 08:08:24'),
(5, 'clients', 'clients', 'کلاینت', 'کلاینت ها', NULL, 'App\\Client', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-14 09:20:12', '2019-12-14 08:07:32'),
(6, 'doctors', 'doctors', 'پزشک', 'پزشک ها', NULL, 'App\\Doctor', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-14 10:40:05', '2019-12-14 08:07:53'),
(11, 'client_initial_histories', 'client-initial-histories', 'سوابق اولیه کاربر', 'سوابق اولیه کاربر', NULL, 'App\\InitHistory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-14 12:36:54', '2019-12-14 08:07:20'),
(12, 'complaints', 'complaints', 'پیشنهادات', 'پیشنهادات', NULL, 'App\\Complaint', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-14 13:08:14', '2019-12-14 08:07:40'),
(13, 'expertises', 'expertises', 'تخصص‌ها', 'تخصص‌ها', NULL, 'App\\Expertise', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"sort_col\",\"order_display_column\":\"expertise_name\",\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-14 13:27:43', '2019-12-14 08:16:35'),
(14, 'user_sicks', 'user-sicks', 'سوابق بیماری کاربر', 'سوابق بیماری کاربر', NULL, 'App\\UserSick', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-14 13:31:39', '2019-12-14 08:09:06'),
(15, 'user_requests', 'user-requests', 'درخواست‌', 'درخواست‌ها', NULL, 'App\\UserRequest', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2019-09-14 13:54:52', '2019-12-14 08:08:56'),
(16, 'rate_answers', 'rate-answers', 'نظرسنجی درخواست‌ها', 'نظرسنجی درخواست‌ها', NULL, 'App\\RateAnswer', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-14 13:59:55', '2019-12-14 08:08:16'),
(17, 'chat_messages', 'chat-messages', 'پیام‌', 'پیام‌ها', NULL, 'App\\ChatMessage', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-14 14:09:52', '2019-11-11 17:08:52'),
(18, 'capsul_settings', 'capsul-settings', 'تنظیمات کپسول', 'تنظیمات کپسول', NULL, 'App\\CapsulSetting', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-15 12:17:25', '2019-11-16 14:06:41'),
(21, 'trans_actions', 'trans-actions', 'تراکنش', 'تراکنش ها', NULL, 'App\\TransAction', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-10-04 16:09:06', '2019-11-11 17:11:25'),
(22, 'banners', 'banners', 'Banner', 'Banners', NULL, 'App\\Banner', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-10-15 07:48:38', '2019-10-15 23:52:43'),
(23, 'supports', 'supports', 'پشتیبانی', 'پیام های پشتیبانی', NULL, 'App\\Support', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-11-16 15:57:59', '2019-11-16 16:42:16'),
(24, 'blogs', 'blogs', 'بلاگ', 'بلاگ ها', NULL, 'App\\Blog', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"created_at\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2019-12-01 15:06:58', '2019-12-03 00:50:51');

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wallet` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `sheba` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_experience` tinyint(4) DEFAULT NULL,
  `exp_id` bigint(20) UNSIGNED DEFAULT NULL,
  `dr_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `documents_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expertises`
--

CREATE TABLE `expertises` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sort_col` int(11) DEFAULT NULL,
  `expertise_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `cost` mediumint(9) NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(1, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"\\u062e\\u0648\\u0628\\u06cc\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:24;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568467341, 1568467341),
(2, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:12:\\\"\\u06a9\\u0628\\u0646\\u06cc\\u0646\\u06cc\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1570452579, 1570452579),
(3, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1570452588, 1570452588),
(4, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:9:\\\"test seda\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1570452934, 1570452934),
(5, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:9:\\\"test seda\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1570455680, 1570455680),
(6, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:10:\\\"\\u06a9\\u0627\\u0644\\u0627\\u062e\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:24;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1570457655, 1570457655),
(7, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:24;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1570457670, 1570457670),
(8, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"skdjghsd\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571064772, 1571064772),
(9, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:7:\\\"jvxludu\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571074029, 1571074029),
(10, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571074333, 1571074333),
(11, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:9:\\\"test seda\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571074804, 1571074804),
(12, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571088787, 1571088787),
(13, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571088807, 1571088807),
(14, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571092233, 1571092233),
(15, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571092355, 1571092355),
(16, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:14:\\\"\\u0627\\u0646\\u0644\\u062d\\u0628\\u0628\\u062d\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571092541, 1571092541),
(17, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571092601, 1571092601),
(18, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571092634, 1571092634),
(19, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571092732, 1571092732),
(20, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571092768, 1571092768),
(21, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571092920, 1571092920),
(22, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571093208, 1571093208),
(23, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571093314, 1571093314),
(24, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571093887, 1571093887),
(25, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571093990, 1571093990),
(26, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571094378, 1571094378),
(27, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571095571, 1571095571),
(28, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571141806, 1571141806),
(29, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571141848, 1571141848),
(30, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571141905, 1571141905),
(31, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:6:\\\"gyuuyf\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571141964, 1571141964),
(32, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571141973, 1571141973),
(33, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:32:\\\"\\u0633\\u0645\\u0644\\u0646\\u0645\\u0633\\u06cc\\u0645\\u0628 \\n\\u0634\\u0633\\u06cc\\u0628\\u0633\\u0634\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:25;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571143366, 1571143366),
(34, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:32:\\\"\\u0633\\u0645\\u0644\\u0646\\u0645\\u0633\\u06cc\\u0645\\u0628 \\n\\u0634\\u0633\\u06cc\\u0628\\u0633\\u0634\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:25;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571143370, 1571143370),
(35, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:32:\\\"\\u0633\\u0645\\u0644\\u0646\\u0645\\u0633\\u06cc\\u0645\\u0628 \\n\\u0634\\u0633\\u06cc\\u0628\\u0633\\u0634\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:25;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571143394, 1571143394),
(36, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571143511, 1571143511),
(37, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571144017, 1571144017),
(38, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571144217, 1571144217),
(39, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:6:\\\"hcdduj\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:26;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571144309, 1571144309),
(40, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:5:\\\"hsheu\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1571303946, 1571303946),
(41, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:7:\\\"salamsm\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:28;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572164711, 1572164711),
(42, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:154:\\\"\\u0633\\u0644\\u0627\\u0645\\u060c \\u0633\\u0631\\u062f\\u0631\\u062f \\u062f\\u0631 \\u06a9\\u062f\\u0627\\u0645 \\u0642\\u0633\\u0645\\u062a \\u0633\\u0631\\u062a\\u0648\\u0646 \\u0647\\u0633\\u062a\\u061f\\n\\u0627\\u0628\\u0627 \\u0636\\u0631\\u0628\\u0627\\u0646\\u062f\\u0627\\u0631 \\u0647\\u0633\\u062a \\u06cc\\u0627 \\u0627\\u06cc\\u0646\\u06a9\\u0647 \\u062f\\u0631\\u062f \\u0645\\u062f\\u0627\\u0648\\u0645 \\u0648 \\u0641\\u0634\\u0627\\u0631\\u06cc \\u062f\\u0627\\u0631\\u0647\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572349966, 1572349966),
(43, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:73:\\\"\\u0633\\u0644\\u0627\\u0645\\u060c \\u0631\\u0648\\u06cc \\u0634\\u0642\\u06cc\\u0642\\u0647 \\u0647\\u0627\\u0645\\n\\u0636\\u0631\\u0628\\u0646 \\u0646\\u062f\\u0627\\u0631\\u0647 \\u0641\\u0634\\u0627\\u0631 \\u062f\\u0627\\u0626\\u0645\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572350014, 1572350014),
(44, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572350108, 1572350108),
(45, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:33:\\\"\\u0633\\u0631\\u062f\\u0631\\u062f \\u0634\\u0645\\u0627 \\u062a\\u0646\\u0634\\u0646 \\u0627\\u0633\\u062a\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572409443, 1572409443),
(46, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:87:\\\"\\u0633\\u0644\\u0627\\u0645 \\u062d\\u0627\\u0645\\u062f \\u0627\\u06cc\\u0646\\u0648 \\u062c\\u0648\\u0627\\u0628 \\u0628\\u062f\\u0647 \\u0633\\u0648\\u0627\\u0644\\u0627\\u062a\\u0648 \\u0628\\u067e\\u0631\\u0633 \\u062a\\u0627 \\u0628\\u0631\\u06cc\\u0645 \\u062c\\u0644\\u0648\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572461280, 1572461280),
(47, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:54;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572462891, 1572462891),
(48, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:15:\\\"gfdfhyhgfrrsfhk\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572549533, 1572549533),
(49, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:4:\\\"\\u06f3\\u06f5\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572625266, 1572625266),
(50, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:60:\\\"\\u0686\\u0647 \\u0645\\u062f\\u062a \\u0627\\u0645\\u0648\\u067e\\u0631\\u0627\\u0632\\u0648\\u0644 \\u0627\\u0633\\u062a\\u0641\\u0627\\u062f\\u0647 \\u0645\\u06cc\\u06a9\\u0646\\u06cc\\u062f\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:54;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572626225, 1572626225),
(51, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:9:\\\"\\u06f1 \\u0645\\u0627\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572626688, 1572626688),
(52, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572626723, 1572626723),
(53, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"\\u0627\\u0648\\u06a9\\u06cc\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:54;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572627430, 1572627430),
(54, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:54;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572627560, 1572627560),
(55, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"\\u0628\\u067e\\u0631\\u0633\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572627748, 1572627748),
(56, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572628178, 1572628178),
(57, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:59:\\\"\\u062f\\u0631\\u0645\\u0627\\u0646 3 \\u062f\\u0627\\u0631\\u0648\\u06cc\\u06cc \\u06a9\\u0647 \\u0646\\u0633\\u062e\\u0634 \\u0627\\u0631\\u0633\\u0627\\u0644 \\u0645\\u06cc\\u0634\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:54;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572628442, 1572628442),
(58, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:112:\\\"\\u0648 \\u0631\\u0698\\u06cc\\u0645 \\u063a\\u0630\\u0627\\u06cc\\u06cc \\u06a9\\u0647 \\u0627\\u0632 \\u0637\\u0631\\u06cc\\u0642 \\u0647\\u0645\\u06cc\\u0646 \\u0627\\u067e\\u0644\\u06cc\\u06a9\\u0634\\u0646 \\u062a\\u0627 \\u0641\\u0631\\u062f\\u0627 \\u0627\\u0631\\u0633\\u0627\\u0644 \\u0645\\u06cc\\u0634\\u0647 \\u0628\\u0631\\u0627\\u062a\\u0648\\u0646\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:54;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572628474, 1572628474),
(59, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:146:\\\"\\u0627\\u06af\\u0631 \\u062a\\u0627 3 \\u0645\\u0627\\u0647 \\u0627\\u06cc\\u0646\\u062f\\u0647 \\u0628\\u0647\\u062a\\u0631 \\u0646\\u0634\\u062f\\u06cc\\u062f \\u0645\\u062c\\u062f\\u062f\\u0627 \\u0648\\u06cc\\u0632\\u06cc\\u062a \\u062e\\u0648\\u0627\\u0647\\u06cc\\u062f \\u0634\\u062f \\u062c\\u0647\\u062a \\u0646\\u06cc\\u0627\\u0632 \\u0627\\u062d\\u062a\\u0645\\u0627\\u0644\\u06cc \\u0628\\u0647 \\u0627\\u0646\\u062f\\u0648\\u0633\\u06a9\\u0648\\u067e\\u06cc\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:54;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572628520, 1572628520),
(60, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:32:\\\"\\u0633\\u0648\\u0632\\u0634 \\u0627\\u062f\\u0631\\u0627\\u0631 \\u062f\\u0627\\u0631\\u06cc\\u062f\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:54;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572628587, 1572628587),
(61, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:6:\\\"\\u0628\\u0644\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572628957, 1572628957),
(62, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:48:\\\"\\u062a\\u0628 \\u0648 \\u0644\\u0631\\u0632 \\u0648 \\u062f\\u0631\\u062f \\u067e\\u0647\\u0644\\u0648\\u0647\\u0627 \\u0686\\u0637\\u0648\\u0631\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:54;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572629043, 1572629043),
(63, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:26:\\\"\\u062f\\u0631\\u062f \\u067e\\u0647\\u0644\\u0648 \\u0646\\u062f\\u0627\\u0631\\u0645\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572629328, 1572629328),
(64, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:32:\\\"\\u0632\\u06cc\\u0631 \\u062f\\u0644\\u0645 \\u062f\\u0631\\u062f \\u0645\\u06cc \\u06a9\\u0646\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572629344, 1572629344),
(65, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:27:\\\"\\u0627\\u0628 \\u0632\\u06cc\\u0627\\u062f \\u0645\\u06cc \\u062e\\u0648\\u0631\\u0645\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572629359, 1572629359);
INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(66, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572638932, 1572638932),
(67, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572639000, 1572639000),
(68, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572639058, 1572639058),
(69, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:6:\\\"\\u0686\\u06cc\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572639088, 1572639088),
(70, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572639126, 1572639126),
(71, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:42:\\\"\\u062a\\u0631\\u0634\\u062d \\u0686\\u0631\\u06a9\\u06cc \\u0628\\u062f \\u0628\\u0648 \\u0646\\u062f\\u0627\\u0631\\u06cc\\u062f\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:54;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572694473, 1572694473),
(72, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:54;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572694958, 1572694958),
(73, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:67:\\\"\\u062f\\u0627\\u0631\\u0648 \\u0648 \\u0627\\u0632\\u0645\\u0627\\u06cc\\u0634 \\u0628\\u0631\\u0627\\u06cc \\u0634\\u0645\\u0627 \\u0627\\u0631\\u0633\\u0627\\u0644 \\u062e\\u0648\\u0627\\u0647\\u062f \\u0634\\u062f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:54;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572695654, 1572695654),
(74, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:14:\\\"jdjd djd dhdjd\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790463, 1572790463),
(75, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:9:\\\"jrirhdhre\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790466, 1572790466),
(76, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:20:\\\"hehrjeu3v3ce rhdhehe\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790469, 1572790469),
(77, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:9:\\\"hfjriejdd\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790487, 1572790487),
(78, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:21:\\\"hrhr r fjfbr rbrjirjr\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790491, 1572790491),
(79, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:39:\\\"hrjrvr rhjricicide rhdvr  rjrjr r rhdhd\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790495, 1572790495),
(80, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:68:\\\"rjrivrjfifbr fhvrbjrbr r r  r  rjrifidibe e r r rbjridodbr r veirid8\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790504, 1572790504),
(81, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:42:\\\"iujhxhx  sbsjejejje  4 rjdiroke  ebejeirie\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790512, 1572790512),
(82, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:41:\\\"irurhr  rrvr bririrjuriedge  r 4 rbhrieir\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790518, 1572790518),
(83, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:13:\\\"hrbe  r ebrie\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790520, 1572790520),
(84, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:7:\\\"nxjde d\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790528, 1572790528),
(85, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"firbe  e\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790531, 1572790531),
(86, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:17:\\\"idkhrhr rbfjr hrf\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790572, 1572790572),
(87, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:21:\\\"hrje ebrbe rheb rheir\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790596, 1572790596),
(88, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:21:\\\"3i39hevr r rvdhrjurid\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790598, 1572790598),
(89, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:13:\\\"hrhr rid evrj\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572790600, 1572790600),
(90, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:7:\\\"hdhdure\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572792110, 1572792110),
(91, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:12:\\\"hfjjer rjrbr\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572792121, 1572792121),
(92, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:7:\\\"hrurhre\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572792123, 1572792123),
(93, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:6:\\\"hrieiw\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572792144, 1572792144),
(94, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:15:\\\"bfjdhehrbr evei\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572792156, 1572792156),
(95, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:24:\\\"u4urhrhe r rbhevr rbrhvr\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572792166, 1572792166),
(96, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:24:\\\"uui4rfffgft t thrjhr t t\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572792175, 1572792175),
(97, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:13:\\\"krkrieb ebrhr\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:62;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572792314, 1572792314),
(98, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:62;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572792323, 1572792323),
(99, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:24:\\\"ncjdje ebeje bebe ejekek\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572792350, 1572792350),
(100, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572792362, 1572792362),
(101, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:20:\\\"salam khoshgel pesar\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572798230, 1572798230),
(102, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"hdhdhdhd\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572798246, 1572798246),
(103, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572798267, 1572798267),
(104, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572798329, 1572798329),
(105, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:43:\\\"\\u0627\\u0642\\u0627 \\u062c\\u0648\\u0627\\u0628 \\u0645\\u0646\\u0648 \\u0628\\u062f\\u0647\\u0647\\u0647\\u0647\\u0647\\u0647\\u0647\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572798593, 1572798593),
(106, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"\\u062f\\u06cc\\u062f\\u06cc\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572798775, 1572798775),
(107, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:20:\\\"\\u0646\\u0631\\u062a\\u0628\\u062a\\u0631\\u0632\\u062e\\u0632\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572800170, 1572800170),
(108, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:22:\\\"\\u0645\\u0647\\u0628\\u0645\\u0647\\u0647\\u0645\\u06cc\\u062c\\u06f6\\u06cc\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:62;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572801634, 1572801634),
(109, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:38:\\\"\\u062d\\u0647\\u0628\\u0647\\u0646\\u06cc\\u062d\\u0639\\u06cc\\u0639\\u062d\\u0639\\u0639\\u0639\\u0639\\u06f6\\u06f6\\u06f7\\u06f7\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572801642, 1572801642),
(110, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"\\u0627\\u0627\\u0647\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572801868, 1572801868),
(111, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"\\u0627\\u0644\\u0639\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572801946, 1572801946),
(112, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:10:\\\"\\u0627\\u0647\\u0647\\u06f7\\u06f8\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572802016, 1572802016),
(113, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:6:\\\"\\u06f7\\u06f8\\u06f8\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572802927, 1572802927),
(114, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:4:\\\"\\u0630\\u0627\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572803324, 1572803324),
(115, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:4:\\\"\\u0630\\u0630\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572803354, 1572803354),
(116, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:4:\\\"\\u062f\\u062a\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572803392, 1572803392),
(117, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:15:\\\"\\u06cc\\u0648\\u0648\\u06cc \\u06cc\\u062a\\u06cc\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:48;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1572803396, 1572803396),
(118, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:5:\\\"hhgfg\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1573197064, 1573197064),
(119, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:3:\\\"nnn\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:54;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1573197410, 1573197410),
(120, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:3:\\\"vfd\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1573203516, 1573203516),
(121, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:3:\\\"xxf\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1573203518, 1573203518),
(122, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:5:\\\"hhgfg\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1573203520, 1573203520),
(123, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:5:\\\"salam\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1573740908, 1573740908),
(124, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1573741164, 1573741164),
(125, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:6:\\\"\\u0644\\u0627\\u0645\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1573754241, 1573754241),
(126, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"\\u0633\\u0644\\u0627\\u0645\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1575959900, 1575959900),
(127, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:5:\\\"salam\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:73;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1575961132, 1575961132),
(128, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:14:\\\"aslfmsaf;asfsa\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:74;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1575961193, 1575961193),
(129, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:35:\\\"\\u0633\\u0644\\u0627\\u0645 \\u0648\\u0648\\u06cc\\u0633 \\u0646\\u0645\\u06cc \\u0641\\u0631\\u0633\\u062a\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1575966067, 1575966067),
(130, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:5:\\\"salam\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:73;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1575974906, 1575974906),
(131, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:32:\\\"\\u0633\\u0645\\u0644\\u0646\\u0645\\u0633\\u06cc\\u0645\\u0628 \\n\\u0634\\u0633\\u06cc\\u0628\\u0633\\u0634\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:66;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1575978323, 1575978323),
(132, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:4:\\\"huij\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:66;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1575992490, 1575992490);
INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(133, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:66;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1575993151, 1575993151),
(134, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:5:\\\"salam\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:73;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1575993416, 1575993416),
(135, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:73;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1575993433, 1575993433),
(136, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:18:\\\"meske dorost shode\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:73;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1575993440, 1575993440),
(137, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:17:\\\"hala chet shode??\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:73;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1575993449, 1575993449),
(138, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:4:\\\"hehe\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:73;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1575993451, 1575993451),
(139, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:32:\\\"\\u0633\\u0644\\u0627\\u0645 \\u0628\\u0647\\u062a\\u0631 \\u0634\\u062f\\u06cc\\u062f\\u061f\\ud83e\\udd14\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576224954, 1576224954),
(140, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:24:\\\"\\u0633\\u0644\\u0627\\u0645 \\u0648\\u0642\\u062a \\u0628\\u062e\\u06cc\\u0631\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576261177, 1576261177),
(141, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:57:\\\"\\u0633\\u0631\\u062f\\u0631\\u062f\\u062a\\u0648\\u0646 \\u06cc\\u06a9\\u0637\\u0631\\u0641\\u0647 \\u0627\\u0633\\u062a \\u06cc\\u0627 \\u062a\\u0645\\u0627\\u0645 \\u0633\\u0631\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576261197, 1576261197),
(142, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:60:\\\"\\u0627\\u06cc\\u0627 \\u0646\\u0648\\u0631 \\u0648 \\u0633\\u0631 \\u0648 \\u0635\\u062f\\u0627 \\u0627\\u0630\\u06cc\\u062a\\u062a\\u0648\\u0646 \\u0645\\u06cc \\u06a9\\u0646\\u0647\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576261217, 1576261217),
(143, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:53:\\\"\\u0627\\u0633\\u062a\\u0641\\u0631\\u0627\\u063a \\u0647\\u0645 \\u06a9\\u0631\\u062f\\u06cc\\u0646 \\u06cc\\u0627 \\u0641\\u0642\\u0637 \\u062a\\u0627\\u0648\\u0639\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576261238, 1576261238),
(144, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"\\u062a\\u0647\\u0648\\u0639\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576261244, 1576261244),
(145, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:56:\\\"\\u0642\\u0628\\u0644\\u0627 \\u0647\\u0645 \\u0627\\u0632 \\u0627\\u06cc\\u0646 \\u0633\\u0631\\u062f\\u0631\\u062f \\u0647\\u0627 \\u062f\\u0627\\u0634\\u062a\\u06cc\\u0646\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576261282, 1576261282),
(146, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576261308, 1576261308),
(147, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:50:\\\"\\u0633\\u0644\\u0627\\u0645 \\u0648\\u0642\\u062a\\u062a\\u0648\\u0646 \\u0628\\u062e\\u06cc\\u0631 \\u062f\\u06a9\\u062a\\u0631 \\u062f\\u0631\\u0628\\u0627\\u0646\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576262795, 1576262795),
(148, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:13:\\\"\\u062a\\u0645\\u0627\\u0645 \\u0633\\u0631\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576262803, 1576262803),
(149, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:24:\\\"\\u0628\\u06cc\\u0634\\u062a\\u0631 \\u062c\\u0644\\u0648\\u06cc \\u0633\\u0631\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576262807, 1576262807),
(150, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576262840, 1576262840),
(151, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576262882, 1576262882),
(152, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576262909, 1576262909),
(153, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:196:\\\"\\u062a\\u0645\\u0627\\u0645 \\u0631\\u0648\\u0632 \\u062d\\u0627\\u0644\\u062a \\u062a\\u0647\\u0648\\u0639 \\u062f\\u0627\\u0631\\u0645 \\u0641\\u06a9\\u0631 \\u0645\\u06cc\\u06a9\\u0646\\u0645 \\u0645\\u06cc\\u062e\\u0627\\u0645 \\u0628\\u0645\\u06cc\\u0631\\u0645 \\u0627\\u0632 \\u0648\\u0642\\u062a\\u06cc \\u0631\\u0641\\u062a\\u0645 \\u06a9\\u0631\\u0628\\u0644\\u0627 \\u06cc\\u06a9\\u0633\\u0631\\u0647 \\u0633\\u0631 \\u062f\\u0631\\u062f\\u0645 \\u062d\\u0627\\u0644\\u062a \\u062a\\u0647\\u0648\\u0639 \\u062f\\u0627\\u0631\\u0645 \\n\\u06af\\u0644\\u0648\\u0645 \\u062f\\u0631\\u062f \\u0645\\u06cc\\u06a9\\u0646\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576262982, 1576262982),
(154, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:32:\\\"\\u062a\\u0628 \\u0648 \\u0644\\u0631\\u0632 \\u0647\\u0645 \\u062f\\u0627\\u0631\\u06cc\\u0646\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576263387, 1576263387),
(155, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:139:\\\"\\u06f2 \\u062a\\u0627 \\u062f\\u0631\\u062e\\u0648\\u0627\\u0633\\u062a \\u0647\\u0645\\u0632\\u0645\\u0627\\u0646 \\u0627\\u0632 \\u06cc\\u06a9 \\u0646\\u0641\\u0631 \\u0631\\u0648 \\u0633\\u06cc\\u0633\\u062a\\u0645 \\u0646\\u0645\\u06cc \\u0630\\u0627\\u0631\\u0647 \\u062c\\u0648\\u0627\\u0628 \\u0628\\u062f\\u0645\\u060c \\u0627\\u0648\\u0644 \\u0627\\u06cc\\u0646 \\u0628\\u0627\\u06cc\\u062f \\u062a\\u0645\\u0648\\u0645 \\u0634\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576263433, 1576263433),
(156, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:15:\\\"\\u0633\\u0631\\u0641\\u0647 \\u0686\\u06cc\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576263453, 1576263453),
(157, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:6:\\\"\\u0628\\u0644\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576263688, 1576263688),
(158, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:40:\\\"\\u0633\\u0631\\u0641\\u0647 \\u0647\\u0627\\u06cc \\u067e\\u06cc \\u062f\\u0631 \\u067e\\u06cc \\u0648 \\u062e\\u0634\\u06a9\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576263746, 1576263746),
(159, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:15:\\\"\\u0686\\u0634\\u0645 \\u062f\\u06a9\\u062a\\u0631\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576263766, 1576263766),
(160, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:63:\\\"\\u0645\\u0631\\u06cc\\u0636\\u06cc\\u0645 \\u0628\\u0686\\u0647 \\u0647\\u0627\\u06cc \\u0645\\u062d\\u0644 \\u0631\\u0648 \\u0647\\u0645 \\u062f\\u0627\\u063a\\u0648\\u0646 \\u06a9\\u0631\\u062f\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576263823, 1576263823),
(161, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:69:\\\"\\u062f\\u0627\\u0631\\u0648 \\u0628\\u0631\\u0627\\u062a\\u0648\\u0646 \\u0645\\u06cc \\u0646\\u0648\\u06cc\\u0633\\u0645 \\u0648 \\u0646\\u0633\\u062e\\u0647 \\u0627\\u0631\\u0633\\u0627\\u0644 \\u0645\\u06cc\\u0634\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576263830, 1576263830),
(162, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:214:\\\"\\u0641\\u0639\\u0644\\u0627 \\u0628\\u0627 \\u062a\\u0634\\u062e\\u06cc\\u0635 \\u0633\\u0631\\u0645\\u0627\\u062e\\u0648\\u0631\\u062f\\u06af\\u06cc\\u060c \\u0648\\u0644\\u06cc \\u0627\\u06af\\u0631 \\u0639\\u0644\\u0627\\u06cc\\u0645 \\u0628\\u06cc\\u0634\\u062a\\u0631 \\u0627\\u0632 \\u06cc\\u06a9 \\u0647\\u0641\\u062a\\u0647 \\u0637\\u0648\\u0644 \\u0628\\u06a9\\u0634\\u0647 \\u0648 \\u0634\\u062f\\u06cc\\u062f\\u062a\\u0631 \\u0628\\u0634\\u0647\\u060c \\u0627\\u0646\\u0641\\u0644\\u0648\\u0627\\u0646\\u0632\\u0627 \\u0645\\u0637\\u0631\\u062d \\u0645\\u06cc\\u0634\\u0647 \\u0648 \\u0645\\u062c\\u062f\\u062f \\u0645\\u0631\\u0627\\u062c\\u0639\\u0647 \\u06a9\\u0646\\u06cc\\u062f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576263936, 1576263936),
(163, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:19:\\\"\\u0639\\u0627\\u0644\\u06cc \\u0645\\u0645\\u0646\\u0648\\u0646\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576264212, 1576264212),
(164, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:19:\\\"\\u0645\\u0646\\u062a\\u0638\\u0631 \\u0646\\u0633\\u062e\\u0647\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576264506, 1576264506),
(165, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:42:\\\"\\u0627\\u0631\\u0633\\u0627\\u0644 \\u0646\\u0633\\u062e\\u0647 \\u0631\\u0648 \\u0686\\u06a9\\u0627\\u0631 \\u06a9\\u0646\\u06cc\\u0645\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576265134, 1576265134),
(166, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:26:\\\"\\u0627\\u0633\\u0647\\u0627\\u0644 \\u0647\\u0645 \\u0647\\u0633\\u062a\\u06cc\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576265210, 1576265210),
(167, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:33:\\\"\\u0628\\u0644\\u0647 \\u0628\\u06cc\\u0631\\u0648\\u0646 \\u0631\\u0648\\u06cc \\u0632\\u06cc\\u0627\\u062f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:75;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576266136, 1576266136),
(168, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"\\u0633\\u0644\\u0627\\u0645\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576377733, 1576377733),
(169, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:19:\\\"\\u067e\\u0627\\u062a \\u0628\\u0647\\u062a\\u0631\\u0647\\u061f\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576377758, 1576377758),
(170, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:10:\\\"\\u0633\\u0644\\u0627\\u0645\\u0645\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:53;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576418362, 1576418362),
(171, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:53;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576418379, 1576418379),
(172, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"\\u0633\\u0644\\u0627\\u0645\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:60;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576786717, 1576786717),
(173, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"\\u0633\\u0644\\u0627\\u0645\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:66;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576786819, 1576786819),
(174, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:8:\\\"\\u0633\\u0644\\u0627\\u0645\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576814221, 1576814221),
(175, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576814234, 1576814234),
(176, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1576814300, 1576814300),
(177, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:3:\\\"hgf\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1577039803, 1577039803),
(178, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:2:\\\"hf\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:52;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1577039851, 1577039851),
(179, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";N;s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1577039873, 1577039873),
(180, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendNotificationJob\",\"command\":\"O:28:\\\"App\\\\Jobs\\\\SendNotificationJob\\\":9:{s:33:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000msg\\\";s:2:\\\"hg\\\";s:40:\\\"\\u0000App\\\\Jobs\\\\SendNotificationJob\\u0000fcm_tokens\\\";a:1:{i:0;i:51;}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1577072728, 1577072728);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-09-14 08:54:57', '2019-09-14 08:54:57');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'داشبورد', '', '_self', 'voyager-boat', NULL, NULL, 1, '2019-09-14 08:54:57', '2019-09-14 08:54:57', 'voyager.dashboard', NULL),
(2, 1, 'چند رسانه ای', '', '_self', 'voyager-images', NULL, NULL, 6, '2019-09-14 08:54:57', '2019-09-14 11:43:46', 'voyager.media.index', NULL),
(3, 1, 'مدیران', '', '_self', 'voyager-person', '#000000', NULL, 3, '2019-09-14 08:54:57', '2019-11-16 15:44:01', 'voyager.users.index', 'null'),
(4, 1, 'نقش ها', '', '_self', 'voyager-lock', NULL, NULL, 2, '2019-09-14 08:54:57', '2019-09-14 08:54:57', 'voyager.roles.index', NULL),
(5, 1, 'ابزارها', '', '_self', 'voyager-tools', NULL, NULL, 19, '2019-09-14 08:54:57', '2019-11-16 16:03:06', NULL, NULL),
(6, 1, 'منو ساز', '', '_self', 'voyager-list', NULL, 5, 1, '2019-09-14 08:54:57', '2019-09-14 14:12:56', 'voyager.menus.index', NULL),
(7, 1, 'دیتابیس', '', '_self', 'voyager-data', NULL, 5, 2, '2019-09-14 08:54:57', '2019-09-14 14:12:56', 'voyager.database.index', NULL),
(8, 1, 'قطب نما', '', '_self', 'voyager-compass', NULL, 5, 3, '2019-09-14 08:54:57', '2019-09-14 14:12:56', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2019-09-14 08:54:57', '2019-09-14 14:12:56', 'voyager.bread.index', NULL),
(10, 1, 'تنظیمات', '', '_self', 'voyager-settings', NULL, NULL, 20, '2019-09-14 08:54:57', '2019-11-16 16:03:06', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, NULL, 7, '2019-09-14 08:54:58', '2019-09-14 14:12:45', 'voyager.hooks', NULL),
(12, 1, 'کلاینت‌ها', '', '_self', 'voyager-group', '#000000', NULL, 4, '2019-09-14 09:20:12', '2019-09-14 11:45:25', 'voyager.clients.index', 'null'),
(13, 1, 'پزشک ها', '', '_self', 'voyager-plus', '#000000', NULL, 5, '2019-09-14 10:40:06', '2019-11-16 14:35:42', 'voyager.doctors.index', 'null'),
(15, 1, 'سوابق اولیه کاربر', '', '_self', 'voyager-receipt', '#000000', NULL, 8, '2019-09-14 12:36:54', '2019-10-04 16:35:12', 'voyager.client-initial-histories.index', 'null'),
(16, 1, 'پیشنهادات', '', '_self', 'voyager-question', '#000000', NULL, 9, '2019-09-14 13:08:14', '2019-10-04 16:35:48', 'voyager.complaints.index', 'null'),
(17, 1, 'تخصص‌ها', '', '_self', 'voyager-documentation', '#000000', NULL, 11, '2019-09-14 13:27:43', '2019-11-16 16:03:15', 'voyager.expertises.index', 'null'),
(18, 1, 'سوابق بیماری کاربر', '', '_self', 'voyager-treasure', '#000000', NULL, 12, '2019-09-14 13:31:39', '2019-11-16 16:03:15', 'voyager.user-sicks.index', 'null'),
(19, 1, 'درخواست‌ها', '', '_self', 'voyager-check-circle', '#000000', NULL, 13, '2019-09-14 13:54:52', '2019-11-16 16:03:06', 'voyager.user-requests.index', 'null'),
(20, 1, 'نظرسنجی درخواست‌ها', '', '_self', 'voyager-telephone', '#000000', NULL, 14, '2019-09-14 13:59:55', '2019-11-16 16:03:06', 'voyager.rate-answers.index', 'null'),
(21, 1, 'پیام‌ها', '', '_self', 'voyager-chat', '#000000', NULL, 16, '2019-09-14 14:09:52', '2019-11-16 16:03:06', 'voyager.chat-messages.index', 'null'),
(22, 1, 'تنظیمات کپسول', '', '_self', 'voyager-chat', '#000000', NULL, 18, '2019-09-15 12:17:25', '2019-11-16 16:03:06', 'voyager.capsul-settings.index', 'null'),
(25, 1, 'تراکنش ها', '', '_self', 'voyager-wallet', '#000000', NULL, 17, '2019-10-04 16:09:06', '2019-11-16 16:03:06', 'voyager.trans-actions.index', 'null'),
(26, 1, 'بنرها', '', '_self', 'voyager-browser', '#000000', NULL, 15, '2019-10-15 07:48:38', '2019-11-16 16:03:06', 'voyager.banners.index', 'null'),
(27, 1, 'پشتیبانی', '', '_self', 'voyager-question', '#000000', NULL, 10, '2019-11-16 15:57:59', '2019-11-16 16:03:46', 'voyager.supports.index', 'null'),
(28, 1, 'بلاگ ها', '', '_self', 'voyager-categories', '#000000', NULL, 21, '2019-12-01 15:06:58', '2019-12-01 15:08:58', 'voyager.blogs.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(17, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(18, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(19, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(20, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(21, '2017_08_05_000000_add_group_to_settings_table', 1),
(22, '2017_11_26_013050_add_user_role_relationship', 1),
(23, '2017_11_26_015000_create_user_roles_table', 1),
(24, '2018_03_11_000000_add_user_settings', 1),
(25, '2018_03_14_000000_add_details_to_data_types_table', 1),
(26, '2018_03_16_000000_make_settings_value_nullable', 1),
(27, '2019_07_13_130057_client_table', 1),
(28, '2019_07_22_113744_sick_table', 1),
(29, '2019_07_22_113803_expertise_table', 1),
(30, '2019_07_22_113814_doctor_table', 1),
(31, '2019_07_22_113951_user_request_table', 1),
(32, '2019_07_22_114232_user_sick_table', 1),
(33, '2019_07_22_114439_complaints_table', 1),
(34, '2019_07_22_114921_chat_messages_table', 1),
(35, '2019_08_06_135256_create_jobs_table', 1),
(36, '2019_08_19_094416_client_initial_history_table', 1),
(37, '2019_08_19_102911_server_messages_table', 1),
(38, '2019_08_19_114542_setting_table', 1),
(39, '2019_09_05_172148_create_rate_answers_tables', 1),
(40, '2019_10_04_165907_create_transactions_tables', 2),
(41, '2013_12_01_181743_blogs_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-09-14 08:54:57', '2019-09-14 08:54:57'),
(2, 'browse_bread', NULL, '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(3, 'browse_database', NULL, '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(4, 'browse_media', NULL, '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(5, 'browse_compass', NULL, '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(6, 'browse_menus', 'menus', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(7, 'read_menus', 'menus', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(8, 'edit_menus', 'menus', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(9, 'add_menus', 'menus', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(10, 'delete_menus', 'menus', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(11, 'browse_roles', 'roles', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(12, 'read_roles', 'roles', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(13, 'edit_roles', 'roles', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(14, 'add_roles', 'roles', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(15, 'delete_roles', 'roles', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(16, 'browse_users', 'users', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(17, 'read_users', 'users', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(18, 'edit_users', 'users', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(19, 'add_users', 'users', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(20, 'delete_users', 'users', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(21, 'browse_settings', 'settings', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(22, 'read_settings', 'settings', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(23, 'edit_settings', 'settings', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(24, 'add_settings', 'settings', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(25, 'delete_settings', 'settings', '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(26, 'browse_hooks', NULL, '2019-09-14 08:54:58', '2019-09-14 08:54:58'),
(27, 'browse_clients', 'clients', '2019-09-14 09:20:12', '2019-09-14 09:20:12'),
(28, 'read_clients', 'clients', '2019-09-14 09:20:12', '2019-09-14 09:20:12'),
(29, 'edit_clients', 'clients', '2019-09-14 09:20:12', '2019-09-14 09:20:12'),
(30, 'add_clients', 'clients', '2019-09-14 09:20:12', '2019-09-14 09:20:12'),
(31, 'delete_clients', 'clients', '2019-09-14 09:20:12', '2019-09-14 09:20:12'),
(32, 'browse_doctors', 'doctors', '2019-09-14 10:40:05', '2019-09-14 10:40:05'),
(33, 'read_doctors', 'doctors', '2019-09-14 10:40:05', '2019-09-14 10:40:05'),
(34, 'edit_doctors', 'doctors', '2019-09-14 10:40:05', '2019-09-14 10:40:05'),
(35, 'add_doctors', 'doctors', '2019-09-14 10:40:05', '2019-09-14 10:40:05'),
(36, 'delete_doctors', 'doctors', '2019-09-14 10:40:05', '2019-09-14 10:40:05'),
(42, 'browse_client_initial_histories', 'client_initial_histories', '2019-09-14 12:36:54', '2019-09-14 12:36:54'),
(43, 'read_client_initial_histories', 'client_initial_histories', '2019-09-14 12:36:54', '2019-09-14 12:36:54'),
(44, 'edit_client_initial_histories', 'client_initial_histories', '2019-09-14 12:36:54', '2019-09-14 12:36:54'),
(45, 'add_client_initial_histories', 'client_initial_histories', '2019-09-14 12:36:54', '2019-09-14 12:36:54'),
(46, 'delete_client_initial_histories', 'client_initial_histories', '2019-09-14 12:36:54', '2019-09-14 12:36:54'),
(47, 'browse_complaints', 'complaints', '2019-09-14 13:08:14', '2019-09-14 13:08:14'),
(48, 'read_complaints', 'complaints', '2019-09-14 13:08:14', '2019-09-14 13:08:14'),
(49, 'edit_complaints', 'complaints', '2019-09-14 13:08:14', '2019-09-14 13:08:14'),
(50, 'add_complaints', 'complaints', '2019-09-14 13:08:14', '2019-09-14 13:08:14'),
(51, 'delete_complaints', 'complaints', '2019-09-14 13:08:14', '2019-09-14 13:08:14'),
(52, 'browse_expertises', 'expertises', '2019-09-14 13:27:43', '2019-09-14 13:27:43'),
(53, 'read_expertises', 'expertises', '2019-09-14 13:27:43', '2019-09-14 13:27:43'),
(54, 'edit_expertises', 'expertises', '2019-09-14 13:27:43', '2019-09-14 13:27:43'),
(55, 'add_expertises', 'expertises', '2019-09-14 13:27:43', '2019-09-14 13:27:43'),
(56, 'delete_expertises', 'expertises', '2019-09-14 13:27:43', '2019-09-14 13:27:43'),
(57, 'browse_user_sicks', 'user_sicks', '2019-09-14 13:31:39', '2019-09-14 13:31:39'),
(58, 'read_user_sicks', 'user_sicks', '2019-09-14 13:31:39', '2019-09-14 13:31:39'),
(59, 'edit_user_sicks', 'user_sicks', '2019-09-14 13:31:39', '2019-09-14 13:31:39'),
(60, 'add_user_sicks', 'user_sicks', '2019-09-14 13:31:39', '2019-09-14 13:31:39'),
(61, 'delete_user_sicks', 'user_sicks', '2019-09-14 13:31:39', '2019-09-14 13:31:39'),
(62, 'browse_user_requests', 'user_requests', '2019-09-14 13:54:52', '2019-09-14 13:54:52'),
(63, 'read_user_requests', 'user_requests', '2019-09-14 13:54:52', '2019-09-14 13:54:52'),
(64, 'edit_user_requests', 'user_requests', '2019-09-14 13:54:52', '2019-09-14 13:54:52'),
(65, 'add_user_requests', 'user_requests', '2019-09-14 13:54:52', '2019-09-14 13:54:52'),
(66, 'delete_user_requests', 'user_requests', '2019-09-14 13:54:52', '2019-09-14 13:54:52'),
(67, 'browse_rate_answers', 'rate_answers', '2019-09-14 13:59:55', '2019-09-14 13:59:55'),
(68, 'read_rate_answers', 'rate_answers', '2019-09-14 13:59:55', '2019-09-14 13:59:55'),
(69, 'edit_rate_answers', 'rate_answers', '2019-09-14 13:59:55', '2019-09-14 13:59:55'),
(70, 'add_rate_answers', 'rate_answers', '2019-09-14 13:59:55', '2019-09-14 13:59:55'),
(71, 'delete_rate_answers', 'rate_answers', '2019-09-14 13:59:55', '2019-09-14 13:59:55'),
(72, 'browse_chat_messages', 'chat_messages', '2019-09-14 14:09:52', '2019-09-14 14:09:52'),
(73, 'read_chat_messages', 'chat_messages', '2019-09-14 14:09:52', '2019-09-14 14:09:52'),
(74, 'edit_chat_messages', 'chat_messages', '2019-09-14 14:09:52', '2019-09-14 14:09:52'),
(75, 'add_chat_messages', 'chat_messages', '2019-09-14 14:09:52', '2019-09-14 14:09:52'),
(76, 'delete_chat_messages', 'chat_messages', '2019-09-14 14:09:52', '2019-09-14 14:09:52'),
(77, 'browse_capsul_settings', 'capsul_settings', '2019-09-15 12:17:25', '2019-09-15 12:17:25'),
(78, 'read_capsul_settings', 'capsul_settings', '2019-09-15 12:17:25', '2019-09-15 12:17:25'),
(79, 'edit_capsul_settings', 'capsul_settings', '2019-09-15 12:17:25', '2019-09-15 12:17:25'),
(80, 'add_capsul_settings', 'capsul_settings', '2019-09-15 12:17:25', '2019-09-15 12:17:25'),
(81, 'delete_capsul_settings', 'capsul_settings', '2019-09-15 12:17:25', '2019-09-15 12:17:25'),
(92, 'browse_trans_actions', 'trans_actions', '2019-10-04 16:09:06', '2019-10-04 16:09:06'),
(93, 'read_trans_actions', 'trans_actions', '2019-10-04 16:09:06', '2019-10-04 16:09:06'),
(94, 'edit_trans_actions', 'trans_actions', '2019-10-04 16:09:06', '2019-10-04 16:09:06'),
(95, 'add_trans_actions', 'trans_actions', '2019-10-04 16:09:06', '2019-10-04 16:09:06'),
(96, 'delete_trans_actions', 'trans_actions', '2019-10-04 16:09:06', '2019-10-04 16:09:06'),
(97, 'browse_banners', 'banners', '2019-10-15 07:48:38', '2019-10-15 07:48:38'),
(98, 'read_banners', 'banners', '2019-10-15 07:48:38', '2019-10-15 07:48:38'),
(99, 'edit_banners', 'banners', '2019-10-15 07:48:38', '2019-10-15 07:48:38'),
(100, 'add_banners', 'banners', '2019-10-15 07:48:38', '2019-10-15 07:48:38'),
(101, 'delete_banners', 'banners', '2019-10-15 07:48:38', '2019-10-15 07:48:38'),
(102, 'browse_supports', 'supports', '2019-11-16 15:57:59', '2019-11-16 15:57:59'),
(103, 'read_supports', 'supports', '2019-11-16 15:57:59', '2019-11-16 15:57:59'),
(104, 'edit_supports', 'supports', '2019-11-16 15:57:59', '2019-11-16 15:57:59'),
(105, 'add_supports', 'supports', '2019-11-16 15:57:59', '2019-11-16 15:57:59'),
(106, 'delete_supports', 'supports', '2019-11-16 15:57:59', '2019-11-16 15:57:59'),
(107, 'browse_blogs', 'blogs', '2019-12-01 15:06:58', '2019-12-01 15:06:58'),
(108, 'read_blogs', 'blogs', '2019-12-01 15:06:58', '2019-12-01 15:06:58'),
(109, 'edit_blogs', 'blogs', '2019-12-01 15:06:58', '2019-12-01 15:06:58'),
(110, 'add_blogs', 'blogs', '2019-12-01 15:06:58', '2019-12-01 15:06:58'),
(111, 'delete_blogs', 'blogs', '2019-12-01 15:06:58', '2019-12-01 15:06:58');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(1, 4),
(2, 4),
(3, 4),
(4, 4),
(5, 4),
(6, 4),
(7, 4),
(8, 4),
(9, 4),
(10, 4),
(11, 4),
(12, 4),
(13, 4),
(14, 4),
(15, 4),
(16, 1),
(16, 3),
(16, 4),
(17, 1),
(17, 3),
(17, 4),
(18, 1),
(18, 4),
(19, 1),
(19, 4),
(20, 4),
(21, 4),
(22, 4),
(23, 4),
(24, 4),
(25, 4),
(26, 4),
(27, 1),
(27, 3),
(27, 4),
(28, 1),
(28, 3),
(28, 4),
(29, 1),
(29, 4),
(30, 1),
(30, 4),
(31, 4),
(32, 1),
(32, 3),
(32, 4),
(33, 1),
(33, 3),
(33, 4),
(34, 1),
(34, 4),
(35, 1),
(35, 4),
(36, 4),
(42, 4),
(43, 1),
(43, 4),
(44, 1),
(44, 4),
(45, 1),
(45, 4),
(46, 1),
(46, 4),
(47, 1),
(47, 3),
(47, 4),
(48, 1),
(48, 3),
(48, 4),
(49, 1),
(49, 4),
(50, 4),
(51, 1),
(51, 3),
(51, 4),
(52, 1),
(52, 4),
(53, 4),
(54, 1),
(54, 4),
(55, 1),
(55, 4),
(56, 1),
(56, 4),
(57, 1),
(57, 4),
(58, 1),
(58, 4),
(59, 1),
(59, 4),
(60, 1),
(60, 4),
(61, 1),
(61, 4),
(62, 1),
(62, 3),
(62, 4),
(63, 1),
(63, 3),
(63, 4),
(64, 1),
(64, 3),
(64, 4),
(65, 1),
(65, 3),
(65, 4),
(66, 1),
(66, 3),
(66, 4),
(67, 1),
(67, 4),
(68, 1),
(68, 4),
(69, 1),
(69, 4),
(70, 1),
(70, 4),
(71, 1),
(71, 4),
(72, 4),
(73, 4),
(74, 4),
(75, 4),
(76, 4),
(77, 1),
(77, 4),
(78, 1),
(78, 4),
(79, 1),
(79, 4),
(80, 4),
(81, 4),
(92, 1),
(92, 4),
(93, 1),
(93, 4),
(94, 1),
(94, 4),
(95, 1),
(95, 4),
(96, 1),
(96, 4),
(97, 1),
(97, 4),
(98, 1),
(98, 4),
(99, 1),
(99, 4),
(100, 1),
(100, 4),
(101, 1),
(101, 4),
(102, 1),
(102, 4),
(103, 1),
(103, 4),
(104, 1),
(104, 4),
(105, 4),
(106, 1),
(106, 4),
(107, 1),
(107, 3),
(107, 4),
(108, 1),
(108, 3),
(108, 4),
(109, 1),
(109, 3),
(109, 4),
(110, 1),
(110, 3),
(110, 4),
(111, 1),
(111, 3),
(111, 4);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rate_answers`
--

CREATE TABLE `rate_answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `req_id` bigint(20) UNSIGNED NOT NULL,
  `rateAnswer1` tinyint(1) DEFAULT NULL,
  `rateAnswer2` tinyint(1) DEFAULT NULL,
  `rateAnswer3` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'ادمین', '2019-09-14 08:54:57', '2019-09-14 08:54:57'),
(2, 'user', 'کاربر عادی', '2019-09-14 08:54:57', '2019-09-14 08:54:57'),
(3, 'viewer', 'ناظر', '2019-11-11 17:17:01', '2019-11-11 17:17:01'),
(4, 'programmer', 'برنامه نویس', '2019-11-16 14:22:09', '2019-11-16 14:22:22');

-- --------------------------------------------------------

--
-- Table structure for table `server_messages`
--

CREATE TABLE `server_messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('all','one') COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'عنوان سایت', 'عنوان سایت', '', 'text', 1, 'Site'),
(2, 'site.description', 'شرح مختصر فعالیت سایت', 'شرح مختصر فعالیت سایت', '', 'text', 2, 'Site'),
(3, 'site.logo', 'لوگوی سایت', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'شناسه رهگیری گوگل آنالیز', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'تصویر پس زمینه برای ادمین', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'عنوان ادمین', 'پنل مدیریت کپسول', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'توضیحات ادمین', 'به پنل مدیریت کپسول خوش آمدید', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'لودر ادمین', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'تصویر آیکون ادمین', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'شناسه گوگل انالیز (برای داشبورد ادمین مورد استفاده قرار می گیرد)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `sicks`
--

CREATE TABLE `sicks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sick_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `supports`
--

CREATE TABLE `supports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `dr_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci,
  `answer` longtext COLLATE utf8mb4_unicode_ci,
  `status` enum('waiting','viewed','closed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'waiting',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trans_actions`
--

CREATE TABLE `trans_actions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `factorId` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transId` bigint(20) UNSIGNED DEFAULT NULL,
  `traceNumber` bigint(20) UNSIGNED DEFAULT NULL,
  `amount` bigint(20) UNSIGNED NOT NULL,
  `mobile` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cardNumber` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `messageReceived` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED DEFAULT NULL,
  `comeFrom` enum('web','app') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'male',
  `status` enum('active','deactive','non_verified') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'non_verified',
  `capsul_role` enum('client','admin','doctor') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'client',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recovery_password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fcm_token` varchar(257) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_verified_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `fname`, `lname`, `name`, `pic`, `sex`, `status`, `capsul_role`, `username`, `email`, `avatar`, `password`, `recovery_password`, `remember_token`, `settings`, `api_token`, `fcm_token`, `phone_verified_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(21, 1, 'میلاد', 'دربان رضوی', NULL, NULL, 'male', 'active', 'admin', 'admin', 'admin@admin.com', 'users/default.png', '$2y$10$44A3gu.ngqoRwmyAvEfkZ.KLFMewqhII5wnzz5D2FVZ65nzgMXdRy', NULL, 'tocSG9UXqAWbQ1xWaoBw6Cim5EcClP3FgHmN67QoY1SAQbTTtvMLwpgZ4UFt', NULL, 'FRuLEkILpajU9eyo6RavdqOlZggI2odLydRKkrK72COq5lQmHuwJOiKeL9HdAPrNL5ealnSM16qvQbVL', NULL, NULL, '2019-09-14 10:31:21', '2019-12-24 20:50:00', NULL),
(65, 3, 'کاربر ناظر', NULL, NULL, NULL, 'male', 'active', 'admin', 'viewer', NULL, 'users/default.png', '$2y$10$MBl7IOkKLYouOF.YVr.FIeSeGZ2V304Iuip0Y9Qui9cKqUmYB5gnO', NULL, NULL, NULL, 'lJ8nYHhEoIPZOP5gbPlFjGqvQ5xW0fUX0THfgHVCYEcn6aBRYJ5zBxWgUBlA88W03kX2iwcsGEZMBnZz', NULL, NULL, '2019-11-11 17:13:54', '2019-12-13 19:54:04', NULL),
(68, 4, 'برنامه نویس', 'پروگرمر', NULL, NULL, 'male', 'active', 'admin', 'programmer', NULL, 'users/default.png', '$2y$10$rqbuWZ6b9XNgTBfdkDZ0LOTzTuwlGrGMRUVOu1Hp9hqEj5Ca6td.G', NULL, NULL, NULL, 'j0IE4vhxxk6RhUIqlJDHIBndSHX6nvRjVoUjgFJXH58yV41lFqxykwxaotO2srP0GD95do4eF9X9Pa01', NULL, NULL, '2019-11-16 14:23:29', '2019-12-24 20:50:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_requests`
--

CREATE TABLE `user_requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `exp_id` bigint(20) UNSIGNED NOT NULL,
  `dr_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` enum('lab','send_prescription','renew_prescription') COLLATE utf8mb4_unicode_ci NOT NULL,
  `exp_cost` mediumint(9) NOT NULL,
  `cost_back_client` tinyint(1) DEFAULT NULL,
  `cost_back_doctor` tinyint(1) DEFAULT NULL,
  `main_reason` longtext COLLATE utf8mb4_unicode_ci,
  `re_visit` mediumtext COLLATE utf8mb4_unicode_ci,
  `answer1` mediumtext COLLATE utf8mb4_unicode_ci,
  `answer2` mediumtext COLLATE utf8mb4_unicode_ci,
  `answer3` mediumtext COLLATE utf8mb4_unicode_ci,
  `read_by_dr` tinyint(1) DEFAULT NULL,
  `dr_first_answer` mediumtext COLLATE utf8mb4_unicode_ci,
  `final_diagnosis` text COLLATE utf8mb4_unicode_ci,
  `status` enum('waiting','accepted','rejected','finished','reserved') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'waiting',
  `viewer_checked` tinyint(1) DEFAULT NULL,
  `reject_reason` enum('presence','emergency','other_exp') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `score` tinyint(4) DEFAULT NULL,
  `reserve_date` date DEFAULT NULL,
  `finished_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_sicks`
--

CREATE TABLE `user_sicks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `sick` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `added_by` enum('dr','client') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'client',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `capsul_settings`
--
ALTER TABLE `capsul_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `chat_messages`
--
ALTER TABLE `chat_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chat_messages_req_id_foreign` (`req_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `clients_phone_unique` (`phone`);

--
-- Indexes for table `client_initial_histories`
--
ALTER TABLE `client_initial_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_initial_histories_user_id_foreign` (`user_id`);

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`),
  ADD KEY `complaints_user_id_foreign` (`user_id`),
  ADD KEY `complaints_dr_id_foreign` (`dr_id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `doctors_phone_unique` (`phone`),
  ADD KEY `doctors_exp_id_foreign` (`exp_id`);

--
-- Indexes for table `expertises`
--
ALTER TABLE `expertises`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `rate_answers`
--
ALTER TABLE `rate_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rate_answers_req_id_foreign` (`req_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `server_messages`
--
ALTER TABLE `server_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `server_messages_user_id_foreign` (`user_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `sicks`
--
ALTER TABLE `sicks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supports`
--
ALTER TABLE `supports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supports_user_id_foreign` (`user_id`),
  ADD KEY `supports_dr_id_foreign` (`dr_id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `trans_actions`
--
ALTER TABLE `trans_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trans_actions_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_requests`
--
ALTER TABLE `user_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_requests_user_id_foreign` (`user_id`),
  ADD KEY `user_requests_exp_id_foreign` (`exp_id`),
  ADD KEY `user_requests_dr_id_foreign` (`dr_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `user_sicks`
--
ALTER TABLE `user_sicks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `capsul_settings`
--
ALTER TABLE `capsul_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_messages`
--
ALTER TABLE `chat_messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `client_initial_histories`
--
ALTER TABLE `client_initial_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=226;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `expertises`
--
ALTER TABLE `expertises`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rate_answers`
--
ALTER TABLE `rate_answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `server_messages`
--
ALTER TABLE `server_messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sicks`
--
ALTER TABLE `sicks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supports`
--
ALTER TABLE `supports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trans_actions`
--
ALTER TABLE `trans_actions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `user_requests`
--
ALTER TABLE `user_requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_sicks`
--
ALTER TABLE `user_sicks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `chat_messages`
--
ALTER TABLE `chat_messages`
  ADD CONSTRAINT `chat_messages_req_id_foreign` FOREIGN KEY (`req_id`) REFERENCES `user_requests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `client_initial_histories`
--
ALTER TABLE `client_initial_histories`
  ADD CONSTRAINT `client_initial_histories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `clients` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `complaints`
--
ALTER TABLE `complaints`
  ADD CONSTRAINT `complaints_dr_id_foreign` FOREIGN KEY (`dr_id`) REFERENCES `doctors` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `complaints_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `doctors`
--
ALTER TABLE `doctors`
  ADD CONSTRAINT `doctors_exp_id_foreign` FOREIGN KEY (`exp_id`) REFERENCES `expertises` (`id`),
  ADD CONSTRAINT `doctors_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rate_answers`
--
ALTER TABLE `rate_answers`
  ADD CONSTRAINT `rate_answers_req_id_foreign` FOREIGN KEY (`req_id`) REFERENCES `user_requests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `server_messages`
--
ALTER TABLE `server_messages`
  ADD CONSTRAINT `server_messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `clients` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supports`
--
ALTER TABLE `supports`
  ADD CONSTRAINT `supports_dr_id_foreign` FOREIGN KEY (`dr_id`) REFERENCES `doctors` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `supports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trans_actions`
--
ALTER TABLE `trans_actions`
  ADD CONSTRAINT `trans_actions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_requests`
--
ALTER TABLE `user_requests`
  ADD CONSTRAINT `user_requests_dr_id_foreign` FOREIGN KEY (`dr_id`) REFERENCES `doctors` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_requests_exp_id_foreign` FOREIGN KEY (`exp_id`) REFERENCES `expertises` (`id`),
  ADD CONSTRAINT `user_requests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `clients` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
